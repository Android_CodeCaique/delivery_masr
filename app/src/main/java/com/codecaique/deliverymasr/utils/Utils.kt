package com.codecaique.deliverymasr.utils

import android.view.View
import androidx.fragment.app.FragmentActivity
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.util.UserInfo

object Utils {


    fun signout(requireActivity: FragmentActivity, view: View) {
        UserInfo(requireActivity!!.applicationContext).logout()
        view.findNavController().navigate(R.id.selestedFragment)
    }
}