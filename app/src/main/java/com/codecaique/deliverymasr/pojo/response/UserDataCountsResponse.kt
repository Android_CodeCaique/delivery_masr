package com.codecaique.deliverymasr.pojo.response

data class UserDataCountsResponse(
    var credit: String,
    var rate: Int,
    var requests: Int
)