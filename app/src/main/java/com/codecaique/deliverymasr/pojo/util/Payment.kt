package com.codecaique.deliverymasr.pojo.util

import android.os.Bundle
import android.util.JsonReader
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.data.ApiCreater
import com.oppwa.mobile.connect.checkout.dialog.CheckoutActivity
import com.oppwa.mobile.connect.checkout.meta.CheckoutSettings
import com.oppwa.mobile.connect.provider.Connect
import kotlinx.android.synthetic.main.fragment_paymment.*
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.util.LinkedHashSet

class Payment : AppCompatActivity()
{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_paymment)

        btn_confirm.setOnClickListener {
            val paymentBrands: MutableSet<String> = LinkedHashSet<String>()

            paymentBrands.add("VISA")
            paymentBrands.add("MASTER")
            paymentBrands.add("DIRECTDEBIT_SEPA")

            Log.e("Request ID", requestCheckoutId() + "          ")
            Log.e("Payments ::  ", paymentBrands.toString() + "          ")



            val checkoutSettings = CheckoutSettings(requestCheckoutId(), paymentBrands, Connect.ProviderMode.TEST)

            checkoutSettings.shopperResultUrl = "Hiakk://result"

            val intent = checkoutSettings.createCheckoutActivityIntent(this)


            startActivityForResult(intent, CheckoutActivity.REQUEST_CODE_CHECKOUT)

        }

    }


    fun requestCheckoutId(): String {
        val url: URL
        val urlString: String
        var connection: HttpURLConnection? = null
        var checkoutId: String? = null
        urlString = ApiCreater.PAYMENT_URL + "?amount=1&currency=USD&paymentType=DB"
        try {
            url = URL(urlString)
            connection = url.openConnection() as HttpURLConnection
            val reader = JsonReader(
                    InputStreamReader(connection.getInputStream(), "UTF-8"))
            reader.beginObject()
            while (reader.hasNext()) {
                if (reader.nextName().equals("checkoutId")) {
                    checkoutId = reader.nextString()
                    break
                }
            }

            Log.e("READER json ::: ", reader.toString() + "      nn  ")


            Log.e("CHECKOUT ID json ::: ", checkoutId.toString() + "      nn  ")
            reader.endObject()
            reader.close()
        } catch (e: java.lang.Exception) {

            e.printStackTrace()
            Log.e("JSON Error :: ",e.message  + "   ")
            /* error occurred */
        } finally {
            Log.e("JSON Error :: ", "FINAL sas")


            if (connection != null) {
                connection.disconnect()
            }
        }
        return checkoutId.toString()
    }



}
