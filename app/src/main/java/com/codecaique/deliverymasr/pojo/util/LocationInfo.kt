package com.codecaique.deliverymasr.pojo.util

import android.content.Context
import android.content.SharedPreferences
import com.google.android.gms.maps.model.LatLng

class LocationInfo(val context: Context) {

    private var sharedPreferences: SharedPreferences = context.getSharedPreferences("location", Context.MODE_PRIVATE)
    var editor: SharedPreferences.Editor = sharedPreferences.edit()

    private val userSharedPreferences: SharedPreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE)

    fun getUserToken(): String {
        return userSharedPreferences.getString("user_token", "")!!
    }

    fun getMyLocation(): LatLng {

        val latitudeStart = userSharedPreferences.getString("latitude", "0")!!.toDouble()
        val longitudeStart = userSharedPreferences.getString("longitude", "0")!!.toDouble()

        return LatLng(latitudeStart, longitudeStart)

    }

    fun getLocationStart(): LatLng {

        val latitudeStart = sharedPreferences.getString("latitude-start", "0")!!.toDouble()
        val longitudeStart = sharedPreferences.getString("longitude-start", "0")!!.toDouble()

        return LatLng(latitudeStart, longitudeStart)

    }


    fun getLocationShare(): LatLng {

        val latitudeStart = sharedPreferences.getString("latitude-share", "0")!!.toDouble()
        val longitudeStart = sharedPreferences.getString("longitude-share", "0")!!.toDouble()

        return LatLng(latitudeStart, longitudeStart)

    }


    fun setLocationShare(latitude: String, longitude: String) {
        editor.putString("latitude-share", latitude)
        editor.putString("longitude-share", longitude)
        editor.apply()
    }

    fun getStreetShare(): String {

        return sharedPreferences.getString("street-share", "")!!
    }

    fun isLocationShare(): Boolean {

        return sharedPreferences.getString("street-share", "")!!.isNotEmpty()

    }

    fun setStreetShare(street: String) {

        editor.putString("street-share", street)!!
        editor.apply()
    }


    fun getStreetStart(): String {

        return sharedPreferences.getString("street-start", "")!!
    }


    fun getStreetEnd(): String {

        return sharedPreferences.getString("street-end", "")!!
    }

    fun setStreetStart(street: String) {

        editor.putString("street-start", street)!!
        editor.apply()
    }

    fun setStreetEnd(street: String) {

        editor.putString("street-end", street)!!
        editor.apply()
    }

    fun getLocationEnd(): LatLng {

        val latitudeStart = sharedPreferences.getString("latitude-end", "0")!!.toDouble()
        val longitudeStart = sharedPreferences.getString("longitude-end", "0")!!.toDouble()

        return LatLng(latitudeStart, longitudeStart)

    }

    fun setLocationStart(latitude: String, longitude: String) {
        editor.putString("latitude-start", latitude)
        editor.putString("longitude-start", longitude)
        editor.apply()
    }

    fun setLocationEnd(latitude: String, longitude: String) {
        editor.putString("latitude-end", latitude)
        editor.putString("longitude-end", longitude)
        editor.apply()
    }


    fun clear() {
        editor.clear().apply()
    }


}