package com.codecaique.deliverymasr.pojo.util

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.net.Uri
import android.telecom.Connection
import com.codecaique.deliverymasr.pojo.response.*
import com.google.android.gms.maps.model.LatLng
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import java.lang.reflect.Type


class UserInfo(context: Context) {

    var sharedPreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE)
    var editor = sharedPreferences.edit()

    fun getUserToken(): String {
        return sharedPreferences.getString("user_token", "")!!
    }
    fun getnamefragment(): String {
        return sharedPreferences.getString("namefrag", "")!!
    }
    fun setnamefragment(name:String){

        editor.putString("namefrag",name)
        editor.apply()
    }

    fun setRingtone(uri: Uri?) {
        editor.putString("Ringtone", uri.toString())
        editor.apply()
    }

    fun getRingtone(): Uri? {
        return Uri.parse(sharedPreferences.getString("Ringtone", ""))
    }

    fun putKey(kay: String) {
        editor.putBoolean(kay, true)
        editor.apply()
    }

    fun getKey(kay: String): Boolean = sharedPreferences.getBoolean(kay, false)

    fun putKeyReceive(kay: String) {
        editor.putBoolean("receive$kay", true)
        editor.apply()
    }

    fun getKeyReceive(kay: String): Boolean = sharedPreferences.getBoolean("receive$kay", false)


    fun putKeyRate(kay: String) {
        editor.putBoolean("rate$kay", true)
        editor.apply()
    }

    fun getKeyRate(kay: String): Boolean = sharedPreferences.getBoolean("rate$kay", false)

    fun setGender(i: Int) {
        editor.putInt("gender", i)
        editor.apply()
    }

    fun getGender(): Int {
        return sharedPreferences.getInt("gender", 2)
    }

    fun setRingtoneName(name: String) {
        editor.putString("Ringtone-name", name)
        editor.apply()
    }

    fun getRingtoneName(): String? {
        return sharedPreferences.getString("Ringtone-name", "")
    }

    fun setLang(name: String) {
        editor.putString("lang", name)
        editor.apply()
    }

    fun getLang(): String? {
        return sharedPreferences.getString("lang", "")
    }

    fun setUserToken(data: String) {
        editor.putString("user_token", data)
        editor.apply()
    }

    fun setState(data: String) {
        editor.putString("state", data)
        editor.apply()
    }

    fun getImage(): String {
        return sharedPreferences.getString("image", "")!!
    }

    fun getName(): String {
        var fname:String= sharedPreferences.getString("first_name", "").toString()
        var lname:String=sharedPreferences.getString("last_name", "").toString()
        return fname+" "+lname
    }

    fun getEmail(): String {
        return sharedPreferences.getString("email", "")!!
    }

    fun getPhone(): String {
        return sharedPreferences.getString("phone", "")!!
    }

    fun setUserData(id: Int, phone: String, state: String, user_token: String) {
        editor.putInt("id", id)
        editor.putString("phone", phone)
        editor.putString("state", state)
        editor.putString("user_token", user_token)
        editor.apply()
    }

    fun isNotRegister(): Boolean {
        return sharedPreferences.getString("state", "")!!.contains("training")
    }


    fun setUserData(id: Int, state: String, user_token: String) {
        editor.putInt("id", id)
        editor.putString("state", state)
        editor.putString("user_token", user_token)
        editor.apply()
    }

    fun setUserData(data: Profile) {

        editor.putString("first_name", data.first_name)
        editor.putString("last_name", data.last_name)
//        editor.putString("password",data.passwods)
        editor.putString("phone", data.phone)
//        editor.putString("whats_phone",data.whats_phone)
        editor.putString("image", data.image)
        editor.putString("email", data.email)
//        editor.putString("firebase_token",data.firebase_token)
//        editor.putString("latitude",data.latitude)
//        editor.putString("longitude",data.longitude)
//        editor.putInt("gender",data.gender.toInt())

        editor.putString("user_token", data.user_token)
        editor.putInt("id", data.id)
//        editor.putString("state",data.state)
//
        editor.apply()
    }

    fun setUserData(data: UserData) {

        editor.putString("first_name", data.first_name)
        editor.putString("last_name", data.last_name)
//        editor.putString("password",data.passwods)
        editor.putString("phone", data.phone)
//        editor.putString("whats_phone",data.whats_phone)
        editor.putString("image", data.image)
        editor.putString("email", data.email)
//        editor.putString("firebase_token",data.firebase_token)
//        editor.putString("latitude",data.latitude)
//        editor.putString("longitude",data.longitude)
        editor.putString("user_token", data.user_token)
        editor.putInt("id", data.id)

//        editor.putInt("gender",data.gender.toInt())
        editor.putString("state", data.state)

        editor.apply()
    }

    fun setUserData(data: CheckEmail) {

        editor.putString("first_name", data.first_name)
        editor.putString("last_name", data.last_name)
//        editor.putString("password",data.passwods)
        editor.putString("phone", data.phone)
//        editor.putString("whats_phone",data.whats_phone)
        editor.putString("image", data.image)
        editor.putString("email", data.email)
//        editor.putString("firebase_token",data.firebase_token)
//        editor.putString("latitude",data.latitude)
//        editor.putString("longitude",data.longitude)
        editor.putString("user_token", data.user_token)
        editor.putInt("id", data.id)
        editor.putInt("gender", data.gender.toInt())

//        editor.putString("state",data.state)

        editor.apply()
    }


    fun setUserData(data: Phone) {

        editor.putString("first_name", data.first_name)
        editor.putString("last_name",data.last_name)
//        editor.putString("password",data.passwods)
        editor.putString("phone", data.phone)
//        editor.putString("whats_phone",data.whats_phone)
        editor.putString("image", data.image)
        editor.putString("email", data.email)
        editor.putInt("gender", data.gender.toInt())
//        editor.putString("firebase_token",data.firebase_token)
//        editor.putString("latitude",data.latitude)
//        editor.putString("longitude",data.longitude)
        editor.putString("user_token", data.user_token)
        editor.putInt("id", data.id)
//        editor.putString("state",data.state)

        editor.apply()
    }


    fun setCaptainData(data: Driver_Response.DataBean) {

        editor.putString("first_name", data.first_name)
        editor.putString("last_name", data.last_name)
//        editor.putString("password",data.passwods)
        editor.putString("phone", data.phone)
//        editor.putString("whats_phone",data.whats_phone)
        editor.putString("image", data.image)
        editor.putString("email", data.email)
//        editor.putString("firebase_token",data.firebase_token)
//        editor.putString("latitude",data.latitude)
//        editor.putString("longitude",data.longitude)
        editor.putString("user_token", data.user_token)
        editor.putInt("id", data.id)
        editor.putString("state", data.state)

        editor.putInt("gender", data.gender.toInt())
        editor.apply()
    }


    fun getLatitude(): String {
        return sharedPreferences.getString("latitude", "1.1")!!
    }

    fun setLocation(latitude: String, longitude: String) {
        editor.putString("latitude", latitude)
        editor.putString("longitude", longitude)
        editor.apply()
    }

    fun getLongitude(): String {
        return sharedPreferences.getString("longitude", "1.1")!!
    }

    fun getMyLocation(): LatLng {

        val latitudeStart = sharedPreferences.getString("latitude", "0")!!.toDouble()
        val longitudeStart =sharedPreferences.getString("longitude", "0")!!.toDouble()

        return LatLng(latitudeStart, longitudeStart)

    }


    fun getId(): Int {
        return sharedPreferences.getInt("id", 0)
    }

    fun getState(): String {
        return sharedPreferences.getString("state", "client")!!
    }

    fun getNotifiState(): String {
        return sharedPreferences.getString("notifi", "on")!!
    }


    fun setNotifiState(state: String) {
        editor.putString("notifi", state)
        editor.apply()
    }

    fun getNotifiCount(): Int {
        return sharedPreferences.getString("notifiCount", "0")!!.toInt()
    }


    fun setNotifiCount(notifiCount: String) {
        editor.putString("notifiCount", notifiCount)
        editor.apply()
    }

    fun setDiscountNotifiCount() {
        var c = sharedPreferences.getString("notifiCount", "0")!!.toInt()
        editor.putString("notifiCount", (--c).toString())
        editor.apply()
    }

    fun logout() {
        editor.clear().apply()
    }


    fun setchatdata(data: Message) {

        val gson = Gson()
        val json = gson.toJson(data)
        editor.putString("json",json)
        editor.apply()

    }



     fun getchatdata(): ArrayList<Message> {
         val gson = Gson()
         var json: String = sharedPreferences.getString("json", null)!!
         val type: Type = object : TypeToken<List<Connection?>?>() {}.getType()
         val connections: List<Connection> = gson.fromJson<List<Connection>>(json, type)
        return connections as ArrayList<Message>
    }

}

