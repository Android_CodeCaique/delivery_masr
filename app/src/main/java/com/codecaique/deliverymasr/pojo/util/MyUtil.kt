package com.codecaique.deliverymasr.pojo.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.*
import android.graphics.drawable.Drawable
import android.icu.text.DecimalFormat
import android.icu.text.SimpleDateFormat
import android.location.Location
import android.net.Uri
import android.os.Build
import android.util.Base64
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.MeasureSpec
import android.view.View.MeasureSpec.makeMeasureSpec
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import java.io.ByteArrayOutputStream
import java.util.*


class MyUtil {

    companion object {

        private const val ALLOWED_CHARACTERS = "qwertyuiopasdfghjklzxcvbnm"

        @RequiresApi(Build.VERSION_CODES.N)
        fun getCurrentFullTime(): String =
                SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().time)
                        .toString()

        fun getRandomName(): String =
                getRandomString(5) + System.currentTimeMillis()

        private fun getRandomString(sizeOfRandomString: Int): String {
            val random = Random()
            val sb = StringBuilder(sizeOfRandomString)
            for (i in 0 until sizeOfRandomString)
                sb.append(ALLOWED_CHARACTERS[random.nextInt(ALLOWED_CHARACTERS.length)])
            return sb.toString()
        }

        @RequiresApi(Build.VERSION_CODES.N)
        fun calculateDistanceByKM(location1: Location, location2: Location): Float =
                DecimalFormat("0.00").format(location1.distanceTo(location2) / 1000).toFloat()

        fun bitMapToString(bitmap: Bitmap): String {
            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos)
            val b = baos.toByteArray()
            return Base64.encodeToString(b, Base64.DEFAULT)
        }

        fun stringToBitMap(encodedString: String?): Bitmap? {
            return try {
                val encodeByte =
                        Base64.decode(encodedString, Base64.DEFAULT)
                BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.size)
            } catch (e: Exception) {
                e.message
                null
            }
        }


        fun call(mContext: Context, phone: String) {

            val i = Intent(Intent.ACTION_DIAL)
            i.data = Uri.parse("tel:$phone")
            if (i.resolveActivity(mContext.packageManager) != null)
                mContext.startActivity(i)

        }

        fun telegram(context: Context, TELEGRAM_PAGE_ID: String) {
            val intent: Intent = try {
                try {
                    context.packageManager.getPackageInfo("org.telegram.messenger", 0)//Check for Telegram Messenger App
                } catch (e: Exception) {
                    context.packageManager.getPackageInfo("org.thunderdog.challegram", 0)//Check for Telegram X App
                }
                Intent(Intent.ACTION_VIEW, Uri.parse("tg://resolve?domain=${TELEGRAM_PAGE_ID}"))
            } catch (e: Exception) { //App not found open in browser
                Intent(Intent.ACTION_VIEW, Uri.parse("http://www.telegram.me/$TELEGRAM_PAGE_ID"))
            }

            context.startActivity(intent)
        }

         fun bitmapDescriptorFromVector(activity: Context, @DrawableRes vectorDrawableResourceId: Int): BitmapDescriptor? {
            val vectorDrawable = ContextCompat.getDrawable(activity, vectorDrawableResourceId)
            vectorDrawable!!.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
            val bitmap = Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bitmap)
            vectorDrawable.draw(canvas)
            return BitmapDescriptorFactory.fromBitmap(bitmap)
        }

         fun bitmapDescriptorFromLayout(activity: Context, layout: Int): BitmapDescriptor? {
            val layoutInflater =activity. getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater?
            val view: View = layoutInflater!!.inflate(layout, null)
            view.measure(makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
                    makeMeasureSpec(0, MeasureSpec.UNSPECIFIED))
            view.layout(0, 0, 333, 333)
//            view.buildDrawingCache()
            val bitmap = Bitmap.createBitmap(333, 333,
                    Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bitmap)
            canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN)
            val drawable: Drawable = view.background
            drawable.draw(canvas)
            view.draw(canvas)
             return BitmapDescriptorFactory.fromBitmap(bitmap)
        }

        fun viewToBitmap(activity: Context, viewId: Int): Bitmap? {
            val layoutInflater =activity. getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater?
            val view: View = layoutInflater!!.inflate(viewId, null, false)
            view.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED)
            view.layout(0, 0, view.measuredWidth, 33)
            Log.e("TAG", "viewToBitmap: ${view.measuredWidth} ,,, ${view.measuredHeight}")
            Log.e("TAG", "viewToBitmap: ${view.width} ,,, ${view.height}")
            val bitmap = Bitmap.createBitmap(view.measuredWidth, 33, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bitmap)
            view.draw(canvas)
            return bitmap
        }

        fun createDrawableFromView(context: Context, view: View): Bitmap? {
            val displayMetrics = DisplayMetrics()
            (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
            view.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
            view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
            view.buildDrawingCache()
            val bitmap = Bitmap.createBitmap(displayMetrics.widthPixels, displayMetrics.heightPixels, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bitmap)
            view.draw(canvas)
            return bitmap
        }

    }
}