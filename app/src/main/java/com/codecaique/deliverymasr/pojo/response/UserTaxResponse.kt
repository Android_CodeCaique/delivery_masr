package com.codecaique.deliverymasr.pojo.response

data class UserTaxResponse(
    var data: Int,
    var error: Int,
    var message: String
)