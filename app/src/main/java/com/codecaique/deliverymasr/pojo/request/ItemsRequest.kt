package com.codecaique.deliverymasr.pojo.request

data class ItemsRequest(
    var amount: String,
    var price: String,
    var product: String,
    var type: String
)