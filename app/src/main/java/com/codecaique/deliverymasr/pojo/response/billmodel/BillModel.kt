package com.codecaique.deliverymasr.pojo.response.billmodel

data class BillModel(
    val `data`: List<Data>,
    val error: Int,
    val message: String
)