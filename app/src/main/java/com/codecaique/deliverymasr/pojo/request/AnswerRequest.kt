package com.codecaique.deliverymasr.pojo.request

data class AnswerRequest(
    var answer: String,
    var id: Int
)