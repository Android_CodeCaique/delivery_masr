package com.codecaique.deliverymasr.pojo.request

class SentMessageRequest (
        var user_token :String,
        var user_id:Int,
        var message:String,
        var request_id:Int
)