package com.codecaique.deliverymasr.pojo.response

data class GeneralResponse(
    var error: Int,
    var message: String
)