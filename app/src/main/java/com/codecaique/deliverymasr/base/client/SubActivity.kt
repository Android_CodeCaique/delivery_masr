package com.codecaique.deliverymasr.base.client

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.RTLActivity
import com.codecaique.deliverymasr.ui.agent.subMain.ui.payWay.PayWayFragment
import com.codecaique.deliverymasr.ui.client.bankAccount.BankAccountData
import com.codecaique.deliverymasr.ui.client.chat.ChatFragment
import com.codecaique.deliverymasr.ui.client.confirmOrders.ConfirmMyOrder
import com.codecaique.deliverymasr.ui.client.coupons.CouponsFragment
import com.codecaique.deliverymasr.ui.client.editProfile.EditProfileFragment
import com.codecaique.deliverymasr.ui.client.joinAsCaptain.JoinAsCaptainFragment
import com.codecaique.deliverymasr.ui.client.myRate.UserNotesFragment
import com.codecaique.deliverymasr.ui.client.registerBankAccount.RegisterBankAccount
import com.codecaique.deliverymasr.ui.client.restaurant.RestaurantFragment
import com.codecaique.deliverymasr.ui.client.shakawy.ShakawyFragment
import com.codecaique.deliverymasr.ui.client.shakawy.ShakawyList2
import com.codecaique.deliverymasr.ui.client.stores.StoresFragment
import com.codecaique.deliverymasr.ui.client.stores.adapters.StoresAdapter
import com.codecaique.deliverymasr.ui.client.tardHayak.SentRequestFragment
import com.codecaique.deliverymasr.ui.client.tardHayak.TardHayyakFragment
import com.codecaique.deliverymasr.ui.client.userNotes.UserNotes4
import com.codecaique.deliverymasr.ui.client.userNotes.UserRatingFragment
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.activity_main3.*


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class SubActivity : RTLActivity() {


    var fragment: Fragment? = null

    companion object {

        const val KEY = "key"
        const val Restaurant = "fragment_kentaky_restaurant"
        const val TardHayak = "fragment_tard__hayak_"
        const val ConfirmMyOrder = "fragment_confirm_my_order"
        const val FarFromYou = "farFromYou"
        const val Shakawy = "fragment_shakawy"
        const val UserNotes = "fragment_user_notes"
        const val USER_RATE = "fragment_user_rating"
        const val EditPtofile = "fragment_edit_ptofile"
        const val JoinAsCaptain = "fragment_join_as_captain"
        const val Coupons = "fragment_coupons"
        const val ChatOrder = "chat-coupons"
        const val PayWay = "sadad"
        const val SubCat = "fragment_subcat"
        const val Purchases = "PurchasesFragment"
        const val ShopsNotSupported = "ShopsNotSupported"

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main3)




        tryShare()

        try {
            val key = intent.extras!!.getString(KEY)
            Log.e("aa", key)
            when (key) {
                Restaurant -> {
                    main3_fragment.findNavController().navigate(R.id.kentakyRestaurant)
                }
                TardHayak -> {

                    val name = intent.extras!!.getString("name", "")
                    val type = intent.extras!!.getString("type", "")
                    val lat = intent.extras!!.getDouble("lat")
                    val lng = intent.extras!!.getDouble("lng")
                    val location = intent.extras!!.getString("location", "")

                    val b = Bundle()
                    b.putString("name", name)
                    b.putString("type", type)
                    b.putDouble("lat", lat)
                    b.putDouble("lng", lng)
                    b.putString("location", location)

                    main3_fragment.findNavController().navigate(R.id.tardHayyakFragment, b)
                }
                ConfirmMyOrder -> {
                    main3_fragment.findNavController().navigate(R.id.confirmOrdersFragment)
                }
                Shakawy -> {
                    main3_fragment.findNavController().navigate(R.id.shakawyFragment)
                }
                UserNotes -> {
                    main3_fragment.findNavController().navigate(R.id.userNotesFragment)
                }
                EditPtofile -> {
                    main3_fragment.findNavController().navigate(R.id.editProfileFragment)
                }
                JoinAsCaptain -> {
                    main3_fragment.findNavController().navigate(R.id.joinAsCaptainFragment)
                }
                Coupons -> {
                    main3_fragment.findNavController().navigate(R.id.couponsFragment)
                }
                ChatOrder -> {
                    val bundle = Bundle()
                    bundle.putString(ChatFragment.REQUEST_ID, intent!!.extras!!.getString(com.codecaique.deliverymasr.ui.client.confirmOrders.ConfirmMyOrder.REQUEST_ID))
                    bundle.putString(ChatFragment.USER_ID, intent!!.extras!!.getString(com.codecaique.deliverymasr.ui.client.confirmOrders.ConfirmMyOrder.USER_ID))

                    main3_fragment.findNavController().navigate(R.id.chatFragment, bundle)
                }
                PayWay -> {
                    main3_fragment.findNavController().navigate(R.id.payWayFragment)
                }

                SubCat -> {
                    main3_fragment.findNavController().navigate(R.id.fragment_SubCat)
                }
                Purchases -> {
                    main3_fragment.findNavController().navigate(R.id.purchasesFragment)
                }
                USER_RATE -> {
                    main3_fragment.findNavController().navigate(R.id.userRatingFragment)
                }
                ShopsNotSupported -> {
                    main3_fragment.findNavController().navigate(R.id.shopLoactionFragment)
                }
                else -> resulte()

            }
        } catch (e: Exception) {
//            resulte()
        }
    }

    private fun tryShare() {

        /*if (Intent.ACTION_SEND == intent.action && intent.type != null) {
            Log.e("data", intent.getStringExtra(Intent.EXTRA_TEXT))
        }
*/

    }

    private fun resulte() {
        when (StoresAdapter.fragName) {
            R.layout.fragment_tard__hayak_ -> {
                fragment = TardHayyakFragment()
            }
            R.layout.fragment_kentaky_restaurant -> {
                fragment = RestaurantFragment()
            }
            R.layout.fragment_confirm_my_order -> {
                fragment = ConfirmMyOrder()
            }
            R.layout.fragment_shakawy_list2 -> {
                fragment = ShakawyList2()
            }
            R.layout.fragment_user_notes4 -> {
                fragment = UserNotes4()
            }
            R.layout.fragment_bank_account_data -> {
                fragment = BankAccountData()
            }
            R.layout.fragment_user_notes -> {
                fragment = UserNotesFragment()
            }
            R.layout.fragment_coupons -> {
                fragment = CouponsFragment()
            }
            R.layout.fragment_register_bank_account -> {
                fragment = RegisterBankAccount()
            }
            R.layout.fragment_shakawy -> {
                fragment = ShakawyFragment()
            }
            R.layout.fragment_edit_ptofile -> {
                fragment = EditProfileFragment()
            }
            R.layout.fragment_join_as_captain -> {
                fragment = JoinAsCaptainFragment()
            }
            R.layout.fragment_user_rating -> {
                fragment = UserRatingFragment()
            }
            R.layout.fragment_pay_way -> {
                fragment = PayWayFragment()
            }
            R.layout.fragment_chat -> {
                fragment = ChatFragment()
            }
            else -> fragment = StoresFragment()
        }
        supportFragmentManager.beginTransaction().replace(R.id.main3_fragment, fragment!!).commit()
    }

    override fun onBackPressed() {
        super.onBackPressed()
//        finish()

        try {
//            main3_fragment.findNavController().popBackStack()
        } catch (e: Exception) {
//            finish()
        }
//        finish()
    }

    fun currant_frag() {}
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE || requestCode==0) {

                onActivityRequestResult(requestCode, resultCode, data, ChatFragment::class.java.simpleName)
                onActivityRequestResulty(requestCode, resultCode, data, SentRequestFragment::class.java.simpleName)

        }
    }


    private fun onActivityRequestResult(requestCode: Int, resultCode: Int, data: Intent?, fragmentName: String) {
        try {

            val navHostFragment: NavHostFragment? = supportFragmentManager.findFragmentById(R.id.main3_fragment) as NavHostFragment?

//            val fm = supportFragmentManager
//            navHostFragment?.getChildFragmentManager()!!.getFragments().get(0);
            if (navHostFragment?.getChildFragmentManager()!!.getFragments().size > 0) {
              for (i in navHostFragment?.getChildFragmentManager()!!.getFragments().indices) {
                    val fragment = navHostFragment?.getChildFragmentManager()!!.getFragments()[i]
                    if (fragment != null && fragment.javaClass.simpleName.equals(fragmentName, ignoreCase = true)) {
                        fragment.onActivityResult(requestCode, resultCode, data)
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun onActivityRequestResulty(requestCode: Int, resultCode: Int, data: Intent?, fragmentName: String) {
        try {
            val navHostFragment: NavHostFragment? = supportFragmentManager.findFragmentById(R.id.main3_fragment) as NavHostFragment?

//            val fm = supportFragmentManager
//            navHostFragment?.getChildFragmentManager()!!.getFragments().get(0);
            if (navHostFragment?.getChildFragmentManager()!!.getFragments().size > 0) {
                for (i in navHostFragment?.getChildFragmentManager()!!.getFragments().indices) {
                    val fragment = navHostFragment?.getChildFragmentManager()!!.getFragments()[i]
                    if (fragment != null && fragment.javaClass.simpleName.equals(fragmentName, ignoreCase = true)) {
                        fragment.onActivityResult(requestCode, resultCode, data)
                    }
                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }
}