package com.codecaique.deliverymasr.base.agent

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.RTLActivity
import com.codecaique.deliverymasr.ui.agent.subMain.ui.postproblem.PostProblemFragment
import com.codecaique.deliverymasr.ui.client.chat.ChatFragment
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.activity_sub_main.*

class SubMainActivity : RTLActivity() {

    companion object {
        val WAY = "way"
        val KEY = "key"
        val CHAT = "chat"
        val ORDERCHAT = "orderchat"
        val DISTANT = "distant"
        val ORDERDETAILS = "orderDetails"
        val SPECIALORDER = "specialOrder"
        val DELIVERY = "delivery"
        val COMMENTS = "comments"
        val COUPONS = "coupons"
        val EDITPROFILE = "editprofile"
        val PROBLEMS = "problems"
        val SHARLOTION = "shar"
        val POSTPROBLEM = "post"
        val PUSHBILL = "Push-bill"
        //
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sub_main)

        val key = intent.extras!!.getString(KEY)!!

        when (key) {
            CHAT -> fragment_sub.findNavController().navigate(R.id.chatFragment)
            ORDERCHAT -> fragment_sub.findNavController().navigate(R.id.orderChatFragment)
            DISTANT -> fragment_sub.findNavController().navigate(R.id.distantFragment)
            ORDERDETAILS -> {
                fragment_sub.findNavController().navigate(R.id.orderDetailsFragment)
            }
            SPECIALORDER -> fragment_sub.findNavController().navigate(R.id.specialOrderFragment)
            DELIVERY -> fragment_sub.findNavController().navigate(R.id.deliveryFragment)
            COMMENTS -> fragment_sub.findNavController().navigate(R.id.commentsFragment)
            COUPONS -> fragment_sub.findNavController().navigate(R.id.couponsFragment)
            EDITPROFILE -> fragment_sub.findNavController().navigate(R.id.editProfileFragment)
            PROBLEMS -> fragment_sub.findNavController().navigate(R.id.mainProblemsFragment)
            SHARLOTION -> fragment_sub.findNavController().navigate(R.id.shareLocationFragment)
            POSTPROBLEM -> {

                val user_id = intent!!.extras!!. getInt (PostProblemFragment.USER_ID).toString()
                val order_id = intent!!.extras!!.getInt(PostProblemFragment.ORDER_ID).toString()

                val bundle = Bundle()
                bundle.putString(PostProblemFragment.USER_ID, user_id)
                bundle.putString(PostProblemFragment.ORDER_ID, order_id)

                fragment_sub.findNavController().navigate(R.id.postProblemFragment, bundle)
            }
            WAY -> fragment_sub.findNavController().navigate(R.id.registerBankAccountFragment)
            PUSHBILL -> fragment_sub.findNavController().navigate(R.id.pushBillFragment)

        }

    }


    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount
        if (count == 0) {
            super.onBackPressed()
            Log.e("ONBACK00", "00000000")
//            supportFragmentManager.popBackStack()
            //additional code
//            finish()
        } else {
            super.onBackPressed()
            supportFragmentManager.popBackStack()
        }
    }

    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }


    @SuppressLint("MissingSuperCall")
    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE || requestCode==0) {
            onActivityRequestResult(requestCode, resultCode, data, com.codecaique.deliverymasr.ui.agent.subMain.ui.chat.ChatFragment::class.java.simpleName)
        }

    }

    private fun onActivityRequestResult(requestCode: Int, resultCode: Int, data: Intent?, fragmentName: String) {
        try {

            val navHostFragment: NavHostFragment? = supportFragmentManager.findFragmentById(R.id.fragment_sub) as NavHostFragment?

//            val fm = supportFragmentManager
//            navHostFragment?.getChildFragmentManager()!!.getFragments().get(0);
            if (navHostFragment?.getChildFragmentManager()!!.getFragments().size > 0) {
                for (i in navHostFragment?.getChildFragmentManager()!!.getFragments().indices) {
                    val fragment = navHostFragment?.getChildFragmentManager()!!.getFragments()[i]
                    if (fragment != null && fragment.javaClass.simpleName.equals(fragmentName, ignoreCase = true)) {
                        fragment.onActivityResult(requestCode, resultCode, data)
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }}
