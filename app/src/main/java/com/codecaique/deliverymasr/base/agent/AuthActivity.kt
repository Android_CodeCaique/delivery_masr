package com.codecaique.deliverymasr.base.agent

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.LanguageHelper
import com.codecaique.deliverymasr.base.RTLActivity
import com.codecaique.deliverymasr.base.SharedHelper
import com.codecaique.deliverymasr.pojo.util.GPSTracker
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.agent.auth.MapViewModel
import com.codecaique.deliverymasr.ui.agent.main.ui.userSettings.UserSettingsFragment
import com.facebook.CallbackManager
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.MediaType
import okhttp3.RequestBody

class AuthActivity : RTLActivity(), GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    lateinit var mapViewModel: MapViewModel

    companion object {

        private const val TAG = "AuthActivity"
        val KEY = "key"
        val JOINUSMANDOWP = "JOINASMANDOWP"

    }
    private var gpsTracker: GPSTracker? = null
    private var currentLocation: LatLng? = null
    var callbackManager = CallbackManager.Factory.create()
    var provider: String = "";
    var finalLoc: Location? = null
//    var gg: GPSTracker? = null

    val REQUEST_CODE = 21212
    val REQUEST_CODE_LOCATION = 221
    val PLaySERVICE_Request = 32

    lateinit var languageHelper: LanguageHelper
    lateinit var sharedHelper: SharedHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        languageHelper = LanguageHelper()
        sharedHelper = SharedHelper()
        if (sharedHelper.getKey(applicationContext, "LANG").equals("en")) {
            languageHelper.ChangeLang(this.resources, "en")
        } else if (sharedHelper.getKey(applicationContext, "LANG").equals("ar")) {
            languageHelper.ChangeLang(this.resources, "ar")
        }
        mapViewModel = ViewModelProviders.of(this).get(MapViewModel::class.java)

        try {
            if (intent!!.extras!!.getString(KEY) == JOINUSMANDOWP)
                fragment_auth.findNavController().navigate(R.id.addMandopFragment)

        } catch (e: Exception) {
        }
    }

    override fun onStart() {
        super.onStart()
        buildLocationNow()
        googleApiClient.connect()
        try {

            val b = intent!!.extras!!.getBoolean(UserSettingsFragment.isConv, false)
            if (b) {
                fragment_auth.findNavController().navigate(R.id.conversionFragment)
            }
        } catch (e: Exception) {
        }
    }

    private fun checkPermissions(): Boolean {

        val locationManager = applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)



        Log.e(TAG, "checkPermissions: isGPSEnabled $isGPSEnabled")

        if (!isGPSEnabled) {
            Toast.makeText(applicationContext, getString(R.string.open_gps), Toast.LENGTH_LONG).show()
            val intent = Intent(
                    Settings.ACTION_LOCATION_SOURCE_SETTINGS
            )
            //todo now
            val location = Location("")
            location.latitude = /*29.0711985*/ 29.1036367
            location.longitude = /*31.1030209*/30.9770791


            startActivity(intent)
//            setLocation(location)
            return false
        }

        if (ContextCompat.checkSelfPermission(
                        applicationContext,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(
                        applicationContext,
                        android.Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(
                        applicationContext,
                        android.Manifest.permission.SEND_SMS
                ) != PackageManager.PERMISSION_GRANTED
        ) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                requestPermissions(
                        arrayOf(
                                android.Manifest.permission.ACCESS_COARSE_LOCATION,
                                android.Manifest.permission.ACCESS_FINE_LOCATION,
                                android.Manifest.permission.SEND_SMS
                        ), REQUEST_CODE_LOCATION
                )
            } //

            return false
        } else
//            Toast.makeText(activity!!.applicationContext,resources.getString(R.string.accept_prem) +"",Toast.LENGTH_LONG).show()
//            setServices(v,0)

            return true
    }


    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {

            REQUEST_CODE_LOCATION -> {

                Log.e("REquest loc", REQUEST_CODE_LOCATION.toString() + "   ")

//                checkPlaySerice()
                createLocationRequest()

//                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {

//                setServices(v,0)

                /*} else {
                    //Granted..
            Toast.makeText(activity!!.applicationContext,resources.getString(R.string.accept_prem) +"",Toast.LENGTH_LONG).show()
                }*/
            }

            /*REQUEST_CODE->{

            }*/

        }
        //...
    }

    lateinit var googleApiClient: GoogleApiClient
    lateinit var locationRequest: LocationRequest
    private fun checkPlaySerice(): Boolean {

        buildLocationNow()

        val resultCode = GoogleApiAvailability.getInstance()
                .isGooglePlayServicesAvailable(applicationContext)
        if (resultCode != ConnectionResult.SUCCESS) {

            if (GoogleApiAvailability.getInstance().isUserResolvableError(resultCode)) {

                GoogleApiAvailability.getInstance()
                        .getErrorDialog(this, resultCode, /*PLaySERVICE_Request*/11).show()
            } else {

                Toast.makeText(
                        applicationContext,
                        "this Device is not Supported",
                        Toast.LENGTH_LONG
                ).show()
//                finish()
            }
            return false
        }

        return true
    }

    private fun buildLocationNow() {


        googleApiClient = GoogleApiClient.Builder(applicationContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build()
    }

    override fun onConnected(p0: Bundle?) {

        createLocationRequest()
    }

    @SuppressLint("RestrictedApi")
    private fun createLocationRequest() {

        locationRequest = LocationRequest()
        locationRequest.interval = 10000         //10 sec
        locationRequest.fastestInterval = 5000   // 5 sec
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        if (!checkPermissions()) {
            Log.e("Location ss", "null 111")
            return
        } else
            Log.e("Location ss", "not null 111")

        val fusedLocationClient =
                LocationServices.getFusedLocationProviderClient(applicationContext)

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // -for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    REQUEST_CODE_LOCATION
            )
            return
        }

//        fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
//            location?.let { it: Location ->
//                // Logic to handle location object
//
//                setLocation(location)
//                //get Weather By Lat and Log request Location
////                observerWeatherAPI()
//
//            } ?: kotlin.run {
//                // Handle Null case or Request periodic location update
//                // https://developer.android.com/training/location/receive-location-updates
//            }
//        }
        gpsTracker = GPSTracker(this)
        if (gpsTracker!!.canGetLocation) {
//          binding.chooseLocationBtn.setEnabled(true);
            currentLocation = LatLng(gpsTracker!!.latitude, gpsTracker!!.longitude)
            setLocation(currentLocation)
        }


    }

    private fun setLocation(location: LatLng?) {

        val userInfo = UserInfo(applicationContext)
        userInfo.setLocation(location!!.latitude.toString(), location.longitude.toString())

        Log.e("LOCATION 2", location.toString() + "      ")

        if (userInfo.getUserToken().isNotEmpty()) {
            mapViewModel.updateLocation(
                    RequestBody.create(MediaType.parse("multipart/form-data"), userInfo.getUserToken()),
                    location.longitude, location.latitude
            )

            mapViewModel.mapMutableLiveData.observe(this, Observer { })
        }


    }

    override fun onConnectionSuspended(p0: Int) {

        googleApiClient.connect()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        Toast.makeText(applicationContext, p0.errorMessage, Toast.LENGTH_LONG).show()

    }

    override fun onLocationChanged(location: Location?) {

       // setLocation(location)

    }

    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
//        TODO("Not yet implemented")
    }

    override fun onProviderEnabled(p0: String?) {
//        TODO("Not yet implemented")
    }

    override fun onProviderDisabled(p0: String?) {
//        TODO("Not yet implemented")
    }


    override fun onDestroy() {
        super.onDestroy()
        googleApiClient.disconnect()
    }

    override fun onResume() {
        super.onResume()

//        checkPlaySerice()
        checkPermissions()
        Log.e(this.javaClass.name, "CurrentScreen")

    }


}