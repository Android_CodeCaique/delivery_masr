package com.codecaique.deliverymasr.base;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;

import com.codecaique.deliverymasr.R;
import com.codecaique.deliverymasr.base.agent.StoresMainActivity;
import com.codecaique.deliverymasr.base.client.MainActivity;
import com.codecaique.deliverymasr.pojo.util.UserInfo;

public class ChangeLanguage extends DialogFragment {

    private View view ;

    private RadioButton arabic , english ;
    private SharedHelper SharedHelpers;
    private LanguageHelper languageHelper;
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.change_language_dialog, container, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getDialog().getWindow().setBackgroundDrawable(getActivity().getDrawable(R.drawable.back_edit_chat));
        }
        SharedHelpers = new SharedHelper();
        languageHelper = new LanguageHelper();
        Window window = getDialog().getWindow();
        if(window != null)
        {
            WindowManager.LayoutParams params = window.getAttributes();
//            params.width = 500;
//            params.height = 450;
            window.setAttributes(params);
        }
        init();
        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
        if(window == null) return;
        WindowManager.LayoutParams params = window.getAttributes();
//        params.width = 500;
//        params.height = 450;
        window.setAttributes(params);
    }
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void init(){
        english = view.findViewById(R.id.english);
        arabic = view.findViewById(R.id.arabic);






        if (!SharedHelpers.getKey(getActivity() , "LANG").isEmpty())
        {
            if (SharedHelpers.getKey(getActivity() , "LANG").equals("en"))
            {
                english.setChecked(true);
                english.setTextDirection(View.TEXT_DIRECTION_LTR);
                arabic.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            }
            else if (SharedHelpers.getKey(getActivity() , "LANG").equals("ar"))
            {
                arabic.setChecked(true);
                english.setTextDirection(View.TEXT_DIRECTION_RTL);
                arabic.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            }
        }
        english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (english.isChecked())
                {

                    languageHelper.ChangeLang(getResources() , "en");
                    SharedHelpers.putKey(getActivity() , "LANG" , "en");
                    SharedHelpers.putKey(getActivity() , "END" , "1");
                    Intent intent;

                    if (new UserInfo(getActivity()).getState().contains("user"))
                    {
                        Log.e("USER", "TRUE");
                        intent = new Intent(getActivity() , MainActivity.class);
                    }
                    else {
                        Log.e("USER", "FALSE");
                        intent = new Intent(getActivity(), StoresMainActivity.class);
                    }




                    startActivity(intent);
                    getActivity().finish();
                }
            }
        });

        arabic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (arabic.isChecked())
                {
                    languageHelper.ChangeLang(getResources() , "ar");
                    SharedHelpers.putKey(getActivity() , "LANG" , "ar");
                    SharedHelpers.putKey(getActivity() , "END" , "1");
                    Intent intent = new Intent(getActivity() , MainActivity.class);

                    if (new UserInfo(getActivity()).getState().contains("user"))
                    {
                        Log.e("USER", "TRUE");
                        intent = new Intent(getActivity() , MainActivity.class);
                    }
                    else {
                        Log.e("USER", "FALSE");
                        intent = new Intent(getActivity(), StoresMainActivity.class);
                    }

                    startActivity(intent);
                    getActivity().finish();
                }
            }
        });
    }



}
