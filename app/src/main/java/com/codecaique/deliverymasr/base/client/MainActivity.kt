package com.codecaique.deliverymasr.base.client

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.LanguageHelper
import com.codecaique.deliverymasr.base.RTLActivity
import com.codecaique.deliverymasr.base.SharedHelper
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.client.allSitting.AllSittingFragment
import com.codecaique.deliverymasr.ui.client.allSitting.SittingsFragment
import com.codecaique.deliverymasr.ui.client.bankAccount.BankAccount
import com.codecaique.deliverymasr.ui.client.deliveryOffers.DeliveryOffersFragment
import com.codecaique.deliverymasr.ui.client.myOrders.MyOrdersFragment
import com.codecaique.deliverymasr.ui.client.notification.NotificationsFragment
import com.codecaique.deliverymasr.ui.client.notification.NotificationsViewModel
import com.codecaique.deliverymasr.ui.client.shakawy.ShakawyFragment
import com.codecaique.deliverymasr.ui.client.stores.StoresFragment
import com.codecaique.deliverymasr.ui.client.stores.adapters.StoresAdapter
import com.codecaique.deliverymasr.utils.Utils
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main2.*
import kotlinx.android.synthetic.main.dialog_menu.*


class MainActivity : RTLActivity() {
    var fragment: Fragment? = null
    var navView: BottomNavigationView? = null
    lateinit var languageHelper: LanguageHelper
    lateinit var sharedHelper: SharedHelper

    lateinit var navController:NavController

    var onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { menuItem ->
        val id = menuItem.itemId
        when (id) {
            R.id.navigation_Sittings -> {
                fragment = AllSittingFragment()
            }
            R.id.navigation_MyOrders -> {
                fragment = MyOrdersFragment()
            }
            R.id.navigation_DeliveryOffers -> {
                fragment = DeliveryOffersFragment()
            }
            R.id.navigation_Notification -> {
                fragment = NotificationsFragment()
                main_nav_view.removeBadge( R.id.navigation_Notification)
            }
            R.id.navigation_Stores -> {
                fragment = StoresFragment()
            }
        }
        supportFragmentManager.beginTransaction().replace(R.id.main_fragment, fragment!!).commit()
        true
    }

    lateinit var myNotificationsViewModel: NotificationsViewModel
    lateinit var usrInfo: UserInfo

    lateinit var tvNavigationView: BadgeDrawable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)



        languageHelper = LanguageHelper()
        sharedHelper = SharedHelper()
        if(sharedHelper.getKey(applicationContext,"LANG").equals("en"))
        {
            languageHelper.ChangeLang(this.resources,"en")
        }else if(sharedHelper.getKey(applicationContext,"LANG").equals("ar"))
        {
            languageHelper.ChangeLang(this.resources,"ar")
        }

        setContentView(R.layout.activity_main2)

        navView = findViewById(R.id.main_nav_view)

        myNotificationsViewModel = ViewModelProviders.of(this).get(NotificationsViewModel::class.java)
        usrInfo = UserInfo(applicationContext)



        tvNavigationView = main_nav_view.getOrCreateBadge( R.id.navigation_Notification)
        tvNavigationView.isVisible = false
        tvNavigationView.maxCharacterCount = 2
        tvNavigationView.backgroundColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
        tvNavigationView.number = 0

        myNotificationsViewModel.getNotifications(usrInfo.getUserToken(),"user", SharedHelper().getKey(applicationContext, "LANG")!!)
        myNotificationsViewModel.myNotificationsMutableLiveData.observe(this, Observer { t ->

            try{
                if (t.error == 3) {
                    Utils.signout(this, view!!)
                    return@Observer;
                }
                val n = t.data.size

                Log.e("TAGa", usrInfo.getNotifiCount().toString())
                Log.e("TAGa",n.toString())

                if (n > 0) {

                    val current = usrInfo.getNotifiCount()
                    if (current < n) {
                        usrInfo.setNotifiCount(n.toString())
                        tvNavigationView.run {
                            isVisible=true
                            number=n-current
                        }
                        Log.e("TAGa","n.toString()")
                    }

                }
            } catch (e:Exception){}

        })


        fragment = StoresFragment()
        try {
            val key = intent.extras!!.getString(KEY)!!
            if (key == DeliveryOffers) {

                fragment = DeliveryOffersFragment()
                supportFragmentManager.beginTransaction().replace(R.id.main_fragment, fragment!!).commit()
                navView!!.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
                navView!!.setSelectedItemId(R.id.navigation_DeliveryOffers)

                return
            }
        } catch (e: Exception) {

            if (StoresAdapter.fragName == R.layout.fragment_my_orders)
                fragment = MyOrdersFragment()
            else if (StoresAdapter.fragName == R.layout.fragment_shakawy)
                fragment = ShakawyFragment()
            else if (StoresAdapter.fragName == R.layout.fragment_bank_account) {
                fragment = BankAccount()
            } else if (StoresAdapter.fragName == R.layout.fragment_all_sitting) {
                fragment = AllSittingFragment()
            } else if (StoresAdapter.fragName == R.layout.fragment_sittings) {
                fragment = SittingsFragment()
            } else if (StoresAdapter.fragName == R.layout.fragment_delivery_offers) {
                fragment = DeliveryOffersFragment()
            }

            supportFragmentManager.beginTransaction().replace(R.id.main_fragment, fragment!!).commit()
            navView!!.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
            navView!!.selectedItemId = R.id.navigation_Stores
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()

    }

    companion object {
        const val KEY = "Key"
        var DeliveryOffers = "DeliveryOffersFragment"
    }

    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }


    private fun onActivityRequestResult(requestCode: Int, resultCode: Int, data: Intent, fragmentName: String) {
        try {
            val fm: FragmentManager = supportFragmentManager
            if (fm.getFragments().size > 0) {
                for (i in 0 until fm.getFragments().size) {
                    val fragment: Fragment = fm.getFragments().get(i)
                    if (fragment != null && fragment.javaClass.simpleName.equals(fragmentName, ignoreCase = true)) {
                        fragment.onActivityResult(requestCode, resultCode, data)
                    }
                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }
}