package com.codecaique.deliverymasr.base.agent

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.RTLActivity
import com.codecaique.deliverymasr.base.SharedHelper
import com.codecaique.deliverymasr.pojo.services.TrackerService
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.agent.main.dialogs.OrderSentDialog
import com.codecaique.deliverymasr.ui.agent.subMain.ui.orderChat.UserInfoDialog
import com.codecaique.deliverymasr.ui.client.notification.NotificationsViewModel
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_stores_main.*


class StoresMainActivity : RTLActivity(), BottomNavigationView.OnNavigationItemSelectedListener {
    companion object {

        val KEY = "key"
        val SETTINGS = "settings"
        val MYORDERS = "my_orders"
        val DELIVERY = "delivery"
        val NOTIFICATION = "notification"
        val SUB_SETTINGS="SUB_SETTINGS"
    }

    lateinit var myNotificationsViewModel: NotificationsViewModel
    lateinit var usrInfo: UserInfo
    private val PERMISSIONS_REQUEST = 1

    lateinit var tvNavigationView: BadgeDrawable
    lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stores_main)

        myNotificationsViewModel = ViewModelProviders.of(this).get(NotificationsViewModel::class.java)
        usrInfo = UserInfo(applicationContext)

        /* val permission = ContextCompat.checkSelfPermission(this,
                 Manifest.permission.ACCESS_FINE_LOCATION)
         if (permission == PackageManager.PERMISSION_GRANTED) {
             startTrackingService()
         } else {
             ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                     PERMISSIONS_REQUEST)
         }
 */

        startTrackingService()

        tvNavigationView = bottom_nav.getOrCreateBadge(R.id.navigation_Notification)
        tvNavigationView.isVisible = false
        tvNavigationView.number = 0
        tvNavigationView.maxCharacterCount = 2
        tvNavigationView.backgroundColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)


        myNotificationsViewModel.getNotifications(usrInfo.getUserToken(), "driver", SharedHelper().getKey(applicationContext, "LANG")!!)
        myNotificationsViewModel.myNotificationsMutableLiveData.observe(this, Observer { t ->

            try {

                val n = t.data.size

                Log.e("TAGa", usrInfo.getNotifiCount().toString())
                Log.e("TAGa", n.toString())

                if (n > 0) {

                    val current = usrInfo.getNotifiCount()
                    if (current < n) {
                        usrInfo.setNotifiCount(n.toString())
                        tvNavigationView.run {
                            isVisible = true
                            number = n - current
                        }
                        Log.e("TAGa", "n.toString()")
                    }

                }

            } catch (e: Exception) {
            }

        })

        navController = Navigation.findNavController(this, R.id.fragment_stores)

        bottom_nav.setOnNavigationItemSelectedListener(this)
        bottom_nav.selectedItemId = R.id.offers_del

    }


    private fun startTrackingService() {

        startService(Intent(this, TrackerService::class.java))

    }


    override fun onBackPressed() {
        super.onBackPressed()

        Log.e("ON back", "TRUE")


        val id = navController.currentDestination!!.id



        Log.e("ON back", "not null" + id.toString() + "------>" + R.id.splashFragment)

        if (navController != null) {


            Log.e("ON back", "not null" + id.toString() + "------>" + R.id.offersDeliveredFragment)


            if (id == R.id.splashFragment) {
                Log.e("IF back", "TRUE")
                val a = Intent(Intent.ACTION_MAIN)
                a.addCategory(Intent.CATEGORY_HOME)
                a.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(a)
//                finish()

            }


        }

    }

    override fun onStart() {
        super.onStart()

        navController.navigate(R.id.offersDeliveredFragment)

        Log.e("TTTTTTT", "sssssssssssss")

        try {

            val b = intent!!.extras!!.getBoolean(OrderSentDialog.isNotifications, false)
            if (b) {
                fragment_stores.findNavController().navigate(R.id.notificationsFragment)
                bottom_nav.selectedItemId = R.id.notifications
            }
        } catch (e: Exception) {
        }

        try {

            val b = intent!!.extras!!.getBoolean(UserInfoDialog.isComments, false)
            if (b) {
                fragment_stores.findNavController().navigate(R.id.comments)
                bottom_nav.selectedItemId = R.id.settings
            }
        } catch (e: Exception) {
        }


        try {

            val s = intent!!.extras!!.getString(KEY)
            if (s == SETTINGS) {
                Log.e("KEY 4505", s)
                fragment_stores.findNavController().navigate(R.id.userSettingsFragment)
                bottom_nav.selectedItemId = R.id.settings
            }

            if (s == SUB_SETTINGS) {
                Log.e("KEY 4505", s)
                bottom_nav.selectedItemId = R.id.settings
                fragment_stores.findNavController().navigate(R.id.settingsFragment)
            }

            if (s == MYORDERS) {
                Log.e("KEY 455", s)
                Log.e("KEY sda", navController.toString() + "    ")
                navController.navigate(R.id.userSettingsFragment)
                bottom_nav.selectedItemId = R.id.my_orders
            }

        } catch (e: Exception) {
            Log.e("ERROR", e.message.toString() + "   ")
        }
    }

    override fun onResume() {
        super.onResume()
//        navController.navigate(R.id.offersDeliveredFragment)
        Log.e(this.javaClass.name, "CurrentScreen")

    }


    override fun onNavigationItemSelected(p0: MenuItem): Boolean {

        when (p0.itemId) {

            R.id.settings -> {
                fragment_stores.findNavController().navigate(R.id.userSettingsFragment)
                return true
            }
            R.id.offers_del -> {
                fragment_stores.findNavController().navigate(R.id.offersDeliveredFragment)
                return true
            }
            R.id.notifications -> {
                fragment_stores.findNavController().navigate(R.id.notificationsFragment)
                bottom_nav.removeBadge(R.id.notifications)
                return true
            }
            R.id.my_orders -> {
                fragment_stores.findNavController().navigate(R.id.myOrdersFragment)
                return true
            }
//            R.id.stores -> {
//                fragment_stores.findNavController().navigate(R.id.homeFragment)
//                return true
//            }

        }

        return false

    }


}
