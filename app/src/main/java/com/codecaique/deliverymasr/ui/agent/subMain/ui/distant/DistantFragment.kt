package com.codecaique.deliverymasr.ui.agent.subMain.ui.distant

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.agent.subMain.ui.delivery.Make_Offer_ViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_delivery.view.*
import kotlinx.android.synthetic.main.fragment_distant.*
import kotlinx.android.synthetic.main.fragment_distant.view.*

/**
 * A simple [Fragment] subclass.
 */
class DistantFragment : Fragment() , OnMapReadyCallback {


    lateinit var viewModel: Make_Offer_ViewModel
    lateinit var userInfo: UserInfo
    lateinit var VIEW:View
    private lateinit var mMap: GoogleMap
    var requestId=""
    var type = ""
    var userID = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        VIEW= inflater.inflate(R.layout.fragment_distant, container, false)
        return VIEW

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        viewModel = ViewModelProviders.of(this).get(Make_Offer_ViewModel::class.java)
        userInfo = UserInfo(activity!!.applicationContext)

        val mapFragment = //(context!! as AppCompatActivity).supportFragmentManager
                childFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment
        mapFragment.getMapAsync(this)

        requestId = try {
            arguments!!.getString("requestId", "0")

        } catch (e: Exception) {
            activity!!.intent.extras!!.getString("requestId", "0")

        }

        userID = try {
            arguments!!.getString("user_id", "0").toInt()

        } catch (e: Exception) {
            activity!!.intent.extras!!.getString("user_id", "0").toInt()

        }

        Log.e("USER UD sa",userID.toString())


        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true/*  enabled by default*/ ) {
            override fun handleOnBackPressed() {
                // Handle the back button event
                Log.e("lk","jgjkuym")
                view.findNavController().popBackStack()

            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)




        type = try {
            arguments!!.getString("type", "0")

        } catch (e: Exception) {
            activity!!.intent.extras!!.getString("type", "0")

        }

        Log.e("request_id", requestId + "    " + type)


        view.im_back.setOnClickListener {
            it.findNavController().popBackStack()
        }



    }


    @SuppressLint("SetTextI18n")
    override fun onMapReady(p0: GoogleMap?) {

        mMap = p0!!
        mMap.mapType = GoogleMap.MAP_TYPE_NORMAL



       img_map.setOnClickListener {
           checked()
       }
        viewModel.showOrderById(userInfo.getUserToken(), requestId.toInt())

        viewModel.showOrderMutableLiveData.observe(this, Observer {


            VIEW.name.text=it.data[0].name
            VIEW.order_id.text=it.data[0].id +" # "
            VIEW.tv_send_address.text=it.data[0].delivery_address+" "
            VIEW.tv_receive_address.text=it.data[0].receiving_address+" "
//            view.textAddress.text=it.data[0].
            var latLng= LatLng(it.data[0].client_latitude.toDouble(),it.data[0].client_longitude.toDouble())


            if (type == "deliver")
            {

                VIEW.user.visibility = View.GONE
                VIEW.tv_1.visibility = View.GONE
                VIEW.tv_receive_address.visibility = View.GONE


                Log.e("data",it.data[0].client_latitude+" nkjrg  "+it.data[0].client_longitude.toDouble())

                mMap.addMarker(MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.group_3581)))

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))

            }

            if (type == "receive")
            {

                VIEW.wave_person.visibility = View.GONE
                VIEW.tv_2.visibility = View.GONE
                VIEW.tv_send_address.visibility = View.GONE


                try {
                    latLng= LatLng(it.data[0].receiving_latitude.toDouble(),it.data[0].receiving_longitude.toDouble())

                    Log.e("data",it.data[0].receiving_latitude+" nkjrg  "+it.data[0].receiving_longitude.toDouble())

                    mMap.addMarker(MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.group_3583)))

                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))

                } catch (e: Exception) {
                    Log.e("EXCEPTION Deliver", e.message)
                }
            }

//            val formatter = DecimalFormat("00.0")
//
//            try {
//                VIEW.tv_receive_distance.text = formatter.format(it.data[0].agent_shope_distance.toDouble()) + " كم "
//                VIEW.tv_sent_distance.text = formatter.format(it.data[0].client_shope_distance.toDouble()) + " كم "
//            } catch (e: Exception) {
//                Log.e("EXCEPTION Deliver", e.message)
//            }


        })


    }

    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }

    private fun checked(){
       rg_views.visibility=View.VISIBLE

        val checkedChangeListener: RadioGroup.OnCheckedChangeListener = object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {

                // Getting Reference to map_view of the layout activity_main

                // Currently checked is rb_map
                if (checkedId == R.id.rb_map) {
                    mMap.mapType = GoogleMap.MAP_TYPE_NORMAL
                  rg_views.visibility=View.GONE
                }

                // Currently checked is rb_satellite
                if (checkedId == R.id.rb_satellite) {
                    mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID)
                rg_views.visibility=View.GONE

                }
            }


        }
        // Setting Checked ChangeListener
       rg_views.setOnCheckedChangeListener(checkedChangeListener)


    }

}