package com.codecaique.deliverymasr.ui.agent.subMain.ui.chat.dialogs

import android.app.Dialog
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.SubMainActivity
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.agent.subMain.ui.chat.ChatFragment
import com.codecaique.deliverymasr.ui.agent.subMain.ui.postproblem.PostProblemFragment
import com.codecaique.deliverymasr.ui.client.chat.ChatViewModel
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.dialog_chosse_action.*
import kotlinx.android.synthetic.main.fragment_chat.*
import java.util.*

class ChooseActionDialog(var context_: Context, var chatViewModel: ChatViewModel, var requestId: Int, var userId: Int, var orderState: String, var cost: String, val date: Date?) : Dialog(context_) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_chosse_action)

        val intent = Intent(context, SubMainActivity::class.java)
        intent.putExtra(PostProblemFragment.USER_ID, userId.toString())
        intent.putExtra(PostProblemFragment.ORDER_ID, requestId.toString())
        val userInfo = UserInfo(context_)

//        done.visibility = View.GONE

        Log.e("orderState", orderState)
        when (orderState) {
            /*"arrived_site" -> {
                changeBill.visibility = View.VISIBLE
            }*/
            // "processed"-> arrived_site.visibility = View.VISIBLE
            "make_bill" -> {
                changeBill.visibility = View.VISIBLE
                tv_out.visibility = View.VISIBLE
            }
            "arrived_site" -> done.visibility = View.VISIBLE
            "processed" -> changeBill.visibility = View.GONE


        }

        if (date != null) {
            tv_out.visibility = View.GONE
        }

        changeBill.setOnClickListener {

            val bundle = Bundle()
            bundle.putString(ChatFragment.COST, cost)
            bundle.putString(ChatFragment.USER_ID, userId.toString())
            bundle.putString(ChatFragment.REQUEST_ID, requestId.toString())

            (context_ as FragmentActivity)
                    .findNavController(R.id.fragment_sub)
                    .navigate(R.id.pushBillFragment, bundle)
//            intent.putExtra(SubMainActivity.KEY, SubMainActivity.PUSHBILL)
//            intent.putExtra(ChatFragment.COST, cost)
//            intent.putExtra(ChatFragment.REQUEST_ID, requestId.toString())
//            context.startActivity(intent)
            dismiss()
        }


        done.setOnClickListener {

            chatViewModel.deliverOrder(userInfo.getUserToken(), requestId)
            chatViewModel.deliverOfferMutableLiveData.observe((context as ContextWrapper).baseContext as FragmentActivity, Observer {

                if (it.error == 0) {

                    (context_ as AppCompatActivity).tbOptions.visibility = View.GONE
                    Toast.makeText(context_, context_.getString(R.string.ordera_delivered), Toast.LENGTH_SHORT).show()
                    dismiss()

                }

            })

        }

        btn_close.setOnClickListener {

            dismiss()
        }
        tv_out.setOnClickListener {

            val dialog = OrderOutDialog(context_, chatViewModel, requestId)
            val window: Window = dialog.window!!
            val wlp: WindowManager.LayoutParams = window.attributes
            wlp.gravity = Gravity.BOTTOM
            wlp.flags = wlp.flags and WindowManager.LayoutParams.FLAG_DIM_BEHIND.inv()
            window.attributes = wlp
            dialog.show()
            dismiss()
        }
        tv_shar.setOnClickListener {

            //  it.findNavController().navigate()

/*
            val b = Bundle()
            b.putString("requestId", requestId.toString())
            it.findNavController().navigate(R.id.distantFragment, b)
*/

            intent.putExtra(SubMainActivity.KEY, SubMainActivity.SHARLOTION)
            context.startActivity(intent)
            dismiss()
        }
        tv_upload.setOnClickListener {
            FirebaseFirestore.getInstance()
                    .collection(ApiCreater.OrdersCollection)
                    .document(requestId.toString())
                    .get().addOnSuccessListener { value->

                        try {

                            if (value!![ApiCreater.ProblemUploadedForDriver].toString().toBoolean()) {

                                Toast.makeText(context, context.getString(R.string.have_problem), Toast.LENGTH_SHORT).show()
                                dismiss()

                            } else {
                                intent.putExtra(SubMainActivity.KEY, SubMainActivity.POSTPROBLEM)
                                intent.putExtra(PostProblemFragment.USER_ID, userId.toString())
                                intent.putExtra(PostProblemFragment.ORDER_ID, requestId.toString())
                                context.startActivity(intent)
                                dismiss()

                            }
                        } catch (e: Exception) {
                            intent.putExtra(SubMainActivity.KEY, SubMainActivity.POSTPROBLEM)
                            intent.putExtra(PostProblemFragment.USER_ID, userId.toString())
                            intent.putExtra(PostProblemFragment.ORDER_ID, requestId.toString())
                            context.startActivity(intent)
                            dismiss()
                        }
                    }

        }
    }
}
