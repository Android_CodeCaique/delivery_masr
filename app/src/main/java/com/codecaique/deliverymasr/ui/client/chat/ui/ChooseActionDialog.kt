package com.codecaique.deliverymasr.ui.client.chat.ui

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.View.GONE
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.SubMainActivity
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.agent.subMain.ui.orderDetails.dialogs.OrderOutDialog
import com.codecaique.deliverymasr.ui.agent.subMain.ui.postproblem.PostProblemFragment
import com.codecaique.deliverymasr.ui.client.chat.ChatViewModel
import com.codecaique.deliverymasr.ui.client.tracking.TrackingActivity
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.dialog_menu.*
import java.util.*

class ChooseActionDialog(var CONTEXT: Context, var chatViewModel: ChatViewModel, var requestId: Int,
                         var userId: Int, val view: View, val date: Date?, var driver_id: Int) : Dialog(CONTEXT) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_menu)

        var intent = Intent(context, SubMainActivity::class.java)


        when (chatViewModel.showOrderMutableLiveData.value!!.data[0].state) {
            "make_bill" -> {
                textShare.text = CONTEXT.getString(R.string.following_order)
                textChange.visibility = GONE
                view1.visibility = GONE

            }
        }

        cancel_action.setOnClickListener {

            dismiss()
        }

        textChange.setOnClickListener {

            chatViewModel.changeAgent(UserInfo(context).getUserToken(), requestId)
            chatViewModel.changeAgentMutableLiveData.observe(CONTEXT as AppCompatActivity, Observer {

                val b = Bundle()
                b.putString("requestId", requestId.toString())
                b.putString("userId", userId.toString())
                view.findNavController().navigate(R.id.waitOffersFragment, b)
                dismiss()

            })

        }

        val date2 = Date(System.currentTimeMillis())

        if (date != null) {
            textChange.visibility = GONE
            view1.visibility = GONE
            textCancelOrder.text = context.getString(R.string.withdraw_order)
        }

        textCancelOrder.setOnClickListener {

            val dialog = OrderOutDialog(context, chatViewModel, requestId, userId, driver_id)
            val window: Window = dialog.window!!
            val wlp: WindowManager.LayoutParams = window.attributes
            wlp.gravity = Gravity.BOTTOM
            wlp.flags = wlp.flags and WindowManager.LayoutParams.FLAG_DIM_BEHIND.inv()
            window.attributes = wlp
            dialog.show()
            dismiss()
        }

        textShare.setOnClickListener {

            val mandowpData = chatViewModel.showUserOfOrderMutableLiveData.value!!.data[0]

            intent = Intent(context, TrackingActivity::class.java)
            intent.putExtra(TrackingActivity.DRIVER_NAME, mandowpData.first_name)
            intent.putExtra(TrackingActivity.DRIVER_ID, mandowpData.id.toString())
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)

            dismiss()

        }

        textComplaint.setOnClickListener {
            FirebaseFirestore.getInstance()
                    .collection(ApiCreater.OrdersCollection)
                    .document(requestId.toString())
                    .get().addOnSuccessListener { value->

                        try {

                            if (value!![ApiCreater.ProblemUploadedForUser].toString().toBoolean()) {

                                Toast.makeText(context, context.getString(R.string.have_problem), Toast.LENGTH_SHORT).show()
                                dismiss()

                            } else {
                                intent.putExtra(SubMainActivity.KEY, SubMainActivity.POSTPROBLEM)
                                intent.putExtra(PostProblemFragment.USER_ID, userId.toString())
                                intent.putExtra(PostProblemFragment.ORDER_ID, requestId.toString())
                                context.startActivity(intent)

                                dismiss()

                            }

                        } catch (e: Exception) {

                            intent.putExtra(SubMainActivity.KEY, SubMainActivity.POSTPROBLEM)
                            intent.putExtra(PostProblemFragment.USER_ID, userId.toString())
                            intent.putExtra(PostProblemFragment.ORDER_ID, requestId.toString())
                            context.startActivity(intent)

                            dismiss()

                        }


                    }


        }
    }
}
