package com.codecaique.deliverymasr.ui.client.shopsNotSupported.shopLocation

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.annotation.DrawableRes
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.databinding.FragmentShopLoationBinding
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.client.shopsNotSupported.details.ShopOrderDetailsFragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.codecaique.deliverymasr.pojo.util.MyUtil
import com.google.firebase.crashlytics.FirebaseCrashlytics
import java.util.*
import kotlin.collections.ArrayList


class ShopLocationFragment : Fragment(), OnMapReadyCallback {

    private val TAG = "ShopLocationFragment"

    private var shopName = ""
    private var shopStreet = ""
    private var shopLat: String? = null
    private var shopLng: String? = null
    private lateinit var binding: FragmentShopLoationBinding
    private lateinit var userInfo: UserInfo
    private lateinit var mMap: GoogleMap
    private lateinit var myLocation: LatLng
    private lateinit var shopLocation: LatLng

    companion object {

        const val SHOP_LATITUDE = "param1"
        const val SHOP_LONGITUDE = "param2"
        const val SHOP_NAME = "param3"

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_shop_loation, container, false)

        return binding.root

    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.N)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        try {
            onBackPressedCallback()

            activity!!.intent!!.extras!!.let {
                shopLat = it.getString(SHOP_LATITUDE)
                shopLng = it.getString(SHOP_LONGITUDE)
                shopName = it.getString(SHOP_NAME)!!
            }

            userInfo = UserInfo(activity!!)
            myLocation = userInfo.getMyLocation()
            shopLocation = LatLng(shopLat!!.toDouble(), shopLng!!.toDouble())
            binding.tvName.text = shopName


            val mapFragment = //(context!! as AppCompatActivity).supportFragmentManager
                    childFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment
            mapFragment.getMapAsync(this)

            val myLoc = Location("")
            myLoc.longitude = myLocation.longitude
            myLoc.latitude = myLocation.latitude

            val shopLoc = Location("")
            shopLoc.longitude = shopLocation.longitude
            shopLoc.latitude = shopLocation.latitude

            binding.tvDistance.text = "${MyUtil.calculateDistanceByKM(myLoc, shopLoc)} ${getString(R.string.km)}"


            binding.btn.setOnClickListener {

                val bundle = Bundle()
                bundle.putString(ShopOrderDetailsFragment.SHOP_NAME, shopName)
                bundle.putString(ShopOrderDetailsFragment.SHOP_LATITUDE, shopLat)
                bundle.putString(ShopOrderDetailsFragment.SHOP_LONGITUDE, shopLng)
                bundle.putString(ShopOrderDetailsFragment.SHOP_STREET, shopStreet)

                it.findNavController().navigate(R.id.shopOrderDetailsFragment, bundle)

            }
        } catch (e: Exception) {

            FirebaseCrashlytics.getInstance().recordException(e)

        }
    }

    private fun onBackPressedCallback() {

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                // Handle the back button event
                requireActivity().finish()
            }
        }

        try {
            requireActivity().onBackPressedDispatcher.addCallback(this, callback)
        } catch (e: Exception) {

            FirebaseCrashlytics.getInstance().recordException(e)

        }
    }


    override fun onMapReady(googleMap: GoogleMap?) {

        mMap = googleMap!!
        binding.progressBar.visibility = View.GONE
        mMap.mapType = GoogleMap.MAP_TYPE_NORMAL

        val geocode = Geocoder(activity!!, Locale("ar"))//.getDefault())
        val addresses: List<Address>?
        myLocation = userInfo.getMyLocation()

        addresses = geocode.getFromLocation(shopLocation.latitude, shopLocation.longitude, 1)

        val myMarker = MarkerOptions()
                .position(myLocation)
                .icon(bitmapDescriptorFromVector(R.drawable.ic_dog_house))
                .title(getString(R.string.you))

        mMap.addMarker(myMarker).showInfoWindow()
//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 15f))

        val shopMarker = MarkerOptions()
                .position(shopLocation)
                .icon(bitmapDescriptorFromVector(R.drawable.ic_fork_and_knife_thin_outline))
                .title(shopName)

        mMap.addMarker(shopMarker).showInfoWindow()


        if (addresses != null && addresses.isNotEmpty()) {

            shopStreet = addresses[0].getAddressLine(0)//thoroughfare
            binding.tvPlace.text = shopStreet

        }

        val markers = ArrayList<MarkerOptions>()
        markers.add(myMarker)
        markers.add(shopMarker)

        val b = LatLngBounds.Builder()
        for (m in markers) {
            b.include(m.position)
        }
        try {

            val bounds = b.build()

            mMap.setOnMapLoadedCallback {
                mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 30))
            }

        } catch (e: Exception) {
            Log.e(TAG, "onMapReady: ${e.message}")
        }

    }

    private fun bitmapDescriptorFromVector(@DrawableRes vectorDrawableResourceId: Int): BitmapDescriptor? {
        val vectorDrawable = ContextCompat.getDrawable(activity!!, vectorDrawableResourceId)
        vectorDrawable!!.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
        val bitmap = Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}