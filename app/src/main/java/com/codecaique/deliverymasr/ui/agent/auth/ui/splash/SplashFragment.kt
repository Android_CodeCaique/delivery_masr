package com.codecaique.deliverymasr.ui.agent.auth.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.StoresMainActivity
import com.codecaique.deliverymasr.base.client.MainActivity
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.client.stores.StoresViewModel
import com.codecaique.deliverymasr.utils.Utils

/**
 * A simple [Fragment] subclass.
 */



class SplashFragment : Fragment() {

    lateinit var storesViewModel: StoresViewModel

    companion object{
        private const val TAG="SplashFragment"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_splash, container, false)

        val token = UserInfo(context!!.applicationContext).getUserToken()


        storesViewModel = ViewModelProviders.of(this).get(StoresViewModel::class.java)


        storesViewModel.getNearStoresAndCategories(token)




        Handler().postDelayed({
            try {


                if (UserInfo(activity!!.applicationContext).getUserToken().isNotEmpty()) {
                    storesViewModel.nearStoresMutableLiveData.observe(this, Observer {
                        if (it.error == 3) {
                            Utils.signout(requireActivity() , view)
                        } else {
                            if (UserInfo(activity!!.applicationContext).getState().contains("user")) {
                                val intent = Intent(activity!!, MainActivity::class.java)
                                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                                activity!!.startActivity(intent)
                            } else {
                                val intent = Intent(activity!!, StoresMainActivity::class.java)
                                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                                activity!!.startActivity(intent)

                            }
                        }
                    })


                } else {
                    view.findNavController().navigate(R.id.selestedFragment)
                }
            } catch (e: Exception) {

                Log.e(TAG, "onCreateView: ", e)

            }

        }, 3000)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}