package com.codecaique.deliverymasr.ui.client.stores

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codecaique.deliverymasr.pojo.response.*
import com.codecaique.deliverymasr.data.ApiCreater
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class StoresViewModel : ViewModel() {

    val nearStoresMutableLiveData = MutableLiveData<ShowAllNearShopsAndCategoriesResponse>()
    val shopsMutableLiveData = MutableLiveData<ShowShopsResponse>()
    val nearPlaces_Response = MutableLiveData<Places_Response>()
    val nearPlaces_ResponseCat = MutableLiveData<Places_Response>()
    val nearPlaces_ResponseCat2 = MutableLiveData<Places_Response>()
//    val nearPlaces_Response = MutableLiveData<Places_Response>()

    var error :Int = 0


    fun getNearStoresAndCategories(token: String) {

        val observable =
                ApiCreater.instance.showAllNearShops(token)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<ShowAllNearShopsAndCategoriesResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: ShowAllNearShopsAndCategoriesResponse) {

                Log.e("ViewModel Message ", t.error.toString() + t.message  + "     ")


                error = t.error

                Log.e("ViewModel listValue", nearStoresMutableLiveData.value.toString() + "     ")

                try {
                    nearStoresMutableLiveData.value = t
                }catch (e:Exception)
                {

                }

            }

            override fun onError(e: Throwable) {
                Log.e("Error message stores", e.message)
            }
        }

        observable.subscribe(observer)

    }

    fun getNearByStore(location:String) {

        val observable =
                ApiCreater.instance.showAllNearByShops(location,"restaurant")
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<Places_Response> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: Places_Response) {

                try {
                    nearPlaces_Response.value = t
                }catch (e:Exception)
                {
                    Log.i("Exception Stores",e.message)
                }
            }

            override fun onError(e: Throwable) {
                Log.e("Error message stores", e.message)
            }
        }

        observable.subscribe(observer)

    }


    fun getNearByStoreAndCat(location:String,cat:String) {

        val observable =
                ApiCreater.instance.showAllNearByShopsByCat(location,cat)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<Places_Response> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: Places_Response) {

                try {
                    nearPlaces_ResponseCat.value = t
                }catch (e:Exception)
                {
                    Log.i("Exception Stores",e.message)
                }
            }

            override fun onError(e: Throwable) {
                Log.e("Error message stores", e.message)
            }
        }

        observable.subscribe(observer)

    }


    fun getNearByStoreAndCat2(location:String,cat:String) {

        val observable =
                ApiCreater.instance.showAllNearByShopsByCat(location,cat)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<Places_Response> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: Places_Response) {

                try {
                    nearPlaces_ResponseCat2.value = t
                }catch (e:Exception)
                {
                    Log.i("Exception Stores",e.message)
                }
            }

            override fun onError(e: Throwable) {
                Log.e("Error message stores", e.message)
            }
        }

        observable.subscribe(observer)

    }


    fun getNearStoresAndCategories(token: String,category: String) {

        val observable =
                ApiCreater.instance.showAllNearShops(token,category)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())

        Log.e("Get BY" , "DONE")

        val observer = object : Observer<ShowAllNearShopsAndCategoriesResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: ShowAllNearShopsAndCategoriesResponse) {

                try {
                    nearStoresMutableLiveData.value = t
                }catch (e:Exception)
                {
                    Log.e("Exception Stores",e.message)
                }
            }

            override fun onError(e: Throwable) {
                Log.e("Error message stores", e.message)
            }
        }

        observable.subscribe(observer)

    }


    fun getShops(token: String) {

        val observable =
                ApiCreater.instance.showShops(token)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<ShowShopsResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: ShowShopsResponse) {

                try{
                    shopsMutableLiveData.value = t
                }catch (e:Exception)
                {
                    Log.i("Exception 222",e.message)
                }
            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }

}