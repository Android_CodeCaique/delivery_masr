package com.codecaique.deliverymasr.ui.client.shakawy

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.client.MainActivity
import com.codecaique.deliverymasr.ui.client.stores.adapters.StoresAdapter


class ShakawyFragment : Fragment() {
    var rv_shakawy: RecyclerView? = null
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_shakawy, container, false)

        rv_shakawy = view.findViewById(R.id.rv_shakawy)
        val linearLayoutManager = LinearLayoutManager(view.context, RecyclerView.VERTICAL, false)
        val shakawyRecyclerViewAdabter = ShakawyRecyclerViewAdabter(view.context)
        val iv_backShakawy = view.findViewById<ImageView>(R.id.iv_backShakawy)
        iv_backShakawy.setOnClickListener { //                SittingsFrag sittingsFrag=new SittingsFrag();
//                FragmentManager manager=getFragmentManager();
//                manager.beginTransaction().replace(R.id.main_fragment,sittingsFrag,sittingsFrag.getTag()).commit();
            val intent = Intent(context, MainActivity::class.java)
            StoresAdapter.fragName = R.layout.fragment_sittings
            startActivity(intent)
        }



/*        val recorder = MediaRecorder()
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC)
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
        recorder.setOutputFile("path")
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC)*/  //        for mp3 audio
        //            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);         for mp3 audio
//        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)
//        recorder.prepare()
//        recorder.start()
//        val file = File(recorder.metrics.toString())
//        val requestFile = RequestBody.create(MediaType.parse("audio/*"), file)
//        val a= MultipartBody.Part.createFormData("KEY", "file.name", requestFile)



        return view
    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}