package com.codecaique.deliverymasr.ui.agent.auth.ui.conversion

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import kotlinx.android.synthetic.main.fragment_conversion.view.*

/**
 * A simple [Fragment] subclass.
 */
class ConversionFragment : Fragment() {

    companion object {
        val ISCONV = "isconv"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_conversion, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.btn.setOnClickListener {

            /*val b=Bundle()
            b.putBoolean(ISCONV,true)
            it.findNavController().navigate(R.id.signInConfirmFragment,b)*/

            view.constraint1.visibility = View.VISIBLE
            view.constraint2.visibility = View.INVISIBLE
        }

        view.buttonConfirm.setOnClickListener {

            it.findNavController().navigate(R.id.bankAccountFragment)

        }

    }

    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}