package com.codecaique.deliverymasr.ui.agent.auth.ui.otp

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.StoresMainActivity
import com.codecaique.deliverymasr.databinding.FragmentSignInConfirmBinding
import com.codecaique.deliverymasr.pojo.response.Driver_Response
import com.codecaique.deliverymasr.pojo.response.Phone
import com.codecaique.deliverymasr.pojo.response.PhoneResponse
import com.codecaique.deliverymasr.pojo.response.RegistrationResponse
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.agent.auth.ui.addMandop.SuccDialog

/**
 * A simple [Fragment] subclass.
 */
class OtpFragment : Fragment() {


    lateinit var binding: FragmentSignInConfirmBinding
    lateinit var viewModel: OtpViewModel
    private lateinit var data: Any
    private var key = "key"

    companion object {

        const val DATA = "data"
        private const val TAG = "OtpFragment"
        private const val NEW = "new"
        private const val OLD = "old"

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_in_confirm, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(OtpViewModel::class.java)

        args()

        binding.tvNoMessage.setOnClickListener {
            args()
        }

        binding.buttonConfirm.setOnClickListener {

            if (binding.otpView.text.toString().isNotEmpty() && binding.otpView.text.toString().length == 6) {

                Log.e(TAG, "onViewCreated: viewModel.otpCodeMutableLiveData.value = "+viewModel.otpCodeMutableLiveData.value.toString())
                Log.e(TAG, "onViewCreated: binding.otpView.text.toString() = "+binding.otpView.text.toString())

                if (viewModel.otpCodeMutableLiveData?.value == binding.otpView?.text?.toString()) {

                    when (key) {
                        OLD -> {
                            UserInfo(requireContext()).setUserData(data as Phone)
                            val intent = Intent(context, StoresMainActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                            requireContext().startActivity(intent)
                        }
                        NEW -> {
                            UserInfo(activity!!.applicationContext).setCaptainData(data as Driver_Response.DataBean)
                            val succDialog = SuccDialog(activity!!)
                            succDialog.show()
                        }
                    }

                } else {
                    Toast.makeText(requireContext(), getString(R.string.otp_code_fialde), Toast.LENGTH_SHORT).show()
                }

            } else {
                Toast.makeText(requireContext(), getString(R.string.enter_otp), Toast.LENGTH_SHORT).show()
            }

        }


    }

    private fun args() {

        data = requireArguments().getSerializable(DATA)!!
        UserInfo(activity!!.applicationContext).setCaptainData(data as Driver_Response.DataBean)

        when (data) {
            is Driver_Response.DataBean -> {

                Log.e(TAG, "args: 1")

                viewModel.otp(binding, requireActivity(), (data as Driver_Response.DataBean).phone!!.toString())
                key = NEW

            }
            is RegistrationResponse -> {
                Log.e(TAG, "args: 2")
            }
            is Phone -> {
                Log.e(TAG, "args: 3")
                viewModel.otp(binding, requireActivity(), (data as Phone).phone)
                key = OLD

            }
        }

    }

    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}
