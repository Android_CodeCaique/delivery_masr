package com.codecaique.deliverymasr.ui.agent.auth.ui.signIn

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.telephony.SmsManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.StoresMainActivity
import com.codecaique.deliverymasr.databinding.FragmentSignInBinding
import com.codecaique.deliverymasr.pojo.response.Phone
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.client.signUp.SignUpViewModel
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.fragment_sign_in.view.*
import okhttp3.MediaType
import okhttp3.RequestBody
import java.util.*


class SignInFragment : Fragment() {


    private lateinit var animation: Animation
    lateinit var signUpViewModel: SignUpViewModel
    var isSigned = false
    var phone = ""
    lateinit var binding: FragmentSignInBinding
    var code = ""
    lateinit var data: Phone

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_in, container, false)
        return binding.root

    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


//        if (BuildConfig.DEBUG) {
//            view.et_phone.setText("01234567895")
//            view.spinner.setSelection(0)
//        }

        view.logo.setImageResource(R.drawable.ic_mandob)
        view.logo_.setImageResource(R.drawable.ic_mandob)
        animation = AnimationUtils.loadAnimation(context, R.anim.anim_trans)

        view.tv_1.visibility = View.INVISIBLE
        view.tv_2.visibility = View.INVISIBLE
//        view.tv_3.visibility = View.INVISIBLE
        view.tv_4.visibility = View.INVISIBLE
        view.et_phone.visibility = View.INVISIBLE
        view.spinner.visibility = View.INVISIBLE
        view.view.visibility = View.INVISIBLE
        view.tv_phone.visibility = View.INVISIBLE

        view.logo.startAnimation(animation)

        UserInfo(activity!!.applicationContext).setState("mandop")

        Handler().postDelayed({

            startAnim(view)

        }, 500)




        view.buttonConfirm_.setOnClickListener {

            val confirm = view.et_code.text.toString()

            UserInfo(activity!!.applicationContext).setState("mandop")

            if (confirm.isNotEmpty() && confirm.length == 4) {

                if (confirm == code) {

                    if (isSigned) {
                        val intent = Intent(activity!!, StoresMainActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                        activity!!.startActivity(intent)

//                        activity!!.startActivity(Intent(activity!!.applicationContext, StoresMainActivity::class.java))
                        UserInfo(activity!!.applicationContext).setUserData(data)
                    } else {
                        val b = Bundle()
                        b.putString("code", view.tv_phone.text.toString())
                        b.putString("phone", phone)
                        view.findNavController().navigate(R.id.addMandopFragment, b)
                    }

                } else {
                    view.et_code.error = "من فضلك اكتب الكود صحيحا"
                }

            } else {
                view.et_code.error = "من فضلك اكتب الكود صحيحا"

            }
        }

        signUpViewModel = ViewModelProviders.of(this).get(SignUpViewModel::class.java)


        val spinnerAdapter = ArrayAdapter(
                activity!!.applicationContext,
                R.layout.spinner_item, R.id.text,
                ArrayList<String>(signUpViewModel.getCountries().keys)
        )

        view.spinner.adapter = spinnerAdapter

        view.spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                view.tv_phone.text = signUpViewModel.getCountries()["مصر"]
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                view.tv_phone.text = signUpViewModel.getCountries()[view.spinner.selectedItem.toString()]
            }
        }

        view.buttonConfirm.setOnClickListener {

            phone = view.et_phone.text.toString()
//            val country = view.spinner.selectedItem.toString()

            if (phone.isNotEmpty() && phone.length > 6) {

                view.buttonConfirm.isEnabled = false
                view.progress_bar.visibility = View.VISIBLE

                val r = Random()
                code = String.format(Locale("en"), "%04d", Integer.valueOf(r.nextInt(9001)) as Any?)
                val smsManager = SmsManager.getDefault()

                Log.e("code", code)

                signUpViewModel.phoneSignUpClient(
                        RequestBody.create(MediaType.parse("multipart/form-data"), binding.spinner.selectedItem.toString()),
                        RequestBody.create(MediaType.parse("multipart/form-data"), binding.tvPhone.text.toString() + phone),
                        RequestBody.create(MediaType.parse("multipart/form-data"),
                                FirebaseInstanceId.getInstance().token),
                        view.progress_bar, context!!,view
                )

                signUpViewModel.signUpMutableLiveData.observe(this, Observer { t ->

                    if (t.error == 0) {
                        Log.e("tag", t.data.id.toString() + " " + t.data.phone + " " + t.data.state + " " + t.data.user_token)
                        UserInfo(activity!!.applicationContext).setUserData(t.data.id, t.data.phone, "mandop", "")

//                        smsManager.sendTextMessage(phone, null, code, null, null);
                        val b = Bundle()
                        b.putString("phone", phone)
                        b.putString("code", view.tv_phone.text.toString())

                        view.findNavController().navigate(R.id.addMandopFragment, b)

                        Log.e("MOB USER", "NEW PHONE")


                    } else {
                        if (t.data.state.contains("driver")) {
                            view.tv_1_.text = getString(R.string.code_sent_1) + phone + getString(R.string.pls_enter_sent_code)
                            UserInfo(activity!!.applicationContext).setUserData(t.data)
                            val intent = Intent(activity!!, StoresMainActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                            activity!!.startActivity(intent)

                            isSigned = true
                            phone = view.tv_phone.text.toString() + phone
                            data = t.data

                            view.buttonConfirm.isEnabled = true

                        } else {
                            changeToDriverOrNo(phone)
                            Toast.makeText(activity!!.applicationContext, getString(R.string.not_driver_account), Toast.LENGTH_LONG).show()
                            view.buttonConfirm.isEnabled = true

                        }

//                        view.constraint1.visibility = View.VISIBLE
//                        view.constraint2.visibility = View.INVISIBLE

//                        navController.navigate(R.id.action_fragment_Mobile_Verification2_to_fragment_SignUp)
//                    } else {
                        /* if (t.data.state.isNotEmpty()) {
                             phone = view.tv_phone.text.toString() + phone
                             data=t.data
                         }*/
                        /* else {
//                            Toast.makeText(activity!!.applicationContext, "هذا الحساب ليس لمندوب", Toast.LENGTH_LONG).show()
                        }*/

                        phone = view.tv_phone.text.toString() + phone
                        data = t.data

                        view.buttonConfirm.isEnabled = true
                    }
                    view.progress_bar.visibility = View.GONE
//                    view.constraint1.visibility = View.VISIBLE
//                    view.constraint2.visibility = View.INVISIBLE
                    UserInfo(activity!!.applicationContext).setState("mandop")

                })

            } else {

                view.et_phone.error = getString(R.string.please_enter_phone)

            }


        }


    }

    private fun startAnim(view: View) {

        view.tv_1.visibility = View.VISIBLE
        view.tv_2.visibility = View.VISIBLE
        view.tv_4.visibility = View.VISIBLE
        view.et_phone.visibility = View.VISIBLE
        view.spinner.visibility = View.VISIBLE
//        view.tv_3.visibility = View.VISIBLE
        view.view.visibility = View.VISIBLE
        view.tv_phone.visibility = View.VISIBLE
        animation = AnimationUtils.loadAnimation(context, R.anim.anim_scale)
        animation.duration = 800

        view.tv_1.startAnimation(animation)
        view.tv_2.startAnimation(animation)
        view.tv_4.startAnimation(animation)
        view.et_phone.startAnimation(animation)
//        view.tv_3.startAnimation(animation)
        view.spinner.startAnimation(animation)
        view.view.startAnimation(animation)
        view.tv_phone.startAnimation(animation)

    }

    private fun changeToDriverOrNo(phone: String) {
        val alertDialog = AlertDialog.Builder(activity!!,R.style.AlertDialogTheme)
        alertDialog.setMessage(getString(R.string.not_driver_account))
        alertDialog.setIcon(R.drawable.app_logo)

        alertDialog.setPositiveButton(getString(R.string.yes)) { p0, p1 ->

            val b = Bundle()
            b.putString("phone", phone)
            b.putString("code", view!!.tv_phone.text.toString())
            view!!.findNavController().navigate(R.id.addMandopFragment, b)


        }

        alertDialog.setNegativeButton(getString(R.string.no)) { p0, p1 ->
            p0!!.dismiss()
        }

        alertDialog.create().show()


    }

    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}