package com.codecaique.deliverymasr.ui.client.changeAgent

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.client.SubActivity
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.pojo.response.Offer
import com.codecaique.deliverymasr.ui.client.confirmOrders.ConfirmMyOrder
import com.codecaique.deliverymasr.ui.client.deliveryOffers.OffersViewModel
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.dialog_change_agent.*
import java.text.DecimalFormat
class NewOfferDialog(var CONTEXT: Context, var offersViewModel: OffersViewModel, var requestId: Int, var userId: Int, val offer: Offer) : Dialog(CONTEXT) {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_change_agent)

        //  val intent= Intent(context, SubMainActivity::class.java)


        if (offer.image != null && offer.image.isNotEmpty())
            Picasso.get().load(ApiCreater.USER_URL + offer.image).into(profile_image)

        tvCost.text = offer.price + "رس"
        tvTime.text = offer.delivery_time
        tv_userName.text = offer.first_name + " " + offer.last_name
        textDescription.text = offer.rate

        val formatter = DecimalFormat("00.0")

        tvAgentDistance.text = formatter.format(offer.agent_Recivingdistance.toDouble()) + " كم"
        tvClientDistance.text = formatter.format(offer.client_Recivingdistance.toDouble()) + " كم"
        tvDistance.text = formatter.format(offer.away_distance.toDouble()) + " كم"


        try {
            rate.rating = offer.rate.toFloat()
        } catch (e: Exception) {
            rate.rating = 0f
        }

        btn_Accept.setOnClickListener { view ->


            val loadingDialog = LoadingDialog(context)
            loadingDialog.show()

            var offer_id = "";
            if(offer.offer_id == null || offer.offer_id == "null"){
                offer_id = offer.id
            }else{
                offer_id = offer.offer_id
            }
            offersViewModel.acceptOffer(UserInfo(context).getUserToken(), offer_id,context)

            offersViewModel.acceptMutableLiveData.observe(CONTEXT as AppCompatActivity, Observer { _ ->

                loadingDialog.dismiss()

                val intent = Intent(view.context, SubActivity::class.java)

                intent.flags=Intent.FLAG_ACTIVITY_NEW_TASK
                //StoresAdapter.fragName = R.layout.fragment_confirm_my_order
                intent.putExtra(SubActivity.KEY, SubActivity.ChatOrder)
                intent.putExtra(ConfirmMyOrder.REQUEST_ID, offer.request_id)
                intent.putExtra(ConfirmMyOrder.USER_ID, offer.delivery_id)



                var offer_id = "";
                if(offer.offer_id == null || offer.offer_id == "null"){
                    offer_id = offer.id
                }else{
                    offer_id = offer.offer_id
                }

                Log.e("dada","arrayItem.offer_id ${offer_id} arrayItem.delivery_id ${offer.delivery_id}")

                context.startActivity(intent)


            })


        }






    }
}