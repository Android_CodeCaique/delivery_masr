package com.codecaique.deliverymasr.ui.client.distance

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.data.ApiCreater
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_far_from_you.view.*
import java.text.DecimalFormat

/**
 * A simple [Fragment] subclass.
 */
class FarFromYouFrag : Fragment(), OnMapReadyCallback,FragmentManager.OnBackStackChangedListener {

    private lateinit var mMap: GoogleMap
    var deliveryImage = ""
    var re_long = ""
    var re_lat = ""
    var distance = ""
    var shopName = ""
    var requestId = ""
    var client_lat = ""
    var client_long = ""
    var type = ""
    lateinit var VIEW: View

    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        VIEW = inflater.inflate(R.layout.fragment_far_from_you, container, false)

        deliveryImage = arguments!!.getString("deliveryImage")!!
        re_lat = arguments!!.getString("re_lat")!!
        re_long = arguments!!.getString("re_long")!!
        client_lat = arguments!!.getString("client_lat")!!
        client_long = arguments!!.getString("client_long")!!
        shopName = arguments!!.getString("shopName")!!
        distance = arguments!!.getString("distance")!!
        requestId = arguments!!.getString("requestId")!!
        type = arguments!!.getString("type")!!


        //client_long   client_lat
        val mapFragment = //(context!! as AppCompatActivity).supportFragmentManager
                childFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment
        mapFragment.getMapAsync(this)

        val formater = DecimalFormat("00.0")

        VIEW.state.text = requestId
        VIEW.tard.text = shopName
        try {

            VIEW.farFromYou.text = "يبعد عنكم ${formater.format(distance)} كم "
        } catch (e: Exception) {
        }

        if (deliveryImage.isNotEmpty())
            Picasso.get().load(ApiCreater.USER_URL + deliveryImage).into(VIEW.profile)

        VIEW.tv_backChat.setOnClickListener {

            it.findNavController().popBackStack()

            /*

            val chatFrag = ChatFragment()
            val manager = fragmentManager
            manager!!.beginTransaction().replace(R.id.main3_fragment, chatFrag, chatFrag.tag).commit()
*/

        }


        return VIEW
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true/*  enabled by default*/) {
            override fun handleOnBackPressed() {
                // Handle the back button event
                view.findNavController().popBackStack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)

    }



    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        deliveryImage = arguments!!.getString("deliveryImage")!!
        re_lat = arguments!!.getString("re_lat")!!
        re_long = arguments!!.getString("re_long")!!
        shopName = arguments!!.getString("shopName")!!
        distance = arguments!!.getString("distance")!!
        requestId = arguments!!.getString("requestId")!!
        client_lat = arguments!!.getString("client_lat")!!
        client_long = arguments!!.getString("client_long")!!

        try {

            // Add a marker in Sydney and move the camera
            if (type == "receive")
            {
                var marker = LatLng(client_lat.toDouble(), client_long.toDouble())
                mMap.addMarker(MarkerOptions().position(marker).icon(BitmapDescriptorFactory.fromResource(R.drawable.group_3583)))
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(marker,15f))
            }
            if (type == "deliver"){
                var marker = LatLng(re_lat.toDouble(), re_long.toDouble())
                mMap.addMarker(MarkerOptions().position(marker).icon(BitmapDescriptorFactory.fromResource(R.drawable.group_3581)))
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(marker,15f))

            }
        } catch (e: Exception) {
        }

    }

    override fun onBackStackChanged() {
        VIEW.findNavController().popBackStack()
    }


    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}