package com.codecaique.deliverymasr.ui.client.bankAccount;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codecaique.deliverymasr.R;
import com.codecaique.deliverymasr.base.client.SubActivity;

import static com.codecaique.deliverymasr.ui.client.stores.adapters.StoresAdapter.fragName;



public class BankAccountRecyclerViewAdabter extends RecyclerView.Adapter<ViewHolderAccount> {
    public BankAccountRecyclerViewAdabter(Context context) {
        this.context = context;
    }


    View view;
    Context context;
    @NonNull
    @Override
    public ViewHolderAccount onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(context).inflate(R.layout.item_bank_accounts,parent,false);
        ViewHolderAccount viewHolderDelivery = new ViewHolderAccount(view);
        return viewHolderDelivery;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderAccount holder, int position) {
//        holder.tvUserName.setText("***سع");
//        holder.tvOrderName.setText("قهوة حجم كبير");
        holder.btn_Edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (holder.getPosition() == 0) {
                    Intent intent = new Intent(holder.itemView.getContext(), SubActivity.class);
                    fragName = R.layout.fragment_user_notes4;
                    context.startActivity(intent);
                }
                if (holder.getPosition() == 1) {
                    Intent intent = new Intent(holder.itemView.getContext(), SubActivity.class);
                    fragName = R.layout.fragment_bank_account_data;
                    context.startActivity(intent);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return 3;
    }
}

class ViewHolderAccount extends RecyclerView.ViewHolder{

    Button btn_Edit;
    TextView tvOrderName;
    public ViewHolderAccount(@NonNull View itemView) {
        super(itemView);
        btn_Edit = itemView.findViewById(R.id.btn_Edit);
//        tvOrderName = itemView.findViewById(R.id.tv_OrderName);
    }
}