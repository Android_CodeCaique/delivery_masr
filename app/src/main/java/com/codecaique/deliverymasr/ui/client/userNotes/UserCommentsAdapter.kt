package com.codecaique.deliverymasr.ui.client.userNotes

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.response.UserRate
import com.codecaique.deliverymasr.data.ApiCreater
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import java.text.SimpleDateFormat

class UserCommentsAdapter(var context: Context) : RecyclerView.Adapter<UserCommentsAdapter.ViewHolderNotes>() {


    var list = ArrayList<UserRate>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserCommentsAdapter.ViewHolderNotes {

        var view: View? = null
        view = LayoutInflater.from(context).inflate(R.layout.item_comment, parent, false)
        return UserCommentsAdapter.ViewHolderNotes(view!!)

    }

    @SuppressLint("SimpleDateFormat")
    override fun onBindViewHolder(holder: ViewHolderNotes, position: Int) {

        val arrayItem = list[position]

        var name = ""

        try {
            if (arrayItem.first_name.length > 3) {

                for (i in arrayItem.first_name.indices) {
                    if (i < 3)
                        name += arrayItem.first_name[i]
                    else
                        name += "*"

                }

            } else
                name = arrayItem.first_name

        } catch (e: Exception) {
        }

        holder.tvUserName.text = name
        holder.tvNote.text = arrayItem.comment

        if (arrayItem.image != null && arrayItem.image.isNotEmpty())
            Picasso.get().load(ApiCreater.USER_URL + arrayItem.image).into(holder.image)

        if (arrayItem.rate.isNotEmpty())
            holder.rating.rating = arrayItem.rate.toFloat()

        val dateFormatter =
                SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val date = dateFormatter.parse(arrayItem.created_at).time

        val timeFormatter =
                SimpleDateFormat("HH:mm a")
        val displayValue = timeFormatter.format(date)

        holder.tvTime.text = displayValue


//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(holder.itemView.getContext(), DelievryPlaceActivity2.class);
//                view.getContext().startActivity(intent);
//            }
//        });

    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setData(a: ArrayList<UserRate>) {
        this.list = a
        notifyDataSetChanged()
    }

    class ViewHolderNotes(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvTime: TextView = itemView.findViewById(R.id.time)
        var tvUserName: TextView = itemView.findViewById(R.id.user_namep)
        var tvNote: TextView = itemView.findViewById(R.id.problemText)
        var image: CircleImageView = itemView.findViewById(R.id.imageView)
        var rating: RatingBar = itemView.findViewById(R.id.ratingBar)

    }

}