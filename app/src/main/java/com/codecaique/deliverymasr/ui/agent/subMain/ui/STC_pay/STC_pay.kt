package com.codecaique.deliverymasr.ui.agent.subMain.ui.STC_pay

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.client.signUp.SignUpViewModel
import kotlinx.android.synthetic.main.activity_s_t_c__p_a_y.view.*
import okhttp3.MediaType
import okhttp3.RequestBody

class STC_pay : Fragment() {

    var accountNumber=""
    var accountId=""

    companion object{
        val AccountNumber="account_number"
        val AccountId="account_id"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return  inflater.inflate(R.layout.activity_s_t_c__p_a_y, container, false)
    }

    lateinit var stcViewModel: STCViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        stcViewModel=ViewModelProviders.of(this).get(STCViewModel::class.java)

        accountNumber=activity!!.intent.extras!!.getString(AccountNumber)!!
        accountId=activity!!.intent.extras!!.getString(AccountId)!!

        val codes=SignUpViewModel().getCountries()

        view.country_spinner.adapter=ArrayAdapter(activity!!,android.R.layout.simple_list_item_1,codes.keys as ArrayList<*> )

        view.country_spinner.onItemSelectedListener=object :AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {
                view.phone_code.text=codes[view.country_spinner.selectedItem.toString()]
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                view.phone_code.text=codes[view.country_spinner.selectedItem.toString()]
            }
        }

        view.mobile_verification_btn.setOnClickListener {

            val name=view.etName.text.toString()
            val phone=view.et_phone.text.toString()

            if (name.isNotEmpty() && phone.isNotEmpty())
            {
                stcViewModel.editAccount(
                        RequestBody.create(MediaType.parse("multipart/form-data"), UserInfo(activity!!.applicationContext).getUserToken()),
                        accountId.toInt(),
                        RequestBody.create(MediaType.parse("multipart/form-data"), name),
                        RequestBody.create(MediaType.parse("multipart/form-data"), ""),
                        RequestBody.create(MediaType.parse("multipart/form-data"), accountNumber),
                        RequestBody.create(MediaType.parse("multipart/form-data"), view.country_spinner.selectedItem.toString()),
                        RequestBody.create(MediaType.parse("multipart/form-data"), view.phone_code.text.toString()+phone)
                        )

                stcViewModel.editMutableLiveData.observe(this, Observer {

                    if (it.error==0){
                        Toast.makeText(context, getString(R.string.wel_come), Toast.LENGTH_LONG).show()
                        activity!!.finish()
                    }
                    else{
                        Toast.makeText(context, it.message, Toast.LENGTH_LONG).show()
                    }

                })

            }
            else{

                if (name.isEmpty())
                    view.etName.error=getString(R.string.sould_be_not_empty)
                if (phone.isEmpty())
                    view.et_phone.error=getString(R.string.sould_be_not_empty)

            }

        }


    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}