package com.codecaique.deliverymasr.ui.client.changeAgent

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.client.deliveryOffers.OffersViewModel
import kotlinx.android.synthetic.main.fragment_wait_offers.*

class WaitOffersFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_wait_offers, container, false)
    }

    lateinit var offersViewModel: OffersViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val requestId = arguments!!.getString("requestId")!!.toInt()
        val userId = arguments!!.getString("userId")!!.toInt()



        offersViewModel = ViewModelProviders.of(this).get(OffersViewModel::class.java)


        offersViewModel.getOffers(UserInfo(activity!!.applicationContext).getUserToken())
        offersViewModel.offersMutableLiveData.observe(this, Observer { t ->

            if (t.data.isNotEmpty()) {

                t.data.forEach {
                    val newOfferDialog = NewOfferDialog(activity!!, offersViewModel, requestId, userId, it)
                    newOfferDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                    newOfferDialog.show()

                }

                btn_close.visibility=View.GONE

            }
            else{

                btn_close.visibility=View.VISIBLE

            }
            progressBar3.visibility=View.GONE

        })

        btn_retry.setOnClickListener {
            progressBar3.visibility=View.VISIBLE
            btn_retry.visibility=View.GONE
            offersViewModel.getOffers(UserInfo(activity!!.applicationContext).getUserToken())


        }

        btn_close.setOnClickListener {
            it.findNavController().popBackStack()
        }

    }

    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}
