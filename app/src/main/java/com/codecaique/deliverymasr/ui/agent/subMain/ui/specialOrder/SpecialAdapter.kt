package com.codecaique.deliverymasr.ui.agent.subMain.ui.specialOrder

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R

class SpecialAdapter(private val context: Context, private val list: List<String>) : RecyclerView.Adapter<SpecialAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var root=itemView.findViewById(R.id.root) as ConstraintLayout

    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.item_special_order, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        /*viewHolder.root.setOnClickListener { view ->
            view.findNavController().navigate(R.id.orderChatFragment)
        }*/
    }

    override fun getItemCount(): Int {
        return list.size
    }

}