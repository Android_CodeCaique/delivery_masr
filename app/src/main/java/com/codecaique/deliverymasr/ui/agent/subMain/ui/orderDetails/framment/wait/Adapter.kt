package com.codecaique.deliverymasr.ui.agent.subMain.ui.orderDetails.framment.wait

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.response.Shop_Orders_Response
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.ui.agent.subMain.ui.orderChat.OrderChatFragment
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import java.text.DecimalFormat
import java.util.ArrayList

class Adapter(private val context: Context) :
        RecyclerView.Adapter<Adapter.ViewHolder>() {

    var waitOrder = ArrayList<Shop_Orders_Response.DataBean>()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var root = itemView.findViewById(R.id.root) as LinearLayout
        var img = itemView.findViewById(R.id.profile_image) as CircleImageView
        var name = itemView.findViewById(R.id.type) as TextView
        var desc = itemView.findViewById(R.id.textDescription) as TextView
        var time = itemView.findViewById(R.id.imageViewMarket) as TextView
        var tvDistance = itemView.findViewById(R.id.tv_distance) as TextView


    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.item_orders_waite, viewGroup, false)
        return ViewHolder(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        val arrayItem = waitOrder[i]

        if (arrayItem.image!!.isNotEmpty()) {
            Picasso.get().load(ApiCreater.USER_URL + arrayItem.image).into(viewHolder.img)
        }

        viewHolder.name.text = arrayItem.name

        viewHolder.desc.text = arrayItem.description


        val formater = DecimalFormat("00.0")

        viewHolder.tvDistance.text = formater.format(arrayItem.distance!!.toDouble()) + " كم "

        viewHolder.time.text = arrayItem.delivery_time


        viewHolder.root.setOnClickListener { view ->
            val bundle = Bundle()
            bundle.putString(OrderChatFragment.REQUEST_ID,arrayItem.id)
            bundle.putString(OrderChatFragment.USER_ID,arrayItem.user_id)
            view.findNavController().navigate(R.id.orderChatFragment,bundle)
        }
    }

    fun setList(list: ArrayList<Shop_Orders_Response.DataBean>) {
        this.waitOrder = list
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return waitOrder.size
    }


}