package com.codecaique.deliverymasr.ui.agent.subMain.ui.pushBill

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Activity.RESULT_CANCELED
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.response.Message
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.agent.subMain.ui.chat.ChatFragment
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.codecaique.deliverymasr.pojo.util.MyUtil
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.dialog_attach_photo.*
import kotlinx.android.synthetic.main.fragment_push_bill.*
import kotlinx.android.synthetic.main.fragment_push_bill.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.nio.ByteBuffer
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class PushBillFragment : Fragment() {

    lateinit var viewModel: BillViewModel
    lateinit var loadingDialog: LoadingDialog
    var uri: Uri? = null
    var uri2: String? = null
    val TAG = "PushBillFragment"

    var currentPhotoPath: String? = null
    var request_id: Int = 0
    var user_id: Int = 0

    lateinit var VIEW: View
    var total:Int=0

    private var dialog: Dialog? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        VIEW = inflater.inflate(R.layout.fragment_push_bill, container, false)

        return VIEW
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(BillViewModel::class.java)
        loadingDialog = LoadingDialog(activity!!)
        val token: String = UserInfo(activity!!).getUserToken()

        request_id = try {
            arguments!!.getString(ChatFragment.REQUEST_ID)!!.toInt()
        } catch (e: Exception) {
            activity!!.intent.extras!!.getString(ChatFragment.REQUEST_ID)!!.toInt()
        }

        user_id = try {
            arguments!!.getString(ChatFragment.USER_ID)!!.toInt()
        } catch (e: Exception) {
            activity!!.intent.extras!!.getString(ChatFragment.USER_ID)!!.toInt()
        }

        db = FirebaseFirestore.getInstance()
                .collection(ApiCreater.OrdersCollection)
                .document(request_id.toString())
                .collection(ApiCreater.ChatCollection)

        var costByTax = try {
            arguments!!.getString(ChatFragment.COST)!!.toString()
        } catch (e: Exception) {
            activity!!.intent.extras!!.getString(ChatFragment.COST)!!.toString()
        }
        var costdelivery = try {
            arguments!!.getString(ChatFragment.COST_DELIVERY)!!.toString()
        } catch (e: Exception) {
            activity!!.intent.extras!!.getString(ChatFragment.COST_DELIVERY)!!.toString()
        }

        Log.e(TAG, "onViewCreated: $request_id")

        view.layout_image.setOnClickListener {
            dialogGetPhoto(layout_image)
        }

        loadingDialog.show()

        viewModel.showUserTax(token)
        viewModel.userTaxMutableLiveData.observe(this, Observer { t ->

            if (t.error == 0) {

                costByTax = t.data.toString().toString()
                view.tv7.text = (costByTax.toInt() + costdelivery.toInt()).toString()
                view.tv8.text = (costByTax.toInt() + costdelivery.toInt()).toString()

            }

            loadingDialog.dismiss()

        })


        view.tv_camera.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

                if (!p0.isNullOrEmpty()) {
                    view.tv_sells.text = p0.toString()
                    view.tv8.text = (p0.toString().toDouble() + costByTax.toInt() + costdelivery.toInt() ).toString()+ " L.E "
                    total= (p0.toString().toInt() + costByTax.toInt() + costdelivery.toInt())
                }

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        view.btn_done.setOnClickListener {

            val price: String = view.tv_camera.text.toString()
            val totalPrice: String = view.tv8.text.toString()

            if (price.isNotEmpty()) {

                loadingDialog.show()
//                loadingDialog.setCancelable(false)
              //  view.btn_done.visibility = View.GONE
                view.progressBarBill.visibility = View.GONE

                Log.e("datadata", "$token, $request_id, $price")
                var price_to:Int
                price_to=price.toInt()
                viewModel.makeBill(token, request_id, price_to, total, getImage(), loadingDialog)

                viewModel.makeBillMutableLiveData.observe(viewLifecycleOwner, Observer {
                    if (it.error == 0) {


                        showBillData(token,request_id)

                    } else {
//                        loadingDialog.dismiss()
                        Toast.makeText(context, "ERROR" + it.message, LENGTH_SHORT).show()
                        loadingDialog.dismiss()
                        view.btn_done.visibility = View.VISIBLE
                        view.progressBarBill.visibility = View.GONE
                    }

                })


            } else {
                view.tv_camera.error = getString(R.string.sould_be_not_empty)
            }


        }


    }

    private fun showBillData(token: String, request_id: Int) {

        viewModel.showBillData(token, request_id)
        viewModel.billMutableLiveData.observe(this, Observer {
            loadingDialog.dismiss()

            if (it.error == 0) {
                view!!.tv_camera.setText(it.data[0].order_price.toString())
               // Picasso.get().load(ApiCreater.BILLS + it.data.image).into(view!!.image_bill)
                view!!.image_.visibility = View.GONE
                view!!.tv_add_more.visibility = View.GONE

                val message = Message()
                message.message = ApiCreater.OrderMessages.makeBillMessage(
                        it.data[0].total_price,
                        it.data[0].tax.toInt(),
                        it.data[0].order_price
                ,it.data[0].delivery_price)
                message.order_state = ApiCreater.OrderStates.makeBill
                message.is_bill = "1"

                if (uri != null || currentPhotoPath != null)
                    message.image = "not null"

                sendMessageToCloud(message, view!!)

            }
        })
    }

    private fun dialogGetPhoto(view: View?) {
        dialog = Dialog(view!!.context)
        dialog!!.setContentView(R.layout.dialog_attach_photo)
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))


        dialog!!.tv_camera_.setOnClickListener {

            isStoragePermissionGranted(222)
            dialog!!.cancel()
        }

        dialog!!.tv_media.setOnClickListener {

            isStoragePermissionGranted(111)
            dialog!!.cancel()
        }
        dialog!!.show()
    }

    var imageUploadType = ""

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (Activity.RESULT_OK == resultCode || resultCode != RESULT_CANCELED) {

            Log.e("a", requestCode.toString())
            when (requestCode) {
                111 -> {
                    imageUploadType = "gallery"
                    Log.e("b", requestCode.toString())
                    VIEW.image_bill.setImageURI(data!!.data!!)
                    uri = data.data!!
                }

                222 -> {
                    imageUploadType = "camera"

                    val bitmapFromMedia = BitmapFactory.decodeFile(currentPhotoPath)
                    VIEW.image_bill.setImageBitmap(bitmapFromMedia)

                }

            }
            view!!.image_.visibility = View.GONE
            view!!.tv_add_more.visibility = View.GONE
        }
    }

    fun getImage(): MultipartBody.Part? {
        if (uri != null) {
            val p: String
            val cursor = activity!!.contentResolver.query(uri!!, null, null, null, null)
            p = if (cursor == null) {
                uri!!.path.toString()
            } else {
                cursor.moveToFirst()
                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                cursor.getString(idx)
            }

            val file = File(p)
            val requestFile = RequestBody.create(MediaType.parse("image/*"), file)
            return MultipartBody.Part.createFormData("image", file.name, requestFile)
        } else if (currentPhotoPath != null) {


            uri = Uri.parse(currentPhotoPath)

            Log.e("URI 222", uri!!.path.toString() + "    ")

            val p: String
            val cursor = activity!!.contentResolver.query(uri!!, null, null, null, null)

            if (cursor == null) {
                p = uri!!.path.toString()
                Log.e("URI 222 0000", uri!!.path.toString() + "    ")
            } else {
                cursor.moveToFirst()
                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                Log.e("URI 222 005500", uri!!.path.toString() + "    ")


                p = cursor.getString(idx)
            }

            val file = File(p)
            val requestFile = RequestBody.create(MediaType.parse("image/*"), file)
            return MultipartBody.Part.createFormData("image", file.name, requestFile)
        }

        return null

    }

    @SuppressLint("WrongConstant")
    fun isStoragePermissionGranted(requestCode: Int): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return if (requestCode == 111) {
                when (PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.checkSelfPermission(context!!, Manifest.permission.READ_EXTERNAL_STORAGE) -> {
                        gotoImage(requestCode)

                        true
                    }
                    else -> {

                        ActivityCompat.requestPermissions(
                                activity!!,
                                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA),
                                requestCode
                        )
                        false
                    }
                }
            } else {
                if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    gotoImage(requestCode)

                    true
                } else {

                    ActivityCompat.requestPermissions(
                            activity!!,
                            arrayOf(Manifest.permission.CAMERA),
                            requestCode
                    )
                    false
                }
            }
        } else {

            gotoImage(requestCode)
            return true
        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            gotoImage(requestCode)
        }
        if (grantResults.isNotEmpty() && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            gotoImage(requestCode)
        }
    }

    private fun gotoImage(requestCode: Int) {

        val intent: Intent

        if (requestCode == 111) {

            intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), requestCode)

        } else {

            /*                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePicture, PICK_IMAGE_FROM_CAMERA);

                SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                int imageId=pref.getInt("image",0);

                File path = new File(getActivity().getFilesDir(), "Gallery/MyImages/");
                if (!path.exists()) path.mkdirs();
                File image = new File(path, "image_capture_"+""+imageId+".jpg");
                Uri imageUri = FileProvider.getUriForFile(getActivity().getApplicationContext(), CAPTURE_IMAGE_FILE_PROVIDER, image);
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                providePermissionForProvider(cameraIntent, imageUri);
                startActivityForResult(cameraIntent, PICK_IMAGE_FROM_CAMERA);*/

            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            // Ensure that there's a camera activity to handle the intent
            // Ensure that there's a camera activity to handle the intent
            if (takePictureIntent.resolveActivity(activity!!.packageManager) != null) {
                // Create the File where the photo should go
                var photoFile: File? = null
                try {
                    photoFile = createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    val photoURI = try {
                        FileProvider.getUriForFile(context!!,
                                "${requireActivity().applicationContext.packageName}.fileprovider",
                                photoFile)
                    } catch (ex: IOException) {
                        Uri.fromFile(photoFile)
                    }
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, requestCode)
                }
            }

        }
    }


    @SuppressLint("SimpleDateFormat")
    @Throws(IOException::class)
    private fun createImageFile(): File? {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = activity!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",  /* suffix */
                storageDir /* directory */
        )

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.absolutePath
        return image
    }


    private var db = FirebaseFirestore.getInstance()
            .collection(ApiCreater.OrdersCollection)

    private var mStorageRef = FirebaseStorage.getInstance().getReference(ApiCreater.OrdersStorage)

    private fun sendMessageToCloud(message: Message, mView: View) {

        message.id = db.document().id
        message.created_at = System.currentTimeMillis().toString()
        message.updated_at = System.currentTimeMillis().toString()
        message.sender_id = UserInfo(activity!!).getId().toString()
        message.receiver_id = user_id.toString()
        message.order_state = ApiCreater.OrderStates.makeBill

        val imageName = MyUtil.getRandomName() + ".jpg"

        if (message.image.isNotEmpty()) {

            mStorageRef = FirebaseStorage.getInstance()
                    .getReference(ApiCreater.OrdersStorage)
                    .child(request_id.toString())
            if (imageUploadType == "gallery") {

                Log.e(TAG, "sendMessageToCloud: gallery")
                mStorageRef.child(imageName).putFile(uri!!).continueWithTask {
                    mStorageRef.child(imageName).downloadUrl
                }.addOnSuccessListener {
                    message.image = it.toString()
                    sendTheMessage(message, mView)
                }
            } else {
                Log.e(TAG, "sendMessageToCloud: camera")

                mStorageRef.child(imageName).putBytes(bitMapToByteArray()).continueWithTask {
                    mStorageRef.child(imageName).downloadUrl
                }.addOnSuccessListener {
                    message.image = it.toString()
                    sendTheMessage(message, mView)
                }.addOnFailureListener {
                    sendTheMessage(message, mView)
                }
            }
        } else {
            sendTheMessage(message, mView)
        }


    }

    private fun sendTheMessage(message: Message, mView: View) {

        db.document(message.id).set(message).addOnSuccessListener {
            Log.e(TAG, "sendTheMessage: addOnSuccessListener")
            mView.findNavController().navigate(R.id.chatFragment)
            Toast.makeText(activity!!, getString(R.string.your_bill_is_ready), LENGTH_SHORT).show()
            view!!.btn_done.visibility = View.VISIBLE
            view!!.progressBarBill.visibility = View.GONE
        }
    }


    fun bitMapToByteArray(): ByteArray {
        val bitmap = (VIEW.image_bill.drawable as BitmapDrawable).bitmap
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        /*val b = */return baos.toByteArray()
//        return Base64.encodeToString(b, Base64.DEFAULT).toByteArray()
    }

    private fun getByteArray(): ByteArray {

        val bitmap = (VIEW.image_bill.drawable as BitmapDrawable).bitmap

        val size: Int = bitmap.rowBytes * bitmap.height
        val byteBuffer: ByteBuffer = ByteBuffer.allocate(size)
        bitmap.copyPixelsToBuffer(byteBuffer)
        return byteBuffer.array()

    }

    private fun getByteArray(encodedString: String): ByteArray =
            Base64.decode(encodedString, Base64.DEFAULT)

    //            uri = Uri.parse(currentPhotoPath)
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}
