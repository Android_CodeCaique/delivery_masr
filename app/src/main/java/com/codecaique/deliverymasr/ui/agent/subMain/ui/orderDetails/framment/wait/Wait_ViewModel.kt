package com.codecaique.deliverymasr.ui.agent.subMain.ui.orderDetails.framment.wait

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codecaique.deliverymasr.pojo.response.Shop_Orders_Response
import com.codecaique.deliverymasr.data.ApiCreater
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class Wait_ViewModel : ViewModel()
{
    var shopOrdersData = MutableLiveData<List<Shop_Orders_Response.DataBean>>()

    fun getShopOrders(token: String,shop_id: String)
    {
        val observable =
                ApiCreater.instance.getShopOrders(token,shop_id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<Shop_Orders_Response>{
            override fun onComplete() {

            }


            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: Shop_Orders_Response) {
                shopOrdersData.value = t.getData() as List<Shop_Orders_Response.DataBean>?
                Log.e("ss", t.getData()!!.size.toString())
            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }

}