package com.codecaique.deliverymasr.ui.agent.subMain.ui.chat

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Activity.RESULT_CANCELED
import android.app.Activity.RESULT_OK
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.icu.text.SimpleDateFormat
import android.media.MediaRecorder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.*
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import cafe.adriel.androidaudiorecorder.AndroidAudioRecorder
import cafe.adriel.androidaudiorecorder.model.AudioChannel
import cafe.adriel.androidaudiorecorder.model.AudioSampleRate
import cafe.adriel.androidaudiorecorder.model.AudioSource
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.StoresMainActivity
import com.codecaique.deliverymasr.base.client.SubActivity
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.response.Message
import com.codecaique.deliverymasr.pojo.response.SentMessageResponse
import com.codecaique.deliverymasr.pojo.response.ShowUserOfOrderResponse
import com.codecaique.deliverymasr.pojo.response.User
import com.codecaique.deliverymasr.pojo.util.LocationInfo
import com.codecaique.deliverymasr.pojo.util.MyUtil
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.agent.subMain.ui.chat.dialogs.ChooseActionDialog
import com.codecaique.deliverymasr.ui.agent.subMain.ui.chat.dialogs.WaitingDialog
import com.codecaique.deliverymasr.ui.client.chat.ChatViewModel
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import com.codecaique.deliverymasr.ui.client.stores.adapters.StoresAdapter
import com.codecaique.deliverymasr.ui.client.userNotes.UserRatingFragment
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.squareup.picasso.Picasso
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import io.reactivex.plugins.RxJavaPlugins
import kotlinx.android.synthetic.main.dialog_attach_photo.*
import kotlinx.android.synthetic.main.dialog_cancle_order.*
import kotlinx.android.synthetic.main.dialog_order_deliverd_or_no.*
import kotlinx.android.synthetic.main.dialog_rate_user.*
import kotlinx.android.synthetic.main.dialog_rate_user.progressBar
import kotlinx.android.synthetic.main.dilalog_user_orders.*
import kotlinx.android.synthetic.main.dilalog_user_orders.civ_user
import kotlinx.android.synthetic.main.dilalog_user_orders.rate
import kotlinx.android.synthetic.main.dilalog_user_orders.tv_user_name
import kotlinx.android.synthetic.main.fragment_chat.view.*
import kotlinx.android.synthetic.main.fragment_confirm_my_order.*
import kotlinx.android.synthetic.main.fragment_push_bill.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.ParseException
import java.util.*


/**
 * A simple [Fragment] subclass.
 */

@Suppress("NAME_SHADOWING")
class ChatFragment : Fragment() {

    lateinit var chatAdapter: ChatAdapter

    var iv_menu: ImageView? = null
    lateinit var chatViewModel: ChatViewModel
    lateinit var userInfo: UserInfo
    lateinit var bundle: Bundle
    lateinit var locationInfo: LocationInfo
    lateinit var loadingDialog: LoadingDialog
    private val TAG = "ChatFragment"
    var userData: ShowUserOfOrderResponse? = null
    lateinit var mandowpData: User
    var phone = "0"
    var cost = "0"
    var cost_delivery="0"
    var orderState = "0"
    var userId = 0
    var requestId = 0
    var isOrderRated = false
    var currentPhotoPath: String? = null
    var imageUploadType = ""

    var client_rated = false
    var driver_rated = false

    companion object {
        val USER_ID = "user_id"
        val REQUEST_ID = "request_id"
        val COST = "cost"
        val COST_DELIVERY="cost_delivery"
    }

    lateinit var VIEW: View
    var uri: Uri?=null
    private var files: String = ""
    private val AUDIO_RECORDER_FILE_EXT_3GP = ".3gp"
    private val AUDIO_RECORDER_FILE_EXT_MP4 = ".mp4"
    private val AUDIO_RECORDER_FOLDER = "AudioRecorder"
    private var recorder: MediaRecorder? = null
    private val currentFormat = 0
    private val output_formats = intArrayOf(MediaRecorder.OutputFormat.MPEG_4, MediaRecorder.OutputFormat.THREE_GPP)
    private val file_exts = arrayOf(AUDIO_RECORDER_FILE_EXT_MP4, AUDIO_RECORDER_FILE_EXT_3GP)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        VIEW = inflater.inflate(R.layout.fragment_chat, container, false)

        RxJavaPlugins.setErrorHandler({ throwable -> }) // nothing or some logging

        //Toast.makeText(requireContext(),"Agent chat fragment",Toast.LENGTH_SHORT).show()
        return VIEW
    }

    /*wait ,
    processed,
    received,
    deliverd,
    arrived_site,
    make_bill,
    cancled
        */

    @RequiresApi(Build.VERSION_CODES.N)
    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        userId = try {
            arguments!!.getString(USER_ID)!!.toInt()
        } catch (e: Exception) {
            activity!!.intent.extras!!.getString(USER_ID)!!.toInt()
        }
        requestId = try {
            arguments!!.getString(REQUEST_ID)!!.toInt()
        } catch (e: Exception) {
            activity!!.intent.extras!!.getString(REQUEST_ID)!!.toInt()
        }

        Log.e(TAG, "onViewCreated: $requestId")

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true/*  enabled by default*/) {
            override fun handleOnBackPressed() {
                // Handle the back button event
                Log.e("lk", "jgjkuym")
                val intent = Intent(context, StoresMainActivity::class.java)
                intent.putExtra(StoresMainActivity.KEY, StoresMainActivity.MYORDERS)
                startActivity(intent)

            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)

        VIEW.btn_c.setOnClickListener {
            if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
                openAudion()

                true
            } else {

                ActivityCompat.requestPermissions(
                        activity!!,
                        arrayOf(Manifest.permission.RECORD_AUDIO,Manifest.permission.WAKE_LOCK),0
                )
                false
            }

        }

        chatViewModel = ViewModelProviders.of(this).get(ChatViewModel::class.java)
        userInfo = UserInfo(activity!!.applicationContext)
        bundle = Bundle()
        chatAdapter = ChatAdapter(activity!!.applicationContext)
        locationInfo = LocationInfo(activity!!.applicationContext)
        loadingDialog = LoadingDialog(activity!!)

        db = FirebaseFirestore.getInstance()
                .collection(ApiCreater.OrdersCollection)
                .document(requestId.toString())
                .collection(ApiCreater.ChatCollection)

        mStorageRef = FirebaseStorage.getInstance().getReference(ApiCreater.OrdersStorage).child(requestId.toString())

        VIEW.iv_loc.visibility = View.GONE
        VIEW.tv_loc.visibility = View.GONE
        VIEW.iv_loc_deliver.visibility = View.GONE
        VIEW.tv_loc_deliver.visibility = View.GONE
        VIEW.tv_pay_type.visibility = View.GONE

        view.lv_chat.adapter = chatAdapter

        fillUserData()
        //fillChat()
        realTimeChat()
        swipeToRefresh()
        fillOrderData()
        iv_menu = view.btn_more
//        loadingDialog.show()

        view.btn_more.setOnClickListener {

        }

        view.btn_payw.setOnClickListener {
            val b = Bundle()
            b.putString(REQUEST_ID, requestId.toString())
            b.putString(USER_ID, userId.toString())
            b.putString(COST, cost)
            b.putString(COST_DELIVERY,cost_delivery)
            it.findNavController().navigate(R.id.pushBillFragment, b)
        }


        VIEW.btn_mandowp_arrived_site.setOnClickListener {

            loadingDialog.show()
            chatViewModel.arrivedSite(userInfo.getUserToken(), requestId)
            chatViewModel.arrivedSiteMutableLiveData.observe(this, Observer {

                if (it.error == 0) {

                    loadingDialog.dismiss()
                    Toast.makeText(activity!!, getString(R.string.arrived_site), Toast.LENGTH_SHORT).show()
                    VIEW.btn_mandowp_arrived_site.visibility = GONE
//                    fillChat()
                    fillOrderData()
                    mandowpArrivedSite()
                }

            })

        }

        VIEW.btn_completOrder.setOnClickListener {

            val dialog1 = Dialog(activity!!)

            dialog1.apply {
                setContentView(R.layout.dialog_order_deliverd_or_no)
                setCancelable(false)
                window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                show()
            }

            dialog1.btn_yes_.setOnClickListener { _ ->


                loadingDialog.show()
                chatViewModel.deliverOrder(userInfo.getUserToken(), requestId)
                chatViewModel.deliverOfferMutableLiveData.observe(this, Observer {

                    if (it.error == 0) {

//                    loadingDialog.dismiss()

                        dialog1.dismiss()
                        Toast.makeText(activity!!, getString(R.string.ordera_delivered), Toast.LENGTH_SHORT).show()
                        VIEW.layout_delivery.visibility = GONE
//                    fillChat()
//                    fillOrderData()
                        var msg = Message()
                        deliverOrder()

                        Log.e(TAG, "onViewCreated: dialog1.btn_yes_.setOnClickListener --> "+getString(R.string.ordera_delivered))
                        rateDialog(msg.id)
                    }

                    dialog1.dismiss()
                })
                loadingDialog.dismiss()

            }

            dialog1.btn_no_.setOnClickListener {
                dialog1.dismiss()
            }

        }

        VIEW.btn_mandowp_Received.setOnClickListener { tt ->

            loadingDialog.show()
            chatViewModel.orderReceived(userInfo.getUserToken(), requestId)
            chatViewModel.orderReceivedMutableLiveData.observe(this, Observer {

                if (it.error == 0) {

                    Toast.makeText(activity!!, getString(R.string.received_order), Toast.LENGTH_SHORT).show()

                    orderReceived()
//                    fillChat()
//                    fillOrderData()
                    //todo now

                    /*   val b = Bundle()
                    b.putString("requestId", requestId.toString())
                    b.putString("type", "deliver")
                    tt.findNavController().navigate(R.id.distantFragment, b)*/


                }
                loadingDialog.dismiss()

            })

        }


        view.btn_send.setOnClickListener {

            val msg = view.et.text.toString()

            if (msg.isNotEmpty()) {

                view.btn_send.visibility = GONE
                view.progressBar.visibility = View.VISIBLE
                ApiCreater.instance.sentMessage(
                        RequestBody.create(MediaType.parse("multipart/form-data"), UserInfo(activity!!.applicationContext).getUserToken()),
                        userId, RequestBody.create(MediaType.parse("multipart/form-data"), msg), null, requestId
                ,null).enqueue(object : Callback<SentMessageResponse> {
                    override fun onFailure(call: Call<SentMessageResponse>, t: Throwable) {

                        Toast.makeText(activity!!, getString(R.string.internt_error), Toast.LENGTH_SHORT).show()

                    }

                    override fun onResponse(call: Call<SentMessageResponse>, response: Response<SentMessageResponse>) {

                        view.btn_send.visibility = View.VISIBLE
                        view.progressBar.visibility = GONE

//                        fillChat()
                        fillOrderData()
                        val message = Message()
                        message.message = msg
                        message.order_state = orderState

                        sendMessageToCloud(message)

                        view.et.setText("")
                        VIEW.lv_chat.setSelection(chatAdapter.list.size - 1)

                    }
                })
            }

        }

        view.back.setOnClickListener {
            val intent = Intent(context, StoresMainActivity::class.java)
            intent.putExtra(StoresMainActivity.KEY, StoresMainActivity.MYORDERS)
            startActivity(intent)
        }


        view.iv_loc.setOnClickListener {


            val geoUri = "http://maps.google.com/maps?saddr=${userInfo.getLatitude()},${userInfo.getLongitude()}" +
                    "&daddr=" + "${chatViewModel.showOrderMutableLiveData.value!!.data[0].receiving_latitude}," +
                    chatViewModel.showOrderMutableLiveData.value!!.data[0].receiving_longitude

            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(geoUri))
            activity!!.startActivity(intent)


            /*
            val b = Bundle()
            b.putString("requestId", requestId.toString())
            b.putString("type", "receive")
            b.putString(USER_ID, userId.toString())
            it.findNavController().navigate(R.id.distantFragment, b)
*/


        }

        view.iv_loc_deliver.setOnClickListener {

            /*val b = Bundle()
            b.putString("requestId", requestId.toString())
            b.putString("type", "deliver")
            it.findNavController().navigate(R.id.distantFragment, b)*/

            val geoUri = "http://maps.google.com/maps?saddr=${userInfo.getLatitude()},${userInfo.getLongitude()}" +
                    "&daddr=" + "${chatViewModel.showOrderMutableLiveData.value!!.data[0].client_latitude}," +
                    chatViewModel.showOrderMutableLiveData.value!!.data[0].client_longitude

            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(geoUri))
            activity!!.startActivity(intent)


        }

        view.img_call.setOnClickListener {

            /*if (date5 > date2) {

                val dialog = Dialog(activity!!)
                dialog.setContentView(R.layout.dialog_wait)

                dialog.tv_timer.text = getString(R.string.wait_5_mints)

                dialog.btn_close.visibility = View.VISIBLE

                dialog.btn_close.setOnClickListener {
                    dialog.dismiss()
                }

                dialog.show()

            } else {

*/
            if (phone.isNotEmpty()) {
                val i = Intent(Intent.ACTION_DIAL)
                i.data = Uri.parse("tel:$phone")
                if (i.resolveActivity(activity!!.packageManager) != null)
                    startActivity(i)
            } else {
                Toast.makeText(
                        activity!!.applicationContext, "Know Error",
                        Toast.LENGTH_SHORT
                ).show()
            }
//            }
        }

        view.img_user!!.setOnClickListener {

            loadingDialog.show()

            chatViewModel.showUserData(userInfo.getUserToken(), userId)
            chatViewModel.showUserOfOrderMutableLiveData.observe(this, Observer {

                Log.e("aaa", "requestId userId")
                loadingDialog.dismiss()

                userData = it
            })
            dialogShowUser(userData?.data!![0])

        }

        view.btn_v.setOnClickListener {
            dialogGetPhoto()
        }

    }

    private fun openAudion() {
        files = Environment.getExternalStorageDirectory().toString() + "/recorded_audio.wav"
        val color = resources.getColor(R.color.colorPrimaryDark)
        val requestCode = 0

        AndroidAudioRecorder.with(requireActivity()) // Required
                .setFilePath(files)
                .setColor(color)
                .setRequestCode(requestCode) // Optional
                .setSource(AudioSource.MIC)
                .setChannel(AudioChannel.STEREO)
                .setSampleRate(AudioSampleRate.HZ_48000)
                .setAutoStart(true)
                .setKeepDisplayOn(true) // Start recording
                .record()
    }


    private fun dialogGetPhoto() {
      val  dialog = Dialog(requireActivity())
        dialog!!.setContentView(R.layout.dialog_attach_photo)
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))


        dialog!!.tv_camera_.setOnClickListener {

        //    isStoragePermissionGranted(222)
            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(activity!!)

            dialog!!.cancel()
        }

        dialog!!.tv_media.setOnClickListener {

            isStoragePermissionGranted(111)
            dialog!!.cancel()
        }
        dialog!!.show()
    }

    val message = Message()

    private fun orderReceived() {

        message.message = ApiCreater.OrderMessages.driverReceivedOrderMessage
        message.order_state = ApiCreater.OrderStates.received

        sendMessageToCloud(message)

        val geoUri = "http://maps.google.com/maps?saddr=${userInfo.getLatitude()},${userInfo.getLongitude()}" +
                "&daddr=" + "${chatViewModel.showOrderMutableLiveData.value!!.data[0].client_latitude}," +
                chatViewModel.showOrderMutableLiveData.value!!.data[0].client_longitude

        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(geoUri))
        activity!!.startActivity(intent)

    }

    private fun deliverOrder() {
        Log.e(TAG, "deliverOrder: ---->>> 111")
        message.message = ApiCreater.OrderMessages.deliveredMessage
        message.order_state = ApiCreater.OrderStates.delivered

        Log.e(TAG, "deliverOrder: --->>> message.id -->"+message.id)
        Log.e(TAG, "deliverOrder: --->>> message.message -->"+message.message)
        Log.e(TAG, "deliverOrder: --->>> message.order_state -->"+message.order_state)
        sendMessageToCloud(message)
    }

    private fun mandowpArrivedSite() {

        message.message = ApiCreater.OrderMessages.arrivedSiteMessage
        message.order_state = ApiCreater.OrderStates.arrivedSite

        sendMessageToCloud(message)
    }

    @SuppressLint("NewApi")
    private fun swipeToRefresh() {

        /*  VIEW.refresh_layout_chat.setOnRefreshListener {
              VIEW.refresh_layout_chat.isRefreshing = false
          }*/

    }

    var isRatDialogShow = false

    private fun rateDialog(message_id: String) {

        Log.e(TAG, "rateDialog: rate state")
        val dialog = Dialog(activity!!)
        dialog.setContentView(R.layout.dialog_rate_user)
        Log.e(TAG, "rateDialog: dialog.isShowing --> "+dialog.isShowing)
        Log.e(TAG, "rateDialog: rate state 2")

        chatViewModel.showUserOfOrderMutableLiveData.observe(this, Observer {

            mandowpData = it.data[0]

            dialog.tv_user_name.text = mandowpData.first_name
            if (mandowpData.image != null && mandowpData.image.isNotEmpty())
            {
                Picasso.get().load(ApiCreater.USER_URL + mandowpData.image).into(dialog.civ_user)
            }


            dialog.btn_done.setOnClickListener {


                isRated = true


//            if (dialog.et_comment.text.isNotEmpty()) {

                dialog.btn_done.visibility = GONE
                dialog.progressBar.visibility = View.VISIBLE

                chatViewModel.rateUser(
                        RequestBody.create(MediaType.parse("multipart/form-data"), userInfo.getUserToken()),
                        userId, dialog.rate.rating.toDouble(),
                        RequestBody.create(MediaType.parse("multipart/form-data"), dialog.et_comment.text.toString() + " "),
                        requestId,
                        dialog
                )

                chatViewModel.rateUserMutableLiveData.observe(this, Observer { t ->

                    if (t.error == 0) {
                        Log.e(TAG, "rateDialog: t.error ->"+t.error)
                        Log.e(TAG, "rateDialog: t.message ->"+t.message)
                        loadingDialog.dismiss()
                        updateRateState(message_id)
                        dialog.btn_done.visibility = GONE
                        dialog.progressBar.visibility = View.VISIBLE
                        dialog.dismiss()
                        userInfo.putKeyRate(requestId.toString())
                        isOrderRated = true
//                        fillOrderData()
                    } else {
                        Log.e(TAG, "rateDialog: t.error ->"+t.error)
                        Log.e(TAG, "rateDialog: t.message ->"+t.message)
                        loadingDialog.dismiss()
                        dialog.dismiss()
                    }

                })
/*

            } else
                dialog.et_comment.error = getString(R.string.sould_be_not_empty)
*/

            }

            if (!userInfo.getKeyRate(requestId.toString())) {
                userInfo.putKeyRate(requestId.toString())
                observeRateUser(dialog,message_id)
                btnDoneClick(dialog,message_id)
                dialog.show()
            }


            else{
                userInfo.putKeyRate(requestId.toString())
                observeRateUser(dialog,message_id)
                btnDoneClick(dialog,message_id)
                dialog.show()
            }


        })

        chatViewModel.rateUserMutableLiveData.observe(this, Observer { t ->

            if (t.error == 0) {
                Log.e(TAG, "rateDialog: t.error ->"+t.error)
                Log.e(TAG, "rateDialog: t.message ->"+t.message)
                loadingDialog.dismiss()
                updateRateState(message_id)
                dialog.btn_done.visibility = GONE
                dialog.progressBar.visibility = View.VISIBLE
                dialog.dismiss()
                userInfo.putKeyRate(requestId.toString())
                isOrderRated = true
//                        fillOrderData()
            } else {
                Log.e(TAG, "rateDialog: t.error ->"+t.error)
                Log.e(TAG, "rateDialog: t.message ->"+t.message)
                loadingDialog.dismiss()
                dialog.dismiss()
            }

        })
        observeRateUser(dialog,message_id)
        btnDoneClick(dialog,message_id)
        observeRateUser(dialog,message_id)
        dialog.show()


    }
    fun btnDoneClick(dialog:Dialog,message_id: String){
        dialog.btn_done.setOnClickListener {


            isRated = true


//            if (dialog.et_comment.text.isNotEmpty()) {

            dialog.btn_done.visibility = GONE
            dialog.progressBar.visibility = View.VISIBLE

            chatViewModel.rateUser(
                    RequestBody.create(MediaType.parse("multipart/form-data"), userInfo.getUserToken()),
                    userId, dialog.rate.rating.toDouble(),
                    RequestBody.create(MediaType.parse("multipart/form-data"), dialog.et_comment.text.toString() + " "),
                    requestId,
                    dialog
            )

            chatViewModel.rateUserMutableLiveData.observe(this, Observer { t ->

                if (t.error == 0) {
                    Log.e(TAG, "rateDialog: t.error ->"+t.error)
                    Log.e(TAG, "rateDialog: t.message ->"+t.message)
                    loadingDialog.dismiss()
                    updateRateState(message_id)
                    dialog.btn_done.visibility = GONE
                    dialog.progressBar.visibility = View.VISIBLE
                    dialog.dismiss()
                    userInfo.putKeyRate(requestId.toString())
                    isOrderRated = true
//                        fillOrderData()
                } else {
                    Log.e(TAG, "rateDialog: t.error ->"+t.error)
                    Log.e(TAG, "rateDialog: t.message ->"+t.message)
                    loadingDialog.dismiss()
                    dialog.dismiss()
                }

            })
/*

            } else
                dialog.et_comment.error = getString(R.string.sould_be_not_empty)
*/
            dialog.dismiss()

        }
        dialog.dismiss()
    }
    fun observeRateUser(dialog:Dialog,message_id: String){
        chatViewModel.rateUserMutableLiveData.observe(this, Observer { t ->

            if (t.error == 0) {
                Log.e(TAG, "rateDialog: t.error ->"+t.error)
                Log.e(TAG, "rateDialog: t.message ->"+t.message)
                loadingDialog.dismiss()
                updateRateState(message_id)
                dialog.btn_done.visibility = GONE
                dialog.progressBar.visibility = View.VISIBLE
                dialog.dismiss()
                userInfo.putKeyRate(requestId.toString())
                isOrderRated = true
//                        fillOrderData()
            } else {
                Log.e(TAG, "rateDialog: t.error ->"+t.error)
                Log.e(TAG, "rateDialog: t.message ->"+t.message)
                loadingDialog.dismiss()
                dialog.dismiss()
            }

        })
    }

    @SuppressLint("SetTextI18n")
    fun dialogShowUser(user: User) {
        val dialog = Dialog(activity!!)
        dialog.setContentView(R.layout.dilalog_user_orders)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.cinComments!!.setOnClickListener {

            Log.e("data", "jwroiuil")

            val intent = Intent(context, SubActivity::class.java)
            StoresAdapter.fragName = R.layout.fragment_user_rating
            intent.putExtra(UserRatingFragment.USER_ID, userId.toString())
            //     intent.putExtra(SubActivity.KEY,SubActivity.UserNotes)
            intent.putExtra(SubActivity.KEY, SubActivity.USER_RATE)
            activity!!.startActivity(intent)

        }

        Log.e("aaa", "requestId  &&  userId")



        if (user.image !== null && user.image.isNotEmpty())
            Picasso.get().load(ApiCreater.USER_URL + user.image).into(dialog.civ_user)

        if (user.rate.isNotEmpty())
            dialog.rate.rating = user.rate.toFloat()

        dialog.tv_user_name.text = user.first_name
        dialog.text_order_num.text = user.requestes_count.toString() + " "
        dialog.text_comments_num.text = user.user_comments.toString() + " "

        dialog.show()


    }


    @RequiresApi(Build.VERSION_CODES.N)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        Log.e("REquest code ", requestCode.toString() + "       ")

        if (Activity.RESULT_OK == resultCode) {
            when (requestCode) {
                111 -> {
                    data?.let {
                        uri = data.data!!

                        //btn_v
                        VIEW.btn_v.visibility = View.INVISIBLE
                        VIEW.btn_send.visibility = GONE
                        VIEW.progressBar.visibility = View.VISIBLE
                        imageUploadType = "gallery"

                        ApiCreater.instance.sentMessage(
                                RequestBody.create(MediaType.parse("multipart/form-data"), UserInfo(activity!!.applicationContext).getUserToken()),
                                userId, RequestBody.create(MediaType.parse("multipart/form-data"), ""), getImage(), requestId
                        ,null).enqueue(object : Callback<SentMessageResponse> {
                            override fun onFailure(call: Call<SentMessageResponse>, t: Throwable) {

                                Toast.makeText(activity!!, getString(R.string.internt_error), Toast.LENGTH_SHORT).show()

                            }

                            override fun onResponse(call: Call<SentMessageResponse>, response: Response<SentMessageResponse>) {

                                VIEW.btn_v.visibility = View.VISIBLE
                                VIEW.btn_send.visibility = View.VISIBLE
                                VIEW.progressBar.visibility = GONE

                                /*
                                chatAdapter.addMsg(Message(
                                        "", t.data.id, t.data.image, "", userId.toString(), "", "", "", requestId.toString(),
                                        userInfo.getId().toString(), "", "", "", "", ""
                                ))
                                VIEW.lv_chat.setSelection(chatAdapter.list.size - 1)
            */

                                val message = Message()
                                message.image = "not null"
                                message.order_state = orderState
                                sendMessageToCloud(message)

                            }
                        })

                    }
                }
                203 -> {


                    if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

                        val result = CropImage.getActivityResult(data)
                        if (resultCode == RESULT_OK) {
                            uri = result.uri
                        } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                            val error = result.error
                        }
                    }


                    imageUploadType = "camera"
                        VIEW.btn_v.visibility = View.INVISIBLE
                        VIEW.btn_send.visibility = View.GONE
                        VIEW.progressBar.visibility = View.VISIBLE

                        val bitmapFromMedia = BitmapFactory.decodeFile(currentPhotoPath)
//                        VIEW.image_bill.setImageBitmap(bitmapFromMedia)

                        ApiCreater.instance.sentMessage(
                                RequestBody.create(MediaType.parse("multipart/form-data"), UserInfo(activity!!.applicationContext).getUserToken()),
                                userId, RequestBody.create(MediaType.parse("multipart/form-data"), ""), getImage(), requestId
                        ,null).enqueue(object : Callback<SentMessageResponse> {
                            override fun onFailure(call: Call<SentMessageResponse>, t: Throwable) {

                                Toast.makeText(activity!!, getString(R.string.internt_error), Toast.LENGTH_SHORT).show()

                            }

                            override fun onResponse(call: Call<SentMessageResponse>, response: Response<SentMessageResponse>) {

                                val t = response.body()!!
                                VIEW.btn_v.visibility = View.VISIBLE
                                VIEW.btn_send.visibility = View.VISIBLE
                                VIEW.progressBar.visibility = View.GONE

                                Log.e("ddatata", "userId $userId requestId $requestId t.data.id,  ${t.data.id}, t.data.image ${t.data.image} , t.data.message ${t.data.message}")

                                val message = Message()
                                message.image = "not null"
                                message.order_state = orderState
                                sendMessageToCloud(message)


                                /*     chatAdapter.addMsg(Message(
                                         "", t.data.id, t.data.image, "", userId.toString(), "", "", "", requestId.toString(),
                                         userInfo.getId().toString(), "", "", "", "", ""
                                 ))
                            */
//                                    VIEW.lv_chat.setSelection(chatAdapter.list.size - 1)

                            }
                        })
                    }
                0->{

                    if (resultCode == RESULT_OK) {
                        // Great! User has recorded and saved the audio file

                         uri = Uri.fromFile(File(files))

                        VIEW.btn_v.visibility = View.INVISIBLE
                        VIEW.btn_send.visibility = View.GONE
                        VIEW.progressBar.visibility = View.VISIBLE

                        val bitmapFromMedia = BitmapFactory.decodeFile(currentPhotoPath)
//                        VIEW.image_bill.setImageBitmap(bitmapFromMedia)

                        ApiCreater.instance.sentMessage(
                                RequestBody.create(MediaType.parse("multipart/form-data"), UserInfo(activity!!.applicationContext).getUserToken()),
                                userId, RequestBody.create(MediaType.parse("multipart/form-data"), ""), null, requestId
                                ,getvoice()).enqueue(object : Callback<SentMessageResponse> {
                            override fun onFailure(call: Call<SentMessageResponse>, t: Throwable) {

                                Toast.makeText(activity!!, getString(R.string.internt_error), Toast.LENGTH_SHORT).show()

                            }

                            override fun onResponse(call: Call<SentMessageResponse>, response: Response<SentMessageResponse>) {

                                val t = response.body()!!
                                VIEW.btn_v.visibility = View.VISIBLE
                                VIEW.btn_send.visibility = View.VISIBLE
                                VIEW.progressBar.visibility = View.GONE

                                Log.e("ddatata", "userId $userId requestId $requestId t.data.id,  ${t.data.id}, t.data.image ${t.data.image} , t.data.message ${t.data.message}")

                                val message = Message()
                                message.image = "not null"
                                message.order_state = orderState
                                sendvoicetocloud(message)


                                /*     chatAdapter.addMsg(Message(
                                         "", t.data.id, t.data.image, "", userId.toString(), "", "", "", requestId.toString(),
                                         userInfo.getId().toString(), "", "", "", "", ""
                                 ))
                            */
//                                    VIEW.lv_chat.setSelection(chatAdapter.list.size - 1)

                            }
                        })


                    } else if (resultCode == RESULT_CANCELED) {
                        // Oops! User has canceled the recording
                    }


            }

            }
        }

    }

    fun getvoice():MultipartBody.Part?{

        // creates RequestBody instance from file
        val file = File(uri!!.path)
        // creates RequestBody instance from file
        val requestFile: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        return MultipartBody.Part.createFormData("voice", file.name, requestFile)

    }
    fun getImage(): MultipartBody.Part? {
        if (uri != null) {
            val p: String
            val cursor = activity!!.contentResolver.query(uri!!, null, null, null, null)
            p = if (cursor == null) {
                uri!!.path.toString()
            } else {
                cursor.moveToFirst()
                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                cursor.getString(idx)
            }

            val file = File(p)
            val requestFile = RequestBody.create(MediaType.parse("image/*"), file)
            return MultipartBody.Part.createFormData("image", file.name, requestFile)
        } else if (currentPhotoPath != null) {


            uri = Uri.parse(currentPhotoPath)

            Log.e("URI 222", uri!!.path.toString() + "    ")

            val p: String
            val cursor = activity!!.contentResolver.query(uri!!, null, null, null, null)

            if (cursor == null) {
                p = uri!!.path.toString()
                Log.e("URI 222 0000", uri!!.path.toString() + "    ")
            } else {
                cursor.moveToFirst()
                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                Log.e("URI 222 005500", uri!!.path.toString() + "    ")


                p = cursor.getString(idx)
            }

            val file = File(p)
            val requestFile = RequestBody.create(MediaType.parse("image/*"), file)
            return MultipartBody.Part.createFormData("image", file.name, requestFile)
        }

        return null

    }

    @SuppressLint("WrongConstant")
    fun isStoragePermissionGranted(requestCode: Int): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return if (requestCode == 111) {
                when (PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.checkSelfPermission(context!!, Manifest.permission.READ_EXTERNAL_STORAGE) -> {
                        gotoImage(requestCode)

                        true
                    }
                    else -> {

                        ActivityCompat.requestPermissions(
                                activity!!,
                                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA),
                                requestCode
                        )
                        false
                    }
                }
            }
            else {
               // if (requestCode == 222) {
                    if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        gotoImage(requestCode)

                        true
                    } else {

                        ActivityCompat.requestPermissions(
                                activity!!,
                                arrayOf(Manifest.permission.CAMERA),
                                requestCode
                        )
                        false

                    }



                }

        } else {

            gotoImage(requestCode)
            return true
        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            gotoImage(requestCode)
        }
        if (grantResults.isNotEmpty() && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            gotoImage(requestCode)
        }
    }

    private fun gotoImage(requestCode: Int) {

        val intent: Intent

        if (requestCode == 111) {

            intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), requestCode)

        } else if(requestCode==222) {

            /*                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePicture, PICK_IMAGE_FROM_CAMERA);

                SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                int imageId=pref.getInt("image",0);

                File path = new File(getActivity().getFilesDir(), "Gallery/MyImages/");
                if (!path.exists()) path.mkdirs();
                File image = new File(path, "image_capture_"+""+imageId+".jpg");
                Uri imageUri = FileProvider.getUriForFile(getActivity().getApplicationContext(), CAPTURE_IMAGE_FILE_PROVIDER, image);
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                providePermissionForProvider(cameraIntent, imageUri);
                startActivityForResult(cameraIntent, PICK_IMAGE_FROM_CAMERA);*/

            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            // Ensure that there's a camera activity to handle the intent
            // Ensure that there's a camera activity to handle the intent
            if (takePictureIntent.resolveActivity(activity!!.packageManager) != null) {
                // Create the File where the photo should go
                var photoFile: File? = null
                try {
                    photoFile = createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    val photoURI = try {
                        FileProvider.getUriForFile(context!!,
                                "${requireActivity().applicationContext.packageName}.fileprovider",
                                photoFile)
                    } catch (ex: IOException) {
                        Uri.fromFile(photoFile)
                    }
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, requestCode)
                }
            }

        }
    }


    @SuppressLint("SimpleDateFormat")
    @Throws(IOException::class)
    private fun createImageFile(): File? {
        // Create an image file name
        val timeStamp = java.text.SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = activity!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",  /* suffix */
                storageDir /* directory */
        )

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.absolutePath
        return image
    }


    var isRated = false

    @RequiresApi(Build.VERSION_CODES.N)
    @SuppressLint("SetTextI18n")
    private fun fillOrderData() {

        Log.e(TAG, "orderState")
        Log.e(TAG, "fillOrderData: isShowing 221 ${loadingDialog.isShowing}")

        loadingDialog.show()
        Log.e(TAG, "fillOrderData: isShowing ww ${loadingDialog.isShowing}")
        Log.e(TAG, "fillOrderData: 1")
        chatViewModel.showOrderById(userInfo.getUserToken(), requestId)
        chatViewModel.showOrderMutableLiveData.observe(this, Observer {

            Log.e(TAG, "fillOrderData: 2")

            try {
                Log.e(TAG, "fillOrderData: 3")
                loadingDialog.dismiss()
            } catch (e: Exception) {
                Log.e(TAG, "fillOrderData: 4")
                loadingDialog.dismiss()
            }
            loadingDialog.dismiss()
            Log.e(TAG, "fillOrderData: isShowing ${loadingDialog.isShowing}")

            VIEW.tv.text = it.data[0].name
            VIEW.tv_num.text = it.data[0].id + " # "
            phone = it.data[0].phone
            VIEW.tv_cost.text = it.data[0].offer_price + "ج"
            cost_delivery=it.data[0].offer_price
            cost = it.data[0].offer_price
            VIEW.tv_user_name.text=it.data[0].first_name+" "+it.data[0].last_name
         //   VIEW.tv_rate.text=it.data[0].rate_state
           // VIEW.tv_cost.text=it.data[0].offer_price
             // VIEW.tv_rate.text = formattedNumber
            // VIEW.ratingBar.rating = it.data[0].rate.toFloat()
            VIEW.tv_distance.text = it.data[0].phone
//            it.data[0]

            try {

                if (it.data[0].payment_id == "1") {
                    VIEW.tv_pay_type.text = getString(R.string.cash_type)
                } else {
                    VIEW.tv_pay_type.text = getString(R.string.pay_type)
                }

            } catch (e: Exception) {
            }


            try {
                orderState = it.data[0].state

                Log.e(TAG, "orderState $orderState")
                cost = it.data[0].offer_price
                Log.e(TAG, "orderState medium")

                /* if (timeForCloseCallAtStart <= date2) {
                     view!!.img_call.visibility = VISIBLE
                 } else {
                     view!!.img_call.visibility = GONE
                 }
                */

                Log.e(TAG, "fillOrderData: ee")

            } catch (e: Exception) {
                Log.e(TAG, "fillOrderData: cat-------")
            }

            Log.e(TAG, "fillOrderData: orderState $orderState")

            val message = Message(order_state = orderState)

            if (it.data[0].accepted_time != "null") {
                val time = it.data[0].accepted_time

                val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                try {

                    Log.e("time ttttttt", time + " ")

                    val date: Date = format.parse(time)
                    val t1: Long = date.time

                    date7 = Date(t1 + (7 * 60000))

                    val deliveryTime = format.parse(it.data[0].deliveryTime).time
                    date30 = Date(deliveryTime + (30 * 60000))

                    Log.e(TAG, "fillOrderData: date7 $date7")
                    Log.e(TAG, "fillOrderData: date30 $date30")

                } catch (e: Exception) {
                    Log.e(TAG, "fillOrderData: Exception")
                }

                if (dateNow > date7)
                    iv_menu!!.setOnClickListener { dialogShowMenu(VIEW, date7) }
                else
                    iv_menu!!.setOnClickListener { dialogShowMenu(VIEW, null) }


                if (dateNow > date30 && orderState == "received_order")
                    view!!.tbOptions.visibility = GONE


//                }

//            changUIByOrderState(message)

            }
            Log.e(TAG, "fillOrderData: out")
            if (it.data[0].image != null && it.data[0].image.isNotEmpty())
                Picasso.get().load(ApiCreater.USER_URL + it.data[0].image).into(VIEW.img_user)

        })

        if (loadingDialog.isShowing)
            loadingDialog.dismiss()

    }

    private fun cancelOrderDialog() {
        val dialog = Dialog(activity!!)

        dialog.apply {
            setContentView(R.layout.dialog_cancle_order)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

        if (!userInfo.getKey("cancel$requestId")) {
            userInfo.putKey("cancel$requestId")
            dialog.show()
        }

        dialog.btn_yes.setOnClickListener {

            dialog.layout_cancel.visibility = View.GONE
            dialog.progressBarCancel.visibility = View.VISIBLE

            chatViewModel.acceptCancelorder(userInfo.getUserToken(), requestId, "yes", activity!!)
            chatViewModel.cancelOrderMutableLiveData.observe(activity!!, Observer {

                Toast.makeText(activity!!, getString(R.string.mangment_replay), Toast.LENGTH_SHORT).show()
                dialog.dismiss()
                activity!!.finish()

            })

        }

        dialog.btn_no.setOnClickListener {

            dialog.layout_cancel.visibility = View.GONE
            dialog.progressBarCancel.visibility = View.VISIBLE

            chatViewModel.acceptCancelorder(userInfo.getUserToken(), requestId, "no", activity!!)
            chatViewModel.cancelOrderMutableLiveData.observe(activity!!, Observer {

                Toast.makeText(activity!!, getString(R.string.mangment_replay), Toast.LENGTH_SHORT).show()
                dialog.dismiss()
                activity!!.finish()

            })


        }

    }

    var date7 = Date()
    var date30 = Date()
    private val dateNow = Date(System.currentTimeMillis())

    lateinit var timeForCloseChat: Date
    lateinit var timeForCloseCall: Date
    lateinit var timeForCloseCallAtStart: Date

    @SuppressLint("SimpleDateFormat")
    @RequiresApi(Build.VERSION_CODES.N)
    private fun fillChat() {

        /* if (VIEW.refresh_layout_chat.isRefreshing)
             loadingDialog.dismiss()
             */

        Log.e(TAG, "fillChat: ${userInfo.getUserToken()}, $userId, $requestId")

        chatViewModel.showMessages(userInfo.getUserToken(), userId, requestId)

        chatViewModel.showMessagesMutableLiveData.observe(this, Observer { t ->
            //            view.progressBar.visibility=View.GONE

            /*    if (VIEW.refresh_layout_chat.isRefreshing)
                    VIEW.refresh_layout_chat.isRefreshing = false
    */
            loadingDialog.dismiss()

            chatAdapter.setData(t.data as ArrayList<Message>)
            VIEW.lv_chat.setSelection(t.data.size - 1)

//            lastItemSelected = t.data.size


            val list = t

            Log.e("TIME asdasda", t.acceptTime)

            if (t.acceptTime != "null") {
                val time = t.acceptTime

                val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                try {

                    Log.e("time ttttttt", time + " ")

                    val date: Date = format.parse(time)
                    val t1: Long = date.time

                    date7 = Date(t1 + (7 * 60000))
                    timeForCloseCallAtStart = Date(t1 + (7 * 60000))

                    val deliveryTime = format.parse(t.deliveryTime.date).time
                    timeForCloseChat = Date(deliveryTime + (30 * 60000))
                    timeForCloseCall = Date(deliveryTime + (7 * 60000))

//                    tbOptions.visibility = GONE

                    Log.e("TIME sasa, date", date7.toString() + "    ")

                    for (datum in list.data) {
                        if ((list.state == "processed" || list.state == "received" || list.state == "deliverd"
                                        || list.state == "make_bill" || list.state == "arrived_site")
                                && dateNow >= date7) {
//                            img_call.visibility = View.VISIBLE
                            break
                        } else {
                            iv_menu!!.setOnClickListener { dialogShowMenu(VIEW, null) }
                            break
                        }
                    }

                    fillOrderData()

                    System.out.println(date)
                } catch (e: ParseException) {

                    Log.e("Error date ", e.message)
                    e.printStackTrace()

                }
            } else {
                iv_menu!!.setOnClickListener { dialogShowMenu(VIEW, null) }
            }


        })

    }

    fun dialogShowMenu(view: View, date: Date?) {
        val dialog = ChooseActionDialog(activity!!, chatViewModel, requestId, userId, orderState, cost, date)

        val window: Window = dialog.window!!
        val wlp: WindowManager.LayoutParams = window.attributes
        wlp.gravity = Gravity.BOTTOM
        wlp.flags = wlp.flags and WindowManager.LayoutParams.FLAG_DIM_BEHIND.inv()
        window.attributes = wlp

        dialog.show()

    }

    var lastItemSelected = 0
    var size = 0
    private fun refreshChat() {


        Log.e("REfresh", lastItemSelected.toString() + "     ")
        chatViewModel.showMessages(userInfo.getUserToken(), userId, requestId)
        chatViewModel.showMessagesMutableLiveData.observe(this, Observer { t ->

            //            view.progressBar.visibility=View.GONE
            Log.e("TAG", "${lastItemSelected} != ${t.data.size}" +
                    "${lastItemSelected != t.data.size}")

            chatAdapter.setData(t.data as ArrayList<Message>)
//            VIEW.lv_chat.setSelection(t.data.size - 1)

            size = t.data.size

            if (lastItemSelected != t.data.size) {

//                VIEW.lv_chat.setSelection(t.data.size - 1)
                lastItemSelected = t.data.size

            }

        })

        /* if (lastItemSelected == size)
             disposable.dispose()
 */

    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun fillUserData() {
        chatViewModel.showUserData(userInfo.getUserToken(), userId)
        chatViewModel.showUserOfOrderMutableLiveData.observe(this, Observer { t ->

            if (t.data[0].image != null && t.data[0].image.isNotEmpty())
                Picasso.get().load(ApiCreater.USER_URL + t.data[0].image).into(VIEW.img_user)

            mandowpData = t.data[0]

            Log.e("mosratag", "kao777777")


            val formatter: NumberFormat = DecimalFormat("#,##")
            val formattedNumber: String = formatter.format(t.data[0].rate.toFloat())


            VIEW.tv_rate.text = formattedNumber
            VIEW.ratingBar.rating = t.data[0].rate.toFloat()
            phone = t.data[0].phone
            VIEW.ratingBar.rating = t.data[0].rate.toFloat()
            VIEW.tv_user_name.text = t.data[0].first_name+" "+t.data[0].last_name
            VIEW.tv_distance.text = t.data[0].phone
           // VIEW.tv_user_name.text = t.data[0].first_name
          // view.tv_user_name.text=t.data.first_name
            fillOrderData()

        })
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onResume() {
        super.onResume()


        if (locationInfo.isLocationShare()) {

            val msg = "http://maps.google.com/maps?q=loc:${locationInfo.getLocationShare().latitude},${locationInfo.getLocationShare().longitude}"

            VIEW.btn_send.visibility = GONE
            VIEW.progressBar.visibility = View.VISIBLE

            ApiCreater.instance.sentMessage(
                    RequestBody.create(MediaType.parse("multipart/form-data"), UserInfo(activity!!.applicationContext).getUserToken()),
                    userId, RequestBody.create(MediaType.parse("multipart/form-data"), msg), null, requestId
            ,null).enqueue(object : Callback<SentMessageResponse> {
                override fun onFailure(call: Call<SentMessageResponse>, t: Throwable) {

                    Toast.makeText(activity!!, getString(R.string.internt_error), Toast.LENGTH_SHORT).show()

                }

                override fun onResponse(call: Call<SentMessageResponse>, response: Response<SentMessageResponse>) {

                    VIEW.btn_send.visibility = View.VISIBLE
                    VIEW.progressBar.visibility = GONE

//                    fillChat()
                    val message = Message()
                    message.message = msg
                    message.order_state = orderState

                    sendMessageToCloud(message)
                    locationInfo.setStreetShare("")
                    VIEW.lv_chat.setSelection(chatAdapter.list.size - 1)

                }
            })


        }

        Log.w("REsume", "dttd")
//        fillOrderData()
//        fillChat()

        /*  if (disposable.isDisposed)
              disposable = Observable.interval(1000, 4000,
                      TimeUnit.MILLISECONDS)
                      .observeOn(AndroidSchedulers.mainThread())
                      .subscribe { refreshChat() }

  */

            Log.e(this.javaClass.name, "CurrentScreen")


    }

    /*var disposable = Observable.interval(1000, 4000,
            TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { refreshChat() }

    override fun onPause() {
        super.onPause()
        disposable.dispose()
    }*/


    private var db = FirebaseFirestore.getInstance()
            .collection(ApiCreater.OrdersCollection)

    private var mStorageRef: StorageReference = FirebaseStorage.getInstance().getReference(ApiCreater.OrdersStorage).child(requestId.toString())

    @RequiresApi(Build.VERSION_CODES.N)
    private fun realTimeChat() {

        var msg = Message()

        db.orderBy("created_at").addSnapshotListener { querySnapshot, _ ->


            try {
                loadingDialog.dismiss()
            } catch (e: Exception) {
            }

            val messages = ArrayList<Message>()

            querySnapshot!!.documents.forEach { documentSnapshot ->
                msg = documentSnapshot.toObject(Message::class.java)!!
                messages.add(msg)

            }

            client_rated = message.client_rated
            driver_rated = message.driver_rated
            changUIByOrderState(msg, msg.driver_rated)

            chatAdapter.submitData(messages)

            VIEW.lv_chat.setSelection(chatAdapter.list.size - 1)

        }


    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun changUIByOrderState(message: Message, driverRated: Boolean) {

        Log.e(TAG, "fillOrderData: isShowing 11 ${loadingDialog.isShowing}")

        if(loadingDialog.isShowing){
            loadingDialog.dismiss()
        }
        VIEW.img_call.visibility = VISIBLE

        orderState = message.order_state

        try {
            loadingDialog.dismiss()
        } catch (e: Exception) {
        }

        try {
            if (loadingDialog.isShowing)
                loadingDialog.dismiss()

        } catch (e: Exception) {
        }


        Log.e(TAG, "changUIByOrderState ------>     : $orderState")
        when (orderState.trim()) {


            "received_order" -> {

                Log.e(TAG, "fillOrderData: ---- received_order")
                VIEW.btn_more.visibility = GONE

                try {
                    view!!.btn_more.visibility = GONE
                } catch (e: Exception) {

                    Log.e(TAG, "fillOrderData: ---- sSa")
                }
                try {
                    if (loadingDialog.isShowing)
                        loadingDialog.dismiss()

                } catch (e: Exception) {
                }


//                Log.e(TAG, "fillOrderData: timeForCloseChat $timeForCloseChat")
//                Log.e(TAG, "fillOrderData: timeForCloseCall $timeForCloseCall")

                Log.e(TAG, "fillOrderData: ccc")

                Log.e(TAG, "fillOrderData: bbb")

                try {

                    VIEW.img_call.visibility = GONE
                } catch (e: Exception) {
                }

                /*  if (timeForCloseCall <= date2)
                                      view!!.img_call.visibility = GONE
              */
                if (!driverRated) {
                    Log.e(TAG, "fillOrderData: aaa")
                    //todo now
                    dialogOrderBegin()
                 //   isRatDialogShow = true
                }

                fillOrderData()

            }
            "deliverd" -> {

                /*VIEW.tbOptions.visibility = GONE
                return@Observer
                if (it.data[0].rate_state != "rated" && !isOrderRated) {
                    Log.e("TAG", it.data[0].rate_state)
                    rateDialog()
                }*/

            }
            "processed" -> {
                VIEW.layout_delivery.visibility = View.VISIBLE
                VIEW.btn_payw.visibility = View.VISIBLE
                VIEW.btn_mandowp_arrived_site.visibility = GONE
                VIEW.btn_mandowp_Received.visibility = GONE

                if (!userInfo.getKey(requestId.toString())) {
                    userInfo.putKey(requestId.toString())
                    val waitingDialog = WaitingDialog(activity!!, message.order_state)
                    waitingDialog.show()

                }

            }
            "arrived_site" -> {
                VIEW.layout_delivery.visibility = View.VISIBLE
                VIEW.btn_mandowp_arrived_site.visibility = GONE
                //make_bill
                VIEW.btn_completOrder.visibility = View.VISIBLE
            }
            "make_bill" -> {
                VIEW.layout_delivery.visibility = View.VISIBLE
                VIEW.btn_mandowp_arrived_site.visibility = GONE
                VIEW.btn_mandowp_Received.visibility = View.VISIBLE
                VIEW.btn_payw.visibility = GONE
            }
            "received" -> {
                VIEW.layout_delivery.visibility = VISIBLE
                VIEW.btn_mandowp_arrived_site.visibility = VISIBLE
                VIEW.btn_payw.visibility = GONE
                VIEW.btn_mandowp_Received.visibility = GONE
            }
            "cancled" -> {
                //todo هل تم اصدار الفاتورة بعد الغاء الطلب ام لا عند الغاء الطلب
                if (message.is_bill == "1")
                    cancelOrderDialog()

            }

            else -> {
                Log.e(TAG, "fillOrderData: the end orderState $orderState")
                Log.e(TAG, "fillOrderData: the end")
                VIEW.layout_delivery.visibility = GONE
            }


        }

        if ((orderState == "processed" || orderState == "received" || orderState == "deliverd"
                        || orderState == "make_bill" || orderState == "arrived_site")
        /*&& date2 >= date5*/) {

            iv_menu!!.setOnClickListener { dialogShowMenu(VIEW, date7) }
        } else {
            iv_menu!!.setOnClickListener { dialogShowMenu(VIEW, null) }
        }
        Log.e(TAG, "fillOrderData: isShowing end ${loadingDialog.isShowing}")

        if(loadingDialog.isShowing){
            loadingDialog.dismiss()
        }

    }
    private fun dialogOrderBegin() {
        var dialog = Dialog(activity!!)
        dialog.setContentView(R.layout.fragment_confirm_my_order)

        dialog.tv_dear.text="اهلا بك:"
        dialog.txt_confirm.text="تأكد من جدية الطلب لانك الوحيد المسئول عنه"

        dialog.buttoConfirm.setOnClickListener {

          // dialogOrderBegin2()

            /*    if (disposable.isDisposed)
                disposable = Observable.interval(1000, 30000,
                        TimeUnit.MILLISECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { refreshChat() }
*/

            dialog.dismiss()
            dialog.cancel()
        }


            dialog.show()

    }

    private fun sendvoicetocloud(message: Message){
        message.id = db.document().id
        message.created_at = System.currentTimeMillis().toString()
        message.updated_at = System.currentTimeMillis().toString()
        message.sender_id = userInfo.getId().toString()
        message.receiver_id = userId.toString()
        message.request_id = requestId.toString()
        val voicename = MyUtil.getRandomName() + ".mp3"
        mStorageRef = FirebaseStorage.getInstance()
                .getReference(ApiCreater.OrdersStorage)
                .child(requestId.toString())
        mStorageRef.child(voicename).putFile(uri!!).continueWithTask {
            mStorageRef.child(voicename).downloadUrl
        }.addOnSuccessListener {
            message.voice = it.toString()
            sendTheMessage(message)
        }

    }
    private fun sendMessageToCloud(message: Message) {

        message.id = db.document().id
        message.created_at = System.currentTimeMillis().toString()
        message.updated_at = System.currentTimeMillis().toString()
        message.sender_id = userInfo.getId().toString()
        message.receiver_id = userId.toString()
        message.request_id = requestId.toString()

        val imageName = MyUtil.getRandomName() + ".jpg"
        val voicename = MyUtil.getRandomName() + ".mp3"

        if (message.image.isNotEmpty()) {

            mStorageRef = FirebaseStorage.getInstance()
                    .getReference(ApiCreater.OrdersStorage)
                    .child(requestId.toString())
            if (imageUploadType == "gallery") {

                Log.e(TAG, "sendMessageToCloud: gallery")
                mStorageRef.child(imageName).putFile(uri!!).continueWithTask {
                    mStorageRef.child(imageName).downloadUrl
                }.addOnSuccessListener {
                    message.image = it.toString()
                    sendTheMessage(message)
                }
            } else {
                Log.e(TAG, "sendMessageToCloud: camera")

                mStorageRef.child(imageName).putFile(uri!!).continueWithTask {
                    mStorageRef.child(imageName).downloadUrl
                }.addOnSuccessListener {
                    message.image = it.toString()
                    sendTheMessage(message)
                }.addOnFailureListener {
                    sendTheMessage(message)
                }
            }
        } else {
            sendTheMessage(message)
        }

    }

    private fun sendTheMessage(message: Message) {
        message.client_rated = client_rated
        message.driver_rated = driver_rated
        db.document(message.id).set(message)

    }

    private fun updateRateState(msg_id: String) {

        val map = HashMap<String, Boolean>()
        map["client_rated"] = client_rated
        map["driver_rated"] = true

        db.document(msg_id).update(map as Map<String, Any>).addOnSuccessListener {

            try {
                loadingDialog.dismiss()
            }catch (e:Exception){}

        }

    }

    fun bitMapToByteArray(): ByteArray {
        val bitmap = (VIEW.image_bill.drawable as BitmapDrawable).bitmap
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        /*val b = */
        return baos.toByteArray()
//        return Base64.encodeToString(b, Base64.DEFAULT).toByteArray()
    }




    private fun getFilename(): String? {
        val filepath = Environment.getExternalStorageDirectory().path
        val file = File(filepath, AUDIO_RECORDER_FOLDER)
        if (!file.exists()) {
            file.mkdirs()
        }
        return file.absolutePath + "/" + System.currentTimeMillis() + file_exts[currentFormat]
    }

    private fun startRecording() {
        recorder = MediaRecorder()
//        recorder!!.setAudioSource(MediaRecorder.AudioSource.MIC)
  //      recorder!!.setOutputFormat(output_formats[currentFormat]);
//        recorder!!.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)
        recorder!!.setOutputFile(getFilename())
        recorder!!.setOnErrorListener(errorListener)
        recorder!!.setOnInfoListener(infoListener)
        try {
            recorder!!.prepare()
            recorder!!.start()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private val errorListener = MediaRecorder.OnErrorListener { mr, what, extra -> "AppLog.logString" }

    private val infoListener = MediaRecorder.OnInfoListener { mr, what, extra -> "info"}

    private fun stopRecording() {
        if (null != recorder) {
//            recorder!!.stop()
            recorder!!.reset()
            recorder!!.release()
            recorder = null
        }
    }



}
