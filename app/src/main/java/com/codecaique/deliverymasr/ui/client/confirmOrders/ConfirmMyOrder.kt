package com.codecaique.deliverymasr.ui.client.confirmOrders

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.ui.client.chat.ChatFragment
import kotlinx.android.synthetic.main.fragment_confirm_my_order.view.*

/**
 * A simple [Fragment] subclass.
 */
class ConfirmMyOrder : Fragment() {

    companion object{
        val REQUEST_ID="OfferId"
        val USER_ID="USER_ID"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_confirm_my_order, container, false)

        view.buttoConfirm.setOnClickListener {

            /*val chatFrag = ChatFrag()
            val manager = fragmentManager
            manager!!.beginTransaction().replace(R.id.main3_fragment, chatFrag, chatFrag.tag).commit()*/

            val offerId=activity!!.intent.extras!!.getString(REQUEST_ID)
            val userId=activity!!.intent.extras!!.getString(USER_ID)

            Log.e("dada","arrayItem.offer_id ${offerId} arrayItem.delivery_id ${userId}")

            val bundle=Bundle()
            bundle.putString(ChatFragment.REQUEST_ID,offerId)
            bundle.putString(ChatFragment.USER_ID,userId)

            it.findNavController().navigate(R.id.chatFragment,bundle)
        }
        return view
    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}