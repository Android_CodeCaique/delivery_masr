package com.codecaique.deliverymasr.ui.client.signUp.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.codecaique.deliverymasr.R;
import com.codecaique.deliverymasr.ui.client.other.JoinUsAsProvider;

import static com.codecaique.deliverymasr.ui.client.userNotes.UserNotes5.STC_Pay;

/**
 * A simple {@link Fragment} subclass.
 */
public class CodeVerification2 extends Fragment {

    public CodeVerification2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_code_verification2, container, false);
        Button mobile_verification_btn2 = view.findViewById(R.id.mobile_verification_btn2);
        mobile_verification_btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (STC_Pay.equals("done")) {
                    STC_Pay = null;
//                    Intent intent=new Intent(getContext(), MainActivity2.class);
//                    fragName=R.layout.fragment_bank_account;
//                    startActivity(intent);
                    JoinUsAsProvider joinUsAsProvider = new JoinUsAsProvider();
                    FragmentManager manager = getFragmentManager();
                    manager.beginTransaction().replace(R.id.main3_fragment, joinUsAsProvider, joinUsAsProvider.getTag()).commit();
                } else {
                    JoinUsAsProvider joinUsAsProvider = new JoinUsAsProvider();
                    FragmentManager manager = getFragmentManager();
                    manager.beginTransaction().replace(R.id.main3_fragment, joinUsAsProvider, joinUsAsProvider.getTag()).commit();
                }


            }
        });
        return view;
    }
}
