package com.codecaique.deliverymasr.ui.agent.subMain.ui.specialOrder

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.ui.agent.subMain.ui.chat.ChatFragment
import kotlinx.android.synthetic.main.fragment_special_order.view.*

/**
 * A simple [Fragment] subclass.
 */
class SpecialOrderFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_special_order, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val b=Bundle()

        val userId = activity!!.intent.extras!!.getString(ChatFragment.USER_ID)
        val requestId = activity!!.intent.extras!!.getString(ChatFragment.REQUEST_ID)

        b.putString(ChatFragment.USER_ID,userId)
        b.putString(ChatFragment.REQUEST_ID,requestId)

        view.buttoConfirm.setOnClickListener {
            it.findNavController().navigate(R.id.chatFragment,b)
        }

    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}
