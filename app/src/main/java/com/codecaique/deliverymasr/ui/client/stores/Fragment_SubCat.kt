package com.codecaique.deliverymasr.ui.client.stores

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.response.NearStore
import com.codecaique.deliverymasr.pojo.response.Places_Response
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.client.restaurant.RestaurantFragment
import com.codecaique.deliverymasr.ui.client.stores.adapters.Map_NearStoresAdapter
import com.codecaique.deliverymasr.ui.client.stores.adapters.NearStoresAdapter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_subcat.view.*

class Fragment_SubCat : Fragment() {
    lateinit var VIEW: View
    val TAG = "Fragment_SubCat"


    lateinit var userInfo: UserInfo
    lateinit var storesViewModel: StoresViewModel
    lateinit var nearStoresAdapter: NearStoresAdapter
    lateinit var catId: String
    lateinit var catImage: String
    lateinit var catName: String
    lateinit var map_nearAdapter: Map_NearStoresAdapter

    var stores = ArrayList<NearStore>()
    var googlePlaces = ArrayList<Places_Response.ResultsBean>()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        VIEW = inflater.inflate(R.layout.fragment_subcat, container, false)

        storesViewModel = ViewModelProviders.of(this).get(StoresViewModel::class.java)
        userInfo = UserInfo(activity!!.applicationContext)

        catId = activity!!.intent.extras!!.getString(RestaurantFragment.ID, "0")
        catImage = activity!!.intent.extras!!.getString(RestaurantFragment.IMAGE, "0")
        catName = activity!!.intent.extras!!.getString(RestaurantFragment.NAME, "0")

        VIEW.tv_title.text = catName

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                // Handle the back button event
                activity!!.finish()//supportFragmentManager.popBackStack()
                Log.e(TAG, "handleOnBackPressed: test back")
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(this, callback)



        nearStoresAdapter = NearStoresAdapter(activity!!.applicationContext)
//        map_nearAdapter = Map_NearStoresAdapter(activity!!.applicationContext,VIEW)
        VIEW.rv_marketList.adapter = nearStoresAdapter
//        VIEW.rv_new_marketList.adapter = map_nearAdapter


        Log.e("tag 0050.", "  $catName")

        val location: String = userInfo.getLatitude() + "," + userInfo.getLongitude()

        Log.e(TAG, "onCreateView: $location")
        catName = "supermarket"
        storesViewModel.getNearByStoreAndCat(location, catName)

        when (catName) {
            "مطاعم" -> {
                catName = "restaurant"
                storesViewModel.getNearByStoreAndCat(location, catName)
            }
            "سوبر ماركت" -> {
                catName = "supermarket"
                storesViewModel.getNearByStoreAndCat(location, catName)
            }
            "كافيه و مشروبات" -> {
                catName = "cafe"
                storesViewModel.getNearByStoreAndCat(location, catName)
            }
        }

        storesViewModel.nearPlaces_ResponseCat.observe(this, androidx.lifecycle.Observer {
//            map_nearAdapter.setData(it.results as ArrayList<Places_Response.ResultsBean>)

            Log.e(TAG, "onCreateView: testetete" )

            googlePlaces = it.results as ArrayList<Places_Response.ResultsBean>
            Log.e("tag 00100. sub", "${it.results.size}   ")

            nearStoresAdapter.setGoogleList(googlePlaces)


        })


        storesViewModel.getNearStoresAndCategories(userInfo.getUserToken(), catId)
        storesViewModel.nearStoresMutableLiveData.observe(viewLifecycleOwner, Observer { t ->

            VIEW.progressBar2.visibility = View.GONE

            stores = t.data as ArrayList<NearStore>

            Log.e("tag 0050.", "${t.data.size}   ${t.category.size}")


            nearStoresAdapter.setData(stores)


        })


        if (catName.contains("مطاعم") || catName.contains("restaurant")) {
            VIEW.video.setImageResource(R.drawable.restaurants)
            VIEW.iv_rest.setImageResource(R.drawable.restaurants)
        } else if (catName.contains("سوبر ماركت") || catName.contains("supermarket")) {
            VIEW.video.setImageResource(R.drawable.supermaret)
            VIEW.iv_rest.setImageResource(R.drawable.supermaret)
        } else {
            Picasso.get().load(ApiCreater.CATEGORIES_URL + catImage).into(VIEW.iv_rest)
            Picasso.get().load(ApiCreater.CATEGORIES_URL + catImage).into(VIEW.video)
        }

        VIEW.back.setOnClickListener {
            activity!!.finish()
        }

//        VIEW.refresh_layout.setOnRefreshListener {
//            storesViewModel.getNearStoresAndCategories(userInfo.getUserToken(),catId)
//            storesViewModel.nearStoresMutableLiveData.observe(viewLifecycleOwner, Observer { t ->
//
//                VIEW.progressBar2.visibility = View.GONE
//                nearStoresAdapter.setData(t.data as ArrayList<NearStore>)
//
//
//                Log.e("tag 0050.","${t.data.size}   ${t.category.size}")
//
//
//            })
//
//            VIEW.refresh_layout.isRefreshing = false
//
//        }

        return VIEW
    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}