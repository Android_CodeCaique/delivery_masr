package com.codecaique.deliverymasr.ui.agent.main.ui.useConditions

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.client.allSitting.SittingsFragment
import com.codecaique.deliverymasr.utils.Utils
import kotlinx.android.synthetic.main.fragment_usage.view.*
import java.lang.Exception

/**
 * A simple [Fragment] subclass.
 */
class UsageFragment : Fragment() {
    companion object {

        val KEY = "key"
        val UseConditions = "show_use_conditions"
        val ShowPolicy = "show_policy"
        val AboutUs = "about_us"

    }

    lateinit var usageViewModel: UsageViewModel
    lateinit var userInfo: UserInfo

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_usage, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        usageViewModel = ViewModelProviders.of(this).get(UsageViewModel::class.java)
        userInfo = UserInfo(activity!!.applicationContext)

        val key = arguments!!.getString(KEY)

        when (key) {

            UseConditions -> {

                view.tv_title.text = getString(R.string.use_con)

                usageViewModel.showUseConditions(userInfo.getUserToken())
                usageViewModel.showUseConditionsMutableLiveData.observe(this, Observer { t ->

                    view.progressBar.visibility = View.INVISIBLE
                    view.tv_details.text = t.data[0].A_text

                })

            }
            ShowPolicy -> {

                view.tv_title.text = getString(R.string.policy)

                usageViewModel.showPolicy(userInfo.getUserToken())
                usageViewModel.showPolicyMutableLiveData.observe(this, Observer { t ->

                    view.progressBar.visibility = View.INVISIBLE
                    view.tv_details.text = t.data[0].text

                })

            }
            AboutUs -> {

                view.tv_title.text = getString(R.string.about_us)

                usageViewModel.aboutUs(userInfo.getUserToken())
                usageViewModel.aboutUsMutableLiveData.observe(this, Observer { t ->

                    if (t.error == 3) {
                        Utils.signout(requireActivity() , view!!)
                        return@Observer;
                    }
                    view.progressBar.visibility = View.INVISIBLE
                    view.tv_details.text = t.data.name + "\n" +
                            "${t.data.details}\n" +
                            t.data.address

                })

            }

        }

        view.back.setOnClickListener {

            try {
                view.findNavController().popBackStack()
            } catch (e: Exception) {

                val allSittingFrag = SittingsFragment()
                val manager = fragmentManager
                manager!!.beginTransaction().replace(R.id.main_fragment, allSittingFrag, allSittingFrag.tag).commit()

            }

        }

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true/*  enabled by default*/) {
            override fun handleOnBackPressed() {
                // Handle the back button event
                try {
                    view.findNavController().popBackStack()
                } catch (e: Exception) {

                    val allSittingFrag = SittingsFragment()
                    val manager = fragmentManager
                    manager!!.beginTransaction().replace(R.id.main_fragment, allSittingFrag, allSittingFrag.tag).commit()

                }
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)


    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}
