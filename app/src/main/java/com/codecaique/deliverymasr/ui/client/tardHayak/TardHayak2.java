package com.codecaique.deliverymasr.ui.client.tardHayak;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.codecaique.deliverymasr.R;
import com.codecaique.deliverymasr.ui.client.restaurant.OrderSentFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class TardHayak2 extends Fragment {

    public TardHayak2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tard_hayak2, container, false);
        Button buttoConfirm = view.findViewById(R.id.buttoConfirm);
        buttoConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderSentFragment orderSented = new OrderSentFragment();
                FragmentManager manager = getFragmentManager();
                manager.beginTransaction().replace(R.id.main3_fragment, orderSented, orderSented.getTag()).commit();
            }
        });

        return view;
    }
}
