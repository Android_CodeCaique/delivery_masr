package com.codecaique.deliverymasr.ui.client.allSitting

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.ChangeLanguage
import com.codecaique.deliverymasr.base.client.SubActivity
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.agent.main.ui.useConditions.UsageFragment
import kotlinx.android.synthetic.main.fragment_sittings.view.*


class SittingsFragment : Fragment() {

    lateinit var userInfo: UserInfo
    val TAG = "SittingsFragment"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_sittings, container, false)

        userInfo = UserInfo(activity!!.applicationContext)
        val intent = Intent(view.context, SubActivity::class.java)

        view.tv_ringtone.text = if (userInfo.getRingtoneName()!!.isNotEmpty()) userInfo.getRingtoneName()
        else
            getString(R.string.default_)


        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                // Handle the back button event
               activity!!. supportFragmentManager.popBackStack()
                Log.e(TAG, "handleOnBackPressed: test back")
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(this, callback)


        view.conShakawy.setOnClickListener { v ->

            intent.putExtra(SubActivity.KEY, SubActivity.Shakawy)
            startActivity(intent)
        }

        view.conEditProfile.setOnClickListener { v ->

            intent.putExtra(SubActivity.KEY, SubActivity.EditPtofile)
            startActivity(intent)
        }

        view.conJoin.setOnClickListener { v ->

            intent.putExtra(SubActivity.KEY, SubActivity.JoinAsCaptain)
            activity!!.startActivity(intent)
        }

        view.iv_backSitting.setOnClickListener {
            val allSittingFrag = AllSittingFragment()
            val manager = fragmentManager
            manager!!.beginTransaction().replace(R.id.main_fragment, allSittingFrag, allSittingFrag.tag).commit()
        }


        val bundle = Bundle()
        val usageFragment = UsageFragment()
        val manager = fragmentManager

        view.con6.setOnClickListener {

            bundle.putString(UsageFragment.KEY, UsageFragment.UseConditions)
            usageFragment.arguments = bundle
            manager!!.beginTransaction().replace(R.id.main_fragment, usageFragment, usageFragment.tag).commit()

        }

        view.con9.setOnClickListener {

            aboutUs()
//            bundle.putString(UsageFragment.KEY, UsageFragment.AboutUs)
//            usageFragment.arguments = bundle
//            manager!!.beginTransaction().replace(R.id.main_fragment, usageFragment, usageFragment.tag).commit()

        }

        view.con7.setOnClickListener {

            rateApp()

        }

        view.con2.setOnClickListener {


            val intent1 = Intent(RingtoneManager.ACTION_RINGTONE_PICKER)
            intent1.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select ringtone for notifications:")
            intent1.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, false)
            intent1.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true)
            intent1.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, userInfo.getRingtone())
            intent1.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_ALARM)
            startActivityForResult(intent1, 999)

        }

        view.con5.setOnClickListener {
            val changeLanguage = ChangeLanguage()
            changeLanguage.show(childFragmentManager, "ChangeLanguage")
        }

        return view
    }

    private fun aboutUs() {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://site.hiakk.com"))
        var flags = Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_MULTIPLE_TASK
        flags = if (Build.VERSION.SDK_INT >= 21) {
            flags or Intent.FLAG_ACTIVITY_NEW_DOCUMENT or Intent.FLAG_ACTIVITY_NEW_TASK
        } else {
            flags or Intent.FLAG_ACTIVITY_NEW_TASK
        }
        intent.addFlags(flags)
//        context!!.startActivity(intent)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == 999) {

            if (data!!.data != null) {
                view!!.tv_ringtone.text = RingtoneManager.getRingtone(activity!!.applicationContext, data.data!!).getTitle(activity!!.applicationContext)

                userInfo.setRingtone(data.data!!)
                userInfo.setRingtoneName(
                        RingtoneManager.getRingtone(activity!!.applicationContext, data.data!!).getTitle(activity!!.applicationContext)
                )

                Log.e("TAG",
                        RingtoneManager.getRingtone(activity!!.applicationContext, data.data!!).getTitle(activity!!.applicationContext)
                )
            } else {
                view!!.tv_ringtone.text = getString(R.string.default_)
                userInfo.setRingtone(null)
                userInfo.setRingtoneName("")
            }
        }
    }

    fun rateApp() {
        try {
            val rateIntent = rateIntentForUrl("market://details")
            startActivity(rateIntent)
        } catch (e: ActivityNotFoundException) {
            val rateIntent = rateIntentForUrl("https://play.google.com/store/apps/details")
            startActivity(rateIntent)
        }
    }

    private fun rateIntentForUrl(url: String): Intent {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(java.lang.String.format("%s?id=%s", url, activity!!.packageName)))
        var flags = Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_MULTIPLE_TASK
        flags = if (Build.VERSION.SDK_INT >= 21) {
            flags or Intent.FLAG_ACTIVITY_NEW_DOCUMENT or Intent.FLAG_ACTIVITY_NEW_TASK
        } else {
            flags or Intent.FLAG_ACTIVITY_NEW_TASK
        }
        intent.addFlags(flags)
        return intent
    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}