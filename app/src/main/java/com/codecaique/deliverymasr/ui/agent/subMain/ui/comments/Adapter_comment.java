package com.codecaique.deliverymasr.ui.agent.subMain.ui.comments;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.codecaique.deliverymasr.R;

import java.util.List;

public class Adapter_comment extends RecyclerView.Adapter<Adapter_comment.ViewHolder> {


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView Uname, Problemtext, Time;

        public ViewHolder(View itemView) {
            super(itemView);
            Uname=itemView.findViewById(R.id.user_namep);
            Problemtext=itemView.findViewById(R.id.problemText);
            Time=itemView.findViewById(R.id.time);

        }
    }


    private Context context;
    private List<Commentmodel> list;

    public Adapter_comment(Context c, List<Commentmodel> C) {
        this.context = c;
        this.list = C;

    }

    @Override
    public Adapter_comment.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v;
        v = LayoutInflater.from(context).inflate(R.layout.item_comment, viewGroup, false);
        return new Adapter_comment.ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(Adapter_comment.ViewHolder viewHolder, int i) {

        Commentmodel commentmodel = list.get(i);
      /*  viewHolder.Uname.setText(commentmodel.getName());
        viewHolder.Problemtext.setText(commentmodel.getProblemtxt());
        viewHolder.Time.setText(commentmodel.getTime());
*/
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
