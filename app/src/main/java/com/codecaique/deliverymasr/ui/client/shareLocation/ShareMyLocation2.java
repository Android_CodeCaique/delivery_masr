package com.codecaique.deliverymasr.ui.client.shareLocation;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.codecaique.deliverymasr.R;
import com.codecaique.deliverymasr.ui.client.tardHayak.TardHayak2;


public class ShareMyLocation2 extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_share_my_location2, container, false);

        Button buttoConfirm = view.findViewById(R.id.buttoConfirm);
        buttoConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TardHayak2 tardHayak2 = new TardHayak2();
                FragmentManager manager = getFragmentManager();
                manager.beginTransaction().replace(R.id.main3_fragment, tardHayak2, tardHayak2.getTag()).commit();
            }
        });

        return view;
    }
}
