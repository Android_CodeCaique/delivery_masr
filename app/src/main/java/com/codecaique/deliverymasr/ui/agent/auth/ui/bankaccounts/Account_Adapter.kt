package com.codecaique.deliverymasr.ui.agent.auth.ui.bankaccounts

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R

class Account_Adapter(private val context: Context, private val list: List<Accountmodel>) : RecyclerView.Adapter<Account_Adapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var Bankname: TextView
        var User: TextView
        var Phone: TextView
        var edit:Button

        init {
            Bankname = itemView.findViewById(R.id.bank_name)
            User = itemView.findViewById(R.id.user_name)
            Phone = itemView.findViewById(R.id.user_num)
            edit=itemView.findViewById(R.id.btn_edit)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v: View
        v = LayoutInflater.from(context).inflate(R.layout.item_acc_bank, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        val p = list[i]
        viewHolder.Bankname.text = p.getBankname()
        viewHolder.User.text = p.getUsername()
        viewHolder.Phone.text = p.getPhone()

        viewHolder.edit.setOnClickListener {
            it.findNavController().navigate(R.id.editbankAccountFragment)
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

}