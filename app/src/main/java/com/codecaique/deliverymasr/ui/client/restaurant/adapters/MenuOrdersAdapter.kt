package com.codecaique.deliverymasr.ui.client.restaurant.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.request.MenuItemOrder
import org.json.JSONArray
import org.json.JSONObject

class MenuOrdersAdapter(var context: Context, val tvTotalPrice:TextView) : RecyclerView.Adapter<MenuOrdersAdapter.ViewHolder>() {

    var q : Int = 0
    var price:Float = 0F
    var total_price:Float = 0F

    var list = ArrayList<MenuItemOrder>()



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_order_list, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val arrayItem = list[position]

        holder.tvName.text = arrayItem.productName
        holder.tvQuantity.text=arrayItem.amount

        q = arrayItem.amount.toInt()

        price = arrayItem.price.toFloat()

        total_price = 0F

        holder.tvPrice.text = (price * q).toString() + " ريال "

        tvTotalPrice.text = (price * q).toString()



        holder.minus.setOnClickListener {

            if (q > 1) {
                q -= 1
                arrayItem.amount = q.toString()

                holder.tvQuantity.text = q.toString()


                total_price = price * q

                Log.e("Menu price ",total_price.toString())
                holder.tvPrice.text = total_price.toString()
                tvTotalPrice.text = total_price.toString()
            }
        }

        holder.plus.setOnClickListener {
//            var q = arrayItem.amount.toInt()
            q += 1
            arrayItem.amount = q.toString()
            holder.tvQuantity.text = q.toString()
            total_price = price * q

            Log.e("Menu price ",total_price.toString())

            holder.tvPrice.text = total_price.toString()
            tvTotalPrice.text = total_price.toString()

        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvName = itemView.findViewById(R.id.tv_camera) as TextView
        var tvPrice = itemView.findViewById(R.id.tv_price) as TextView
        var tvQuantity = itemView.findViewById(R.id.num) as TextView
        val plus = itemView.findViewById(R.id.plus) as ImageView
        val minus = itemView.findViewById(R.id.down) as ImageView
    }

    fun setData(a: ArrayList<MenuItemOrder>) {
        this.list = a
        notifyDataSetChanged()
    }

     fun getTotalPrice(): Int {
        var t = 0

        list.forEach { i ->

            try {

                t += (i.price.toDouble() * i.amount.toDouble()).toInt()

            }catch (e:Exception){}

        }
        return t
    }

    public fun getMenuItem():JSONArray{

//        val a=ArrayList<MenuItem>()
        val aa= JSONArray()
        val     z=JSONObject()

        list.forEach {

            Log.e("",q.toString() + total_price.toString())

            z.put("product",it.product)
            z.put("type",it.type)
            z.put("amount",q)
            z.put("price",total_price)
//            a.add(MenuItem(it.product,it.type,it.amount,it.price))
            aa.put(z)
        }

        return aa
    }
}