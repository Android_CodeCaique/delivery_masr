package com.codecaique.deliverymasr.ui.agent.subMain.ui.orderChat

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.pojo.response.Message
import com.google.api.AnnotationsProto.http
import com.rygelouv.audiosensei.player.AudioSenseiPlayerView
import com.squareup.picasso.Picasso
import java.util.ArrayList

public class OrderChatAdapter(var context: Context) :
        BaseAdapter() {

    val TAG = "ChatAdapter"
    var list = ArrayList<Message>()
    var userId = UserInfo(context).getId()

    @SuppressLint("InflateParams")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val arrayItem = list[position]

        val view = if (arrayItem.sender_id.toInt() == userId)
            LayoutInflater.from(context)
                    .inflate(R.layout.item_chat, parent, false)
        else
            LayoutInflater.from(context)
                    .inflate(R.layout.item_chat_back, parent, false)

        //  ui
        val msg = view.findViewById(R.id.tv_msg) as TextView
        val img = view.findViewById(R.id.img) as ImageView
        val player = view.findViewById(R.id.audio_player) as AudioSenseiPlayerView


      //  Picasso.get().load(arrayItem.image).into(img)
        //  set data
       // val imge:String=ApiCreater.ORDER_URL+arrayItem.image

        if (arrayItem.image != null && arrayItem.image.isNotEmpty()) {
            Log.e(TAG, arrayItem.image)
            Picasso.get().load(arrayItem.image).into(img)
            img.visibility = View.VISIBLE
            msg.text = arrayItem.message
           player.visibility=View.GONE

        } else {
            Log.e(TAG, "getView: message isNotEmpty")
            img.visibility = View.GONE
            msg.text = arrayItem.message
            player.visibility=View.GONE

        }


//        if(arrayItem.sender_image!=null && arrayItem.sender_image.isNotEmpty()){
//            Picasso.get().load(imge).into(img)
//            img.visibility = View.VISIBLE
//        }

        msg.setOnClickListener {
            try {

                val geoUri = arrayItem.message
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(geoUri))
                context.startActivity(intent)

            } catch (e: Exception) {
            }
        }


        return view
    }

    override fun getItem(p0: Int): Any {
        return list[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return list.size
    }

    fun setData(a: ArrayList<Message>) {
        this.list = ArrayList()
        this.list = a
        notifyDataSetChanged()
    }

    fun submitData(a: ArrayList<Message>) {
        this.list = ArrayList()
        this.list = a
        notifyDataSetChanged()
    }

    fun addMsg(a: Message) {
        this.list.add(a)
        notifyDataSetChanged()
    }
    fun addimage(a: String) {
        this.list.get(0).image=a
        notifyDataSetChanged()
    }

}
