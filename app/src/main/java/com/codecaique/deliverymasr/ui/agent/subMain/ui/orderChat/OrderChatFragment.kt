package com.codecaique.deliverymasr.ui.agent.subMain.ui.orderChat

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.client.SubActivity
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.response.Message
import com.codecaique.deliverymasr.pojo.response.ShowUserOfOrderResponse
import com.codecaique.deliverymasr.pojo.response.User
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.agent.subMain.ui.chat.ChatFragment
import com.codecaique.deliverymasr.ui.client.chat.ChatViewModel
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import com.codecaique.deliverymasr.ui.client.stores.adapters.StoresAdapter
import com.codecaique.deliverymasr.ui.client.userNotes.UserRatingFragment
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.squareup.picasso.Picasso
import io.reactivex.plugins.RxJavaPlugins
import kotlinx.android.synthetic.main.dilalog_user_orders.*
import kotlinx.android.synthetic.main.dilalog_user_orders.tv_user_name
import kotlinx.android.synthetic.main.fragment_chat.view.*
import kotlinx.android.synthetic.main.fragment_confirm_my_order.*
import kotlinx.android.synthetic.main.fragment_order_chat.*
import kotlinx.android.synthetic.main.fragment_order_chat.view.*
import kotlinx.android.synthetic.main.fragment_order_chat.view.back
import kotlinx.android.synthetic.main.fragment_order_chat.view.img_call
import kotlinx.android.synthetic.main.fragment_order_chat.view.img_user
import kotlinx.android.synthetic.main.fragment_order_chat.view.iv_loc
import kotlinx.android.synthetic.main.fragment_order_chat.view.lv_chat
import kotlinx.android.synthetic.main.fragment_order_chat.view.ratingBar
import kotlinx.android.synthetic.main.fragment_order_chat.view.tv
import kotlinx.android.synthetic.main.fragment_order_chat.view.tv_distance
import kotlinx.android.synthetic.main.fragment_order_chat.view.tv_num
import kotlinx.android.synthetic.main.fragment_order_chat.view.tv_pay_type
import kotlinx.android.synthetic.main.fragment_order_chat.view.tv_rate
import kotlinx.android.synthetic.main.fragment_order_chat.view.tv_user_name
import kotlinx.android.synthetic.main.item_problem.*
import java.text.DecimalFormat
import java.text.NumberFormat


/**
 * A simple [Fragment] subclass.
 */
class OrderChatFragment : Fragment() {

    lateinit var chatAdapter: OrderChatAdapter
    val TAG = "OrderChatFragment"

    lateinit var chatViewModel: ChatViewModel
    lateinit var userInfo: UserInfo
    lateinit var bundle: Bundle

    lateinit var mandowpData: User
    var userId = 0
    var requestId = 0
    var phone = ""
    var payment = ""
    var userData: ShowUserOfOrderResponse?=null


    companion object {
        val USER_ID = "user_id"
        val REQUEST_ID = "request_id"
        val PAYMENT = "payment"
    }

    var deliveryImage = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_order_chat, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        RxJavaPlugins.setErrorHandler { throwable: Throwable? -> }

        userId = try {
            arguments!!.getString(USER_ID)!!.toInt()
        } catch (e: Exception) {
            activity!!.intent.extras!!.getString(USER_ID)!!.toInt()

        }

        payment = try {
            arguments!!.getString(PAYMENT)!!
        } catch (e: Exception) {
            activity!!.intent.extras!!.getString(PAYMENT)!!
        }

        requestId = try {
            arguments!!.getString(REQUEST_ID)!!.toInt()
        } catch (e: Exception) {
            activity!!.intent.extras!!.getString(REQUEST_ID)!!.toInt()

        }

        view.back.setOnClickListener {

            activity!!.finish()

        }
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                // Handle the back button event
                activity!!. finish()//supportFragmentManager.popBackStack()
                Log.e(TAG, "handleOnBackPressed: test back")
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(this, callback)


        chatViewModel = ViewModelProviders.of(this).get(ChatViewModel::class.java)
        userInfo = UserInfo(activity!!.applicationContext)
        bundle = Bundle()
        chatAdapter = OrderChatAdapter(activity!!.applicationContext)

        //        view.lv_chat

        view.lv_chat.adapter = chatAdapter

        Log.e(TAG, "${userInfo.getUserToken()} \n, ${userId}\n,${requestId}")


        chatViewModel.showUserData(userInfo.getUserToken(), userId)
        chatViewModel.showUserOfOrderMutableLiveData.observe(this, Observer { t ->


            if (t.data[0].image != null && t.data[0].image.isNotEmpty())
                Picasso.get().load(ApiCreater.USER_URL + t.data[0].image).into(view.img_user)

            mandowpData = t.data[0]

            Log.e(TAG, "kao777777")
            val formatter: NumberFormat = DecimalFormat("#,##")
            val formattedNumber: String = formatter.format(t.data[0].rate.toFloat())


            view.tv_rate.text =formattedNumber
            view.ratingBar.rating = t.data[0].rate.toFloat()
            view.tv_distance.text = t.data[0].phone
            view.tv_user_name.text = t.data[0].first_name+" "+t.data[0].last_name

        })

        Log.e(TAG, "onViewCreated: E ${userInfo.getUserToken()}, $requestId")
        chatViewModel.showOrderById(userInfo.getUserToken(), requestId)
        chatViewModel.showOrderMutableLiveData.observe(this, Observer {

            Log.e("asasas", "${it.data.size}")

            try {
                if (it.data[0].image != null)
                    deliveryImage =it.data[0].image

                // "http://api.deliverymasr.net/uploads/orders/"
            } catch (e: Exception) {

            }


//            re_lat= it.data[0].receiving_latitude
//            re_long= it.data[0].receiving_longitude
//            distance=it.data[0].agent_shope_distance
//            shopName=it.data[0].name

            view.tv.text = it.data[0].name
//            view.cost.text=it.data[0].co
            view.tv_num.text = it.data[0].id + " # "
            phone = it.data[0].phone
//            it.data[0]

            if (it.data[0].payment_id == "1") {
                view.tv_pay_type.text = getString(R.string.cash_type)
            } else {
                view.tv_pay_type.text = getString(R.string.pay_type)
            }

            try {

                if (it.data[0].image != null && it.data[0].image.isNotEmpty())
                    Picasso.get().load(ApiCreater.USER_URL + it.data[0].image).into(view.img_user)

            } catch (e: Exception) {

            }

            realTimeChat()

        })




        view.iv_loc.setOnClickListener {


            val b = Bundle()
            b.putString("requestId", requestId.toString())
            b.putString("type", "receive")
            b.putString(ChatFragment.USER_ID, userId.toString())
            it.findNavController().navigate(R.id.distantFragment, b)


        }

        view.iv_loc_deliver2.setOnClickListener {
            val b = Bundle()
            b.putString("requestId", requestId.toString())
            b.putString("type", "deliver")
            it.findNavController().navigate(R.id.distantFragment, b)
        }

        view.tb_ready.setOnClickListener {
            /*  val dialog = OrderSentDialog(activity!!)
              dialog.show()*/
            Log.e("request_id", requestId.toString())
            val b = Bundle()
            b.putString("requestId", requestId.toString())
            b.putString("payment", payment)
            it.findNavController().navigate(R.id.deliveryFragment, b)

            dialogOrderBegin()

        }

        view.img_user.setOnClickListener { v ->
            val loadingDialog = LoadingDialog(activity!!)
            loadingDialog.show()

            chatViewModel.showUserData(userInfo.getUserToken(), userId)
            chatViewModel.showUserOfOrderMutableLiveData.observe(this, Observer {

                Log.e("aaa 0000011", "requestId userId")
                loadingDialog.dismiss()

                userData = it

            })

            dialogShowUser(v, userData!!.data[0])


        }

        view.img_call.setOnClickListener {

            if (phone.isNotEmpty()) {
                val i = Intent(Intent.ACTION_DIAL)
                i.data = Uri.parse("tel:$phone")
                if (i.resolveActivity(activity!!.packageManager) != null)
                    startActivity(i)
            } else {
                Toast.makeText(
                        activity!!.applicationContext, "Know Error",
                        Toast.LENGTH_SHORT
                ).show()
            }

        }
        db = FirebaseFirestore.getInstance()
                .collection(ApiCreater.OrdersCollection)
                .document(requestId.toString())
                .collection(ApiCreater.ChatCollection)

        mStorageRef = FirebaseStorage.getInstance()
                .getReference(ApiCreater.OrdersStorage)
                .child(requestId.toString())

    }

    @SuppressLint("SetTextI18n")
    fun dialogShowUser(view: View?, user: User) {

        val dialog = Dialog(view!!.context)

        dialog.setContentView(R.layout.dilalog_user_orders)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.cinComments.setOnClickListener(View.OnClickListener {

            val intent = Intent(context, SubActivity::class.java)
            StoresAdapter.fragName = R.layout.fragment_user_rating
//            intent.putExtra(SubActivity.KEY,SubActivity.UserNotes)
            intent.putExtra(SubActivity.KEY, SubActivity.UserNotes)
            intent.putExtra(UserRatingFragment.USER_ID, userId.toString())
            startActivity(intent)

        })

        Log.e("aaa", "requestId  &&  userId")


        if (user.image != null && user.image.isNotEmpty())
            Picasso.get().load(ApiCreater.USER_URL + user.image).into(dialog.civ_user)

        if (user.rate.isNotEmpty())
            dialog.rate.rating = user.rate.toFloat()

        dialog.tv_user_name.text = user.first_name
        dialog.text_order_num.text = user.requestes_count.toString() + " "
        dialog.text_comments_num.text = user.user_comments.toString() + " "

        dialog.show()


    }

    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
    private var db = FirebaseFirestore.getInstance()
            .collection(ApiCreater.OrdersCollection)

    private var mStorageRef: StorageReference = FirebaseStorage.getInstance().getReference(ApiCreater.OrdersStorage).child(requestId.toString())

    private fun realTimeChat() {


        var msg = Message()

        db.orderBy("created_at").addSnapshotListener { querySnapshot, _ ->

            val messages = ArrayList<Message>()

            querySnapshot!!.documents.forEach { documentSnapshot ->
                msg = documentSnapshot.toObject(Message::class.java)!!
                messages.add(msg)
            }



            chatAdapter.submitData(messages)
            try {
                lv_chat.setSelection(chatAdapter.list.size - 1)
            }catch (e:Exception){
                Log.e(TAG, "realTimeChat: ${e.message}")
            }

        }


    }
    private fun dialogOrderBegin() {
        var dialog = Dialog(activity!!)
        dialog.setContentView(R.layout.fragment_confirm_my_order)

        dialog.tv_dear.text="اهلا بك:"
        dialog.txt_confirm.text="تأكد من جدية الطلب لانك الوحيد المسئول عنه"

        dialog.buttoConfirm.setOnClickListener {



            dialog.dismiss()
            dialog.cancel()
        }


        dialog.show()

    }

}
