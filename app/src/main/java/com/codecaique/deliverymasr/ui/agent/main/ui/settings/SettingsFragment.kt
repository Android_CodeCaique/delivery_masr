package com.codecaique.deliverymasr.ui.agent.main.ui.settings

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.ChangeLanguage
import com.codecaique.deliverymasr.base.agent.AuthActivity
import com.codecaique.deliverymasr.base.agent.SubMainActivity
import com.codecaique.deliverymasr.databinding.FragmentSettingsBinding
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.agent.main.ui.useConditions.UsageFragment
import kotlinx.android.synthetic.main.fragment_settings.view.*

/**
 * A simple [Fragment] subclass.
 */
class SettingsFragment : Fragment() {

    lateinit var binding: FragmentSettingsBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentSettingsBinding.inflate(inflater)

        return binding.root
    }


    lateinit var userInfo: UserInfo

    //    lateinit var languageHelper: LanguageHelper
//    lateinit var sharedHelper: SharedHelper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        languageHelper = LanguageHelper()
//        sharedHelper = SharedHelper()
        userInfo = UserInfo(activity!!.applicationContext)

        view.tv_ringtone.text = if (userInfo.getRingtoneName()!!.isNotEmpty()) userInfo.getRingtoneName()
        else
            getString(R.string.default_)

        val intent = Intent(view.context, SubMainActivity::class.java)
        intent.flags=Intent.FLAG_ACTIVITY_NEW_TASK


        view.constraintLang.setOnClickListener {
            Log.i("Language Constraint", "Clicked")
            val changeLanguage = ChangeLanguage()
            changeLanguage.show(childFragmentManager, "ChangeLanguage")
        }

        view.back.setOnClickListener { it.findNavController().popBackStack() }
//        handleLanguageSpinner()
        view.con3.setOnClickListener {
            intent.putExtra(SubMainActivity.KEY, SubMainActivity.PROBLEMS)

            startActivity(intent)
        }
        view.con4.setOnClickListener {

            intent.putExtra(SubMainActivity.KEY, SubMainActivity.EDITPROFILE)
            startActivity(intent)
        }

//    view.con5.setOnClickListener {
//        Toast.makeText(activity,"Clicked",Toast.LENGTH_SHORT).show()
//        val  changeLanguage = ChangeLanguage()
//        changeLanguage.show(childFragmentManager, "ChangeLanguage")
//    }


        if (UserInfo(activity!!).getState() == "mandop")
            view.con10.visibility = View.GONE

        view.con10.setOnClickListener {
            val p0 = Intent(context, AuthActivity::class.java)
            p0.putExtra(AuthActivity.KEY, AuthActivity.JOINUSMANDOWP)
            context!!.startActivity(p0)
        }

        view.con9.setOnClickListener {

            aboutUs()
//            val bundle=Bundle()
//            bundle.putString(UsageFragment.KEY,UsageFragment.AboutUs)
//            it.findNavController().navigate(R.id.usageFragment,bundle)

        }


        view.con14.setOnClickListener {

            val bundle = Bundle()
            bundle.putString(UsageFragment.KEY, UsageFragment.ShowPolicy)
            it.findNavController().navigate(R.id.usageFragment, bundle)

        }


        view.con6.setOnClickListener {

            val bundle = Bundle()
            bundle.putString(UsageFragment.KEY, UsageFragment.UseConditions)
            it.findNavController().navigate(R.id.usageFragment, bundle)

        }


        view.con7.setOnClickListener {

            rateApp()
        }

        view.con2.setOnClickListener {


            val intent1 = Intent(RingtoneManager.ACTION_RINGTONE_PICKER)
            intent1.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select ringtone for notifications:")
            intent1.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, false)
            intent1.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true)
            intent1.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, userInfo.getRingtone())
            intent1.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_ALARM)
            startActivityForResult(intent1, 999)

        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == 999) {

            if (data!!.data != null) {
                view!!.tv_ringtone.text = RingtoneManager.getRingtone(activity!!.applicationContext, data.data!!).getTitle(activity!!.applicationContext)

                userInfo.setRingtone(data.data!!)
                userInfo.setRingtoneName(
                        RingtoneManager.getRingtone(activity!!.applicationContext, data.data!!).getTitle(activity!!.applicationContext)
                )

                Log.e("TAG",
                        RingtoneManager.getRingtone(activity!!.applicationContext, data.data!!).getTitle(activity!!.applicationContext)
                )
            } else {
                view!!.tv_ringtone.text = getString(R.string.default_)
                userInfo.setRingtone(null)
                userInfo.setRingtoneName("")
            }
        }
    }

    fun rateApp() {
        try {
            val rateIntent = rateIntentForUrl("market://details")
            startActivity(rateIntent)
        } catch (e: ActivityNotFoundException) {
            val rateIntent = rateIntentForUrl("https://play.google.com/store/apps/details")
            startActivity(rateIntent)
        }
    }

    private fun rateIntentForUrl(url: String): Intent {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(java.lang.String.format("%s?id=%s", url, activity!!.packageName)))
        var flags = Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_MULTIPLE_TASK
        flags = if (Build.VERSION.SDK_INT >= 21) {
            flags or Intent.FLAG_ACTIVITY_NEW_DOCUMENT or Intent.FLAG_ACTIVITY_NEW_TASK
        } else {
            flags or Intent.FLAG_ACTIVITY_NEW_TASK
        }
        intent.addFlags(flags)
        return intent
    }

    private fun aboutUs() {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://site.hiakk.com"))
        var flags = Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_MULTIPLE_TASK
        flags = if (Build.VERSION.SDK_INT >= 21) {
            flags or Intent.FLAG_ACTIVITY_NEW_DOCUMENT or Intent.FLAG_ACTIVITY_NEW_TASK
        } else {
            flags or Intent.FLAG_ACTIVITY_NEW_TASK
        }
        intent.addFlags(flags)
//        context!!.startActivity(intent)
    }

//    private fun handleLanguageSpinner() {
//        try {
//            view?.spinner?.setItems("اللغات", "English", "العربيه")
//            view?.spinner?.setOnItemSelectedListener { view, position, id, item ->
//                if (position == 1) {
//                    Log.i("Lang", "1")
////                        setLocate("en")
//                    languageHelper.ChangeLang(this.resources , "en")
//                    sharedHelper.putKey(requireContext(),"MyLang","en")
//                    val intent = Intent(context, MainActivity::class.java)
//                    (context as Activity).finish()
//                    (context as Activity).startActivity(intent)
////                    settingViewModel.lang.value = "en"
////                    settingViewModel.changeLang(requireActivity())
//
//                }
//                if (position == 2) {
//
////                        setLocate("ar")
//                    languageHelper.ChangeLang(this.resources , "ar")
//                    sharedHelper.putKey(requireContext(),"MyLang","ar")
//                    val intent = Intent(context, MainActivity::class.java)
//                    (context as Activity).finish()
//                    (context as Activity).startActivity(intent)
//
////                    settingViewModel.lang.value = "ar"
////                    settingViewModel.changeLang(requireActivity())
//
//                }
//            }
//        }catch (e: Exception) {
//            Toast.makeText(requireActivity(), e.message, Toast.LENGTH_SHORT).show()
//        }
//    }
override fun onResume() {
    super.onResume()
    Log.e(this.javaClass.name, "CurrentScreen")

}
}
