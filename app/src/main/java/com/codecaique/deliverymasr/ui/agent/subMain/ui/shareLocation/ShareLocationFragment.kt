package com.codecaique.deliverymasr.ui.agent.subMain.ui.shareLocation

import android.annotation.SuppressLint
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.util.LocationInfo
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_share_location.view.*
import java.io.IOException
import java.util.*

class ShareLocationFragment : Fragment(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    lateinit var locationInfo: LocationInfo
    lateinit var myLocation: LatLng

    lateinit var VIEW: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        VIEW = inflater.inflate(R.layout.fragment_share_location, container, false)
        return VIEW
    }

    @SuppressLint("ResourceAsColor")
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        locationInfo = LocationInfo(activity!!.applicationContext)

        val mapFragment = //(context!! as AppCompatActivity).supportFragmentManager
                childFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment
        mapFragment.getMapAsync(this)

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true/*  enabled by default*/) {
            override fun handleOnBackPressed() {
                // Handle the back button event
                requireActivity().finish()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)

        view.btn.setOnClickListener {

            if (view.et_place.text.toString().isNotEmpty() && view.tv_place.text.toString().isNotEmpty()) {

                locationInfo.setLocationShare(myLocation.latitude.toString(), myLocation.longitude.toString())
                locationInfo.setStreetShare(view.tv_place.text.toString())

                requireActivity().finish()

                // it.findNavController().popBackStack()

            } else {
                if (view.tv_place.text.toString().isEmpty()) {
                    Toast.makeText(activity!!.applicationContext, "اختر الموقع اولا", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }
                if (view.et_place.text.toString().isEmpty()) {
                    view.et_place.error = getString(R.string.enter_need_data)
                    return@setOnClickListener
                }

            }
        }

        view.type_satlite.setOnClickListener {

            mMap.mapType = GoogleMap.MAP_TYPE_SATELLITE

            view.type_satlite.setTextColor(R.color.white)
            view.type_map.setTextColor(R.color.colorPrimary)

            view.type_satlite.background = resources.getDrawable(R.drawable.border_left_fill, null)
            view.type_map.background = resources.getDrawable(R.drawable.border_right, null)


        }


        view.type_map.setOnClickListener {

            mMap.mapType = GoogleMap.MAP_TYPE_NORMAL

            view.type_map.setTextColor(R.color.white)
            view.type_satlite.setTextColor(R.color.colorPrimary)

            view.type_map.background = resources.getDrawable(R.drawable.border_right_fill, null)
            view.type_satlite.background = resources.getDrawable(R.drawable.border_left, null)

        }


    }


    override fun onMapReady(googleMap: GoogleMap?) {

        mMap = googleMap!!
        VIEW.progressBar.visibility = View.GONE
        mMap.mapType = GoogleMap.MAP_TYPE_NORMAL

        val geocode = Geocoder(context, Locale("ar"))//.getDefault())
        var addresses: List<Address>? = null

        Log.e("location ====== ", "locationInfo.geaaaaaaaaaatStreetStart()")

        myLocation = locationInfo.getMyLocation()

        addresses = geocode.getFromLocation(myLocation.latitude, myLocation.longitude, 1)

        mMap.addMarker(MarkerOptions().position(myLocation))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 15f))

        if (addresses != null && addresses.isNotEmpty()) {

            VIEW.tv_place.text = addresses[0].getAddressLine(0)//thoroughfare

        }

        mMap.setOnMapClickListener { latLng ->


            myLocation = latLng
            Log.e("location ====== ", "${latLng.latitude} uu")

            mMap.clear()

            mMap.addMarker(MarkerOptions().position(myLocation))//.title("Marker in Sydney"))
            mMap.moveCamera(CameraUpdateFactory.newLatLng(myLocation))

            try {

                addresses = geocode.getFromLocation(latLng.latitude, latLng.longitude, 1)

                if (addresses != null && addresses!!.isNotEmpty()) {
                    VIEW.tv_place.text = addresses!![0].getAddressLine(0)//thoroughfare
//                    Log.e("qq",addresses[0].getAddressLine(0)+"\n"+addresses[0].getAddressLine(1)+"\n"+addresses[0].getAddressLine(2)+"\n"+addresses[0].getAddressLine(3))
                }

            } catch (ignored: IOException) {
                //do something
            }
        }

    }

    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}