package com.codecaique.deliverymasr.ui.agent.main.ui.totaldeliverymoney;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.codecaique.deliverymasr.data.ApiCreater;
import com.codecaique.deliverymasr.pojo.response.Total_Delivery_Response;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class TotalViewModel extends ViewModel
{
    MutableLiveData<Total_Delivery_Response> data = new MutableLiveData<>();

    public void getTotalSum(String user_token)
    {
        Observable observable =
                ApiCreater
                        .Companion.getInstance()
                        .getTotalDelivery(user_token).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        Observer<Total_Delivery_Response> observer = new Observer<Total_Delivery_Response>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Total_Delivery_Response total_delivery_response) {
                data.setValue(total_delivery_response);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);
    }

}
