package com.codecaique.deliverymasr.ui.agent.subMain.ui.problemDetails

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.response.Answer

class AnswersAdapter(var context: Context) : RecyclerView.Adapter<AnswersAdapter.ViewHolderCoupon>() {

    var list=ArrayList<Answer>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderCoupon {
        val view = LayoutInflater.from(context).inflate(R.layout.item_answer, parent, false)
        return ViewHolderCoupon(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolderCoupon, position: Int) {

        val arrayItem=list[position]

        holder.text.text=arrayItem.name +"   "+arrayItem.answer

    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class ViewHolderCoupon(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var text:TextView = itemView.findViewById(R.id.tv)
    }

    fun setData(a: ArrayList<Answer>) {
        this.list = a
        notifyDataSetChanged()
    }

}