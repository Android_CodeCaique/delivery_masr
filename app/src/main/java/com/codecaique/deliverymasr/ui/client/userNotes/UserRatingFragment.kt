package com.codecaique.deliverymasr.ui.client.userNotes

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.pojo.response.UserRate
import com.codecaique.deliverymasr.utils.Utils
import kotlinx.android.synthetic.main.fragment_user_rating.view.*

class UserRatingFragment : Fragment() {

    lateinit var ratingsOfUsersViewModel: RatingsOfUsersViewModel
    lateinit var userCommentsAdapter: UserCommentsAdapter

    companion object{
        val USER_ID="userID"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_user_rating, container, false)

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true/*  enabled by default*/) {
            override fun handleOnBackPressed() {
//                 Handle the back button event
                requireActivity().finish()
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(this, callback)


        ratingsOfUsersViewModel= ViewModelProviders.of(this).get(RatingsOfUsersViewModel::class.java)
        userCommentsAdapter= UserCommentsAdapter(activity!!.applicationContext)

        view.rv_userRating.adapter=userCommentsAdapter

        val userID=activity!!.intent.extras!!.getString(USER_ID,"0")!!

        ratingsOfUsersViewModel.showRatingOfUser(UserInfo(activity!!.applicationContext).getUserToken(),userID)
        ratingsOfUsersViewModel.notesOfUserMutableLiveData.observe(this, Observer { t->
            if (t.error == 3) {
                Utils.signout(requireActivity() , view!!)
                return@Observer;
            }
            view.progressBar.visibility=View.GONE
            userCommentsAdapter.setData(t.data as ArrayList<UserRate>)
            if (t.data.size == 0)
                view.no_data.visibility = View.VISIBLE

        })

        view.back.setOnClickListener {
            activity!!.finish()
            /*   val chatFrag = ChatFragment()
            val manager = fragmentManager
            manager!!.beginTransaction().replace(R.id.main3_fragment, chatFrag, chatFrag.tag).commit()*/
        }


        return view
    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}