package com.codecaique.deliverymasr.ui.client.restaurant

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.bumptech.glide.Glide
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.util.LocationInfo
import com.codecaique.deliverymasr.pojo.request.MenuItemOrder
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import com.codecaique.deliverymasr.ui.client.restaurant.adapters.MenuOrdersAdapter
import com.codecaique.deliverymasr.ui.client.tardHayak.MakeOrderViewModel
import kotlinx.android.synthetic.main.dialog_add_copoun.*
import kotlinx.android.synthetic.main.dialog_order_wait.*
import kotlinx.android.synthetic.main.fragment_order_food.view.*
import kotlinx.android.synthetic.main.fragment_order_food.view.rg_payment
import okhttp3.MediaType
import okhttp3.RequestBody
import java.util.*
import kotlin.collections.ArrayList

@Suppress("UNCHECKED_CAST", "UNREACHABLE_CODE")
class OrderFoodFragment : Fragment() {

    private val TAG = "OrderFoodFragment"
    companion object {
        val ITEMS = "items"
        val TIME = "time"
        val ShopId = "shopId"
        const val ShopImage = "shopImage"
    }

    var payment = 1
    var coupon = "1"

    lateinit var VIEW: View
    lateinit var makeOrderViewModel: MakeOrderViewModel
    lateinit var locationInfo: LocationInfo

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        VIEW = inflater.inflate(R.layout.fragment_order_food, container, false)
        makeOrderViewModel = ViewModelProviders.of(this).get(MakeOrderViewModel::class.java)
        locationInfo = LocationInfo(activity!!.applicationContext)

        val items = arguments!!.getSerializable(ITEMS)
        val shopId = arguments!!.getInt(ShopId)
        val time = arguments!!.getString(TIME)

        Log.e("PRICE details",items.toString())

        val name = arguments!!.getString("name")
        val photo = arguments!!.getString(ShopImage)

        try {

            Glide.with(activity!!).load(ApiCreater.STORES_URL+photo).into(VIEW.civ_restaurant)

        }catch (e:Exception){}

        val menuOrdersAdapter = MenuOrdersAdapter(activity!!.applicationContext, VIEW.tv_total)
        VIEW.rv_details.adapter = menuOrdersAdapter
        VIEW.header.text = name

        var total = 0F
        (items as ArrayList<MenuItemOrder>).forEach {
            total += it.price.toFloat()
        }

        VIEW.tv_total.text = total.toString()

        menuOrdersAdapter.setData(items as ArrayList<MenuItemOrder>)

        VIEW.addCoupon.setOnClickListener { dialogAddCoupon() }

//        VIEW.con2.setOnClickListener { dialogPayment() }

        VIEW.rg_payment.setOnCheckedChangeListener { _, i ->

            when (i) {
                R.id.rb_cash -> payment = 1
                R.id.rb_credit -> payment = 2
            }

        }


        VIEW.btn_completOrder.setOnClickListener {

            val loadingDialog = LoadingDialog(activity!!)
            loadingDialog.show()
            loadingDialog.setCancelable(false)
            val currentTime = Calendar.getInstance().time

            Log.e("TAGAGA", menuOrdersAdapter.getMenuItem().toString())
            Log.e("msg ", shopId.toString())


            Log.e(TAG, "onCreateView: sentTard 1")
            makeOrderViewModel.sentTard(
                    RequestBody.create(MediaType.parse("multipart/form-data"), locationInfo.getUserToken()),
                    shopId,
                    RequestBody.create(MediaType.parse("multipart/form-data"), locationInfo.getStreetStart()),
                    locationInfo.getLocationStart().latitude,
                    locationInfo.getLocationStart().longitude,
                    RequestBody.create(MediaType.parse("multipart/form-data"), locationInfo.getStreetEnd()),
                    locationInfo.getLocationEnd().latitude,
                    locationInfo.getLocationEnd().longitude,
                    currentTime,
                    RequestBody.create(MediaType.parse("multipart/form-data"), VIEW.et_details.text.toString() + " "),
                    null,
                    RequestBody.create(MediaType.parse("multipart/form-data"), coupon),
                    payment,
                    RequestBody.create(MediaType.parse("multipart/form-data"),menuOrdersAdapter.getMenuItem().toString()),
                    menuOrdersAdapter.getTotalPrice().toDouble(),loadingDialog,context!!
//                    )
            )

            makeOrderViewModel.tardHayyakMutableLiveData.observe(this, Observer { t ->

                loadingDialog.dismiss()

                if (t.error == 0) {
                    Log.e("msg", t.message)
                    Log.e("msg", t.data.id.toString())
                    Log.e("msg", locationInfo.getUserToken())

                    it.findNavController().navigate(R.id.orderSented)
                } else if (t.error == 4) {
                    waiteOrderDialog()
                } else {
                    Toast.makeText(activity!!.applicationContext, t.message, Toast.LENGTH_SHORT).show()
                }

            })

            /*val orderSented = OrderSentFragment()
            val manager = fragmentManager
            manager!!.beginTransaction().replace(R.id.main3_fragment, orderSented, orderSented.tag).commit()*/

        }

        return VIEW
    }

    private fun dialogAddCoupon() {

        val dialog = Dialog(activity!!)
        dialog.setContentView(R.layout.dialog_add_copoun)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.btn_done.setOnClickListener {

            if (dialog.tv_camera.text.toString().isNotEmpty()) {
                coupon = dialog.tv_camera.text.toString()
                dialog.dismiss()
            } else {
                dialog.tv_camera.error = "ادخل كوبونك اولا "
            }

        }


        dialog.show()
    }

    private fun dialogPayment() {
       /* val dialog = Dialog(view!!.context)
        dialog.setContentView(R.layout.dialog_pay_method)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        (dialog.window!!.decorView as ViewGroup)
                .getChildAt(0).startAnimation(AnimationUtils.loadAnimation(
                        context, android.R.anim.slide_out_right))


        if (payment == 1) {
            dialog.done_cash.visibility = View.VISIBLE
            dialog.done_credit.visibility = View.INVISIBLE

        } else {
            dialog.done_credit.visibility = View.VISIBLE
            dialog.done_cash.visibility = View.INVISIBLE

        }
        dialog.credit!!.setOnClickListener {

            payment = 2

            VIEW.iv2.setImageResource(R.drawable.payment)
            VIEW.tv4.text = "credit"

            dialog.done_credit.visibility = View.VISIBLE
            dialog.done_cash.visibility = View.INVISIBLE
            dialog.dismiss()

        }

        dialog.cash!!.setOnClickListener {

            payment = 1

            VIEW.iv2.setImageResource(R.drawable.euro)
            VIEW.tv4.text = "cash"

            dialog.done_cash.visibility = View.VISIBLE
            dialog.done_credit.visibility = View.INVISIBLE
            dialog.dismiss()

        }
        dialog.show()*/
    }

    private fun waiteOrderDialog() {
        val dialog = Dialog(view!!.context)
        dialog.setContentView(R.layout.dialog_order_wait)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.show()

        dialog.tv_ok.setOnClickListener {
            activity!!.finish()
        }

    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}