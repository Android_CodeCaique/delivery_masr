package com.codecaique.deliverymasr.ui.agent.subMain.ui.problems_main

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codecaique.deliverymasr.pojo.response.GeneralResponse
import com.codecaique.deliverymasr.pojo.response.ProblemDetailsResponse
import com.codecaique.deliverymasr.pojo.response.ProblemsResponse
import com.codecaique.deliverymasr.pojo.response.ShowComplainQuestionsResponse
import com.codecaique.deliverymasr.data.ApiCreater
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class ProblemsViewModel :ViewModel(){

    val showProblemsMutableLiveData = MutableLiveData<ProblemsResponse>()
    val showQuestionsMutableLiveData = MutableLiveData<ShowComplainQuestionsResponse>()
    val makeProblemsMutableLiveData = MutableLiveData<GeneralResponse>()
    val makeProblemByIdMutableLiveData = MutableLiveData<ProblemDetailsResponse>()

    fun showComplianceById( user_token: String
                            , id: String)

    {
        val observable =
                ApiCreater.instance.showComplianceById(user_token, id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object  : Observer<ProblemDetailsResponse> {
            override fun onComplete() {

            }


            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: ProblemDetailsResponse) {
                makeProblemByIdMutableLiveData.value = t
            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)
    }


    fun showProblems(token:String )
    {
        val observable =
                ApiCreater.instance.showProblems(token)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object  : Observer<ProblemsResponse> {
            override fun onComplete() {

            }


            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: ProblemsResponse) {
                showProblemsMutableLiveData.value = t
            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)
    }


    fun showComplainQuestions(token:String )
    {
        val observable =
                ApiCreater.instance.showComplainQuestions(token)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object  : Observer<ShowComplainQuestionsResponse> {
            override fun onComplete() {

            }


            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: ShowComplainQuestionsResponse) {
                showQuestionsMutableLiveData.value = t
            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)
    }

   /* fun showProblems(user_token : RequestBody,
                     user_id:RequestBody,
                     order_id: Int,
                     comment: RequestBody?,
                     reason: Int,
                     image: MultipartBody.Part?,
                     notes: RequestBody?,
                     details: RequestBody?,
                     answers: JSONArray)
    {
        val observable =
                ApiCreater.instance.sentShakway(user_token,user_id, order_id, comment, reason, image, notes, details, answers)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object  : Observer<GeneralResponse> {
            override fun onComplete() {

            }


            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: GeneralResponse) {
                makeProblemsMutableLiveData.value = t
            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)
    }
*/


}