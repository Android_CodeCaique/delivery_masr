package com.codecaique.deliverymasr.ui.agent.subMain.ui.postproblem

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.pojo.response.Question
import com.codecaique.deliverymasr.pojo.response.Reason
import com.codecaique.deliverymasr.ui.agent.subMain.ui.problems_main.ProblemsViewModel
import com.codecaique.deliverymasr.ui.client.chat.ChatViewModel
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.dialog_problem_questions.*
import kotlinx.android.synthetic.main.fragment_post_problem.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File


class PostProblemFragment : Fragment(R.layout.fragment_post_problem) {

    private val TAG = "PostProblemFragment"
    lateinit var chatViewModel: ChatViewModel
    lateinit var userInfo: UserInfo
    var reasons = ArrayList<String>()
    var reasonsList = ArrayList<Reason>()
    var uri: Uri? = null
    lateinit var VIEW: View
    var userId = 0
    var orderId = 0
    lateinit var loadingDialog: LoadingDialog

    lateinit var problemViewModel: ProblemsViewModel

    lateinit var questionsAdapter: QuestionsAdapter

    companion object {
        val USER_ID = "user-id"
        val ORDER_ID = "order-id"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true/*  enabled by default*/) {
            override fun handleOnBackPressed() {
                // Handle the back button event

                activity!!.finish()

            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(this, callback)

        chatViewModel = ViewModelProviders.of(this).get(ChatViewModel::class.java)
        problemViewModel = ViewModelProviders.of(this).get(ProblemsViewModel::class.java)
        userInfo = UserInfo(activity!!.applicationContext)
        userId = arguments!!.getString(USER_ID)!!.toInt()
        orderId = arguments!!.getString(ORDER_ID)!!.toInt()
        loadingDialog = LoadingDialog(activity!!)

        Log.e(TAG, "${userInfo.getId()}  z $orderId")

        questionsAdapter = QuestionsAdapter(activity!!)

        problemViewModel.showComplainQuestions(userInfo.getUserToken())
        problemViewModel.showQuestionsMutableLiveData.observe(this, Observer { t ->

            Log.e(TAG, "onViewCreated: ds ${t.data.size}")

            showDialog(t.data)


        })

        view.back.setOnClickListener {
            activity!!.finish()
        }

        chatViewModel.showReasons(userInfo.getUserToken())
        chatViewModel.showReasonsMutableLiveData.observe(this, Observer { t ->

            reasonsList = t.data as ArrayList<Reason>

            t.data.forEach {

                reasons.add(it.text)
            }

            view.spinner.adapter = ArrayAdapter(activity!!, android.R.layout.simple_list_item_1, reasons)
        })

        view.image.setOnClickListener {
            isStoragePermissionGranted()
//                gotoImage()
        }

        view.image_.setOnClickListener {
            isStoragePermissionGranted()
//                gotoImage()
        }

        view.imageq.setOnClickListener {
            isStoragePermissionGranted()
//                gotoImage()
        }

        view.send.setOnClickListener {

            Log.e("shakway", ",kjkl")

            loadingDialog.show()

            Log.e("datadta", "${userInfo.getUserToken()} z ${userId} z ${orderId} z ${reasonsList[view.spinner.selectedItemPosition].text + "test"} z" +
                    "${reasonsList[view.spinner.selectedItemPosition].id} z ${view.et_notes.text.toString()} z " +
                    "${view.et_notes.text.toString()} " +
                    "${questionsAdapter.getAnswers()}")

            chatViewModel.sentShakaway(
                    RequestBody.create(MediaType.parse("multipart/form-data"), userInfo.getUserToken()),
                    RequestBody.create(MediaType.parse("multipart/form-data"), userId.toString() + ""),
                    RequestBody.create(MediaType.parse("multipart/form-data"), orderId.toString() + ""),
                    /*RequestBody.create(MediaType.parse("multipart/form-data"),""),
                    RequestBody.create(MediaType.parse("multipart/form-data"),""),
                    null,
                    RequestBody.create(MediaType.parse("multipart/form-data"),""),
                    RequestBody.create(MediaType.parse("multipart/form-data"),""),
                    JSONArray()*/
                    RequestBody.create(MediaType.parse("multipart/form-data"), view.spinner.selectedItem.toString() + " "),
                    RequestBody.create(MediaType.parse("multipart/form-data"), reasonsList[view.spinner.selectedItemPosition].text + " "),
                    //   reasonsList[view.spinner.selectedItemPosition],/*view.spinner.selectedItem.toString() +*/
                    if (uri != null) getImage() else null,
                    RequestBody.create(MediaType.parse("multipart/form-data"), view.et_notes.text.toString()),
                    RequestBody.create(MediaType.parse("multipart/form-data"), ""),
                    RequestBody.create(MediaType.parse("multipart/form-data"), questionsAdapter.getAnswers().toString())
            )

            chatViewModel.sentShakwayMutableLiveData.observe(this, Observer {

                val map = HashMap<String, Boolean>()
                map[
                        if (userInfo.getState().contains("user"))
                            ApiCreater.ProblemUploadedForUser
                        else
                            ApiCreater.ProblemUploadedForDriver
                ] = true


                FirebaseFirestore.getInstance()
                        .collection(ApiCreater.OrdersCollection)
                        .document(orderId.toString())
                        .update(map as Map<String, Any>)
                        .addOnSuccessListener { _ ->


                            loadingDialog.dismiss()

                            if (it.error == 0) {
                                Toast.makeText(activity!!, "تم ارسال مشكلتك بنجاح وسيتم الرد عليك لاحقا", Toast.LENGTH_SHORT).show()
                                activity!!.finish()
                            } else {
                                Toast.makeText(activity!!, it.message, Toast.LENGTH_SHORT).show()
                            }
                        }
            })

        }


    }

    private fun showDialog( data: List<Question>) {

        val dialog = Dialog(activity!!)
        dialog.setContentView(R.layout.dialog_problem_questions)
        dialog.setCancelable(false)

        Log.e(TAG, "showDialog: before ${data.size}")

        dialog.rv_que.adapter = questionsAdapter
        questionsAdapter.setData(data as ArrayList<Question>)

        Log.e(TAG, "showDialog: after ${data.size}")

        dialog.btn_close.setOnClickListener {


            dialog.dismiss()
        }

        dialog.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 111 && Activity.RESULT_OK == resultCode) {
            data?.let {
                view!!.image.setImageURI(data.data!!)
                uri = data.data!!
                view!!.image_.visibility=View.GONE
                view!!.tv_add_more.visibility=View.GONE
            }
        }
    }

    fun getImage(): MultipartBody.Part? {
        val p: String
        val cursor = activity!!.contentResolver.query(uri!!, null, null, null, null)

        p = if (cursor == null) {
            uri!!.path.toString()
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            cursor.getString(idx)
        }

        val file = File(p)
        val requestFile = RequestBody.create(MediaType.parse("image/*"), file)
        return MultipartBody.Part.createFormData("image[]", file.name, requestFile)
    }

    @SuppressLint("WrongConstant")
    fun isStoragePermissionGranted(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                gotoImage()

                true
            } else {

                ActivityCompat.requestPermissions(
                        activity!!,
                        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        111
                )
                false
            }
        } else {

            gotoImage()
            return true
        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            gotoImage()
        } else {
            /*
        Toast.makeText(applicationContext, R.string.imagePprem_accept, Toast.LENGTH_SHORT)
                .show()
*/
        }
    }

    private fun gotoImage() {
        val intent = Intent(
                Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 111)
    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }

}