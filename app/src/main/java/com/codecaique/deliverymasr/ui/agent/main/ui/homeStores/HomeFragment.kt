package com.codecaique.deliverymasr.ui.agent.main.ui.homeStores

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.pojo.response.Category
import com.codecaique.deliverymasr.pojo.response.NearStore
import com.codecaique.deliverymasr.ui.client.stores.StoresViewModel
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.fragment_home.view.*
import java.util.*
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 */

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class HomeFragment : Fragment() {

    lateinit var storesViewModel: StoresViewModel
    lateinit var userInfo: UserInfo
    lateinit var categoriesAdapter: CategoriesAdapter
    lateinit var nearStoresAdapter: NearStoresAdapter
    var catList = ArrayList<Category>()
    var stores = ArrayList<NearStore>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

//        RxJavaPlugins.setErrorHandler {}

        val view= inflater.inflate(R.layout.fragment_home, container, false)


        Log.e("data", FirebaseInstanceId.getInstance().getToken().toString())


        storesViewModel = ViewModelProviders.of(this).get(StoresViewModel::class.java)
        nearStoresAdapter = NearStoresAdapter(activity!!)
        categoriesAdapter = CategoriesAdapter(activity!!.applicationContext)

        view.rv_cat.adapter = categoriesAdapter
        view.rv_marketList.adapter = nearStoresAdapter


        getNearStoresAndCategories(view)

        view.et_search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                filter(p0.toString().trim())
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })
        return view
    }

    private fun getNearStoresAndCategories(view: View) {

        val token: String = UserInfo(activity!!).getUserToken()

        storesViewModel.getNearStoresAndCategories(token)//userInfo.getUserToken())
        storesViewModel.nearStoresMutableLiveData.observe(this, androidx.lifecycle.Observer { t ->

            view.progressBar1.visibility = View.GONE
            nearStoresAdapter.setData(t.data as ArrayList<NearStore>)

            stores = t.data as ArrayList<NearStore>

            catList.clear()
            catList.add(Category(0, "", "الكل"))
            catList.addAll(t.category as ArrayList<Category>)

            categoriesAdapter.setData(catList)
            categoriesAdapter.setNearAdapter(nearStoresAdapter)

            Log.e("tag ", "${t.data.size}   ${t.category.size}")

        })




    }

    fun filter(name: String) {

        val search = ArrayList<NearStore>()

        stores.forEach {

            if (it.name.toLowerCase(Locale.ROOT).contains(name.toLowerCase(Locale.ROOT)))
                search.add(it)

        }

        nearStoresAdapter.setData(search)

    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }

}

/*     val intent=Intent(context, SubMainActivity::class.java)
            intent.putExtra(SubMainActivity.KEY, SubMainActivity.ORDERDETAILS)
            context.startActivity(intent)
*/