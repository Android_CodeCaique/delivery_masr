package com.codecaique.deliverymasr.ui.client.allSitting

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codecaique.deliverymasr.pojo.response.UserDataCountsResponse
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.response.TurnNotificationResponse
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class SettingsViewModel :ViewModel(){

    val settingsMutableLiveData=MutableLiveData<UserDataCountsResponse>()
    val turnNotificationsMutableLiveData = MutableLiveData<TurnNotificationResponse>()

    fun getSettings(token:String){

            val observable =
                    ApiCreater.instance.userDataCounts(token)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread());

            val observer = object : Observer<UserDataCountsResponse> {
                override fun onComplete() {

                }

                override fun onSubscribe(d: Disposable) {
                }

                override fun onNext(t: UserDataCountsResponse) {

                    settingsMutableLiveData.value = t

                }

                override fun onError(e: Throwable) {
                    Log.e("ss", e.message)
                }
            }

            observable.subscribe(observer)

        }


    fun turnNotifications(token: String) {

        val observable =
                ApiCreater.instance.turnOnNotifications(token)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<TurnNotificationResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: TurnNotificationResponse) {

                turnNotificationsMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }


}