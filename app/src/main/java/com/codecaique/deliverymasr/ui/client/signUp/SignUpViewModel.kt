package com.codecaique.deliverymasr.ui.client.signUp

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.telephony.SmsManager
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.widget.ProgressBar
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.StoresMainActivity
import com.codecaique.deliverymasr.base.client.MainActivity
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.response.*
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.client.otp.OtpFragment
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_mobile_verification.view.*
import kotlinx.android.synthetic.main.fragment_mobile_verification.view.progress_bar
import kotlinx.android.synthetic.main.fragment_sign_in.view.*
import kotlinx.android.synthetic.main.fragment_splash2.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody

class SignUpViewModel : ViewModel() {

    val enterUserDataMutableLiveData = MutableLiveData<RegistrationResponse>()
    val signUpMutableLiveData = MutableLiveData<PhoneResponse>()
    val notNowMutableLiveData = MutableLiveData<NotNowResponse>()
    val checkEmailMutableLiveData = MutableLiveData<CheckEmailResponse>()
    val logoutMutableLiveData = MutableLiveData<GeneralResponse>()



    fun checkEmail( email: String)
    {
        val observable =
                ApiCreater.instance.checkEmail(email)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<CheckEmailResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: CheckEmailResponse) {

                checkEmailMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }

    fun notNow()
    {
        val observable =
                ApiCreater.instance.notNow(3)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<NotNowResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: NotNowResponse) {

                notNowMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)
    }

    fun phoneSignUpClient(
            country: RequestBody,
            phone: RequestBody,
            firebase_token: RequestBody,
            progressBar: ProgressBar,
            context:Context,
            view:View
    ) {


        progressBar.visibility=View.VISIBLE
        view.btn_not_now.visibility=View.GONE

        val observable = ApiCreater.instance.phoneSignUp(country, phone,firebase_token)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<PhoneResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: PhoneResponse) {
                Log.e("ss ssss", t.message)

                /*progressBar.visibility = GONE
                signUpMutableLiveData.value = t*/

                val smsManager = SmsManager.getDefault()
//                    smsManager.sendTextMessage(phone, null, "12343", null, null);

                if (t.error == 0) {
                    /*  val b = Bundle()
                  b.putString(Fragment_Code_Verification.PHONE, view.phone_code.text.toString() + phone)
                  b.putBoolean("isSigned",isSigned)
                  UserInfo(activity!!.applicationContext).setUserData(t.data.id, t.data.phone, "user","")
                  it.findNavController().navigate(R.id.action_fragment_Mobile_Verification2_to_fragment_Code_Verification, b)

                  //     navController.navigate(R.id.action_fragment_Mobile_Verification2_to_fragment_SignUp)

              } else {
                  isSigned = true
//                        if (t.data.state=="user") {

                      val b = Bundle()
                      b.putString(Fragment_Code_Verification.PHONE, view.phone_code.text.toString() + phone)
                      b.putBoolean("isSigned",isSigned)
                      b.putSerializable("data",t.data)
                      it.findNavController().navigate(R.id.action_fragment_Mobile_Verification2_to_fragment_Code_Verification, b)*/

/*
                        } else {
                            Toast.makeText(activity!!.applicationContext, "رقم التليفون موجود بالفعل", Toast.LENGTH_LONG).show()
*/
                    Log.e("MOB USER", "NEW PHONE")

                    UserInfo(context).setUserData(t.data.id, t.data.phone, "user", "")
                    Log.e("TAG", "onViewCreated: data.id ${t.data.id}")
                    val bundle = Bundle()
                    bundle.putString("code", "+2")
                    bundle.putString("phone",  t.data.phone)

//                    Log.e("CODE verify", view.phone_code.text.toString())

                    view.findNavController().navigate(R.id.fragment_SignUp, bundle)

                } else {
                    progressBar.visibility = GONE
                    view.btn_not_now.visibility=View.VISIBLE

//                    val bundle=Bundle()
//                    bundle.putSerializable(OtpFragment.DATA,t.data)
//                    view.findNavController().navigate(R.id.otpFragment,bundle)

                    UserInfo(context).setUserData(t.data )
                    UserInfo(context).setState("user")
//                            activity!!.startActivity(Intent(activity!!.applicationContext, MainActivity::class.java))
                    val intent = Intent(context, MainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                    context.startActivity(intent)



//                        }

                }


            }

            override fun onError(e: Throwable) {
                Log.e("ss 00", e.message)
                progressBar.visibility = GONE
                view.btn_not_now.visibility=View.VISIBLE
               // Toast.makeText(this,"Connection Error",Toast.LENGTH_LONG)
            }
        }

        observable.subscribe(observer)

    }


    fun enterUserData(
            user_id: Int?,
            first_name: String,
            last_name: String,
            passwords: RequestBody,
            image: MultipartBody.Part?,
            phone: RequestBody,
            email: RequestBody,
            gender_id: Int,
            longitude: Double,
            latitude: Double,
            firebase_token: RequestBody,
            progressBar: ProgressBar,
            context:Context

    ) {

        val name = first_name.split(" ")
        var lName = ""
        val fName = name[0]
        if (name.size > 1) for (i in name.indices) {
            if (i != 0)
                lName += name[i]
        }

        val observable =
                ApiCreater.instance.enterUserData(user_id,
                                RequestBody.create(MediaType.parse("multipart/form-data"), first_name),
                                RequestBody.create(MediaType.parse("multipart/form-data"), last_name)
                                , passwords, image, phone, email, gender_id, longitude, latitude, firebase_token)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<RegistrationResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: RegistrationResponse) {
                enterUserDataMutableLiveData.value = t

                progressBar.visibility = View.INVISIBLE


                Log.e("ss done", t.message)


            }

            override fun onError(e: Throwable) {



                Log.e("ssssss error", e.message)
                Log.e("ssssss error2", e.localizedMessage + " sad")

                progressBar.visibility = View.INVISIBLE



                Toast.makeText(context,"There is an error please try again",LENGTH_SHORT).show()




            }
        }

        observable.subscribe(observer)

    }

    fun logout(
            user_token:String
    ) {

        val observable =
                ApiCreater.instance.logout(
                        RequestBody.create(MediaType.parse("multipart/form-data"), user_token))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<GeneralResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: GeneralResponse) {

                logoutMutableLiveData.value = t
                Log.e("ss  00000", t.message)


            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }


    fun getCountries(): HashMap<String, String> {

        val map = HashMap<String, String>()

        map["مصر"] = "+2"

        return map
    }



}