package com.codecaique.deliverymasr.ui.client.shareLocation

import android.annotation.SuppressLint
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.util.LocationInfo
import com.codecaique.deliverymasr.pojo.response.Branche
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_share_location.view.*
import java.io.IOException
import java.util.*


class ShareLocationFragment : Fragment(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    lateinit var locationInfo: LocationInfo
    lateinit var myLocation: LatLng
    val TAG="ShareLocationFragment"

    companion object {
        val KEY = "key"
        val Chat = "chat"
        val TardHayyakStart = "TardHayyakFragmentStart"
        val TardHayyakEnd = "TardHayyakFragmentEnd"

        val StoreStart = "StoreStart"
        val StoreEnd = "StoreEnd"
        val NearPlaces = "near_places"

    }

    lateinit var VIEW: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        VIEW = inflater.inflate(R.layout.fragment_share_location, container, false)
        return VIEW

    }

    @SuppressLint("ResourceAsColor", "NewApi")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true/*  enabled by default*/) {
            override fun handleOnBackPressed() {
                // Handle the back button event
                view.findNavController().popBackStack()
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(this, callback)




        locationInfo = LocationInfo(activity!!.applicationContext)

        val mapFragment = //(context!! as AppCompatActivity).supportFragmentManager
                childFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment
        mapFragment.getMapAsync(this)

        try {

            val key = arguments!!.getString(KEY)!!

            if ( key == StoreEnd)
                view.et_place.visibility = View.GONE

            Log.e(TAG, "onViewCreated: $key")

            view.back.setOnClickListener { it.findNavController().popBackStack() }

            view.btn.setOnClickListener {

                if (view.tv_place.text.toString().isNotEmpty()) {
                    when (key) {
                        TardHayyakStart -> {
                            locationInfo.setLocationStart(myLocation.latitude.toString(), myLocation.longitude.toString())
                            locationInfo.setStreetStart(
                                    if (view.et_place.text.toString().isNotEmpty())
                                        view.et_place.text.toString() + " " + VIEW.tv_place.text.toString()
                                    else
                                        VIEW.tv_place.text.toString()
                            )
                        }
                        TardHayyakEnd -> {
                            locationInfo.setLocationEnd(myLocation.latitude.toString(), myLocation.longitude.toString())
                            locationInfo.setStreetEnd(
                                    if (view.et_place.text.toString().isNotEmpty())
                                        view.et_place.text.toString() + " " + VIEW.tv_place.text.toString()
                                    else
                                        VIEW.tv_place.text.toString()
                            )
                        }
                        StoreStart -> {
                            locationInfo.setLocationStart(myLocation.latitude.toString(), myLocation.longitude.toString())
                            locationInfo.setStreetStart(
                                    if (view.et_place.text.toString().isNotEmpty())
                                        view.et_place.text.toString() + " " + VIEW.tv_place.text.toString()
                                    else
                                        VIEW.tv_place.text.toString()
                            )
                        }
                        StoreEnd -> {
                            locationInfo.setLocationEnd(myLocation.latitude.toString(), myLocation.longitude.toString())
                            locationInfo.setStreetEnd(
                                    if (view.et_place.text.toString().isNotEmpty())
                                        view.et_place.text.toString() + " " + VIEW.tv_place.text.toString()
                                    else
                                        VIEW.tv_place.text.toString()
                            )
                        }
                    }


                    it.findNavController().popBackStack()

                } else {
                    if (view.tv_place.text.toString().isEmpty()) {
                        Toast.makeText(activity!!.applicationContext, "اختر الموقع اولا", Toast.LENGTH_LONG).show()
                        return@setOnClickListener
                    }

                }
                //   it.findNavController().navigate(R.id.chatFragment)

            }

        } catch (e: Exception) {
        }


        view.type_satlite.setOnClickListener {

            mMap.mapType = GoogleMap.MAP_TYPE_SATELLITE

            view.type_map.setTextColor(R.color.colorPrimary)
            view.type_satlite.setTextColor(Color.parseColor("#ffffff"))

            view.type_satlite.background = resources.getDrawable(R.drawable.border_left_fill, null)
            view.type_map.background = resources.getDrawable(R.drawable.border_right, null)


        }


        view.type_map.setOnClickListener {

            mMap.mapType = GoogleMap.MAP_TYPE_NORMAL

            view.type_map.setTextColor(Color.parseColor("#ffffff"))
            view.type_satlite.setTextColor(R.color.colorPrimary)

            view.type_map.background = resources.getDrawable(R.drawable.border_right_fill, null)
            view.type_satlite.background = resources.getDrawable(R.drawable.border_left, null)

        }


    }

    override fun onMapReady(googleMap: GoogleMap?) {

        mMap = googleMap!!
        VIEW.progressBar.visibility = View.GONE
        mMap.mapType = GoogleMap.MAP_TYPE_NORMAL

        val geocode = Geocoder(context, Locale("ar"))//.getDefault())
        var addresses: List<Address>? = null

        Log.e("location ====== ", "locationInfo.geaaaaaaaaaatStreetStart()")


        val key = arguments!!.getString(KEY)!!


        if (key == StoreEnd) {

            val branches = arguments!!.getSerializable("branches") as ArrayList<Branche>

            branches.forEach {

                addresses = geocode.getFromLocation(it.latitude.toDouble(), it.longitude.toDouble(), 1)

                Log.e("marker", it.id.toString())
                val latLng = LatLng(it.latitude.toDouble(), it.longitude.toDouble())

                mMap.addMarker(MarkerOptions().position(latLng).title(it.name))

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))

                //                marker.tag = it.id;

            }

        } else if (key == NearPlaces) {
            myLocation = locationInfo.getMyLocation()

            addresses = geocode.getFromLocation(myLocation.latitude, myLocation.longitude, 1)

            mMap.addMarker(MarkerOptions().position(myLocation))
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 15f))

            if (addresses != null && addresses!!.isNotEmpty()) {

                VIEW.tv_place.text = addresses!![0].getAddressLine(0)//thoroughfare

            }
        } else {
            myLocation = locationInfo.getMyLocation()

            addresses = geocode.getFromLocation(myLocation.latitude, myLocation.longitude, 1)

            mMap.addMarker(MarkerOptions().position(myLocation))
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 15f))

            if (addresses != null && addresses!!.isNotEmpty()) {

                VIEW.tv_place.text = addresses!![0].getAddressLine(0)//thoroughfare

            }
        }

        mMap.setOnMarkerClickListener {
            if (key == StoreEnd) {

                addresses = geocode.getFromLocation(it.position.latitude, it.position.longitude, 1)
                myLocation = LatLng(it.position.latitude, it.position.longitude)

                if (addresses != null && addresses!!.isNotEmpty()) {

                    VIEW.tv_place.text = addresses!![0].getAddressLine(0)//thoroughfare
                    VIEW.et_place.setText(it.title)

                }
            }
            false

        }


        /*
        try {

            addresses = geocode.getFromLocation(myLocation.latitude, myLocation.longitude, 1)

            if (addresses != null && addresses!!.isNotEmpty()) {
                VIEW.tv_place.text = addresses!![0].getAddressLine(0)//thoroughfare
//                    Log.e("qq",addresses[0].getAddressLine(0)+"\n"+addresses[0].getAddressLine(1)+"\n"+addresses[0].getAddressLine(2)+"\n"+addresses[0].getAddressLine(3))
            }

        } catch (ignored: IOException) {
            //do something
        }
*/


        mMap.setOnMapClickListener { latLng ->

            if (key != StoreEnd) {

                myLocation = latLng
                Log.e("location ====== ", "${latLng.latitude} uu")

                mMap.clear()

                mMap.addMarker(MarkerOptions().position(myLocation))//.title("Marker in Sydney"))
                mMap.moveCamera(CameraUpdateFactory.newLatLng(myLocation))

                try {

                    addresses = geocode.getFromLocation(latLng.latitude, latLng.longitude, 1)

                    if (addresses != null && addresses!!.isNotEmpty()) {
                        VIEW.tv_place.text = addresses!![0].getAddressLine(0)//thoroughfare
//                    Log.e("qq",addresses[0].getAddressLine(0)+"\n"+addresses[0].getAddressLine(1)+"\n"+addresses[0].getAddressLine(2)+"\n"+addresses[0].getAddressLine(3))
                    }

                } catch (ignored: IOException) {
                    //do something
                }
            }
        }

    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}