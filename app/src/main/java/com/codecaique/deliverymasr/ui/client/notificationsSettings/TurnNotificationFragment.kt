package com.codecaique.deliverymasr.ui.client.notificationsSettings

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import com.codecaique.deliverymasr.ui.client.notification.NotificationsViewModel
import kotlinx.android.synthetic.main.fragment_turn_notification.view.*
import java.util.*


/**
 * A simple [Fragment] subclass.
 */
class TurnNotificationFragment : Fragment() {

    lateinit var navController: NavController
    lateinit var userInfo: UserInfo
    lateinit var notificationsViewModel: NotificationsViewModel
    var loadingDialog: LoadingDialog?=null




    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_turn_notification, container, false)


        view.iv_backNotifications.setOnClickListener {
            val fm: FragmentManager = activity!!.supportFragmentManager
            fm.popBackStack()
        }



        loadingDialog = LoadingDialog(activity!!)
        notificationsViewModel = ViewModelProviders.of(this).get(NotificationsViewModel::class.java)
        userInfo = UserInfo(activity!!.applicationContext)



        view.switchNotifi.isChecked = userInfo.getNotifiState() == "on"

        view.switchNotifi.setOnCheckedChangeListener { _, _ ->

            loadingDialog!!.show()
            notificationsViewModel.turnNotifications(userInfo.getUserToken())
            notificationsViewModel.turnNotificationsMutableLiveData.observe(this, Observer {

                loadingDialog!!.dismiss()
                userInfo.setNotifiState(it.data.notification.toLowerCase(Locale.ROOT))

                if (it.data.notification.toLowerCase(Locale.ROOT) == "on") {
                    Toast.makeText(activity!!.applicationContext, getString(R.string.notifi_is_on), Toast.LENGTH_LONG).show()
                } else {
                     Toast.makeText(activity!!.applicationContext, getString(R.string.notifi_is_off), Toast.LENGTH_LONG).show()
                }

            })

        }
        return view
    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}
