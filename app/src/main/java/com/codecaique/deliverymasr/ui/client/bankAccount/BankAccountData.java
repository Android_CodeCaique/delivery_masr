package com.codecaique.deliverymasr.ui.client.bankAccount;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.codecaique.deliverymasr.R;


public class BankAccountData extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bank_account_data, container, false);
        Button btn_saveData = view.findViewById(R.id.btn_saveData);
        btn_saveData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Intent intent=new Intent(getContext(), MainActivity2.class);
//                    fragName=R.layout.fragment_bank_account;
//                    startActivity(intent);
                BankAccount bankAccount = new BankAccount();
                FragmentManager manager = getFragmentManager();
                manager.beginTransaction().replace(R.id.fragment_sub, bankAccount, bankAccount.getTag()).commit();

            }
        });

        ImageView back = view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
                BankAccount bankAccount = new BankAccount();
                FragmentManager manager = getFragmentManager();
                manager.beginTransaction().replace(R.id.fragment_sub, bankAccount, bankAccount.getTag()).commit();

            }
        });
        return view;
    }
}
