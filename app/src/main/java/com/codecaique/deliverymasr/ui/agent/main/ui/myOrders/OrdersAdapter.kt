package com.codecaique.deliverymasr.ui.agent.main.ui.myOrders

import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.SubMainActivity
import com.codecaique.deliverymasr.base.client.SubActivity
import com.codecaique.deliverymasr.pojo.response.MyOrder
import com.codecaique.deliverymasr.ui.client.chat.ChatFragment
import com.codecaique.deliverymasr.data.ApiCreater
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

class OrdersAdapter(private val context: Context)
    : RecyclerView.Adapter<OrdersAdapter.ViewHolder>() {

    var list = ArrayList<MyOrder>()

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.item_order_main, viewGroup, false)
        return ViewHolder(v)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {

        val arrayItem = list[i]

        viewHolder.tvDelName.text = arrayItem.first_name
        viewHolder.tvOrderName.text = arrayItem.name
        viewHolder.tvOrderTime.text = arrayItem.delivery_time


        /*val dateFormatter =
                SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val date = dateFormatter.parse(arrayItem.created_at).time
        val now=Calendar.getInstance().timeInMillis

        val timeFormatter =
                SimpleDateFormat("MMM-dd")
        val orderTime = timeFormatter.format(date)

        holder.date.text = displayValue*/

        viewHolder.tvOrderTime.text = arrayItem.created_at
        viewHolder.tvOrderNo.text = arrayItem.id

        if (arrayItem.shope_image != null && arrayItem.shope_image.isNotEmpty())
            Picasso.get().load(ApiCreater.STORES_URL + arrayItem.shope_image).into(viewHolder.orderImage)

        when (arrayItem.state.trim()) {
            "wait" -> {
                viewHolder.tvState.text = context.resources.getString(R.string.waite)
            }
            "deliverd", "received_order" -> {
                viewHolder.tvState.text = context.resources.getString(R.string.received)
            }
            "canceled" -> {
                viewHolder.tvState.text = context.resources.getString(R.string.canceled)
            }
            "rated" -> {
                viewHolder.tvState.text = context.resources.getString(R.string.received)
            }
            else -> {
                viewHolder.tvState.text = context.resources.getString(R.string.processed)
            }
        }

        viewHolder.root.setOnClickListener {
//            StoresAdapter.fragName == R.layout.fragment_chat
            if (!arrayItem.state.equals("canceled")) {
                val intent = Intent(context, SubMainActivity::class.java)
                intent.putExtra(ChatFragment.USER_ID, arrayItem.id)
                intent.putExtra(ChatFragment.REQUEST_ID, arrayItem.id)
                intent.putExtra(SubActivity.KEY, SubMainActivity.CHAT)

                context.startActivity(intent)
            }
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setData(l: ArrayList<MyOrder>) {
        this.list = l
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val root = itemView.findViewById(R.id.root) as ConstraintLayout
        val orderImage = itemView.findViewById(R.id.civ_order) as CircleImageView
        val tvOrderNo = itemView.findViewById(R.id.tv_order_no) as TextView
        val tvState = itemView.findViewById(R.id.tv_state) as TextView
        val tvDelName = itemView.findViewById(R.id.tv_del_name) as TextView
        val tvOrderName = itemView.findViewById(R.id.tv_order_name) as TextView
        val tvOrderTime = itemView.findViewById(R.id.tv_order_time) as TextView
        val tvTardName = itemView.findViewById(R.id.tv_tard_name) as TextView
    }

}