package com.codecaique.deliverymasr.ui.agent.subMain.ui.orderDetails.framment.wait

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.pojo.response.Shop_Orders_Response
import kotlinx.android.synthetic.main.fragment_orders_waits.view.*

class OrdersWaitsFragment : Fragment() {

    lateinit var viewModel: Wait_ViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_orders_waits, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val shop_id :String = activity!!.intent.extras!!.getString("shop_id","0")
        Log.e("shop_id",shop_id)

        val token : String = UserInfo(activity!!).getUserToken()
        Log.e("token",token)

        viewModel = ViewModelProviders.of(this).get(Wait_ViewModel::class.java)

        viewModel.getShopOrders(token,shop_id)

        val adapter=Adapter(activity!!)
        view.rv_wait.adapter=adapter

        viewModel.shopOrdersData.observe(this, Observer {
            adapter.setList(it as java.util.ArrayList<Shop_Orders_Response.DataBean>)
        })


    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}
