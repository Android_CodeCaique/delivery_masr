package com.codecaique.deliverymasr.ui.client.tardHayak

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.response.Message
import com.codecaique.deliverymasr.pojo.util.LocationInfo
import com.codecaique.deliverymasr.pojo.util.MyUtil
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.dialog_add_copoun.*
import kotlinx.android.synthetic.main.dialog_attach_photo.*
import kotlinx.android.synthetic.main.dialog_order_wait.*
import kotlinx.android.synthetic.main.fragment_sent_requist.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import pl.aprilapps.easyphotopicker.ChooserType
import pl.aprilapps.easyphotopicker.EasyImage
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

@Suppress("CAST_NEVER_SUCCEEDS")
class SentRequestFragment : Fragment() {

    val TAG = "SentRequestFragment"
    var iv_openCamera: ImageView? = null
    var tv_media: TextView? = null
    var tv_add_coupon: TextView? = null
    var conCash: ConstraintLayout? = null
    var conPayment: ConstraintLayout? = null
    var btn_doneAddCoupon: Button? = null
    private lateinit var startLocation: LatLng
    private lateinit var endLocation: LatLng
    private lateinit var streetStart: String
    private lateinit var streetEnd: String
    private var easyImage: EasyImage? = null

    var currentPhotoPath: String? = null


    lateinit var loadingDialog: LoadingDialog

    var payment = 1
    var coupon = ""
    var uri: Uri? = null


    private var dialog1: Dialog? = null
    lateinit var VIEW: View
    var uriUserImage: Uri? = null
    lateinit var makeOrderViewModel: MakeOrderViewModel
    lateinit var locationInfo: LocationInfo
    lateinit var userInfo: UserInfo
    private lateinit var navController: NavController


    companion object {
        const val TIME = "time"
        const val LocationStart = "LocationStart"
        const val LocationEnd = "LocationEnd"
        const val Type = "type"
        const val StreetStart = "StreetStart"
        const val StreetEnd = "StreetEnd"
        const val TARD = "tard"
        const val PURCHASES = "Purchases"
        private const val TARD_ID = 4
        private const val PURCHASES_ID = 2
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        setUp()
        VIEW = inflater.inflate(R.layout.fragment_sent_requist, container, false)

        makeOrderViewModel = ViewModelProviders.of(this).get(MakeOrderViewModel::class.java)
        locationInfo = LocationInfo(activity!!.applicationContext)
        userInfo = UserInfo(activity!!)

        val time = arguments!!.getString(TIME)
        val type = arguments!!.getString(Type)
        startLocation = arguments!!.getParcelable(LocationStart)!!
        endLocation = arguments!!.getParcelable(LocationEnd)!!
        streetStart = arguments!!.getString(StreetStart)!!
        streetEnd = arguments!!.getString(StreetEnd)!!

        when (type) {
            TARD -> {

                VIEW.notification_title.text=getText(R.string.receved_loc_tard)
                VIEW.tv21.text=getText(R.string.send_loc_tard)

            }
            }

        VIEW.tv_street_start.text =  streetStart
//        else "من اي مكان"
        VIEW.tv_street_end.text = streetEnd

        Log.e("TYPE 22035", "$type   ")


        Log.e(TAG, "LOC" + startLocation.latitude.toString() + "\n" +
                startLocation.longitude + "\n" +
                endLocation.latitude + "\n" +
                endLocation.longitude
        )

        easyImage =EasyImage.Builder(activity!!)
                .setChooserTitle("Pick media")
                .setCopyImagesToPublicGalleryFolder(false)     //.setChooserType(ChooserType.CAMERA_AND_DOCUMENTS)
                .setChooserType(ChooserType.CAMERA_AND_GALLERY)
                .setFolderName("EasyImage sample")
                .allowMultiple(false)
                .build()
        loadingDialog = LoadingDialog(activity!!)

        VIEW.tv_sentRequest.setOnClickListener {

            /*val orderSented = OrderSentFragment()
            val manager = fragmentManager
            manager!!.beginTransaction().replace(R.id.main3_fragment, orderSented, orderSented.tag).commit()*/


            if (userInfo.isNotRegister()) {
                Toast.makeText(activity!!, getString(R.string.should_register), Toast.LENGTH_SHORT).show()
                return@setOnClickListener

            }

            val msg = VIEW.tv_camera.text.toString()

            if (msg.isNotEmpty()) {

                loadingDialog.show()
                val message = Message()
                message.message = msg

                when (type) {
                    TARD -> {
                        val currentTime = Calendar.getInstance().time
                        Log.e("TYPE 22", "$type zzzzzzz  ")
                        Log.e(TAG, "onCreateView: sentTard 3")
                        makeOrderViewModel.sentTard(
                                RequestBody.create(MediaType.parse("multipart/form-data"), userInfo.getUserToken()),
                                TARD_ID,
                                RequestBody.create(MediaType.parse("multipart/form-data"), streetStart),
                                startLocation.latitude,
                                startLocation.longitude,
                                RequestBody.create(MediaType.parse("multipart/form-data"), streetEnd),
                                endLocation.latitude,
                                endLocation.longitude,
                                currentTime,
                                RequestBody.create(MediaType.parse("multipart/form-data"), msg),
                                if (uri != null) image() else null,
                                RequestBody.create(MediaType.parse("multipart/form-data"), coupon),
                                payment,
                                null,
                                0.0, loadingDialog, context!!
                                //                    )
                        )

                        //                    RequestBody.create(MediaType.parse("multipart/form-data"),
                        //                            "[{"+"product"+":"+"3"+","+"type"+":"+"1"+","+"amount"+":"+"5"+","+"price"+":"+"38"+"}]"),


                    }
                    PURCHASES -> {
//                        if (startLocation.latitude==0.0) null else
//                            if (startLocation.longitude==0.0) null else
                        val currentTime = Calendar.getInstance().time
                        Log.e("TYPE 22", type + "   ")
                        Log.e("TYPE 22", type + "   ")
                        Log.e("TYPE 22", type + "   ")
                        Log.e("TYPE 22", type + "   ")

                        Log.e(TAG, "onCreateView: sentTard 4")

                        if (startLocation.latitude == 0.0 || startLocation.longitude ==0.0) {
                            startLocation = endLocation;
                        } else {
                            Log.e(TAG, "onCreateView: else ---->> lat => ${startLocation.latitude}   lon =>${startLocation.longitude} ")
                        }
                        makeOrderViewModel.sentTard(
                                RequestBody.create(MediaType.parse("multipart/form-data"), userInfo.getUserToken()),
                                PURCHASES_ID,
                                RequestBody.create(MediaType.parse("multipart/form-data"), streetStart),
                                startLocation.latitude,
                                startLocation.longitude,
                                RequestBody.create(MediaType.parse("multipart/form-data"), streetEnd),
                                endLocation.latitude,
                                endLocation.longitude,
                                currentTime,
                                RequestBody.create(MediaType.parse("multipart/form-data"), msg),
                                if (uri != null) image() else null,
                                RequestBody.create(MediaType.parse("multipart/form-data"), coupon),
                                payment,
                                null,
                                0.0, loadingDialog, context!!
                                //                    )
                        )

                    }
                }

                makeOrderViewModel.tardHayyakMutableLiveData.observe(this, Observer { t ->


                    Log.e(TAG, "onCreateView: ${t}")
                    when (t.error) {
                        0 -> {

                            if (uri != null)
                                message.image = "uhd"
                               sendMessageToCloud(message, it, t.data.id.toString())
                        }
                        4 -> {
                            loadingDialog.dismiss()
                            waiteOrderDialog()
                        }
                        else -> {
                            loadingDialog.dismiss()
                            Log.e(TAG, "onCreateView: ${t.error}")
                            Toast.makeText(activity!!.applicationContext, t.message, Toast.LENGTH_SHORT).show()
                        }
                    }

                })


            } else
                VIEW.tv_camera.error = getString(R.string.sould_be_not_empty)

        }

        iv_openCamera = VIEW.findViewById(R.id.iv_openCamera)
        iv_openCamera!!.setOnClickListener {
            dialogGetPhoto(iv_openCamera)
        }
//        conPayment = VIEW.findViewById(R.id.conPayment)
//        conPayment!!.setOnClickListener(View.OnClickListener { dialogPayment(conPayment) })
        tv_add_coupon = VIEW.findViewById(R.id.tv_add_coupon)

        VIEW.rg_payment.setOnCheckedChangeListener { _, i ->

            when (i) {
                R.id.rb_cash -> payment = 1
                R.id.rb_credit -> payment = 2
            }

        }

        tv_add_coupon!!.setOnClickListener { dialogAddCoupon(tv_add_coupon) }
        return VIEW
    }

    private lateinit var db: CollectionReference
    private lateinit var mStorageRef: StorageReference

    private fun sendMessageToCloud(message: Message, it: View, requestId: String) {

        db = FirebaseFirestore.getInstance()
                .collection(ApiCreater.OrdersCollection)
                .document(requestId)
                .collection(ApiCreater.ChatCollection)


        mStorageRef = FirebaseStorage.getInstance()
                .getReference(ApiCreater.OrdersStorage).child(requestId.toString())

        message.id = db.document().id
        message.created_at = System.currentTimeMillis().toString()
        message.updated_at = System.currentTimeMillis().toString()
        message.sender_id = UserInfo(activity!!).getId().toString()
        message.order_state = ApiCreater.OrderStates.receivedOrder

        val imageName = MyUtil.getRandomName() + ".jpg"

        if (message.image.isNotEmpty()) {

            mStorageRef.child(imageName).putFile(uri!!).continueWithTask {
                mStorageRef.child(imageName).downloadUrl
            }.addOnSuccessListener { u ->
                message.image = u.toString()
                sendTheMessage(message, it)
            }


        } else
            sendTheMessage(message, it)


    }

    private fun sendTheMessage(message: Message, it: View) {

        loadingDialog.show()
        db.document(message.id).set(message).addOnSuccessListener { _ ->
            loadingDialog.dismiss()
            navController.navigate(R.id.orderSented)

        }
    }

    private fun dialogGetPhoto(view: View?) {
        dialog1 = Dialog(view!!.context)
        dialog1!!.setContentView(R.layout.dialog_attach_photo)
        dialog1!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))


        dialog1!!.tv_camera_.setOnClickListener {

         //   isStoragePermissionGranted(222)
            UserInfo(activity!!).setnamefragment(TardHayyakFragment::class.java.simpleName)

            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(activity!!)

          //  dialog!!.cancel()
            dialog1!!.cancel()
        }

        dialog1!!.tv_media.setOnClickListener {

            isStoragePermissionGranted(111)
            dialog1!!.cancel()
        }
        dialog1!!.show()
    }

    private fun dialogAddCoupon(view: View?) {
        dialog1 = Dialog(view!!.context)
        dialog1!!.setContentView(R.layout.dialog_add_copoun)
        dialog1!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog1!!.btn_done.setOnClickListener {

            if (dialog1!!.tv_camera.text.toString().isNotEmpty()) {
                coupon = dialog1!!.tv_camera.text.toString()
                tv_add_coupon!!.text = coupon
                dialog1!!.dismiss()
            } else {
                dialog1!!.tv_camera.error = getString(R.string.enter_your_copon)
            }

        }


        dialog1!!.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (Activity.RESULT_OK == resultCode) {

            Log.e("a", requestCode.toString())
            when (requestCode) {
                203->{

                    if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                        val result = CropImage.getActivityResult(data)
                        if (resultCode == Activity.RESULT_OK) {
                            uri = result.uri
                            VIEW.img_request.setImageURI(uri)
                        } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                            val error = result.error
                        }
                    }
//                    uri = data.data!!
//                    var  aa=uri as Bitmap
                }
                111 -> {
                    Log.e("b", requestCode.toString())
                    VIEW.img_request.setImageURI(data!!.data!!)
                    uri = data.data!!
                }

            }
        }
    }


    private fun image(): MultipartBody.Part? {
        if (uri != null) {

            Log.e("SSSSS", "SSSSSSSSSSSSSSS")

            val p: String
            val cursor = activity!!.contentResolver.query(uri!!, null, null, null, null)

            p = if (cursor == null) {
                uri!!.path.toString()
            } else {
                cursor.moveToFirst()
                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                cursor.getString(idx)
            }

            val file = File(p)
            val requestFile = RequestBody.create(MediaType.parse("image/*"), file)
            return MultipartBody.Part.createFormData("image", file.name, requestFile)
        } else if (uri == null && currentPhotoPath != null) {


            uri = Uri.parse(currentPhotoPath)

            Log.e("URI 222", uri!!.path.toString() + "    ")

            val p: String
            val cursor = activity!!.contentResolver.query(uri!!, null, null, null, null)

            if (cursor == null) {
                p = uri!!.path.toString()
                Log.e("URI 222 0000", uri!!.path.toString() + "    ")
            } else {
                cursor.moveToFirst()
                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                Log.e("URI 222 005500", uri!!.path.toString() + "    ")


                p = cursor.getString(idx)
            }

            val file = File(p)
            val requestFile = RequestBody.create(MediaType.parse("image/*"), file)
            return MultipartBody.Part.createFormData("image", file.name, requestFile)
        }

        return null

    }

    @SuppressLint("WrongConstant")
    fun isStoragePermissionGranted(requestCode: Int): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return if (requestCode == 111) {
                when (PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.checkSelfPermission(context!!, Manifest.permission.READ_EXTERNAL_STORAGE) -> {
                        gotoImage(requestCode)

                        true
                    }
                    else -> {

                        ActivityCompat.requestPermissions(
                                activity!!,
                                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA),
                                requestCode
                        )
                        false
                    }
                }
            } else {
                if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    gotoImage(requestCode)

                    true
                } else {

                    ActivityCompat.requestPermissions(
                            activity!!,
                            arrayOf(Manifest.permission.CAMERA),
                            requestCode
                    )
                    false
                }
            }
        } else {

            gotoImage(requestCode)
            return true
        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            gotoImage(requestCode)
        }
        if (grantResults.isNotEmpty() && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            gotoImage(requestCode)
        } else {
            /*
        Toast.makeText(applicationContext, R.string.imagePprem_accept, Toast.LENGTH_SHORT)
                .show()
*/
        }
    }

    private fun gotoImage(requestCode: Int) {

        val intent: Intent

        if (requestCode == 111) {

            intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), requestCode)

        } else {

//                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(takePicture, PICK_IMAGE_FROM_CAMERA);

//                SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
//                int imageId=pref.getInt("image",0);
//
//                File path = new File(getActivity().getFilesDir(), "Gallery/MyImages/");
//                if (!path.exists()) path.mkdirs();
//                File image = new File(path, "image_capture_"+""+imageId+".jpg");
//                Uri imageUri = FileProvider.getUriForFile(getActivity().getApplicationContext(), CAPTURE_IMAGE_FILE_PROVIDER, image);
//                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
//                providePermissionForProvider(cameraIntent, imageUri);
//                startActivityForResult(cameraIntent, PICK_IMAGE_FROM_CAMERA);
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            // Ensure that there's a camera activity to handle the intent
            // Ensure that there's a camera activity to handle the intent
            if (takePictureIntent.resolveActivity(activity!!.packageManager) != null) {
                // Create the File where the photo should go
                var photoFile: File? = null
                try {
                    photoFile = createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    val photoURI = FileProvider.getUriForFile(context!!,
                            "${requireActivity().applicationContext.packageName}.fileprovider",
                            photoFile)
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, requestCode)
                }
            }

        }
    }


    private fun waiteOrderDialog() {
        val dialog = Dialog(view!!.context)
        dialog.setContentView(R.layout.dialog_order_wait)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.show()

        dialog.tv_ok.setOnClickListener {
            activity!!.finish()
        }

    }


    @Throws(IOException::class)
    private fun createImageFile(): File? {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = activity!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",  /* suffix */
                storageDir /* directory */
        )

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.absolutePath
        return image
    }

    private fun setUp() {

//        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        val wlp = dialog!!.window!!.attributes
//        wlp.gravity = Gravity.BOTTOM
//        wlp.flags = wlp.flags and WindowManager.LayoutParams.FLAG_DIM_BEHIND.inv()
//        dialog!!.window!!.attributes = wlp

    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
    private fun initialize(view: View) {
        navController = Navigation.findNavController(view)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize(view)
    }


}