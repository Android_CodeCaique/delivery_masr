package com.codecaique.deliverymasr.ui.agent.main.dialogs

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.StoresMainActivity
import kotlinx.android.synthetic.main.dialog_order_sent.*

class OrderSentDialog(context: Context) : Dialog(context) {

    companion object
    {
        val isNotifications="isNotifications"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_order_sent)

        tv_ok.setOnClickListener {
//            ownerActivity!!.findNavController(R.id.fragment_stores).navigate(R.id.notificationsFragment)
//            Navigation.findNavController(ownerActivity!!,R.id.tv_ok).navigate(R.id.notificationsFragment)

            val intent=Intent(context, StoresMainActivity::class.java)
            intent.putExtra(isNotifications,true)
            context.startActivity(intent)
            dismiss()
        }


    }
}
