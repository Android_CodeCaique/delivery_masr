package com.codecaique.deliverymasr.ui.client.chat

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.response.*
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.response.billmodel.BillModel
import com.codecaique.deliverymasr.pojo.services.DeliveredDialog
import com.codecaique.deliverymasr.ui.agent.subMain.ui.postproblem.PostProblemFragment
import com.codecaique.deliverymasr.ui.agent.subMain.ui.pushBill.PushBillFragment
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.dialog_rate_user.*
import kotlinx.android.synthetic.main.fragment_strore_info.*
import okhttp3.MultipartBody
import okhttp3.RequestBody

class ChatViewModel : ViewModel() {

    private val TAG = "ChatViewModel"
    val showMessagesMutableLiveData = MutableLiveData<ShowMessagesResponse>()
    val cancelOrderMutableLiveData = MutableLiveData<GeneralResponse>()
    val cancelOfferMutableLiveData = MutableLiveData<GeneralResponse>()
    val deliverOfferMutableLiveData = MutableLiveData<GeneralResponse>()
    val orderReceivedMutableLiveData = MutableLiveData<GeneralResponse>()
    val arrivedSiteMutableLiveData = MutableLiveData<GeneralResponse>()
    val sentShakwayMutableLiveData = MutableLiveData<GeneralResponse>()
    val showUserOfOrderMutableLiveData = MutableLiveData<ShowUserOfOrderResponse>()
    val showReasonsMutableLiveData = MutableLiveData<ShowComplainReasonsResponse>()
    val showOrderMutableLiveData = MutableLiveData<ShowOrderResponse>()
    val changeAgentMutableLiveData = MutableLiveData<GeneralResponse>()
    val rateUserMutableLiveData = MutableLiveData<GeneralResponse>()
    val requestCheckoutIdMutableLiveData = MutableLiveData<PaymentResponse>()
    val paymentStateMutableLiveData = MutableLiveData<PaymentStateResponse>()
    val billMutableLiveData = MutableLiveData<BillModel>()
    val userReceivedOrderMutableLiveData = MutableLiveData<GeneralResponse>()
    val clientSayPaymentMutableLiveData = MutableLiveData<GeneralResponse>()

    fun rateUser(
            user_token: RequestBody,
            user_id: Int,
            rate_number: Double,
            comment: RequestBody,
            request_id: Int,
            dialog: Dialog?
    ) {

        Log.e(TAG, "rateUser: --->>> 11111")
        val observable =
                ApiCreater.instance.rateUser(user_token, user_id, rate_number, comment, request_id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<GeneralResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: GeneralResponse) {

                Log.e(TAG, "onNext: t.error---> "+t.error)
                Log.e(TAG, "onNext: t.message---> "+t.message)
                rateUserMutableLiveData.value = t
                rateUserMutableLiveData.value = t
                dialog?.dismiss()
                dialog!!.dismiss()
            }

            override fun onError(e: Throwable) {
                Log.e("ss error 101", e.message)
            }
        }

        observable.subscribe(observer)


    }

    fun changeAgent(user_token: String, request_id: Int) {

        val observable =
                ApiCreater.instance.changeAgent(user_token, request_id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<GeneralResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: GeneralResponse) {

                changeAgentMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)


    }

    fun orderReceived(user_token: String, request_id: Int) {

        val observable =
                ApiCreater.instance.orderReceived(user_token, request_id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<GeneralResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: GeneralResponse) {

                orderReceivedMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)


    }

    fun showOrderById(token: String, request_id: Int) {

        val observable =
                ApiCreater.instance.orderById(token, request_id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<ShowOrderResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: ShowOrderResponse) {
                Log.e("ss chat", t.message)

                showOrderMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }


    fun showMessages(token: String, user_id: Int, request_id: Int) {

        val observable =
                ApiCreater.instance.showMessages(token, user_id, request_id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())

        val observer = object : Observer<ShowMessagesResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: ShowMessagesResponse) {

                showMessagesMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }

    fun sentMessage(
            user_token: RequestBody,
            user_id: Int?,
            message: RequestBody?,
            image: MultipartBody.Part?,
            request_id: Int
    ) {

        /* val observable =
                 ApiCreater.instance.sentMessage(user_token, user_id, message, image, request_id)
                         .subscribeOn(Schedulers.io())
                         .observeOn(AndroidSchedulers.mainThread());

         val observer = object : Observer<SentMessageResponse> {
             override fun onComplete() {

             }

             override fun onSubscribe(d: Disposable) {
             }

             override fun onNext(t: SentMessageResponse) {

                 sentMessageMutableLiveData.value = t

             }

             override fun onError(e: Throwable) {
                 Log.e("ss", e.message)
             }
         }

         observable.subscribe(observer)*/

    }


    fun deliverOrder(user_token: String, request_id: Int) {
        val observable =
                ApiCreater.instance.deliverOrder(user_token, request_id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<GeneralResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: GeneralResponse) {

                deliverOfferMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }

    fun arrivedSite(user_token: String, request_id: Int) {
        val observable =
                ApiCreater.instance.arrivedSite(user_token, request_id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<GeneralResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: GeneralResponse) {

                arrivedSiteMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }

    fun cancelOffer(
            user_token: String,
            request_id: Int,
            cancel_reason: String
    ) {
        val observable =
                ApiCreater.instance.cancelOffer(user_token, request_id, cancel_reason)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<GeneralResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: GeneralResponse) {

                cancelOfferMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }


    fun cancelOrder(token: String, request_id: Int, cancel_reason: String, driver_id: Int?, loadingDialog: LoadingDialog) {

        val observable =
                ApiCreater.instance.cancelOrder(token, request_id, cancel_reason, driver_id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<GeneralResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: GeneralResponse) {

                try {

                loadingDialog.dismiss()
                }catch (e:Exception){}

                cancelOrderMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                try {
                loadingDialog.dismiss()
                    Log.e("ss", e.message)

            }catch (e:Exception){}
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }

    fun acceptCancelorder(token: String, request_id: Int, action: String, context: Context) {

        val observable =
                ApiCreater.instance.accept_cancelRequest(token, request_id, action)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<GeneralResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: GeneralResponse) {

                cancelOrderMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {

                Toast.makeText(context, "There is an error in connection please try again", Toast.LENGTH_SHORT).show()
                DeliveredDialog().finish()

                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }

    fun userReceivedOrder(context:Context,
            user_token: String,
            request_id: Int,
            driver_id: Int) {
        val observable =
                ApiCreater.instance.userReceivedOrder(user_token, request_id, driver_id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<GeneralResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: GeneralResponse) {

                userReceivedOrderMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {

                Toast.makeText(context, context.getString(R.string.there_is_an_error_in_connection_please_try_again), Toast.LENGTH_SHORT).show()

                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }


    fun sentShakaway(
            user_token: RequestBody,
            user_id: RequestBody,
            order_id: RequestBody,
            comment: RequestBody?,
            reason: RequestBody,
            image: MultipartBody.Part?,
            notes: RequestBody?,
            details: RequestBody?,
            answers: RequestBody) {

        val observable =
                ApiCreater.instance.sentShakway(user_token, user_id, order_id, comment, reason, image, notes, details, answers)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<GeneralResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: GeneralResponse) {

                sentShakwayMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ssa", e.message)
                PostProblemFragment().loadingDialog.dismiss()
            }
        }

        observable.subscribe(observer)

    }

    fun showUserData(token: String, user_id: Int) {

        val observable =
                ApiCreater.instance.showUserOfOrder(token, user_id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<ShowUserOfOrderResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: ShowUserOfOrderResponse) {

                showUserOfOrderMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }

    fun showReasons(token: String) {

        val observable =
                ApiCreater.instance.showComplainReasons(token)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())

        val observer = object : Observer<ShowComplainReasonsResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: ShowComplainReasonsResponse) {

                showReasonsMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }

    fun paymentRequest(user_token: String,
                       merchantTransactionId: Int,
                       amount: Int,
                       email: String) {

        val observable =
                ApiCreater.instance.paymentRequest(user_token, merchantTransactionId, amount, email)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())

        val observer = object : Observer<PaymentResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: PaymentResponse) {

                requestCheckoutIdMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }

    fun paymentStatus(user_token: String,
                      requestCheckoutId: String) {

        val observable =
                ApiCreater.instance.paymentStatus(user_token, requestCheckoutId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())

        val observer = object : Observer<PaymentStateResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: PaymentStateResponse) {

                paymentStateMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }

    @SuppressLint("CheckResult")
    fun showBillData(token: String, request_id: Int) {
        val observable =
                ApiCreater.instance.showBillByRequestId(token, request_id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())

        val observer = object : Observer<BillModel> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {

            }

            override fun onNext(t: BillModel) {
                billMutableLiveData.value = t
            }

            override fun onError(e: Throwable) {
                PushBillFragment().loadingDialog.dismiss()
            }
        }
        observable.subscribe(observer)


    }

    fun clientSayPayment(
            user_token: String,
            request_id: Int,
            driver_id: Int){

        val observable =
                ApiCreater.instance.clientSayPayment(user_token, request_id, driver_id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())

        val observer = object : Observer<GeneralResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {

            }

            override fun onNext(t: GeneralResponse) {
                clientSayPaymentMutableLiveData.value = t
            }

            override fun onError(e: Throwable) {
                PushBillFragment().loadingDialog.dismiss()
            }
        }
        observable.subscribe(observer)



    }

}