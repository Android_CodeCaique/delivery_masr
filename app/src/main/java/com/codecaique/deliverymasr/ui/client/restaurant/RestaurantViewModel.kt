package com.codecaique.deliverymasr.ui.client.restaurant

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codecaique.deliverymasr.pojo.response.ShowProduceTypesResponse
import com.codecaique.deliverymasr.pojo.response.ShowShopByIdResponse
import com.codecaique.deliverymasr.data.ApiCreater
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.ArrayList

class RestaurantViewModel : ViewModel() {

    val typesMutableLiveData = MutableLiveData<ShowProduceTypesResponse>()
    val menuItemsMutableLiveData = MutableLiveData<ShowShopByIdResponse>()

    fun showProductTypes(user_token: String, product_id: Int) {

        val observable =
                ApiCreater.instance.showProductTypes(user_token, product_id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<ShowProduceTypesResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: ShowProduceTypesResponse) {

                typesMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }

    fun showShopById(user_token: String, shop_id: Int) {

        val observable =
                ApiCreater.instance.showShopById(user_token, shop_id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())

        val observer = object : Observer<ShowShopByIdResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: ShowShopByIdResponse) {


                menuItemsMutableLiveData.value = t
            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)
    }

    fun getTimes(): List<String> {

        val time: MutableList<String> = ArrayList()
        time.add("1 ساعة")
        time.add("2 ساعة")
        time.add("3 ساعة")
        time.add("4 ساعة")

//        val map = HashMap<String, Int>()
//
//        map["1 ساعة"] = 1
//        map["2 ساعة"] = 2
//        map["3 ساعة"] = 3
//        map["4 ساعة"] = 4

        return time
    }


}