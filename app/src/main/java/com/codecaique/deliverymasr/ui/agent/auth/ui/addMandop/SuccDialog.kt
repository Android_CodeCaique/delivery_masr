package com.codecaique.deliverymasr.ui.agent.auth.ui.addMandop

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.StoresMainActivity
import kotlinx.android.synthetic.main.dialog_captain.*

class SuccDialog(val mContext: Context) :Dialog(mContext) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_captain)

        tv_ok.setOnClickListener {
            dismiss()
//            context.finish()
//            context.startActivity(Intent(context,StoresMainActivity::class.java))

            val intent =Intent(mContext,StoresMainActivity::class.java)
            intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            mContext.startActivity(intent)

        }

    }

}