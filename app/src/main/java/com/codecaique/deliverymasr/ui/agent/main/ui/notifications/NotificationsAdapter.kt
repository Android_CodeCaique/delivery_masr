package com.codecaique.deliverymasr.ui.agent.main.ui.notifications

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.SubMainActivity
import com.codecaique.deliverymasr.pojo.response.Notification
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.services.DeliveredDialog
import com.codecaique.deliverymasr.ui.agent.subMain.ui.chat.ChatFragment
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import java.text.SimpleDateFormat

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class NotificationsAdapter(private val context: Context) : RecyclerView.Adapter<NotificationsAdapter.ViewHolderNotification>() {

    var list = ArrayList<Notification>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderNotification {
        val view = LayoutInflater.from(context).inflate(R.layout.item_notifications, parent, false)
        return ViewHolderNotification(view)
    }

    @SuppressLint("SimpleDateFormat")
    override fun onBindViewHolder(holder: ViewHolderNotification, position: Int) {

        val arrayItem = list[position]

        val dateFormatter =
                SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val date = dateFormatter.parse(arrayItem.created_at)

        val timeFormatter =
                SimpleDateFormat("hh:mm a")
        val orderTime = timeFormatter.format(date)

        holder.message.text = orderTime
        holder.title.text = arrayItem.body

        if (arrayItem.user_image != null && arrayItem.user_image.isNotEmpty())
            Picasso.get().load(ApiCreater.USER_URL + arrayItem.user_image).into(holder.image)

        holder.itemView.setOnClickListener {

            if(arrayItem.user_id=="1"){
                return@setOnClickListener
            }

            if(arrayItem.body.contains("إلغاء")  ||arrayItem.body.contains("cance")){
                return@setOnClickListener
            }


            if (arrayItem.click_action == "2")
            {
                val intent = Intent(context, DeliveredDialog::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.putExtra("action","cancel")


                val text1 = arrayItem.body + "\n"
                val text2 =   " رقم " + arrayItem.request_id.toString()

                val text =    " هل تريد الموافقة ؟"

                val text3 = text1+text2+text

                Log.e("request",arrayItem.request_id.toString() + "       ")

                intent.putExtra("text",text3)
                intent.putExtra("request_id",arrayItem.request_id)
                intent.putExtra("action","cancel")
                context.startActivity(intent)
            }

            else
            {

                val intent=Intent(context,SubMainActivity::class.java)
                intent.putExtra(SubMainActivity.KEY,SubMainActivity.CHAT)
                intent.putExtra(ChatFragment.REQUEST_ID,arrayItem.request_id.toString())
                intent.putExtra(ChatFragment.USER_ID,arrayItem.user_id.toString())
                intent.flags=Intent.FLAG_ACTIVITY_NEW_TASK
                context.startActivity(intent)
            }



        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class ViewHolderNotification(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView = itemView.findViewById(R.id.market_name)
        var message: TextView = itemView.findViewById(R.id.order_time)
        var image: CircleImageView = itemView.findViewById(R.id.market_logo)
    }

    fun setData(a: ArrayList<Notification>) {
        this.list = a
        notifyDataSetChanged()
    }


    fun notifyAdapter() {
//        list.removeAt(position)
        notifyDataSetChanged()
    }
}