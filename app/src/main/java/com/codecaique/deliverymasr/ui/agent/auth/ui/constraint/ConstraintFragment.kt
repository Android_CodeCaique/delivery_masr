package com.codecaique.deliverymasr.ui.agent.auth.ui.constraint

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.agent.main.ui.useConditions.UsageViewModel
import kotlinx.android.synthetic.main.fragment_constraint.view.*

/**
 * A simple [Fragment] subclass.
 */
class ConstraintFragment : Fragment() {

    lateinit var usageViewModel: UsageViewModel
    lateinit var userInfo: UserInfo

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_constraint, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        usageViewModel = ViewModelProviders.of(this).get(UsageViewModel::class.java)
        userInfo = UserInfo(activity!!.applicationContext)


        usageViewModel.showUseConditions(userInfo.getUserToken())
        usageViewModel.showUseConditionsMutableLiveData.observe(this, Observer { t ->

            view.progressBar4.visibility = View.INVISIBLE
            view.tv_details.text = t.data[0].A_text

        })


    }

    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}
