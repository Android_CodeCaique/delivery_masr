package com.codecaique.deliverymasr.ui.client.myOrders

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.client.SubActivity
import com.codecaique.deliverymasr.pojo.response.MyOrder
import com.codecaique.deliverymasr.ui.client.chat.ChatFragment
import com.codecaique.deliverymasr.ui.client.stores.adapters.StoresAdapter
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.ui.client.chat.ChatViewModel
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.dialog_cancle_order.*
import kotlinx.android.synthetic.main.order_deliverd_or_no.*
import kotlinx.android.synthetic.main.order_deliverd_or_no.btn_no
import kotlinx.android.synthetic.main.order_deliverd_or_no.btn_yes

class OrdersAdapter(private val context: Context, val user_token: String, val request_Id: Int, var chatViewModel: ChatViewModel)
    : RecyclerView.Adapter<OrdersAdapter.ViewHolder>() {

    val TAG = "OrdersAdapter"
    var list = ArrayList<MyOrder>()
    lateinit var mLoadingDialog: LoadingDialog


    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.item_order_main, viewGroup, false)
        return ViewHolder(v)
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.N)
    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {

        val arrayItem = list[i]

        viewHolder.tvDelName.text = arrayItem.first_name
        viewHolder.tvOrderName.text = arrayItem.description + " "
        viewHolder.tvOrderTime.text = arrayItem.delivery_time
        viewHolder.tvTardName.text = arrayItem.name

        mLoadingDialog = LoadingDialog(context)

        /*val dateFormatter =
                SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val date = dateFormatter.parse(arrayItem.created_at).time
        val now=Calendar.getInstance().timeInMillis

        val timeFormatter =
                SimpleDateFormat("MMM-dd")
        val orderTime = timeFormatter.format(date)

        holder.date.text = displayValue*/

        viewHolder.tvOrderTime.text = arrayItem.created_at
        viewHolder.tvOrderNo.text = arrayItem.id

        if (arrayItem.shope_image != null && arrayItem.shope_image.isNotEmpty())
            Picasso.get().load(ApiCreater.STORES_URL + arrayItem.shope_image).into(viewHolder.orderImage)

        Log.e(TAG, "onBindViewHolder: arrayItem.state ${arrayItem.state} ")

        when (arrayItem.state.trim()) {
            "wait" -> {
                viewHolder.tvState.text = context.resources.getString(R.string.waite)

                viewHolder.itemView.setOnLongClickListener {

                    val dialog = Dialog(context)

                    dialog.apply {
                        setContentView(R.layout.dialog_cancle_order)
                        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                        tv_upload.text = context.getString(R.string.withdraw_order)
                        show()
                    }

                    //layout_cancel progressBarCancel
                    dialog.btn_yes.setOnClickListener {

                        Log.e(TAG, "onBindViewHolder: user_token $user_token \n" +
                                "request id ${arrayItem.id}")

                        dialog.layout_cancel.visibility = View.GONE
                        dialog.progressBarCancel.visibility = View.VISIBLE
                        mLoadingDialog.show()

                        chatViewModel.cancelOrder(user_token, arrayItem.id.toInt(), "-1", -1, mLoadingDialog)
                        chatViewModel.cancelOrderMutableLiveData.observe(context as LifecycleOwner, Observer {

                            Log.e(TAG, "onBindViewHolder: error ${it.error} message ${it.message}")
                            dialog.dismiss()
                            mLoadingDialog.dismiss()
                            list.remove(arrayItem)
                            notifyDataSetChanged()

                        })

                    }

                    dialog.btn_no.setOnClickListener {
                        dialog.dismiss()
                    }

                    false
                }

            }
            "deliverd", "received_order" -> {
                viewHolder.tvState.text = context.resources.getString(R.string.received)
            }
            "canceled" -> {
                viewHolder.tvState.text = context.resources.getString(R.string.canceled)
            }
            "rated" -> {
                viewHolder.tvState.text = context.resources.getString(R.string.received)
            }
            else -> {
                viewHolder.tvState.text = context.resources.getString(R.string.processed)
            }
        }


        viewHolder.root.setOnClickListener {
            Log.e(TAG, "onBindViewHolder: root.setOnClickListener 1")
            if (arrayItem.state != "wait" && arrayItem.state != "canceled") {

                Log.e(TAG, "onBindViewHolder: root.setOnClickListener 2")

                StoresAdapter.fragName == R.layout.fragment_chat
                val intent = Intent(context, SubActivity::class.java)
                Log.e(TAG, "${arrayItem.user_id} aa ${arrayItem.id}")
                intent.putExtra(ChatFragment.USER_ID, arrayItem.user_id.toString())
                intent.putExtra(ChatFragment.REQUEST_ID, arrayItem.id.toString())
                intent.putExtra(SubActivity.KEY, SubActivity.ChatOrder)

                Log.e(TAG, "onBindViewHolder: root.setOnClickListener  4")
                context.startActivity(intent)
                Log.e(TAG, "onBindViewHolder: root.setOnClickListener  5")
            }
            Log.e(TAG, "onBindViewHolder: root.setOnClickListener  3")

            /*            if (arrayItem.state == "wait")
                                {
                                    deliverdOrNo(arrayItem.id.toInt())
                                }*/

        }


    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setData(l: ArrayList<MyOrder>) {
        this.list = l
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val root = itemView.findViewById(R.id.root) as ConstraintLayout
        val orderImage = itemView.findViewById(R.id.civ_order) as CircleImageView
        val tvOrderNo = itemView.findViewById(R.id.tv_order_no) as TextView
        val tvState = itemView.findViewById(R.id.tv_state) as TextView
        val tvDelName = itemView.findViewById(R.id.tv_del_name) as TextView
        val tvOrderName = itemView.findViewById(R.id.tv_order_name) as TextView
        val tvOrderTime = itemView.findViewById(R.id.tv_order_time) as TextView
        val tvTardName = itemView.findViewById(R.id.tv_tard_name) as TextView
    }


    private fun deliverdOrNo(request_Id: Int) {
        Log.e("asasas", "asasas")
        val dialog = Dialog(context)
        dialog.setContentView(R.layout.order_deliverd_or_no)

        dialog.tv_1.text = " هل تريد الغاء الطلب بالفعل "

        dialog.btn_yes.setOnClickListener {

            chatViewModel.cancelOrder(user_token, request_Id, "", null, mLoadingDialog)

            chatViewModel.cancelOrderMutableLiveData.observe(this.context as LifecycleOwner, Observer {

                if (it.error == 0)
                    Toast.makeText(context, "تم الغاء الطلب", LENGTH_SHORT).show()
                else
                    Toast.makeText(context, it.message + "  ", LENGTH_SHORT).show()

            })


            dialog.dismiss()
            dialog.cancel()
        }
        dialog.btn_no.setOnClickListener {
            dialog.dismiss()
            dialog.cancel()


        }

        dialog.show()

    }

}