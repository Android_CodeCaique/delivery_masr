package com.codecaique.deliverymasr.ui.agent.main.ui.homeStores

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.response.Category
import com.codecaique.deliverymasr.data.ApiCreater
import com.squareup.picasso.Picasso

class CategoriesAdapter(private val context: Context) : RecyclerView.Adapter<CategoriesAdapter.ViewHolder>() {

    var list = ArrayList<Category>()
    var vLast: TextView? = null
    lateinit var nearStoresAdapter: NearStoresAdapter

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView = itemView.findViewById(R.id.btn_11)
        var image: ImageView = itemView.findViewById(R.id.iv)
        var root: ConstraintLayout = itemView.findViewById(R.id.root)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.item_cat, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {

        val arrayItem = list[i]

        viewHolder.name.text = arrayItem.name

        if (vLast == null) {
            vLast = viewHolder.name
            select(viewHolder.name)
        }


        viewHolder.name.setOnClickListener {

            if (vLast != null)
                deSelect(vLast!!)
            vLast = viewHolder.name
            select(vLast!!)

            // filter stores
            nearStoresAdapter.filter(arrayItem.name)

        }

        if (arrayItem.id != 0) {

            viewHolder.image.visibility = View.VISIBLE

            if (arrayItem.image != null && arrayItem.image.isNotEmpty())
                Picasso.get().load(ApiCreater.CATEGORIES_URL + arrayItem.image).into(viewHolder.image)

        } else {
            viewHolder.name.setPadding(24, 0, 24, 0)
            viewHolder.image.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun select(i: TextView) {
        i.background = ContextCompat.getDrawable(context, R.drawable.bordered_circle_gray_2)
    }

    fun deSelect(i: TextView) {
        i.background = ContextCompat.getDrawable(context, R.drawable.bg_cornor_gray_1)
    }

    fun setData(a: ArrayList<Category>) {
        list=ArrayList()
        this.list = a
        notifyDataSetChanged()
    }

    fun setNearAdapter(n: NearStoresAdapter) {
        this.nearStoresAdapter = n
    }

}