package com.codecaique.deliverymasr.ui.agent.subMain.ui.pushBill

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.response.BillPriceResponse
import com.codecaique.deliverymasr.pojo.response.BillResponse
import com.codecaique.deliverymasr.pojo.response.UserTaxResponse
import com.codecaique.deliverymasr.pojo.response.billmodel.BillModel
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody

class BillViewModel : ViewModel() {
    val makeBillMutableLiveData = MutableLiveData<BillPriceResponse>()
    val billMutableLiveData = MutableLiveData<BillModel>()
    val userTaxMutableLiveData = MutableLiveData<UserTaxResponse>()


    fun makeBill(token: String, request_id: Int, price: Int, total_price: Int, image: MultipartBody.Part?
                 , loadingDialog: LoadingDialog) {
        val observable =
                ApiCreater.instance.makeBill(
                        RequestBody.create(MediaType.parse("multipart/form-data"), token)
                        , request_id
                        ,  price,
                        total_price,
                         image)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())

        val observer = object : Observer<BillPriceResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {

            }

            override fun onNext(t: BillPriceResponse) {

                makeBillMutableLiveData.value = t


                Log.e("sssss 0000",t.message + "     " + t.error.toString() + " sadsa")
                loadingDialog.dismiss()
            }

            override fun onError(e: Throwable) {
                Log.e("PUSh bill on error",e.message)
                loadingDialog.dismiss()
            }
        }
        observable.subscribe(observer)

    }

    fun showUserTax(token: String) {
        val observable =
                ApiCreater.instance.showUserTax(token)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())

        val observer = object : Observer<UserTaxResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {

            }

            override fun onNext(t: UserTaxResponse) {
                userTaxMutableLiveData.value = t
            }

            override fun onError(e: Throwable) {
                PushBillFragment().loadingDialog.dismiss()
            }
        }
        observable.subscribe(observer)

    }

    fun showBillData(token: String,request_id: Int) {
        val observable =
                ApiCreater.instance.showBillByRequestId(token,request_id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())

        val observer = object : Observer<BillModel> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {

            }

            override fun onNext(t: BillModel) {

                billMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                PushBillFragment().loadingDialog.dismiss()
            }
        }
        observable.subscribe(observer)

    }

}