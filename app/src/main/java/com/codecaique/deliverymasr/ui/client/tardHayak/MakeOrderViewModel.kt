package com.codecaique.deliverymasr.ui.client.tardHayak

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.response.MakeOrderResponse
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.util.*
import kotlin.collections.MutableList

class MakeOrderViewModel :ViewModel() {

    val TAG="MakeOrderViewModel"

    val tardHayyakMutableLiveData=MediatorLiveData<MakeOrderResponse>()

    fun sentTard(
            user_token: RequestBody,
            shope_id: Int?,
            receive_place: RequestBody,
            receiving_latitude: Double?,
            receiving_longitude: Double?,
            delivery_place: RequestBody,
            delivery_latitude: Double,
            delivery_longitude: Double,
            delivery_time: Date,
            description: RequestBody,
            image: MultipartBody.Part?,
            copon: RequestBody,
            payment_id: Int,
            menu: RequestBody?,
            total_price: Double,
            loadingDialog: LoadingDialog,
            context: Context
    ){

        Log.e(TAG, "sentTard: lat --> $receiving_latitude    lon ----> $receiving_longitude")
        Log.e(TAG, "sentTard: shop_id --> $shope_id")
        val observable =
                ApiCreater.instance.makeOrder(user_token, shope_id, receive_place, receiving_latitude,
                        receiving_longitude, delivery_place, delivery_latitude, delivery_longitude,
                        delivery_time, description, image, copon, payment_id, menu, total_price)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<MakeOrderResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: MakeOrderResponse) {
//                loadingDialog.dismiss()

                tardHayyakMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                loadingDialog.dismiss()
                Log.e(TAG, e.message!!)//message)

                Toast.makeText(context,"There is an error in connection " +
                        "\n please try again later",Toast.LENGTH_SHORT).show()
            }
        }

        observable.subscribe(observer)


    }

    fun getTimes():List<String>{


        val time: MutableList<String> = ArrayList()
        time.add("1 ساعة")
        time.add("2 ساعة")
        time.add("3 ساعة")
        time.add("4 ساعة")

//        val map=HashMap<String,Int>()
//
//        map["1 ساعة"]=1
//        map["2 ساعة"]=2
//        map["3 ساعة"]=3
//        map["4 ساعة"]=4

        return time
    }


}