package com.codecaique.deliverymasr.ui.agent.main.ui.totaldeliverymoney;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codecaique.deliverymasr.R;
import com.codecaique.deliverymasr.pojo.util.UserInfo;
import com.codecaique.deliverymasr.pojo.response.Total_Delivery_Response;

public class TotalDeliveryMoney extends Fragment {
    Total_delivAdapter adapter;
    RecyclerView recyclerView;
    ImageView back;
    TextView total_tv;
    String total = "0";

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_total_delivery_money, container, false);

        total_tv = view.findViewById(R.id
                .total_sum);
        back = view.findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).popBackStack();
            }
        });

        String token = new UserInfo(getActivity()).getUserToken();

        TotalViewModel viewModel = ViewModelProviders.of(this).get(TotalViewModel.class);
        viewModel.getTotalSum(token);

        recyclerView = view.findViewById(R.id.rec_total_del_money);

        adapter = new Total_delivAdapter(getContext());

        viewModel.data.observe(getViewLifecycleOwner(), new Observer<Total_Delivery_Response>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onChanged(Total_Delivery_Response dataBeans) {
                adapter.setList(dataBeans.getData());

                total = dataBeans.getTotal();

                total_tv.setText(total + " ر.س ");

            }
        });


        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        return view;
    }

}
