package com.codecaique.deliverymasr.ui.agent.subMain.ui.comments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.StoresMainActivity
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.pojo.response.Comment
import com.codecaique.deliverymasr.ui.client.myRate.RatingsViewModel
import com.codecaique.deliverymasr.ui.client.myRate.UserNotesAdapter
import com.codecaique.deliverymasr.utils.Utils
import kotlinx.android.synthetic.main.fragment_comments.view.*
import java.util.*

class Comments : Fragment() {

    lateinit var ratingsViewModel: RatingsViewModel
    lateinit var userNotesAdapter: UserNotesAdapter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_comments, container, false)


        val intent = Intent(view.context, StoresMainActivity::class.java)
        intent.flags=Intent.FLAG_ACTIVITY_NEW_TASK

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true/*  enabled by default*/) {
            override fun handleOnBackPressed() {
                // Handle the back button event
                intent.putExtra(StoresMainActivity.KEY,StoresMainActivity.SETTINGS)
                startActivity(intent)
                requireActivity().finish()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)


        ratingsViewModel= ViewModelProviders.of(this).get(RatingsViewModel::class.java)
        userNotesAdapter= UserNotesAdapter(activity!!.applicationContext)

        view.rec_comments.adapter=userNotesAdapter

        ratingsViewModel.showRating(UserInfo(activity!!.applicationContext).getUserToken())
        ratingsViewModel.notesMutableLiveData.observe(this, androidx.lifecycle.Observer { t->

            if (t.error == 3) {
                Utils.signout(requireActivity() , view!!)
                return@Observer;
            }
//            view.progressBar.visibility=View.GONE
            userNotesAdapter.setData(t.data as ArrayList<Comment>)

            if(t.data.size == 0)
            {
                view.no_data.visibility = View.VISIBLE
            }

        })


        view.back.setOnClickListener{ view_ ->
            intent.putExtra(StoresMainActivity.KEY,StoresMainActivity.SETTINGS)
            startActivity(intent)
            requireActivity().finish()
//            view_.findNavController().popBackStack()
        }

        return view
    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}


/*
* val callback: OnBackPressedCallback = object : OnBackPressedCallback(true /* enabled by default */) {
                override fun handleOnBackPressed() {
                    // Handle the back button event
                    Navigation.findNavController(view_).navigate(R.id.orderChatFragment)
                }
            }
            requireActivity().onBackPressedDispatcher.addCallback(callback)*/