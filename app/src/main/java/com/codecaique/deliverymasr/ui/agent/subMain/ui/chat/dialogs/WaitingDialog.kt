package com.codecaique.deliverymasr.ui.agent.subMain.ui.chat.dialogs

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import com.codecaique.deliverymasr.R
import kotlinx.android.synthetic.main.fragment_confirm_my_order.*
import kotlinx.android.synthetic.main.fragment_special_order.*
import kotlinx.android.synthetic.main.fragment_special_order.buttoConfirm
import kotlinx.android.synthetic.main.fragment_special_order.con2

class WaitingDialog(var context_: Context, var state_: String) : Dialog(context_) {

    private val TAG = "WaitingDialog"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_special_order)
        setCancelable(false)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        if (state_ == "1") {
            tv_ensure.text = context_.getString(R.string.ensure_push)
        }
        else{
            con2.visibility=View.GONE
        }

        settext()
        buttoConfirm.setOnClickListener {
            Log.e(TAG, "onCreate: click")
            val dialog=WaitDialog2(context_)
            dialog.show()
            this.dismiss()
        }


        //        startTimer()

    }

    private fun startTimer() {

        val timer = object : CountDownTimer(60000, 1000) {
            @SuppressLint("SetTextI18n")
            override fun onTick(millisUntilFinished: Long) {

            //      tv_timer.text = context_.getString(R.string.can_wait) + " ${millisUntilFinished / 1000} " + context_.getString(R.string.wait_while_can_be_draw)

            }

            override fun onFinish() {

                //        btn_close.visibility = View.VISIBLE

            }
        }
        timer.start()

    }
    public fun settext(){
        tv.text="اهلا بك:"
        txt_confirmm.text="تأكد من جدية الطلب لانك الوحيد المسئول عنه"
    }
}
