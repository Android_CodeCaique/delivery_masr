package com.codecaique.deliverymasr.ui.client.chat

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Activity.RESULT_CANCELED
import android.app.Activity.RESULT_OK
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.icu.text.SimpleDateFormat
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import cafe.adriel.androidaudiorecorder.AndroidAudioRecorder
import cafe.adriel.androidaudiorecorder.model.AudioChannel
import cafe.adriel.androidaudiorecorder.model.AudioSampleRate
import cafe.adriel.androidaudiorecorder.model.AudioSource
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.SubMainActivity
import com.codecaique.deliverymasr.base.client.SubActivity
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.response.*
import com.codecaique.deliverymasr.pojo.response.billmodel.Data
import com.codecaique.deliverymasr.pojo.services.DeliveredDialog
import com.codecaique.deliverymasr.pojo.util.LocationInfo
import com.codecaique.deliverymasr.pojo.util.MyUtil
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.agent.subMain.ui.postproblem.PostProblemFragment
import com.codecaique.deliverymasr.ui.client.chat.ui.ChooseActionDialog
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import com.codecaique.deliverymasr.ui.client.stores.adapters.StoresAdapter
import com.codecaique.deliverymasr.ui.client.tracking.TrackingActivity
import com.codecaique.deliverymasr.ui.client.userNotes.UserRatingFragment
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.oppwa.mobile.connect.checkout.dialog.CheckoutActivity
import com.oppwa.mobile.connect.checkout.meta.CheckoutSettings
import com.oppwa.mobile.connect.exception.PaymentError
import com.oppwa.mobile.connect.provider.Connect
import com.squareup.picasso.Picasso
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import io.reactivex.plugins.RxJavaPlugins
import kotlinx.android.synthetic.main.dialog_attach_photo.*
import kotlinx.android.synthetic.main.dialog_order_sent.*
import kotlinx.android.synthetic.main.dialog_pay_method.*
import kotlinx.android.synthetic.main.dialog_rate_user.*
import kotlinx.android.synthetic.main.dialog_rate_user.progressBar
import kotlinx.android.synthetic.main.dilalog_user_orders.*
import kotlinx.android.synthetic.main.dilalog_user_orders.civ_user
import kotlinx.android.synthetic.main.dilalog_user_orders.rate
import kotlinx.android.synthetic.main.dilalog_user_orders.tv_user_name
import kotlinx.android.synthetic.main.fragment_chat.*
import kotlinx.android.synthetic.main.fragment_chat.view.*
import kotlinx.android.synthetic.main.fragment_confirm_my_order.*
import kotlinx.android.synthetic.main.order_deliverd_or_no.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import pl.aprilapps.easyphotopicker.ChooserType
import pl.aprilapps.easyphotopicker.EasyImage
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.ParseException
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

@Suppress("UNREACHABLE_CODE")
class ChatFragment : Fragment() {
    private val TAG = "Client ChatFragment"
    private var easyImage: EasyImage? = null

    var img_user: ImageView? = null
    var iv_menu: ImageView? = null
    var cinComments: ConstraintLayout? = null
    private var dialog: Dialog? = null
    lateinit var mandowpData: User
    lateinit var chatAdapter: ChatAdapter
    lateinit var locationInfo: LocationInfo
    lateinit var date5: Date
    lateinit var loadingDialog: LoadingDialog
    var currentPhotoPath: String? = null
    var imageUploadType = ""
    private var files: String = ""

    var phone = ""
    var client_rated = false
    var driver_rated = false
    lateinit var chatViewModel: ChatViewModel
    lateinit var userInfo: UserInfo
    lateinit var bundle: Bundle
    var client_pay: Boolean = false
     var p: String=""
    var userId = 0
    var requestId = 0
    var orderState = ""

    companion object {
        val USER_ID = "user_id"
        val REQUEST_ID = "request_id"
        val TYPE = "type"
        val ACTIVITY = "activity"
        val FRAGMENT = "FRAGMENT"
    }

    var deliveryImage = ""
    var re_lat = ""
    var re_long = ""
    var client_lat = ""
    var client_long = ""
    var distance = ""
    var shopName = ""
     var uri: Uri?=null
    lateinit var VIEW: View
    var isOrderRated = false

    lateinit var userData: ShowUserOfOrderResponse

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Log.e(TAG, "onCreate: ----->>>")
        RxJavaPlugins.setErrorHandler { _ -> } // nothing or some logging


        chatViewModel = ViewModelProviders.of(this).get(ChatViewModel::class.java)
        userInfo = UserInfo(activity!!.applicationContext)
        bundle = Bundle()
        chatAdapter = ChatAdapter(activity!!.applicationContext)
        locationInfo = LocationInfo(activity!!.applicationContext)
//        isOrderDataShow=

//        fillOrderData()
    }

    protected fun onPostExecute(result: Void?) {
        if (isAdded) {
            resources.getString(R.string.app_name)
        }
    }
    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        /*wait ,
        processed,
        received,
        deliverd,
        arrived_site,
        make_bill,
        cancled
            */

        // Inflate the layout for this fragment
        VIEW = inflater.inflate(R.layout.fragment_chat, container, false)

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true/*  enabled by default*/) {
            override fun handleOnBackPressed() {
//                 Handle the back button event
                requireActivity().finish()
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
        loadingDialog = LoadingDialog(activity!!)

//        disposable.dispose()

        try {

            userId = arguments!!.getString(USER_ID)!!.toInt()
            requestId = arguments!!.getString(REQUEST_ID)!!.toInt()

        } catch (e: Exception) {

            userId = activity?.intent?.extras?.getString(USER_ID)!!.toInt()
            requestId = activity?.intent?.extras?.getString(REQUEST_ID)!!.toInt()

        }
        Log.e("aaa", "$requestId  &&  $userId")
        date5 = Date()

        VIEW.back.setOnClickListener { activity!!.finish() }

        Log.e("dtata", isOrderRated.toString())

        VIEW.iv_loc.visibility = View.GONE
        VIEW.tv_loc.visibility = View.GONE
        VIEW.iv_loc_deliver.visibility = View.GONE
        VIEW.tv_loc_deliver.visibility = View.GONE
        VIEW.tv_pay_type.visibility = View.GONE

        db = FirebaseFirestore.getInstance()
                .collection(ApiCreater.OrdersCollection)
                .document(requestId.toString())
                .collection(ApiCreater.ChatCollection)

        mStorageRef = FirebaseStorage.getInstance()
                .getReference(ApiCreater.OrdersStorage)
                .child(requestId.toString())
        easyImage =EasyImage.Builder(activity!!)
                .setChooserTitle("Pick media")
                .setCopyImagesToPublicGalleryFolder(false)     //.setChooserType(ChooserType.CAMERA_AND_DOCUMENTS)
                .setChooserType(ChooserType.CAMERA_AND_GALLERY)
                .setFolderName("EasyImage sample")
                .allowMultiple(false)
                .build()

//        fillChat()
        realTimeChat()
        fillUserData()
        swipeToRefresh()

//        VIEW.btn_pay.visibility = View.GONE
        VIEW.btn_pay.setOnClickListener {

            //todo now
            paymentDialog()

//            it.findNavController().navigate(R.id.)
//            val farFromYouFrag = PushBillFragment()
//            val manager = fragmentManager
//            manager!!.beginTransaction().replace(R.id.main3_fragment, farFromYouFrag, farFromYouFrag.tag).commit()
        }
        VIEW.btn_c.setOnClickListener {
            if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
                openAudion()

                true
            } else {

                ActivityCompat.requestPermissions(
                        activity!!,
                        arrayOf(Manifest.permission.RECORD_AUDIO, Manifest.permission.WAKE_LOCK), 0
                )
                false
            }

        }

        img_user = VIEW.findViewById(R.id.img_user)
        img_user!!.setOnClickListener {

            val loadingDialog = LoadingDialog(activity!!)
            loadingDialog.show()

            chatViewModel.showUserData(userInfo.getUserToken(), userId)
            chatViewModel.showUserOfOrderMutableLiveData.observe(this, Observer {

                Log.e("aaa", "requestId userId")
                loadingDialog.dismiss()

                userData = it

            })

            dialogShowUser(img_user, userData.data[0])


        }

        iv_menu = VIEW.findViewById(R.id.btn_more)

        VIEW.lv_chat.adapter = chatAdapter

        VIEW.et.requestFocus()


        VIEW.btn_send.setOnClickListener {

            val msg = VIEW.et.text.toString()

            if (msg.isNotEmpty()) {

                VIEW.btn_send.visibility = View.GONE
                VIEW.progressBar.visibility = View.VISIBLE


                ApiCreater.instance.sentMessage(
                        RequestBody.create(MediaType.parse("multipart/form-data"), UserInfo(activity!!.applicationContext).getUserToken()),
                        userId, RequestBody.create(MediaType.parse("multipart/form-data"), msg), null, requestId, null).enqueue(object : Callback<SentMessageResponse> {
                    override fun onFailure(call: Call<SentMessageResponse>, t: Throwable) {

                        Toast.makeText(activity!!, getString(R.string.internt_error), Toast.LENGTH_SHORT).show()

                    }

                    override fun onResponse(call: Call<SentMessageResponse>, response: Response<SentMessageResponse>) {
                        VIEW.btn_send.visibility = View.VISIBLE
                        VIEW.progressBar.visibility = View.GONE
//                        fillChat()
                        val message = Message()
                        message.message = msg
                        message.order_state = orderState
                        sendMessageToCloud(message)
                        VIEW.et.setText("")
//                        VIEW.lv_chat.setSelection(chatAdapter.list.size - 1)

                    }
                })


            }

        }

        VIEW.iv_loc.setOnClickListener {

            val b = Bundle()
            b.putString("deliveryImage", deliveryImage)
            b.putString("re_long", re_long)
            b.putString("re_lat", re_lat)
            b.putString("client_long", client_long)
            b.putString("client_lat", client_lat)
//            b.putString("re_lat",re_lat)
            b.putString("distance", distance)
            b.putString("shopName", shopName)
            b.putString("requestId", requestId.toString())
            b.putString("type", "receive")

            it.findNavController().navigate(R.id.farFromYouFragment, b)

        }

        VIEW.iv_loc_deliver.setOnClickListener {
            val b = Bundle()
            b.putString("deliveryImage", deliveryImage)
            b.putString("re_long", re_long)
            b.putString("re_lat", re_lat)
            b.putString("client_long", client_long)
            b.putString("client_lat", client_lat)
//            b.putString("re_lat",re_lat)
            b.putString("distance", distance)
            b.putString("shopName", shopName)
            b.putString("requestId", requestId.toString())
            b.putString("type", "deliver")


            it.findNavController().navigate(R.id.farFromYouFragment, b)
        }

        VIEW.img_call.setOnClickListener {

            if (phone.isNotEmpty()) {
                val i = Intent(Intent.ACTION_DIAL)
                i.data = Uri.parse("tel:$phone")
                if (i.resolveActivity(activity!!.packageManager) != null)
                    startActivity(i)
            } else {
                Toast.makeText(
                        activity!!.applicationContext, "Know Error",
                        Toast.LENGTH_SHORT
                ).show()
            }

        }

        VIEW.btn_v.setOnClickListener {
            dialogGetPhoto()
        }

        return VIEW
    }

    private fun openAudion() {
        files = Environment.getExternalStorageDirectory().toString() + "/recorded_audio.wav"
        val color = resources.getColor(R.color.colorPrimaryDark)
        val requestCode = 0

        AndroidAudioRecorder.with(requireActivity()) // Required
                .setFilePath(files)
                .setColor(color)
                .setRequestCode(requestCode) // Optional
                .setSource(AudioSource.MIC)
                .setChannel(AudioChannel.STEREO)
                .setSampleRate(AudioSampleRate.HZ_48000)
                .setAutoStart(true)
                .setKeepDisplayOn(true) // Start recording
                .record()
    }

    private fun dialogGetPhoto() {
        dialog = Dialog(requireActivity())
        dialog!!.setContentView(R.layout.dialog_attach_photo)
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))


        dialog!!.tv_camera_.setOnClickListener {

       //   isStoragePermissionGranted(222)
//        easyImage!!.openCameraForImage(activity!!)
          //  CropImage.activity().start(requireContext(),this)
            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(activity!!)

            dialog!!.cancel()
        }

        dialog!!.tv_media.setOnClickListener {

            isStoragePermissionGranted(111)
            dialog!!.cancel()
        }
        dialog!!.show()
    }

    @SuppressLint("SetTextI18n")
    fun dialogShowUser(view: View?, user: User) {
        dialog = Dialog(view!!.context)
        dialog!!.setContentView(R.layout.dilalog_user_orders)
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        dialog!!.dismiss()
        cinComments = dialog!!.findViewById(R.id.cinComments)
        cinComments!!.setEnabled(true)
        cinComments!!.setOnClickListener(View.OnClickListener {

            //TODO("now")
            val intent = Intent(context, SubActivity::class.java)
            StoresAdapter.fragName = R.layout.fragment_user_rating
            intent.putExtra(UserRatingFragment.USER_ID, userId.toString())
            intent.putExtra(SubActivity.KEY, SubActivity.USER_RATE)
            startActivity(intent)

            //                UserRating userRating = new UserRating();
//                FragmentManager manager = getFragmentManager();
//                manager.beginTransaction().replace(R.id.main3_fragment, userRating, userRating.getTag()).commit();
        })

        Log.e("aaa", "requestId  &&  userId")


        if (user.image != null && user.image.isNotEmpty())
            Picasso.get().load(ApiCreater.USER_URL + user.image).into(dialog!!.civ_user)

        if (user.rate.isNotEmpty())
            dialog!!.rate.rating = user.rate.toFloat()

        dialog!!.tv_user_name.text = user.first_name
        dialog!!.text_order_num.text = user.requestes_count.toString() + " "
        dialog!!.text_comments_num.text = user.user_comments.toString()

        dialog!!.show()


    }

    private fun rateDialog(message_id: String) {
        val dialog1 = Dialog(activity!!)
        dialog1.setContentView(R.layout.dialog_rate_user)

        Log.e(TAG, "rateDialog: Test")

        try {
            dialog1.tv_user_name.text = mandowpData.first_name
        } catch (e: Exception) {
        }

        chatViewModel.showUserOfOrderMutableLiveData.observe(this, Observer {

            mandowpData = it.data[0]

            Log.e(TAG, "rateDialog: Test 2")
            if (mandowpData.image != null && mandowpData.image.isNotEmpty())
                Picasso.get().load(ApiCreater.USER_URL + mandowpData.image).into(dialog1.civ_user)


            Log.e(TAG, "rateDialog: Test 3")
            dialog1.btn_done.text = getString(R.string.thanks)
            dialog1.btn_done.setOnClickListener {

                if (dialog1.smile_rating.rating < 1) {
                    Toast.makeText(activity, getString(R.string.ratt), Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }

                dialog1.btn_done.visibility = View.GONE
                dialog1.progressBar.visibility = View.VISIBLE

                chatViewModel.rateUser(
                        RequestBody.create(MediaType.parse("multipart/form-data"), userInfo.getUserToken()),
                        userId, dialog1.smile_rating.rating.toDouble(),
                        RequestBody.create(MediaType.parse("multipart/form-data"), dialog1.et_comment.text.toString() + " "),
                        requestId,
                        dialog1
                )

                chatViewModel.rateUserMutableLiveData.observe(this, Observer { t ->

                    Log.e(TAG, "rateDialog: ${t.error} aa ${t.message}")

                    if (t.error == 0) {
                        updateRateState(message_id, dialog1)
                        dialog1.dismiss()
//                    fillOrderData()

                    } else {
                        Log.e("TAGA 00", t.error.toString())
                        progressBar.visibility = View.INVISIBLE
                        dialog1.dismiss()
                    }
                    dialog1.dismiss()


                })


            }


            Log.e(TAG, "rateDialog: ${userInfo.getKeyRate(requestId.toString())} ")
            if (!userInfo.getKeyRate(requestId.toString())) {
                userInfo.putKeyRate(requestId.toString())
                dialog1.show()
            }

        })

    }

    private fun dialogShowMenu(view: View, date: Date?, userId: Int) {
        dialog = ChooseActionDialog(activity!!, chatViewModel, requestId, userId, view, date, userId)

        //dialog!!.setContentView(R.layout.dialog_menu)
        val window = dialog!!.window
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val wlp = window!!.attributes
        wlp.gravity = Gravity.BOTTOM
        wlp.flags = wlp.flags and WindowManager.LayoutParams.FLAG_DIM_BEHIND.inv()
        window.attributes = wlp

        dialog!!.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)




        when (resultCode) {

            Activity.RESULT_OK -> {
                when (requestCode) {
                    111 -> {

                        data?.let {
//                            val photo = data.extras!!["data"] as Bitmap?
//                            VIEW.img_call.setImageBitmap(photo)

                            uri = data.data!!

                            //btn_v
                            VIEW.btn_v.visibility = View.INVISIBLE
                            VIEW.btn_send.visibility = View.GONE
                            VIEW.progressBar.visibility = View.VISIBLE
                            imageUploadType = "gallery"



                            ApiCreater.instance.sentMessage(
                                    RequestBody.create(MediaType.parse("multipart/form-data"), UserInfo(activity!!.applicationContext).getUserToken()),
                                    userId, RequestBody.create(MediaType.parse("multipart/form-data"), ""), getImage(), requestId, null).enqueue(object : Callback<SentMessageResponse> {
                                override fun onFailure(call: Call<SentMessageResponse>, t: Throwable) {

                                    Toast.makeText(activity!!, getString(R.string.internt_error), Toast.LENGTH_SHORT).show()

                                }

                                override fun onResponse(call: Call<SentMessageResponse>, response: Response<SentMessageResponse>) {

                                    val t = response.body()!!
                                    VIEW.btn_v.visibility = View.VISIBLE
                                    VIEW.btn_send.visibility = View.VISIBLE
                                    VIEW.progressBar.visibility = View.GONE

                                    Log.e("ddatata", "userId $userId requestId $requestId t.data.id,  ${t.data.id}, t.data.image ${t.data.image} , t.data.message ${t.data.message}")

                                    val message = Message()
                                    message.image = "not null"
                                    message.order_state = orderState
                                    sendMessageToCloud(message)


                                    /*     chatAdapter.addMsg(Message(
                                                 "", t.data.id, t.data.image, "", userId.toString(), "", "", "", requestId.toString(),
                                                 userInfo.getId().toString(), "", "", "", "", ""
                                         ))
                                    */
//                                    VIEW.lv_chat.setSelection(chatAdapter.list.size - 1)

                                }
                            })

                        }

                    }

                    203 -> {

//                        if (data != null) {
//                            uri = data.data!!
//                        }
                        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                            val result = CropImage.getActivityResult(data)
                            if (resultCode == RESULT_OK) {
                                uri = result.uri
                            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                                val error = result.error
                            }
                        }


                        imageUploadType = "camera"
                        VIEW.btn_v.visibility = View.INVISIBLE
                        VIEW.btn_send.visibility = View.GONE
                        VIEW.progressBar.visibility = View.VISIBLE

                        //     val photo = data!!.extras!!["data"] as Bitmap?
                        val bitmapFromMedia = BitmapFactory.decodeFile(currentPhotoPath)

                        //img_call.setImageBitmap(photo)
                        // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                        // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                        //  var selectedImage = getImageUri(activity!!, photo!!)
//                        val realPath = getRealPathFromURI(selectedImage)
//                        selectedImage = Uri.parse(realPath)
                        ApiCreater.instance.sentMessage(
                                RequestBody.create(MediaType.parse("multipart/form-data"), UserInfo(activity!!.applicationContext).getUserToken()),
                                userId, RequestBody.create(MediaType.parse("multipart/form-data"), ""), getImage(), requestId, null).enqueue(object : Callback<SentMessageResponse> {
                            override fun onFailure(call: Call<SentMessageResponse>, t: Throwable) {

                                Toast.makeText(activity!!, getString(R.string.internt_error), Toast.LENGTH_SHORT).show()

                            }

                            override fun onResponse(call: Call<SentMessageResponse>, response: Response<SentMessageResponse>) {

                                val t = response.body()!!
                                VIEW.btn_v.visibility = View.VISIBLE
                                VIEW.btn_send.visibility = View.VISIBLE
                                VIEW.progressBar.visibility = View.GONE

                                Log.e("ddatata", "userId $userId requestId $requestId t.data.id,  ${t.data.id}, t.data.image ${t.data.image} , t.data.message ${t.data.message}")

                                val message = Message()
                                message.image = "not null"
                                message.order_state = orderState
                                sendMessageToCloud(message)


                                /*     chatAdapter.addMsg(Message(
                                             "", t.data.id, t.data.image, "", userId.toString(), "", "", "", requestId.toString(),
                                             userInfo.getId().toString(), "", "", "", "", ""
                                     ))
                                */
//                                    VIEW.lv_chat.setSelection(chatAdapter.list.size - 1)

                            }
                        })
                    }
                    0 -> {


                        if (resultCode == RESULT_OK) {
                            // Great! User has recorded and saved the audio file

                            uri = Uri.fromFile(File(files))

                            VIEW.btn_v.visibility = View.INVISIBLE
                            VIEW.btn_send.visibility = View.GONE
                            VIEW.progressBar.visibility = View.VISIBLE

                            val bitmapFromMedia = BitmapFactory.decodeFile(currentPhotoPath)
//                        VIEW.image_bill.setImageBitmap(bitmapFromMedia)

                            ApiCreater.instance.sentMessage(
                                    RequestBody.create(MediaType.parse("multipart/form-data"), UserInfo(activity!!.applicationContext).getUserToken()),
                                    userId, RequestBody.create(MediaType.parse("multipart/form-data"), ""), null, requestId, getvoice()).enqueue(object : Callback<SentMessageResponse> {
                                override fun onFailure(call: Call<SentMessageResponse>, t: Throwable) {

                                    Toast.makeText(activity!!, getString(R.string.internt_error), Toast.LENGTH_SHORT).show()

                                }

                                override fun onResponse(call: Call<SentMessageResponse>, response: Response<SentMessageResponse>) {

                                    val t = response.body()!!
                                    VIEW.btn_v.visibility = View.VISIBLE
                                    VIEW.btn_send.visibility = View.VISIBLE
                                    VIEW.progressBar.visibility = View.GONE

                                    Log.e("ddatata", "userId $userId requestId $requestId t.data.id,  ${t.data.id}, t.data.image ${t.data.image} , t.data.message ${t.data.message}")

                                    val message = Message()
                                    message.image = "not null"
                                    message.order_state = orderState
                                    sendvoicetocloud(message)


                                    /*     chatAdapter.addMsg(Message(
                                         "", t.data.id, t.data.image, "", userId.toString(), "", "", "", requestId.toString(),
                                         userInfo.getId().toString(), "", "", "", "", ""
                                 ))
                            */
//                                    VIEW.lv_chat.setSelection(chatAdapter.list.size - 1)

                                }
                            })


                        } else if (resultCode == RESULT_CANCELED) {
                            // Oops! User has canceled the recording
                        }


                    }

                    else -> {
                        Log.e(TAG, "onActivityResult: other ")
                    }
                }

            }

            CheckoutActivity.RESULT_OK -> {
                when (requestCode) {
                    CheckoutActivity.REQUEST_CODE_CHECKOUT -> {

                        paymentState(checkoutId)
                    }
                    else -> {
                        Log.e(TAG, "onActivityResult: CheckoutActivity no thing" +
                                "\n requestCode $requestCode")
                        Toast.makeText(activity!!, " خطأ غير معروف", Toast.LENGTH_SHORT).show()
                    }

                }
            }
//            0->{
////                if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
////                    val result:CropImage.ActivityResult = CropImage.getActivityResult(data)
////                    if (resultCode == 0) {
////                        uri = result.uri
////                    } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
////                        val error = result.error
////                    }
////                }
//                Toast.makeText(activity!!, " خطأ غير معروف", Toast.LENGTH_SHORT).show()
//
//            }

            CheckoutActivity.RESULT_CANCELED -> {
                Log.e(TAG, "onActivityResult: CheckoutActivity.RESULT_CANCELED")
                Toast.makeText(activity!!, " خطأ غير معروف", Toast.LENGTH_SHORT).show()
            }

            CheckoutActivity.RESULT_ERROR -> {
                Log.e(TAG, "onActivityResult: CheckoutActivity.RESULT_ERROR")
                val error: PaymentError = data!!.getParcelableExtra(CheckoutActivity.CHECKOUT_RESULT_ERROR)!!
                Log.e(TAG, "onActivityResult: CheckoutActivity.RESULT_ERROR ${error.errorMessage}")
                Toast.makeText(activity!!, " خطأ غير معروف", Toast.LENGTH_SHORT).show()
            }


            else -> {
                Log.e(TAG, "onActivityResult: RESULT  NOT OK")
            }

        }

    }

    fun getvoice():MultipartBody.Part?{

        // creates RequestBody instance from file
        val file = File(uri!!.path)
        // creates RequestBody instance from file
        val requestFile: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        return MultipartBody.Part.createFormData("voice", file.name, requestFile)

    }

    private fun sendvoicetocloud(message: Message){
        message.id = db.document().id
        message.created_at = System.currentTimeMillis().toString()
        message.updated_at = System.currentTimeMillis().toString()
        message.sender_id = userInfo.getId().toString()
        message.receiver_id = userId.toString()
        message.request_id = requestId.toString()
        val voicename = MyUtil.getRandomName() + ".mp3"
        mStorageRef = FirebaseStorage.getInstance()
                .getReference(ApiCreater.OrdersStorage)
                .child(requestId.toString())
        mStorageRef.child(voicename).putFile(uri!!).continueWithTask {
            mStorageRef.child(voicename).downloadUrl
        }.addOnSuccessListener {
            message.voice = it.toString()
            sendTheMessage(message)
        }

    }

    private fun clientSayPayment() {

        chatViewModel.clientSayPayment(userInfo.getUserToken(), requestId, userId)
        chatViewModel.clientSayPaymentMutableLiveData.observe(this, Observer {

            val message = Message()
            message.id = db.document().id
            message.created_at = System.currentTimeMillis().toString()
            message.updated_at = System.currentTimeMillis().toString()
            message.receiver_id = userId.toString()
            message.sender_id = userInfo.getId().toString()
            message.request_id = requestId.toString()
            message.message = ApiCreater.OrderMessages.PaymentDoneMessage
            client_pay = true
            sendTheMessage(message)

        })
    }

    fun getImage(): MultipartBody.Part? {
        if (uri != null) {

            val cursor = activity!!.contentResolver.query(uri!!, null, null, null, null)
            p = if (cursor == null) {
                uri!!.path.toString()
            } else {
                cursor.moveToFirst()
                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                cursor.getString(idx)
            }

            val file = File(p)
            val requestFile = RequestBody.create(MediaType.parse("image/*"), file)
            return MultipartBody.Part.createFormData("image", file.name, requestFile)
        } else if (currentPhotoPath != null) {


            uri = Uri.parse(currentPhotoPath)

            Log.e("URI 222", uri!!.path.toString() + "    ")

            val p: String
            val cursor = activity!!.contentResolver.query(uri!!, null, null, null, null)

            if (cursor == null) {
                p = uri!!.path.toString()
                Log.e("URI 222 0000", uri!!.path.toString() + "    ")
            } else {
                cursor.moveToFirst()
                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                Log.e("URI 222 005500", uri!!.path.toString() + "    ")


                p = cursor.getString(idx)
            }

            val file = File(p)
            val requestFile = RequestBody.create(MediaType.parse("image/*"), file)
            return MultipartBody.Part.createFormData("image", file.name, requestFile)
        }

        return null

    }

    fun getImageUri(inContext: Context, inImage: Bitmap): Uri? {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null)
        return Uri.parse(path)
    }

    fun getRealPathFromURI(contentUri: Uri?): String? {
        var cursor: Cursor? = null
        return try {
            val proj = arrayOf(MediaStore.Images.Media.DATA)
            cursor = activity!!.contentResolver.query(contentUri!!, proj, null, null, null)
            val column_index: Int = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            cursor.getString(column_index)
        } finally {
            if (cursor != null) {
                cursor.close()
            }
        }
    }

    @SuppressLint("WrongConstant")
    fun isStoragePermissionGranted(requestCode: Int): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return if (requestCode == 111) {
                when (PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.checkSelfPermission(context!!, Manifest.permission.READ_EXTERNAL_STORAGE) -> {
                        gotoImage(requestCode)

                        true
                    }
                    else -> {

                        ActivityCompat.requestPermissions(
                                activity!!,
                                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA),
                                requestCode
                        )
                        false
                    }
                }
            } else {
                if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    gotoImage(requestCode)

                    true
                } else {

                    ActivityCompat.requestPermissions(
                            activity!!,
                            arrayOf(Manifest.permission.CAMERA),
                            requestCode
                    )
                    false
                }
            }
        } else {

            gotoImage(requestCode)
            return true
        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            gotoImage(requestCode)
        }
        if (grantResults.isNotEmpty() && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            gotoImage(requestCode)
        }
    }

    private fun gotoImage(requestCode: Int) {

        val intent: Intent

        if (requestCode == 111) {

            intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), requestCode)

        } else {

            /*                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePicture, PICK_IMAGE_FROM_CAMERA);

                SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                int imageId=pref.getInt("image",0);

                File path = new File(getActivity().getFilesDir(), "Gallery/MyImages/");
                if (!path.exists()) path.mkdirs();
                File image = new File(path, "image_capture_"+""+imageId+".jpg");
                Uri imageUri = FileProvider.getUriForFile(getActivity().getApplicationContext(), CAPTURE_IMAGE_FILE_PROVIDER, image);
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                providePermissionForProvider(cameraIntent, imageUri);
                startActivityForResult(cameraIntent, PICK_IMAGE_FROM_CAMERA);*/

            //val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            // Ensure that there's a camera activity to handle the intent
            // Ensure that there's a camera activity to handle the intent
            val photo = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)

            var uri = Uri.parse("file:///sdcard/photo.jpg")
            if (photo.resolveActivity(activity!!.packageManager) != null) {
                // Create the File where the photo should go
                var photoFile: File? = null
                try {
                    photoFile = createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    uri = try {
                        FileProvider.getUriForFile(context!!,
                                "${requireActivity().applicationContext.packageName}.fileprovider",
                                photoFile)
                      //  Uri.fromFile(photoFile)

                    } catch (ex: IOException) {
                        Uri.fromFile(photoFile)
                    }
                    photo.putExtra("return-data", true);
                    photo.putExtra(MediaStore.EXTRA_OUTPUT, uri)
                    startActivityForResult(Intent.createChooser(photo, "Select Picture"), 222)
                    //   startActivityForResult(photo, 111)

                }
            }


        }
    }

    private fun galleryAddPic() {
        val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val f = File(p)
        val contentUri = Uri.fromFile(f)
        mediaScanIntent.data = contentUri
        activity!!.sendBroadcast(mediaScanIntent)
    }

    @SuppressLint("SimpleDateFormat")
    @Throws(IOException::class)
    private fun createImageFile(): File? {
        // Create an image file name
        val timeStamp = java.text.SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = activity!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",  /* suffix */
                storageDir /* directory */
        )

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.absolutePath
        return image
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")
        Log.e(TAG, "onResume: aa")

        try {

            if (locationInfo.isLocationShare()) {

                val msg = "http://maps.google.com/maps?q=loc:${locationInfo.getLocationShare().latitude},${locationInfo.getLocationShare().longitude}"

                VIEW.btn_send.visibility = View.GONE
                VIEW.progressBar.visibility = View.VISIBLE


                ApiCreater.instance.sentMessage(
                        RequestBody.create(MediaType.parse("multipart/form-data"), UserInfo(activity!!.applicationContext).getUserToken()),
                        userId, RequestBody.create(MediaType.parse("multipart/form-data"), msg), null, requestId, null).enqueue(object : Callback<SentMessageResponse> {
                    override fun onFailure(call: Call<SentMessageResponse>, t: Throwable) {

                        Toast.makeText(activity!!, getString(R.string.internt_error), Toast.LENGTH_SHORT).show()

                    }

                    override fun onResponse(call: Call<SentMessageResponse>, response: Response<SentMessageResponse>) {

                        VIEW.btn_send.visibility = View.VISIBLE
                        VIEW.progressBar.visibility = View.GONE

                        val t = response.body()!!

                        /*
                        chatAdapter.addMsg(Message(
                                "", t.data.id, "", msg, userId.toString(), "", "", "", requestId.toString(),
                                userInfo.getId().toString(), "", "", "", "", ""
                        ))*/

                        locationInfo.setStreetShare("")
//                        VIEW.lv_chat.setSelection(chatAdapter.list.size - 1)
//                        fillChat()
                        val message = Message()
                        message.message = msg
                        message.message = orderState
                        sendMessageToCloud(message)

                    }
                })

            }

        } catch (e: Exception) {
        }

        /*
        if (disposable.isDisposed)
            disposable = Observable.interval(1000, 30000,
                    TimeUnit.MILLISECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { refreshChat() }*/

        //        fillChat()
        //        fillOrderData()

    }

    lateinit var timeForCloseChat: Date
    lateinit var timeForCloseCall: Date
    lateinit var closeTimer: Date

    @RequiresApi(Build.VERSION_CODES.N)
    private fun fillChat() {

        //todo
//        if (VIEW.refresh_layout_chat.isRefreshing)
//            loadingDialog.dismiss()
        Log.e(TAG, "fillChat: no mo")

        chatViewModel.showMessages(userInfo.getUserToken(), userId, requestId)

        chatViewModel.showMessagesMutableLiveData.observe(this, Observer { t ->
//            view.progressBar.visibility=View.GONE
            chatAdapter.setData(t.data as ArrayList<Message>)
//            VIEW.lv_chat.setSelection(t.data.size - 1)

            loadingDialog.dismiss()
//            if (VIEW.refresh_layout_chat.isRefreshing)
//                VIEW.refresh_layout_chat.isRefreshing = false

            val list = t

            Log.e("TIME asdasda", t.acceptTime)


            Log.e("STATE chat", t.state)

            // when state == processed and user accepts order and enter chat for the first time -->> dialogOrderBegin()

            if (t.acceptTime != "null") {
                val time = t.acceptTime


                val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                try {

                    Log.e("time ttttttt", time + " ")

                    val date: Date = format.parse(time)
                    val t1: Long = date.time

                    date5 = Date(t1 + (5 * 60000))
                    closeTimer = Date(t1 + (7 * 60000))

                    // val date2 = Date(System.currentTimeMillis())

                    val deliveryTime = format.parse(t.deliveryTime.date).time

//                    timeForCloseChat = Date(deliveryTime + (30 * 60000))
//                    timeForCloseCall = Date(deliveryTime + (7 * 60000))

                    /*

                if (closeTime <= date2)
                    tbOptions.visibility= View.GONE
*/

                    for (datum in list.data) {
                        if ((list.state == "processed" || list.state == "received"
                                        || list.state == "deliverd"
                                        || list.state == "make_bill" || list.state == "arrived_site")
                                && dateNow >= date5) {
//                            img_call.visibility = View.VISIBLE
                            iv_menu!!.setOnClickListener { dialogShowMenu(VIEW, date5, userId) }
                            break
                        } else {
                            iv_menu!!.setOnClickListener { dialogShowMenu(VIEW, null, userId) }
                            break
                        }
                    }

                    System.out.println(date)
                } catch (e: ParseException) {

                    Log.e("Error date ", e.message)
                    e.printStackTrace()

                }
            } else {
                iv_menu!!.setOnClickListener { dialogShowMenu(VIEW, null, userId) }
            }

            VIEW.btn_send.visibility = View.VISIBLE
            VIEW.progressBar.visibility = View.GONE
            fillUserData()

        })

    }

    var size = 0
    var lastItemSelected = 0
    private val dateNow = Date(System.currentTimeMillis())
    var date7 = Date()
    var date30 = Date()

    @RequiresApi(Build.VERSION_CODES.N)
    @SuppressLint("SetTextI18n")
    private fun fillOrderData() {

        chatViewModel.showOrderById(userInfo.getUserToken(), requestId)
        chatViewModel.showOrderMutableLiveData.observe(this, Observer {
            VIEW.tv_user_name.text = it.data[0].first_name + " " + it.data[0].last_name
            VIEW.tv_distance.text = it.data[0].phone
            VIEW.tv_cost.text = it.data[0].offer_price


            Log.e(TAG, "fillOrderData now ${it.data.size}")
            try {

                deliveryImage = it.data[0].image
            } catch (e: Exception) {

            }

            try {

                client_lat = it.data[0].client_latitude
                client_long = it.data[0].client_longitude
                distance = it.data[0].agent_shope_distance
                shopName = it.data[0].name
                re_lat = it.data[0].receiving_latitude
                re_long = it.data[0].receiving_longitude

            } catch (e: Exception) {
            }

            VIEW.tv.text = it.data[0].name
            VIEW.tv_num.text = it.data[0].id + " # "
            phone = it.data[0].phone
            VIEW.tv_cost.text = it.data[0].offer_price + "ج"
            val orderState = it.data[0].state
            Log.e(TAG, "OrderData it.id ${it.data[0].id} orderState $orderState")

//            img_call.visibility = View.VISIBLE
            visibleTracking(true)

            when (orderState.trim()) {


                /* "deliverd" -> {

                     val intent = Intent(activity!!, DeliveredDialog::class.java).apply {
                         flags = Intent.FLAG_ACTIVITY_NEW_TASK
                         putExtra("action", "deliverd")
                         putExtra("request_id", requestId.toString())
                         putExtra("user_id", userId.toString())
                     }
                             .apply {
                                 putExtra("action", "delivered")
                                 putExtra("request_id", requestId.toString())
                                 putExtra("user_id", userId.toString())
                             }

                     if (!userInfo.getKeyReceive(requestId.toString())) {

                         userInfo.putKeyReceive(requestId.toString())
                         activity!!.startActivity(intent)
                     }
                     img_call.visibility = View.GONE

                 }*/

                //deliverd
                /* "received_order" -> {

                     btn_more.visibility = View.GONE
                     VIEW.btn_pay.visibility = View.GONE

                     if (timeForCloseChat <= dateNow) {
                         tbOptions.visibility = View.GONE
                     }

                     //     VIEW.tbOptions.visibility = View.GONE
                     //     if (it.data[0].rate_state != "rated" && !isOrderRated)
                     //         refreshChat()
                     //         deliverdOrNo(it.data[0].rate_state)

                     if (!isRateDialogShow) {
                         isRateDialogShow = true
 //                        rateDialog()
                     }
                 }*/

                "received_order" -> {
                    visibleTracking(false)
                }

                "make_bill" -> {

                    if (client_pay)
                        VIEW.btn_pay.visibility = View.GONE
                    else
                        VIEW.btn_pay.visibility = View.VISIBLE
                }

                "processed" -> {

                    dialogOrderBegin()


                }
            }
//            view!!.img_call.visibility = View.VISIBLE

            Log.e(TAG, "fillOrderData: aaaaaaaa")

            /*
            try {

                if (closeTimer <= dateNow) {
                    Log.e(TAG, "fillOrderData: nnnowoewqowq ")
                    Log.e(TAG, "fillOrderData: time - time   :: ${dateNow.time - closeTimer.time}")
                    startTimer(closeTimer.time - dateNow.time)

                } else {
//                view!!.img_call.visibility = View.GONE
                }
*/

            // it.data[0]
            /* } catch (e: Exception) {
             }
 */

            Log.e(TAG, "fillOrderData: zzzzz")

            if (it.data[0].image != null && it.data[0].image.isNotEmpty())
                Picasso.get().load(ApiCreater.USER_URL + it.data[0].image).into(VIEW.img_user)

            if ((orderState == "processed" || orderState == "received" || orderState == "deliverd"
                            || orderState == "make_bill" || orderState == "arrived_site")
            /*&& date2 >= date5*/) {
//                            img_call.visibility = View.VISIBLE
                iv_menu!!.setOnClickListener { dialogShowMenu(VIEW, date5, userId) }
            } else {
                iv_menu!!.setOnClickListener { dialogShowMenu(VIEW, null, userId) }
            }
            Log.e(TAG, "fillOrderData: dddddddddddd")

            if (it.data[0].accepted_time != "null") {
                val time = it.data[0].accepted_time

                val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                try {

                    Log.e("time ttttttt", time + " ")

                    val date: Date = format.parse(time)
                    val t1: Long = date.time

                    date7 = Date(t1 + (7 * 60000))

                    val deliveryTime = format.parse(it.data[0].deliveryTime).time
                    date30 = Date(deliveryTime + (30 * 60000))

                } catch (e: Exception) {
                }


                Log.e(TAG, "fillOrderData: date7 $date7")
                Log.e(TAG, "fillOrderData: date30 $date30")

                if (dateNow > date7) {
                    iv_menu!!.setOnClickListener { dialogShowMenu(VIEW, date7, userId) }
                } else {
                    iv_menu!!.setOnClickListener { dialogShowMenu(VIEW, null, userId) }

                    startTimer(date7.time - dateNow.time)

                }


                if (dateNow > date30 && orderState == "received_order")
                    view!!.tbOptions.visibility = View.GONE


//                }

//            changUIByOrderState(message)

            }

        })

    }


    @SuppressLint("NewApi")
    private fun swipeToRefresh() {

        /* VIEW.refresh_layout_chat.setOnRefreshListener {
 //            fillChat()
             VIEW.refresh_layout_chat.isRefreshing=false
         }
 */
    }


    var isRateDialogShow = false

    private fun refreshChat() {

        Log.e(TAG, "refreshChat: ${System.currentTimeMillis()}")

        chatViewModel.showMessages(userInfo.getUserToken(), userId, requestId)

        chatViewModel.showMessagesMutableLiveData.observe(this, Observer { t ->
//            view.progressBar.visibility=View.GONE
            Log.e(TAG, "refreshChat: ${t.data.size}")
            chatAdapter.setData(t.data as ArrayList<Message>)

            val state: String = t.state

            size = t.data.size

            if (lastItemSelected != t.data.size) {

//                VIEW.lv_chat.setSelection(t.data.size - 1)
                lastItemSelected = t.data.size

            }

        })

//        if (lastItemSelected == size)
//            disposable.dispose()

    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun fillUserData() {

        chatViewModel.showUserData(userInfo.getUserToken(), userId)
        chatViewModel.showUserOfOrderMutableLiveData.observe(this, Observer { t ->


            if (t.data[0].image != null && t.data[0].image.isNotEmpty()) {
                Picasso.get().load(ApiCreater.USER_URL + t.data[0].image).into(VIEW.img_user)
                deliveryImage = t.data[0].image
            }

            mandowpData = t.data[0]

            Log.e("mosratag", "kao777777")

            val formatter: NumberFormat = DecimalFormat("#,##")
            val formattedNumber: String = formatter.format(t.data[0].rate.toFloat())


            VIEW.tv_rate.text = formattedNumber
            VIEW.ratingBar.rating = t.data[0].rate.toFloat()
            VIEW.tv_distance.text = t.data[0].phone
            //  VIEW.tv_user_name.text = t.data[0].first_name
            VIEW.tv_user_name.text = t.data[0].first_name + " " + t.data[0].last_name

            VIEW.iv_deliver_tracking.setOnClickListener {

                val intent = Intent(activity!!, TrackingActivity::class.java)
                intent.putExtra(TrackingActivity.DRIVER_NAME, mandowpData.first_name)
                intent.putExtra(TrackingActivity.DRIVER_ID, mandowpData.id.toString())
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)

            }

            fillOrderData()

        })

    }

    private fun deliveredOrNo(rateState: String) {
        Log.e(TAG, "asasas")
        val dialog = Dialog(activity!!)
        var msg = Message()
        dialog.setContentView(R.layout.order_deliverd_or_no)
        dialog.btn_yes.setOnClickListener {
            dialog.dismiss()
            dialog.cancel()

           rateDialog(msg.id)
//            disposable.dispose()
        }
        dialog.btn_no.setOnClickListener {

            dialog.dismiss()
//           disposable.dispose()
            val intent = Intent(context, SubMainActivity::class.java)
            intent.putExtra(PostProblemFragment.USER_ID, userId.toString())
            intent.putExtra(PostProblemFragment.ORDER_ID, requestId.toString())
            intent.putExtra(SubMainActivity.KEY, SubMainActivity.POSTPROBLEM)
            intent.putExtra(PostProblemFragment.USER_ID, userId.toString())
            intent.putExtra(PostProblemFragment.ORDER_ID, requestId.toString())
            context!!.startActivity(intent)

        }
        dialog.show()

    }

    private fun paymentDialog() {

        val dialog = Dialog(activity!!)
        dialog.setContentView(R.layout.dialog_pay_method)

        dialog.credit.setOnClickListener {


            requestBillId()
            dialog.dismiss()
        }

        dialog.cash.setOnClickListener {
            dialog.dismiss()
        }


        dialog.show()


    }

    private fun requestBillId() {

        loadingDialog.show()
        chatViewModel.showBillData(userInfo.getUserToken(), requestId)
        chatViewModel.billMutableLiveData.observe(this, Observer { t ->

            Log.e(TAG, "requestBillId: t.data ${t.data}")
            requestCheckoutId(t.data)

        })

    }

    var checkoutId = ""

    private fun requestCheckoutId(mBillResponse: List<Data>) {

        val total = (mBillResponse[0].delivery_price + mBillResponse[0].order_price + mBillResponse[0].tax.toInt())

        chatViewModel.paymentRequest(userInfo.getUserToken(), mBillResponse[0].id, total, userInfo.getEmail())
        chatViewModel.requestCheckoutIdMutableLiveData.observe(this, Observer {

            //            paymentState(it.id)
            checkoutId = it.id

            Log.e(TAG, "requestCheckoutId: $checkoutId")

            presentCheckoutUI(checkoutId)

        })

    }

    private fun presentCheckoutUI(checkoutId: String) {

        loadingDialog.dismiss()

        val paymentBrands: MutableSet<String> = LinkedHashSet()

        paymentBrands.add("VISA")
        paymentBrands.add("MASTER")
        paymentBrands.add("DIRECTDEBIT_SEPA")
        paymentBrands.add("APPLEPAY")
        paymentBrands.add("GOOGLEPAY")
        paymentBrands.add("STC_PAY")
        paymentBrands.add("PAYPAL")

        val checkoutSettings = CheckoutSettings(checkoutId, paymentBrands, Connect.ProviderMode.TEST)

        // Set shopper result URL

        checkoutSettings.shopperResultUrl = "Hiakk://result"

        val intent = checkoutSettings.createCheckoutActivityIntent(activity!!)

        startActivityForResult(intent, CheckoutActivity.REQUEST_CODE_CHECKOUT)

    }


    private fun paymentState(requestCheckoutId: String) {


        loadingDialog.show()
        chatViewModel.paymentStatus(userInfo.getUserToken(), requestCheckoutId)
        chatViewModel.paymentStateMutableLiveData.observe(this, Observer {

            Log.e(TAG, "paymentState: $it")

            loadingDialog.dismiss()


            updatePaymentStateUI(it)

        })

    }

    private fun updatePaymentStateUI(it: PaymentStateResponse) {

        when (it.result.code) {
            "000.100.110", "000.100.111", "000.100.112" -> {
                client_pay = true
                clientSayPayment()
                VIEW.btn_pay.visibility = View.GONE
                Toast.makeText(activity!!, "تمت عملية الدفع بنجاج ", Toast.LENGTH_SHORT).show()
            }
            "800.100.151" -> {
                Toast.makeText(activity!!, " بطاقة غير صالحة ", Toast.LENGTH_SHORT).show()
            }
            "800.100.153" -> {
                Toast.makeText(activity!!, " (رمز CVV غير صالح) ", Toast.LENGTH_SHORT).show()
            }
            "800.100.155" -> {
                Toast.makeText(activity!!, "تجاوز المبلغ الائتمان ", Toast.LENGTH_SHORT).show()
            }
            "800.100.157" -> {
                Toast.makeText(activity!!, "  (تاريخ انتهاء صلاحية خاطئ)", Toast.LENGTH_SHORT).show()
            }
            "800.100.172" -> {
                Toast.makeText(activity!!, "  صفقة مرفوضة (الحساب محجوب) ", Toast.LENGTH_SHORT).show()
            }
            "800.100.176" -> {
                Toast.makeText(activity!!, " (الحساب غير متاح مؤقتًا. يرجى المحاولة مرة أخرى لاحقًا) ", Toast.LENGTH_SHORT).show()
            }
            "800.100.192" -> {
                Toast.makeText(activity!!, "  (رمز CVV غير صالح ، لا يزال المبلغ محجوزًا على بطاقة العميل وسيتم تحريره في غضون أيام عمل قليلة. يرجى التأكد من دقة رمز CVV قبل إعادة محاولة المعاملة)", Toast.LENGTH_SHORT).show()
            }
            "800.100.201" -> {
                Toast.makeText(activity!!, "  تفاصيل الحساب أو البنك غير صحيحة", Toast.LENGTH_SHORT).show()
            }
            "800.100.203" -> {
                Toast.makeText(activity!!, " أموال غير كافية ", Toast.LENGTH_SHORT).show()
            }
            "800.100.202" -> {
                Toast.makeText(activity!!, " الحساب مغلق ", Toast.LENGTH_SHORT).show()
            }
            else -> {
                Toast.makeText(activity!!, " خطأ غير معروف", Toast.LENGTH_SHORT).show()
            }


        }

    }

    private fun dialogOrderBegin() {
        val dialog = Dialog(activity!!)
        dialog.setContentView(R.layout.fragment_confirm_my_order)

        if (chatViewModel.showOrderMutableLiveData.value!!.data[0].payment_id == "1") {
            dialog.tv_push.text = getString(R.string.ensure_push_client)

        }

        dialog.buttoConfirm.setOnClickListener {
            dialogOrderBegin2()

            /*    if (disposable.isDisposed)
                disposable = Observable.interval(1000, 30000,
                        TimeUnit.MILLISECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { refreshChat() }
*/

            dialog.dismiss()
            dialog.cancel()
        }

        if (!userInfo.getKey(requestId.toString())) {
            userInfo.putKey(requestId.toString())
            dialog.show()
        }
    }


    private fun dialogOrderBegin2() {
        val dialog = Dialog(activity!!)
        dialog.setContentView(R.layout.dialog_order_sent)

        dialog.order_text.text = resources.getString(R.string.orderBegin)
        dialog.tv_ok.setOnClickListener {
            dialog.dismiss()
            dialog.cancel()
        }
        dialog.show()

    }

    /*

var disposable = Observable.interval(1000, 30000,
        TimeUnit.MILLISECONDS)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe { refreshChat() }



override fun onPause() {
    super.onPause()
    disposable.dispose()
}
*/

    var first = true
    private fun startTimer(end: Long) {

        if (!first)
            return

        if (first)
            first = false

        view!!.tv_pay_type.visibility = View.VISIBLE
//        view!!.img_call.visibility = View.VISIBLE

        val timer = object : CountDownTimer(end, 1000) {
            @SuppressLint("SetTextI18n")
            override fun onTick(millisUntilFinished: Long) {

                try {

                    view!!.tv_pay_type.text = getString(R.string.can_be_exit_order) + " ${(millisUntilFinished % 60000) / 1000} : ${millisUntilFinished / 60000} " + getString(R.string.minutes)

                } catch (e: Exception) {
                }

            }

            override fun onFinish() {

                try {
                    VIEW.tv_pay_type.visibility = View.GONE
                } catch (e: Exception) {
                }

            }
        }

        timer.start()

    }

    private var db = FirebaseFirestore.getInstance()
            .collection(ApiCreater.OrdersCollection)

    private var mStorageRef: StorageReference = FirebaseStorage.getInstance().getReference(ApiCreater.OrdersStorage).child(requestId.toString())

    @RequiresApi(Build.VERSION_CODES.N)
    private fun realTimeChat() {

        Log.e(TAG, "realTimeChat: requestId $requestId")

        var msg = Message()

        db.orderBy("created_at").addSnapshotListener { querySnapshot, _ ->

            val messages = ArrayList<Message>()

            querySnapshot!!.documents.forEach { documentSnapshot ->
                msg = documentSnapshot.toObject(Message::class.java)!!
                messages.add(msg)
            }

            orderState = msg.order_state
            client_rated = msg.client_rated
            driver_rated = msg.driver_rated
            changUIByOrderState(msg, msg.client_rated)
            fillOrderData()

            chatAdapter.submitData(messages)
            VIEW.lv_chat.setSelection(chatAdapter.list.size - 1)

        }


    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun changUIByOrderState(message: Message, clientRated: Boolean) {

        orderState = message.order_state
        client_pay = message.client_pay
        val clientRated__ = message.client_rated
        visibleTracking(true)

        if (client_pay)
            VIEW.btn_pay.visibility = View.GONE

        when (orderState.trim()) {

            "" -> {

            }

            "deliverd" -> {

                val intent = Intent(requireActivity(), DeliveredDialog::class.java)
                intent.apply {
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    putExtra("request_id", requestId.toString())
                    putExtra("user_id", userId.toString())
                    putExtra("action", "delivered")
                    putExtra("user_id", userId.toString())
                }

//                if (!userInfo.getKeyReceive(requestId.toString())) {

//                    userInfo.putKeyReceive(requestId.toString())
                startActivity(intent)
//                }
                img_call.visibility = View.GONE

            }

            //deliverd
            "received_order" -> {

                VIEW.btn_more.visibility = View.GONE
                VIEW.btn_pay.visibility = View.GONE
                VIEW.img_call.visibility = View.GONE
                visibleTracking(false)

                try {
/*

                    if (timeForCloseChat <= dateNow) {
                        tbOptions.visibility = View.GONE
                    }
*/

                } catch (ex: Exception) {
                }


                Log.e(TAG, "changUIByOrderState: clientRated $clientRated")

                if (!clientRated__) {
                    //    rateDialog(message.id)
                }
                /*
                if (!isRateDialogShow) {
                    isRateDialogShow = true
//                        rateDialog()
                }
*/
//
//                fillOrderData()


            }

            "make_bill" -> {
                if (client_pay)
                    VIEW.btn_pay.visibility = View.GONE
                else
                    VIEW.btn_pay.visibility = View.VISIBLE
            }
        }

    }

    private fun sendMessageToCloud(message: Message) {

        message.id = db.document().id
        message.created_at = System.currentTimeMillis().toString()
        message.updated_at = System.currentTimeMillis().toString()
        message.receiver_id = userId.toString()
        message.sender_id = userInfo.getId().toString()
        message.request_id = requestId.toString()

        val imageName = MyUtil.getRandomName() + ".jpg"

        if (message.image.isNotEmpty()) {

            mStorageRef = FirebaseStorage.getInstance()
                    .getReference(ApiCreater.OrdersStorage)
                    .child(requestId.toString())
            if (imageUploadType == "gallery") {

                Log.e(TAG, "sendMessageToCloud: gallery")
                mStorageRef.child(imageName).putFile(uri!!).continueWithTask {
                    mStorageRef.child(imageName).downloadUrl
                }.addOnSuccessListener {
                    message.image = it.toString()
                    sendTheMessage(message)
                }
            } else {
                Log.e(TAG, "sendMessageToCloud: camera")

                mStorageRef.child(imageName).putFile(uri!!).continueWithTask {
                    mStorageRef.child(imageName).downloadUrl
                }.addOnSuccessListener {
                    message.image = it.toString()
                    sendTheMessage(message)
                }.addOnFailureListener {
                    sendTheMessage(message)
                }
            }
        } else {
            sendTheMessage(message)
        }


    }

    fun bitMapToByteArray(): ByteArray {

//       val bitmap = (VIEW.image as BitmapDrawable).bitmap
//        val baos = ByteArrayOutputStream()
//        bitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, baos)
//        /*val b = */return baos.toByteArray()
////        return Base64.encodeToString(b, Base64.DEFAULT).toByteArray()
        var d: Drawable?=null
        val bitmap = (d as? BitmapDrawable)!!.bitmap

        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
        val byteArray = stream.toByteArray()
        bitmap.recycle()
        return stream.toByteArray()
    }

    private fun sendTheMessage(message: Message) {
        message.client_rated = client_rated
        message.driver_rated = driver_rated
        message.client_pay = client_pay
        db.document(message.id).set(message)
    }

    private fun updateRateState(msg_id: String, dialog1: Dialog) {

        val map = HashMap<String, Boolean>()
        map["client_rated"] = true
        map["driver_rated"] = driver_rated

        db.document(msg_id).update(map as Map<String, Any>).addOnSuccessListener {

            userInfo.putKeyRate(requestId.toString())

            dialog1.apply {
                btn_done.visibility = View.GONE
                progressBar.visibility = View.VISIBLE
                dismiss()
            }
            isOrderRated = true
        }

    }

    private fun visibleTracking(vis: Boolean) {

        if (vis) {
            VIEW.iv_deliver_tracking.visibility = View.VISIBLE
            VIEW.tv_deliver_tracking.visibility = View.VISIBLE

        } else {
            VIEW.iv_deliver_tracking.visibility = View.GONE
            VIEW.tv_deliver_tracking.visibility = View.GONE
        }

    }

}