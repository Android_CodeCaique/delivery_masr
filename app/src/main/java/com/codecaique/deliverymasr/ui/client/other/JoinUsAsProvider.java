package com.codecaique.deliverymasr.ui.client.other;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.codecaique.deliverymasr.R;
import com.codecaique.deliverymasr.base.client.MainActivity;

import static com.codecaique.deliverymasr.ui.client.stores.adapters.StoresAdapter.fragName;


public class JoinUsAsProvider extends Fragment {

    TextView tv_ok, tv_Continue;
    private Dialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_join_us_as_provider, container, false);
        ImageView back = view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                SittingsFrag sittingsFrag=new SittingsFrag();
//                FragmentManager manager=getFragmentManager();
//                manager.beginTransaction().replace(R.id.main_fragment,sittingsFrag,sittingsFrag.getTag()).commit();
                Intent intent = new Intent(getContext(), MainActivity.class);
                fragName = R.layout.fragment_sittings;
                startActivity(intent);
            }
        });
        tv_Continue = view.findViewById(R.id.tv_Continue);
        tv_Continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCoupon6(tv_Continue);
            }
        });

        return view;
    }

    public void dialogCoupon6(final View view) {

        dialog = new Dialog(view.getContext());
        dialog.setContentView(R.layout.dilalog_coupon6);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.dismiss();
        tv_ok = dialog.findViewById(R.id.tv_ok);
        tv_ok.setEnabled(true);
        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), MainActivity.class);
                fragName = R.layout.fragment_sittings;
                startActivity(intent);
            }
        });


        dialog.show();
    }
}
