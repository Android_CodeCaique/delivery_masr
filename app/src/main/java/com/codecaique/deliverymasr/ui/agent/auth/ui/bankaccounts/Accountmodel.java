package com.codecaique.deliverymasr.ui.agent.auth.ui.bankaccounts;

public class Accountmodel {
    String bankname, username, phone;

    public Accountmodel() {
    }

    public Accountmodel(String bankname, String username, String phone) {
        this.bankname = bankname;
        this.username = username;
        this.phone = phone;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
