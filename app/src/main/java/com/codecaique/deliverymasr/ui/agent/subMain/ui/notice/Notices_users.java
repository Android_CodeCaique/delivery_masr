package com.codecaique.deliverymasr.ui.agent.subMain.ui.notice;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codecaique.deliverymasr.R;
import com.codecaique.deliverymasr.ui.agent.subMain.ui.comments.Adapter_comment;
import com.codecaique.deliverymasr.ui.agent.subMain.ui.comments.Commentmodel;

import java.util.ArrayList;

public class Notices_users extends Fragment {


    Adapter_comment adapter;
    ArrayList<Commentmodel> commentmodels;
    RecyclerView recyclerView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_notices,container,false);


        recyclerView = view.findViewById(R.id.rec_notices);
        commentmodels=new ArrayList<>();
        commentmodels.add(new Commentmodel("حسن","كل التطبيقات بتحدد سعر المشوار ولازم توافق","11:20"));
        commentmodels.add(new Commentmodel(" حسن ","كل التطبيقات بتحدد سعر المشوار ولازم توافق","4:7"));
        commentmodels.add(new Commentmodel(" حسن ","كل التطبيقات بتحدد سعر المشوار ولازم توافق","2:20"));
        commentmodels.add(new Commentmodel("حسن","كل التطبيقات بتحدد سعر المشوار ولازم توافق","11:20"));

        adapter=new Adapter_comment(getContext(),commentmodels);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        return  view;
    }
}
