package com.codecaique.deliverymasr.ui.agent.main.dialogs

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.response.Message
import com.codecaique.deliverymasr.pojo.response.User
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.client.chat.ChatViewModel
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import com.google.firebase.firestore.FirebaseFirestore
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.dialog_rate_user.*
import okhttp3.MediaType
import okhttp3.RequestBody

class RateUserDialog : AppCompatActivity() {

    private val TAG = "RateUserDialog"

    lateinit var mandowpData: User

    lateinit var loadingDialog: LoadingDialog
    lateinit var viewModel: ChatViewModel
    lateinit var userInfo: UserInfo
    var userId: Int = 0
    var requestId: Int = 0

    companion object {
        const val ORDER_ID = "ORDER_ID"
        const val USER_ID = "USER_ID"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_rate_user)


        Log.e(TAG, "onCreate: nn")

        loadingDialog = LoadingDialog(this)
        viewModel = ViewModelProvider(this).get(ChatViewModel::class.java)
        userInfo = UserInfo(applicationContext)
        mandowpData = User()
        userId = intent!!.extras!!.getString(USER_ID)!!.toInt()
        requestId = intent!!.extras!!.getString(ORDER_ID)!!.toInt()


        Log.e(TAG, "onCreate: userId  $userId")
        Log.e(TAG, "onCreate: requestId  $requestId")

        driverData()

        Log.e(TAG, "rateDialog: Test 3")
        btn_done.text = getString(R.string.thanks)

        btn_done.setOnClickListener {
            done_click()
        }
        Log.e(TAG, "onCreate: end oncreate")
    }

    private fun done_click() {

        if (smile_rating.rating < 1) {
            Toast.makeText(this, getString(R.string.ratt), Toast.LENGTH_SHORT).show()
            return
        }

        btn_done.visibility = View.GONE
        progressBar.visibility = View.VISIBLE

        viewModel.rateUser(
                RequestBody.create(MediaType.parse("multipart/form-data"), userInfo.getUserToken()),
                userId, smile_rating.rating.toDouble(),
                RequestBody.create(MediaType.parse("multipart/form-data"), et_comment.text.toString() + " "),
                requestId,
                null
        )

        viewModel.rateUserMutableLiveData.observe(this, Observer { t ->

            Log.e(TAG, "onCreate: message --> ${t.message}")

            if (t.error == 0) {

                allMessages()

            } else {
                Log.e(TAG, t.error.toString())
                progressBar.visibility = View.GONE
                btn_done.visibility = View.VISIBLE
                Toast.makeText(this, " ${t.message}", Toast.LENGTH_SHORT).show()
                finish()
            }


        })
        Log.e(TAG, "done_f: end")
    }


    private var db = FirebaseFirestore.getInstance()
            .collection(ApiCreater.OrdersCollection)

    private fun updateRateState(msg_id: String) {

        val map = HashMap<String, Boolean>()
        map["client_rated"] = true
        map["driver_rated"] = driver_rated

        db.document(msg_id).update(map as Map<String, Any>).addOnSuccessListener {

            btn_done.visibility = View.GONE
            progressBar.visibility = View.VISIBLE
            userInfo.putKeyRate(requestId.toString())
            Toast.makeText(this, getString(R.string.thanks), Toast.LENGTH_SHORT).show()
            finish()

        }

    }

    var client_rated = false
    var driver_rated = false

    private fun allMessages() {

        db = FirebaseFirestore.getInstance()
                .collection(ApiCreater.OrdersCollection).document(requestId.toString())
                .collection(ApiCreater.ChatCollection)

        var msg = Message()

        db.orderBy("created_at").addSnapshotListener { querySnapshot, _ ->

            val messages = ArrayList<Message>()

            querySnapshot!!.documents.forEach { documentSnapshot ->
                msg = documentSnapshot.toObject(Message::class.java)!!
                messages.add(msg)
            }

            client_rated = msg.client_rated
            driver_rated = msg.driver_rated

            updateRateState(msg.id)

        }

    }

    private fun driverData() {

        Log.e(TAG, "driverData: start")

        btn_done.visibility = View.GONE
        progressBar.visibility = View.VISIBLE
        userInfo.putKeyRate(requestId.toString())

        loadingDialog.show()
        viewModel.showUserData(userInfo.getUserToken(), userId)
        viewModel.showUserOfOrderMutableLiveData.observe(this, Observer {
            Log.e(TAG, "driverData: dd")
            loadingDialog.dismiss()
            mandowpData = it.data[0]
            Log.e(TAG, "rateDialog: Test 2")
            tv_user_name.text = mandowpData.first_name
            if (mandowpData.image != null && mandowpData.image.isNotEmpty())
                Picasso.get().load(ApiCreater.USER_URL + mandowpData.image).into(civ_user)


            btn_done.visibility = View.VISIBLE
            progressBar.visibility = View.GONE

//            loadingDialog.show()

        })

        Log.e(TAG, "driverData: end")

    }


    override fun onStart() {
        super.onStart()


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            window.decorView.importantForAutofill =
                    View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS

            Log.e(TAG, "onStart: ifeeeeeee")
        } else {
            Log.e(TAG, "onStart: elseeeee")
        }
        Log.e(TAG, "onStart: start activity")
    }

    override fun onResume() {
        super.onResume()
        Log.e(TAG, "onResume: resume activity")
    }

    override fun onRestart() {
        super.onRestart()


        Log.e(TAG, "onRestart: restart activity")
    }

    override fun onStop() {
        super.onStop()
        Log.e(TAG, "onStop: stop activity")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e(TAG, "onDestroy: destroy activity")
    }

    override fun onPause() {
        super.onPause()
        Log.e(TAG, "onPause: pause activity")
    }
}
