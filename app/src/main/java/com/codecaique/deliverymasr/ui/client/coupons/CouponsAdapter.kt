package com.codecaique.deliverymasr.ui.client.coupons

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.response.Coupon
import com.codecaique.deliverymasr.ui.client.coupons.CouponsAdapter.ViewHolderCoupon

class CouponsAdapter(var context: Context) : RecyclerView.Adapter<ViewHolderCoupon>() {

    var list=ArrayList<Coupon>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderCoupon {
        val view = LayoutInflater.from(context).inflate(R.layout.item_coupon, parent, false)
        return ViewHolderCoupon(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolderCoupon, position: Int) {

        val arrayItem=list[position]

        holder.tvCouponTime.text=arrayItem.created_at
        holder.tvCouponCancel.text=arrayItem.copon
        holder.tvCouponDiscount.text="%100"
        holder.tvCouponDiscount.visibility = View.INVISIBLE
        holder.tvCouponType.text=context.resources.getString(R.string.coupon_discount)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class ViewHolderCoupon(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvCouponType: TextView = itemView.findViewById(R.id.market_name)
        var tvCouponTime: TextView = itemView.findViewById(R.id.order_time)
        var tvCouponDiscount: TextView = itemView.findViewById(R.id.coupon_per)
        var tvCouponCancel: TextView = itemView.findViewById(R.id.coupon_cancel)
    }

    fun setData(a: ArrayList<Coupon>) {
        this.list=ArrayList()
        this.list = a
        notifyDataSetChanged()
    }

    fun addCoupon(coupon: Coupon){
        this.list.add(coupon)
        notifyDataSetChanged()//ItemInserted(list.size-1)
    }

}