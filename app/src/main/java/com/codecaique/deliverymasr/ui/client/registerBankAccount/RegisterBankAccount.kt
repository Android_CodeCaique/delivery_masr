package com.codecaique.deliverymasr.ui.client.registerBankAccount

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.StoresMainActivity
import com.codecaique.deliverymasr.ui.client.bankAccount.BankAccount
class RegisterBankAccount : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_register_bank_account, container, false)
        val mobile_verification_btn = view.findViewById<Button>(R.id.mobile_verification_btn)
        mobile_verification_btn.setOnClickListener {
            val bankAccount = BankAccount()
            val manager = fragmentManager
            manager!!.beginTransaction().replace(R.id.fragment_sub, bankAccount, bankAccount.tag).commit()
        }


        val intent = Intent(view.context, StoresMainActivity::class.java)
        intent.flags= Intent.FLAG_ACTIVITY_NEW_TASK

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true/*  enabled by default*/) {
            override fun handleOnBackPressed() {
                // Handle the back button event
                intent.putExtra(StoresMainActivity.KEY, StoresMainActivity.SETTINGS)
                startActivity(intent)
                requireActivity().finish()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)



        return view
    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}