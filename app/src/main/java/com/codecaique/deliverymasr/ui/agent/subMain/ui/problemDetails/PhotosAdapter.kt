package com.codecaique.deliverymasr.ui.agent.subMain.ui.problemDetails

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.response.Image
import com.squareup.picasso.Picasso

class PhotosAdapter(var context: Context) : RecyclerView.Adapter<PhotosAdapter.ViewHolderCoupon>() {

    var list=ArrayList<Image>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderCoupon {
        val view = LayoutInflater.from(context).inflate(R.layout.item_photo, parent, false)
        return ViewHolderCoupon(view)
    }

    override fun onBindViewHolder(holder: ViewHolderCoupon, position: Int) {

        val arrayItem=list[position]

        Picasso.get().load(ApiCreater.PROBLEMS_URL+arrayItem.image).into(holder.img)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class ViewHolderCoupon(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var img:ImageView = itemView.findViewById(R.id.img)
    }

    fun setData(a: ArrayList<Image>) {
        this.list = a
        notifyDataSetChanged()
    }

    fun addCoupon(coupon: Image){
        this.list.add(coupon)
        notifyDataSetChanged()
    }

}