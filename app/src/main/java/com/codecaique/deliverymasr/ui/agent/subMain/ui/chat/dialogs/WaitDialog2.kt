package com.codecaique.deliverymasr.ui.agent.subMain.ui.chat.dialogs

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import com.codecaique.deliverymasr.R
import kotlinx.android.synthetic.main.dialog_order_wait.*

class WaitDialog2(var context_: Context) : Dialog(context_) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_order_wait)

        tv_ok.setOnClickListener {

            dismiss()
        }

        tv_title.text = context_.getString(R.string.orderBegin_2)

    }
}
