package com.codecaique.deliverymasr.ui.agent.main.ui.offersdelivery

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.AuthActivity
import com.codecaique.deliverymasr.base.agent.SubMainActivity
import com.codecaique.deliverymasr.pojo.response.DeliveryOffers_Response
import com.codecaique.deliverymasr.ui.client.deliveryOffers.OffersViewModel
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.agent.subMain.ui.orderChat.OrderChatFragment
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.dialog_should_register.*
import java.text.DecimalFormat

class OffersAdapter(var context: Context, var offersViewModel: OffersViewModel) : RecyclerView.Adapter<OffersAdapter.DeliveryOffersViewHolder>() {

    var list = ArrayList<DeliveryOffers_Response.DataBean>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeliveryOffersViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_offer_delivery, parent, false)
        return DeliveryOffersViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: DeliveryOffersViewHolder, position: Int) {

        val arrayItem = list[position]

        if (arrayItem.image != null && arrayItem.image.isNotEmpty()) {
            Picasso.get().load(ApiCreater.STORES_URL + arrayItem.image).into(holder.userImage)
        }else {
            Picasso.get().load(R.drawable.app_logo).into(holder.userImage)
        }


        holder.offer_details.text = arrayItem.description
        holder.offer_name.text = arrayItem.name

        val formater = DecimalFormat("00.0")

        try {
            holder.tvAgentDistance.text = formater.format(arrayItem.agent_Recivingdistance.toDouble()) + " كم "
            Log.e("DIST", arrayItem.agent_Recivingdistance)
            holder.tvClientDistance.text = formater.format(arrayItem.client_Recivingdistance.toDouble()) + " كم "
            Log.e("DIST", arrayItem.client_Recivingdistance)

        } catch (e: Exception) {
            Log.e("Exception", e.message)
        }

        holder.tvTime.text = arrayItem.delivery_time + " ساعات "

//        holder.delivery_address.text = arrayItem.delivery_address
//        holder.receiving_address.text = arrayItem.receiving_address

        try {

            if (arrayItem.payment!=null && arrayItem.payment == "2")
            {
                holder.cash.setImageResource(R.drawable.payment)
            }
        } catch (e: Exception) {
        }


        holder.itemView.setOnClickListener {

            if (UserInfo(context).isNotRegister()) {
                UserInfo(context).logout()
                val dialog = Dialog(context)
                dialog.setContentView(R.layout.dialog_should_register)
                dialog.setCancelable(false)
                dialog.tv_ok.setOnClickListener {
                    val intent = Intent(context, AuthActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    context.startActivity(intent)
                }
                dialog.show()
                return@setOnClickListener
            }


            val intent = Intent(context, SubMainActivity::class.java)
            intent.putExtra(OrderChatFragment.REQUEST_ID, arrayItem.id)
//            intent.putExtra(OrderChatFragment.REQUEST_ID,arrayItem.d)
//            intent.putExtra("client", arrayItem.client_Recivingdistance)
//            intent.putExtra("agent", arrayItem.agent_Recivingdistance)
            intent.putExtra(OrderChatFragment.USER_ID, arrayItem.delivery_id)
            intent.putExtra(OrderChatFragment.PAYMENT, arrayItem.payment)
            intent.putExtra(SubMainActivity.KEY, SubMainActivity.ORDERCHAT)
            context.startActivity(intent)
        }

        //todo : push way

    }


    override fun getItemCount(): Int {
        Log.e("LIST SIZE", list.size.toString())
        return list.size
    }

    class DeliveryOffersViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        //        var btnAccept: Button = itemView.findViewById(R.id.btn_Accept)
        var userImage: CircleImageView = itemView.findViewById(R.id.cir_order)
        var offer_name: TextView = itemView.findViewById(R.id.offername)
        var offer_details: TextView = itemView.findViewById(R.id.offer_details)

        //        var rating: RatingBar = itemView.findViewById(R.id.rate)
//        var tvRate: TextView = itemView.findViewById(R.id.textDescription)
//        var tvPushWay: TextView = itemView.findViewById(R.id.tvPushWay)
        var tvClientDistance: TextView = itemView.findViewById(R.id.tv_start_point)
        var tvAgentDistance: TextView = itemView.findViewById(R.id.tv_recieving_point)

        //        var tvDistance: TextView = itemView.findViewById(R.id.tvDistance)
        var credit: ImageView = itemView.findViewById(R.id.credit)
        var cash: ImageView = itemView.findViewById(R.id.cash)

        var delivery_address: TextView = itemView.findViewById(R.id.delivery_address)
        var receiving_address: TextView = itemView.findViewById(R.id.receiving_address)

        var tvTime: TextView = itemView.findViewById(R.id.time_offer_delivery)
    }

    fun setData(a: ArrayList<DeliveryOffers_Response.DataBean>) {
        this.list = a
        notifyDataSetChanged()
    }

}