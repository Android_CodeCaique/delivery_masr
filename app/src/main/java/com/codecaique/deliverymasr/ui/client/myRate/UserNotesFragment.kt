package com.codecaique.deliverymasr.ui.client.myRate

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.pojo.response.Comment
import kotlinx.android.synthetic.main.fragment_user_notes.view.*


// for me

class UserNotesFragment : Fragment() {

        lateinit var ratingsViewModel: RatingsViewModel
        lateinit var userNotesAdapter: UserNotesAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_user_notes, container, false)

        ratingsViewModel=ViewModelProviders.of(this).get(RatingsViewModel::class.java)
        userNotesAdapter= UserNotesAdapter(activity!!.applicationContext)

        view.rv_userNotes.adapter=userNotesAdapter

        ratingsViewModel.showRating(UserInfo(activity!!.applicationContext).getUserToken())
        ratingsViewModel.notesMutableLiveData.observe(this, Observer { t->

            view.progressBar.visibility=View.GONE
            userNotesAdapter.setData(t.data as ArrayList<Comment>)

            if(t.data.size == 0)
                view.no_data.visibility = View.VISIBLE

        })

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true/*  enabled by default*/ ) {
            override fun handleOnBackPressed() {
                // Handle the back button event
                requireActivity().finish()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)


        view.iv_backNotes.setOnClickListener {
            activity!!.finish()
            /*val intent = Intent(context, MainActivity::class.java)
            StoresAdapter.fragName = R.layout.fragment_all_sitting
            startActivity(intent)*/
        }


        return view
    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}