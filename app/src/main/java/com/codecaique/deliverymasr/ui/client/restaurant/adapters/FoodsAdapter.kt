package com.codecaique.deliverymasr.ui.client.restaurant.adapters

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.pojo.request.MenuItemOrder
import com.codecaique.deliverymasr.pojo.response.FoodType
import com.codecaique.deliverymasr.pojo.response.Menu
import com.codecaique.deliverymasr.ui.client.restaurant.RestaurantViewModel
import com.codecaique.deliverymasr.ui.client.restaurant.adapters.FoodsAdapter.ViewHolderFoodOffers
import com.codecaique.deliverymasr.data.ApiCreater
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.dialog_food_type.*

class FoodsAdapter(var context: Context, val restaurantViewModel: RestaurantViewModel) : RecyclerView.Adapter<ViewHolderFoodOffers>() {

    var list = ArrayList<Menu>()
    var myOrderList = ArrayList<MenuItemOrder>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderFoodOffers {
        val view = LayoutInflater.from(context).inflate(R.layout.item_kfc_food_list, parent, false)
        return ViewHolderFoodOffers(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolderFoodOffers, position: Int) {

        val arrayItem = list[position]

        holder.tvName.text = arrayItem.product
        holder.tvPrice.text = arrayItem.price + "ريال";

        if (arrayItem.image != null && arrayItem.image.isNotEmpty()) {
            Picasso.get().load(ApiCreater.PRODUCTS_URL + arrayItem.image).into(holder.image)
        }

        holder.itemView.setOnClickListener {
            val dialog = Dialog(context)
            dialog.setContentView(R.layout.dialog_food_type)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.window!!.setGravity(Gravity.BOTTOM)

            dialog.market_name.text=arrayItem.product

            //  TODo-> set image
            if (arrayItem.image != null && arrayItem.image.isNotEmpty())
                Picasso.get().load(ApiCreater.PRODUCTS_URL + arrayItem.image).into(dialog.image)

            var quantity = 0

            val foodTypesAdapter = FoodTypesAdapter(context, dialog.tv_price_constant, dialog.tv_q)

            dialog.rv_types.adapter = foodTypesAdapter

            restaurantViewModel.showProductTypes(UserInfo(context).getUserToken(), arrayItem.id)
            restaurantViewModel.typesMutableLiveData.observe(context as FragmentActivity, Observer { t ->

                foodTypesAdapter.setData(t.data as ArrayList<FoodType>)
                Log.e("size", t.data.size.toString())

            })

            var price = arrayItem.price.toFloat()

            var total_price:Float = 0F

            quantity = 1

            dialog.tv_price.text = price.toString()

            dialog.tv_p.setOnClickListener {
                try {

                    quantity +=1
                    total_price = price * quantity

                    dialog.tv_price.text = (total_price).toString()
                    dialog.tv_q.text = quantity.toString()

                    Log.e("Sss price", total_price.toString())
                }catch (e:Exception){}
            }

            dialog.tv_m.setOnClickListener {
                try {

                    if (quantity > 1)
                    {
                        quantity -=1

                        total_price = price * quantity

                        dialog.tv_price.text = (total_price).toString()
                        dialog.tv_q.text = quantity.toString()

                        Log.e("Sss price", total_price.toString() + quantity)
                    }

                }catch (e:Exception){}
            }

            dialog.tv_price.text = arrayItem.price

            dialog.tv_price_constant.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                    try {
                        dialog.tv_price.text = (p0.toString().toFloat() * dialog.tv_q.text.toString().toInt()).toString()
                    } catch (e: Exception) {
                    }

                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }
            })

            dialog.btn_orderKentaky.setOnClickListener {

                Log.e("TAGA",arrayItem.product )
                myOrderList.add(MenuItemOrder(arrayItem.product , arrayItem.id.toString(), foodTypesAdapter.selectedTypeId.toString(), quantity.toString(), price.toString()))
                dialog.dismiss()
            }
            dialog.show()
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolderFoodOffers(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvPrice = itemView.findViewById(R.id.tv_price) as TextView
        var tvName = itemView.findViewById(R.id.tv_foodName) as TextView
        val image = itemView.findViewById(R.id.image) as ImageView
    }

    fun setData(a: ArrayList<Menu>) {
        this.list = a
        notifyDataSetChanged()
    }

}