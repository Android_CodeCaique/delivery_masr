package com.codecaique.deliverymasr.ui.client.tardHayak

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.RadioGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.databinding.FragmentTardHayakBinding
import com.codecaique.deliverymasr.pojo.util.MyUtil
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.agent.subMain.ui.delivery.direction.drawroute
import com.facebook.FacebookSdk.getApplicationContext
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.google.maps.DirectionsApi
import com.google.maps.DirectionsApiRequest
import com.google.maps.GeoApiContext
import com.google.maps.model.*
import kotlinx.android.synthetic.main.dialog_time.*
import kotlinx.android.synthetic.main.fragment_tard__hayak_.*
import java.util.*

class PurchasesFragment : Fragment(), OnMapReadyCallback, Animation.AnimationListener {

    lateinit var bundle: Bundle

    private lateinit var mMap: GoogleMap
    private lateinit var myLocation: LatLng
    private lateinit var startLocation: LatLng
    private lateinit var endLocation: LatLng
    private lateinit var timedialog: Dialog
    private lateinit var streetStart: String
    private lateinit var streetEnd: String
    private lateinit var geocode :Geocoder

    private var locationState = Start
    var animFadein: Animation? = null
    private var mapFragment: SupportMapFragment? = null
    private lateinit var navController: NavController

    companion object {
        var CURRENT_LOCATION = ""
        const val Start = "start"
        const val End = "end"
        const val Finish = "finish"
        var DATA = "data"
        val LOCATION_START = "lo-start"
        val LOCATION_END = "lo-end"
        val TIME = "time"
        val StreetStar = "s-start"
        val StreetEEnd = "s-end"
        private const val TAG = "PurchasesFragment"
    }

    var time = 1

    lateinit var makeOrderViewModel: MakeOrderViewModel
    lateinit var binding: FragmentTardHayakBinding

    @SuppressLint("MissingPermission")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_tard__hayak_, container, false)
        animFadein = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fade_in);
        animFadein!!.setAnimationListener(this)

        makeOrderViewModel = ViewModelProviders.of(this).get(MakeOrderViewModel::class.java)
        geocode = Geocoder(context, Locale("ar"))//.getDefault())

        onBack()

        myLocation = UserInfo(activity!!.applicationContext).getMyLocation()

        startLocation = LatLng(0.0, 0.0)
        endLocation = LatLng(0.0, 0.0)

         mapFragment = //(context!! as AppCompatActivity).supportFragmentManager
                childFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment
        mapFragment!!.getMapAsync(this)

        val geocode = Geocoder(context, Locale("ar"))//.getDefault())
        var addresses: List<Address>? = null

        streetStart = ""
        binding.tvStartPoint.visibility = View.VISIBLE
        binding.tvEndPoint.visibility = View.INVISIBLE
        binding.tvAnyWhere.visibility = View.VISIBLE

        binding.tvAnyWhere.setOnClickListener {

            streetStart = "من اي مكان"
            startLocation= LatLng(0.0,0.0)
            binding.conMap.startAnimation(animFadein)
            binding.tvStartPoint.visibility = View.GONE
            binding.tvEndPoint.visibility = View.VISIBLE
            binding.tvAnyWhere.visibility = View.GONE
            locationState = End
            binding.yalla.isEnabled=false

        }


        binding.yalla.setOnClickListener {

            Log.e(TAG, "onCreateView: locationState $locationState")

            when (locationState) {

                TardHayyakFragment.Start -> {
                    mMap.clear()
                    locationState = TardHayyakFragment.End

                    addresses = geocode.getFromLocation(startLocation.latitude, startLocation.longitude, 1)

                    if (addresses != null && addresses!!.isNotEmpty()) {
                        streetStart = addresses!![0].getAddressLine(0)//thoroughfare
                    }else{
                        streetStart=""
                    }
                    binding.conMap.startAnimation(animFadein)
                    binding.tvStartPoint.visibility = View.GONE
                    binding.tvEndPoint.visibility = View.VISIBLE
                    binding.tvAnyWhere.visibility = View.GONE
                    binding.yalla.isEnabled = false
                }

                End -> {

                    addresses = geocode.getFromLocation(endLocation.latitude, endLocation.longitude, 1)

                    if (addresses != null && addresses!!.isNotEmpty()) {
                        streetEnd = addresses!![0].getAddressLine(0)//thoroughfare
                    }else{
                        streetEnd=""
                    }

                    //set 2 marks at map
                    locationState = Finish
                    binding.tvStartPoint.visibility = View.GONE
                    binding.tvEndPoint.visibility = View.GONE
                    binding.tvStartPointTard.visibility=View.GONE

                    completeRequest()
//                    set2Locations()
                }
                Finish -> {

//                    completeRequest()
//                    showTimeDialog()

                }
            }
            Log.e(TAG, "onCreateView: locationState $locationState")

        }

        binding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {

                binding.btnSearch.visibility = if (p0.toString().isNotEmpty())
                    View.VISIBLE
                else
                    View.GONE

            }
        })


        binding.btnSearch.setOnClickListener {

            searchPlace(binding.etSearch.text.toString().trim())

            binding.etSearch.setText("")

        }

        return binding.root
    }

    private fun completeRequest() {

        val args = Bundle()

        args.apply {
            putParcelable(SentRequestFragment.LocationStart, startLocation)
            putParcelable(SentRequestFragment.LocationEnd, endLocation)
            putString(SentRequestFragment.TIME, "0")
            putString(SentRequestFragment.Type, SentRequestFragment.PURCHASES)
            putString(SentRequestFragment.StreetStart, streetStart)
            putString(SentRequestFragment.StreetEnd, streetEnd)
        }

//        val dialog = SentRequestFragment(requireView())
//        dialog.arguments = args
//        dialog.show(childFragmentManager, TAG)

        navController.navigate(R.id.action_tardHayyakFragment_to_sentRequestFragment,args)
    }


    private fun searchPlace(place: String) {


        Log.e(TAG, "searchPlace: place $place")

        val places = geocode.getFromLocationName(place, 1) as ArrayList<Address>

        Log.e(TAG, "searchPlace: places $places")

        if (places.isEmpty()) {
            Toast.makeText(requireContext(), getString(R.string.place_not_founded), Toast.LENGTH_SHORT).show()
            return
        }

        val latLngSearch = LatLng(places[0].latitude, places[0].longitude)
        binding.yalla.isEnabled = true
        binding.yalla.isEnabled = true
        Log.e(TAG, "onCreateView: locationState $locationState")

        if (locationState == End) {


            mMap.clear()
            endLocation = latLngSearch

            mMap.addMarker(MarkerOptions().position(endLocation)//.title("Marker in Sydney"))
                    .icon(MyUtil.bitmapDescriptorFromVector(requireContext(), R.drawable.ic_pin_delivery)))
            mMap.moveCamera(CameraUpdateFactory.newLatLng(endLocation))


        }



    }

    private fun onBack() {

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                // Handle the back button event
                requireActivity().finish()
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(this, callback)


    }

    private fun set2Locations() {

        mMap.clear()


        val endMarker = MarkerOptions()
                .position(endLocation)
                .icon(MyUtil.bitmapDescriptorFromVector(requireContext(), R.drawable.ic_pin_delivery))
                .title(getString(R.string.send_loc))

        mMap.addMarker(endMarker).showInfoWindow()

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(endLocation, 24F))

    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap?) {

        mMap = googleMap!!
        //    mMap.mapType = GoogleMap.MAP_TYPE_NORMAL
        mMap.uiSettings.isZoomGesturesEnabled=true



        binding.imgGetlocation.setOnClickListener {
            checked()
        }


        Log.e("location ====== ", "locationInfo.geaaaaaaaaaatStreetStart()")
        mMap.addMarker(MarkerOptions().position(myLocation)//.title("Marker in Sydney"))
                .icon(MyUtil.bitmapDescriptorFromVector(requireContext(), R.drawable.pingray)))
 //  mMap.addMarker(MarkerOptions().position(myLocation))//.title("Marker in Sydney"))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 10F))
  //      mMap.setPadding(700,800 , 0, 0); //numTop = padding of your choice

        mMap.isMyLocationEnabled=true

        val routePath = drawroute(getApplicationContext(), mMap)
        routePath.setRoutePath(LatLng(29.116247342773434, 31.064659861885772), LatLng(29.11733461416393, 31.054360179941444))


                    //Define list to get all latlng for the route
            val path: List<LatLng> = ArrayList()
            //Execute Directions API request
            val context: GeoApiContext = GeoApiContext.Builder()
                    .apiKey("AIzaSyCnv8gMDU4VO1npdblSccBTgwRv7qjAgXc")
                    .build()
            val req: DirectionsApiRequest = DirectionsApi.getDirections(context, LatLng(29.116247342773434, 31.064659861885772).toString(), LatLng(29.11733461416393, 31.054360179941444).toString())
            try {
                val res: DirectionsResult = req.await()

                //Loop through legs and steps to get encoded polylines of each step
                if (res.routes != null && res.routes.size > 0) {
                    val route: DirectionsRoute = res.routes.get(0)
                    if (route.legs != null) {
                        for (i in 0 until route.legs.size) {
                            val leg: DirectionsLeg = route.legs.get(i)
                            if (leg.steps != null) {
                                for (j in 0 until leg.steps.size) {
                                    val step: DirectionsStep = leg.steps.get(j)
                                    if (step.steps != null && step.steps.size > 0) {
                                        for (k in 0 until step.steps.size) {
                                            val step1: DirectionsStep = step.steps.get(k)
                                            val points1: EncodedPolyline = step1.polyline
                                            if (points1 != null) {
                                                //Decode polyline and add points to list of route coordinates
                                                val coords1: List<com.google.maps.model.LatLng> = points1.decodePath()
                                                for (coord1 in coords1) {
                                                    path.toMutableList().add(LatLng(coord1.lat, coord1.lng))
                                                }
                                            }
                                        }
                                    } else {
                                        val points: EncodedPolyline = step.polyline
                                        if (points != null) {
                                            //Decode polyline and add points to list of route coordinates
                                            val coords: List<com.google.maps.model.LatLng> = points.decodePath()
                                            for (coord in coords) {
                                                path.toMutableList().add(LatLng(coord.lat, coord.lng))
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (ex: java.lang.Exception) {
                Log.e(TAG, ex.localizedMessage)
            }

            //Draw the polyline
//            if (path.size > 0) {
//                val opts = PolylineOptions().addAll(path).color(Color.BLUE).width(5f)
//                mMap.addPolyline(opts)
//            }
        mMap.setOnMapClickListener { latLng ->

            binding.yalla.isEnabled = true
            Log.e(TAG, "onCreateView: locationState $locationState")

            if (locationState == Start) {

                mMap.clear()
                startLocation = latLng

                mMap.addMarker(MarkerOptions().position(latLng))//.title("Marker in Sydney"))
//                mMap.moveCamera(CameraUpdateFactory.newLatLng(startLocation))


            } else if (locationState == End) {


                mMap.clear()
                endLocation = latLng

                mMap.addMarker(MarkerOptions().position(latLng)//.title("Marker in Sydney"))
//                        .icon(MyUtil.bitmapDescriptorFromVector(requireContext(), R.drawable.ic_pin_delivery))
                )
//                mMap.moveCamera(CameraUpdateFactory.newLatLng(endLocation))


            }
        }

    }


    private fun showTimeDialog() {

        timedialog = Dialog(requireActivity())
        setUp()
        timedialog.setContentView(R.layout.dialog_time)
        timedialog.show()


        val list = makeOrderViewModel.getTimes().toList()
        val arrayAdapter = ArrayAdapter(activity!!.applicationContext, R.layout.spinner_item, R.id.text, list)
        timedialog.spinner.adapter = arrayAdapter

        timedialog.spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                time = makeOrderViewModel.getTimes()[0].length
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                time = p2 + 1
            }
        }

        timedialog.buttonConfirm.setOnClickListener {

            val time: Int = timedialog.spinner.selectedItemPosition + 1

            timedialog.dismiss()
        }

    }

    private fun setUp() {

        timedialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val wlp = timedialog.window!!.attributes
        wlp.gravity = Gravity.BOTTOM
        wlp.flags = wlp.flags and WindowManager.LayoutParams.FLAG_DIM_BEHIND.inv()
        timedialog.window!!.attributes = wlp

    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }

    override fun onAnimationEnd(animation: Animation) {
        // Take any action after completing the animation

        // check for fade in animation

    }

    override fun onAnimationRepeat(animation: Animation?) {
        // TODO Auto-generated method stub
    }

    override fun onAnimationStart(animation: Animation?) {
        // TODO Auto-generated method stub
    }

    public fun checked(){
        rg_views.visibility=View.VISIBLE

        val checkedChangeListener: RadioGroup.OnCheckedChangeListener = object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {

                // Getting Reference to map_view of the layout activity_main

                // Currently checked is rb_map
                if (checkedId == R.id.rb_map) {
                    mMap.mapType = GoogleMap.MAP_TYPE_NORMAL
                    rg_views.visibility=View.GONE
                }

                // Currently checked is rb_satellite
                if (checkedId == R.id.rb_satellite) {
                    mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID)
                    rg_views.visibility=View.GONE



                }
            }


        }
        // Setting Checked ChangeListener
        rg_views.setOnCheckedChangeListener(checkedChangeListener)


    }

    private fun initialize(view: View) {
        navController = Navigation.findNavController(view)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize(view)
    }

}