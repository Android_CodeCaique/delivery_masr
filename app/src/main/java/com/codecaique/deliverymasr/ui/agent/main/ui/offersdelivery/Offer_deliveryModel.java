package com.codecaique.deliverymasr.ui.agent.main.ui.offersdelivery;

public class Offer_deliveryModel {
    String OfferName,OfferDes,Dis1,Dis2,Time_ago;

    public String getOfferName() {
        return OfferName;
    }

    public void setOfferName(String offerName) {
        OfferName = offerName;
    }

    public String getOfferDes() {
        return OfferDes;
    }

    public void setOfferDes(String offerDes) {
        OfferDes = offerDes;
    }

    public String getDis1() {
        return Dis1;
    }

    public void setDis1(String dis1) {
        Dis1 = dis1;
    }

    public String getDis2() {
        return Dis2;
    }

    public void setDis2(String dis2) {
        Dis2 = dis2;
    }

    public String getTime_ago() {
        return Time_ago;
    }

    public void setTime_ago(String time_ago) {
        Time_ago = time_ago;
    }

    public Offer_deliveryModel() {
    }

    public Offer_deliveryModel(String offerName, String offerDes, String dis1, String dis2, String time_ago) {
        OfferName = offerName;
        OfferDes = offerDes;
        Dis1 = dis1;
        Dis2 = dis2;
        Time_ago = time_ago;
    }
}
