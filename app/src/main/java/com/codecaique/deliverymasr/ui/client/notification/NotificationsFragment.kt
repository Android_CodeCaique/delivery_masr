package com.codecaique.deliverymasr.ui.client.notification

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.SharedHelper
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.pojo.response.Notification
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import com.codecaique.deliverymasr.ui.client.stores.StoresFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.fragment_notifications.view.*


class NotificationsFragment : Fragment() {

    lateinit var myNotificationsViewModel: NotificationsViewModel
    lateinit var usrInfo: UserInfo
    lateinit var notificationsAdapter: NotificationsAdapter
    lateinit var loadingDialog: LoadingDialog

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_notifications, container, false)

        myNotificationsViewModel = ViewModelProviders.of(this).get(NotificationsViewModel::class.java)
        usrInfo = UserInfo(activity!!.applicationContext)
        notificationsAdapter = NotificationsAdapter(activity!!.applicationContext)
        loadingDialog = LoadingDialog(activity!!)

        view.rv_notifications.adapter = notificationsAdapter

        myNotificationsViewModel.getNotifications(usrInfo.getUserToken(),"user", SharedHelper().getKey(activity!!, "LANG")!!)
        myNotificationsViewModel.myNotificationsMutableLiveData.observe(this, Observer { t ->

            view.progressBar.visibility = View.GONE
            notificationsAdapter.setData(t.data as ArrayList<Notification>)

            if (t.data.isEmpty())
                Toast.makeText(activity!!.applicationContext, getString(R.string.no_notifi), Toast.LENGTH_SHORT).show()

        })

        enableSwipeToDeleteAndUndo(view)


        view.back.setOnClickListener {
            activity!!.supportFragmentManager.beginTransaction().replace(R.id.main_fragment, StoresFragment()).commit()
//            it.findNavController().navigate(R.id.notificationsFragment)
            activity!!.findViewById<BottomNavigationView>(R.id.main_nav_view).selectedItemId = R.id.navigation_Stores
        }

        view.refresh_layout.setOnRefreshListener {
            myNotificationsViewModel.getNotifications(usrInfo.getUserToken(),"user", SharedHelper().getKey(activity!!, "LANG")!!)
            myNotificationsViewModel.myNotificationsMutableLiveData.observe(this, Observer { t ->

                view.progressBar.visibility = View.GONE
                notificationsAdapter.setData(t.data as ArrayList<Notification>)

                if (t.data.isEmpty())
                    Toast.makeText(activity!!.applicationContext, getString(R.string.no_notifi), Toast.LENGTH_SHORT).show()

            })

            view.refresh_layout.isRefreshing = false

        }


        return view
    }

    private fun enableSwipeToDeleteAndUndo(v: View) {
        val swipeToDeleteCallback: SwipeToDeleteCallback = object : SwipeToDeleteCallback(activity!!) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, i: Int) {
                val position = viewHolder.adapterPosition

                myNotificationsViewModel.deleteNotification(activity!!.applicationContext, usrInfo, notificationsAdapter, loadingDialog, position)
                myNotificationsViewModel.deleteNotificationsMutableLiveData.observe(this@NotificationsFragment,
                        Observer {

                    Log.e("ERRORR" , it.error.toString())


                    if (it.error == 0) {

                        usrInfo.setDiscountNotifiCount()
//                        notificationsAdapter.notifyAdapter()
                        loadingDialog.dismiss()
//                        notificationsAdapter.list.removeAt(position)
//                        v.rv_notifications.adapter!!.notifyDataSetChanged()

                        Log.e("ERRORR" , it.error.toString())

                    }

                })


            }
        }
        val itemTouchpaper = ItemTouchHelper(swipeToDeleteCallback)
        itemTouchpaper.attachToRecyclerView(v.rv_notifications)

        /*

        val aa = object : ItemTouchHelper.Callback() {
            override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
                return 0
            }

            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                Log.e("TAG", "iij")
                viewHolder.itemView.btn_remove.visibility = View.VISIBLE
            }
        }

        val itemTouchHelper = ItemTouchHelper(aa)
        itemTouchHelper.attachToRecyclerView(v.rv_notifications)
        Log.e("TAG", "xadiij")
*/

    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}