package com.codecaique.deliverymasr.ui.client.signUp.ui

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.client.MainActivity
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.client.otp.OtpFragment
import com.codecaique.deliverymasr.ui.client.signUp.SignUpViewModel
import com.google.firebase.iid.FirebaseInstanceId
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.android.synthetic.main.fragment_signup.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class SignUpFragment : Fragment() {

    lateinit var v: View
    private val TAG = "SignUpFragment"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_signup, container, false)
        return v
    }

    var isSocialMediaSignUP = false

    lateinit var signUpViewModel: SignUpViewModel
    var uri: Uri? = null

    @SuppressLint("NewApi", "ResourceAsColor", "SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        signUpViewModel = ViewModelProviders.of(this).get(SignUpViewModel::class.java)


        try {
            //view.user_Edit_Photo
            Picasso.get().load(arguments!!.getString("image")).into(object : Target {
                override fun onPrepareLoad(placeHolderDrawable: Drawable?) {

                }

                override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {
                }

                override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                    view.user_Edit_Photo.setImageBitmap(bitmap)
                    isSocialMediaSignUP = true
                }
            })
//            view.et_user_Edit_Name.setText(arguments!!.getString("name"))
//            view.et_user_Edit_Email.setText(arguments!!.getString("email"))
            Log.e(TAG, "onViewCreated: try isSocialMediaSignUP $isSocialMediaSignUP")
        } catch (e: Exception) {
            Log.e(TAG, "onViewCreated: catch isSocialMediaSignUP $isSocialMediaSignUP")
        }

        view.tv_user_Edit_whats.text = getString(R.string.phone_num)


        var gender = 2

        view.tv_male.setOnClickListener {

            gender = 2
            view.tv_male.background = resources.getDrawable(R.drawable.border_right_fill, null)
            view.tv_female.background = resources.getDrawable(R.drawable.border_left, null)
            view.tv_male.setTextColor(ContextCompat.getColor(activity!!, R.color.white))
            view.tv_female.setTextColor(ContextCompat.getColor(activity!!, R.color.color_5))
        }

        val code = arguments!!.getString("code")

        Log.e("CODE register", code)

        val phone = arguments!!.getString("phone")


        view.tv_female.setOnClickListener {

            gender = 1
            view.tv_female.background = resources.getDrawable(R.drawable.border_left_fill, null)
            view.tv_male.background = resources.getDrawable(R.drawable.border_right, null)
            view.tv_female.setTextColor(ContextCompat.getColor(activity!!, R.color.white))
            view.tv_male.setTextColor(ContextCompat.getColor(activity!!, R.color.color_5))
        }

        view.user_Edit_Photo.setOnClickListener {
            isStoragePermissionGranted()
//                gotoImage()
        }

        view.et_user_Edit_whats.setText("$phone")

        view.signup_btn.setOnClickListener {

//            view.signup_btn.isEnabled = false

            val name = view.et_user_Edit_Name.text.toString().trim()
            val lname = view.et_user_Edit_lName.text.toString().trim()
            val email = view.et_user_Edit_Email.text.toString().trim()
            var whatsNo = view.et_user_Edit_whats.text.toString().trim()

            try {
                if (name.isNotEmpty() &&lname.isNotEmpty() && email.isNotEmpty() && whatsNo.isNotEmpty()) {

                    Log.e("SIGN UP", "IF 00000")

                    v.progressBar.visibility = View.VISIBLE
//                    view.signup_btn.isEnabled = false


//                    if (isSocialMediaSignUP)
//                        whatsNo = "+966" + view.et_user_Edit_whats.text.toString().trim()

                    val userInfo = UserInfo(activity!!.applicationContext)

                    Log.e("WHATS sa", whatsNo)
                    Log.e(TAG, "onViewCreated: getId ${userInfo.getId()}")
                    Log.e(TAG, "onViewCreated: isSocialMediaSignUP $isSocialMediaSignUP")

                    signUpViewModel.enterUserData(
                            if (isSocialMediaSignUP) null else userInfo.getId(),
                            name,
                             lname,
                            RequestBody.create(MediaType.parse("multipart/form-data"), ""),
                            if (uri != null || isSocialMediaSignUP) getImage() else null,
                            RequestBody.create(MediaType.parse("multipart/form-data"), phone),
                            RequestBody.create(MediaType.parse("multipart/form-data"), email),
                            gender,
                            userInfo.getLongitude().toDouble(), userInfo.getLatitude().toDouble(),
                            RequestBody.create(MediaType.parse("multipart/form-data"),
                                    FirebaseInstanceId.getInstance().token), v.progressBar, context!!
                    )


                    signUpViewModel.enterUserDataMutableLiveData.observe(this, Observer { t ->
                        Log.e("SIGN UP", "IF 0000220" + t.error.toString())

                        v.progressBar.visibility = View.INVISIBLE
//                        view.signup_btn.isEnabled = true


                        when (t.error) {
                            0 -> {
                                v.progressBar.visibility = View.INVISIBLE
                                //
                                val bundle=Bundle()
                                bundle.putSerializable(OtpFragment.DATA,t.data)
                                view.findNavController().navigate(R.id.otpFragment,bundle)
                            }
                            5 -> {
                                v.progressBar.visibility = View.INVISIBLE

                                view.et_user_Edit_Email.error = getString(R.string.email_exist)
                            }
                            else -> {
                                v.progressBar.visibility = View.INVISIBLE

                                Log.e("TAG", "aa ${t.error} ${t.message} ")
                                Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()
                            }
                        }

//                        view.signup_btn.isEnabled = true

                    })

                } else {
                    if (name.isEmpty())
                        view.et_user_Edit_Name.error = getString(R.string.sould_be_not_empty)
                    if (email.isEmpty())
                        view.et_user_Edit_Email.error = getString(R.string.sould_be_not_empty)
                    if (whatsNo.isEmpty())
                        view.et_user_Edit_whats.error = getString(R.string.sould_be_not_empty)
                    if(lname.isEmpty())
                        view.et_user_Edit_lName.error=getString(R.string.sould_be_not_empty)
                }

            } catch (e: Exception) {
                v.progressBar.visibility = View.INVISIBLE
                Toast.makeText(activity, e.message, Toast.LENGTH_SHORT).show()
//                view.signup_btn.isEnabled = true

            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 111 && RESULT_OK == resultCode) {
            data?.let {
                v.user_Edit_Photo.setImageURI(data.data!!)
                uri = data.data!!
                isSocialMediaSignUP = false
            }
        }
    }

    fun getImage(): MultipartBody.Part? {
        if (isSocialMediaSignUP) {

            val bitmap: Bitmap = (view!!.user_Edit_Photo.drawable as BitmapDrawable).bitmap
            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            val imageInByte: ByteArray = baos.toByteArray()
            val requestFile: RequestBody =
                    RequestBody.create(MediaType.parse("image/*"), imageInByte)

            return MultipartBody.Part.createFormData("image", "${Calendar.getInstance().timeInMillis}.png", requestFile)
        } else {
            val p: String
            val cursor = activity!!.contentResolver.query(uri!!, null, null, null, null)

            p = if (cursor == null) {
                uri!!.path.toString()
            } else {
                cursor.moveToFirst()
                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                cursor.getString(idx)
            }

            val file = File(p)
            val requestFile = RequestBody.create(MediaType.parse("image/*"), file)
            return MultipartBody.Part.createFormData("image", file.name, requestFile)
        }

    }

    @SuppressLint("WrongConstant")
    fun isStoragePermissionGranted(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                gotoImage()

                true
            } else {

                ActivityCompat.requestPermissions(
                        activity!!,
                        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        111
                )
                false
            }
        } else {

            gotoImage()
            return true
        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            gotoImage()
        } else {
/*
            Toast.makeText(applicationContext, R.string.imagePprem_accept, Toast.LENGTH_SHORT)
                    .show()
*/
        }
    }

    private fun gotoImage() {
        val intent = Intent(
                Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 111)
    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }

}
/*
        */