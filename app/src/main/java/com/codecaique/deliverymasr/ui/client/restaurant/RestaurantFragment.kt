package com.codecaique.deliverymasr.ui.client.restaurant

import android.app.Dialog
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.AuthActivity
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.util.LocationInfo
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.pojo.response.Branche
import com.codecaique.deliverymasr.pojo.response.Menu
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import com.codecaique.deliverymasr.ui.client.restaurant.adapters.FoodsAdapter
import com.codecaique.deliverymasr.ui.client.shareLocation.ShareLocationFragment
import com.codecaique.deliverymasr.ui.client.tardHayak.SentRequestFragment
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.dialog_should_register.*
import kotlinx.android.synthetic.main.fragment_kentaky_restaurant.view.*
import kotlinx.android.synthetic.main.fragment_kentaky_restaurant.view.back
import java.util.*
import kotlin.collections.ArrayList


class RestaurantFragment : Fragment() {

    private val TAG = "RestaurantFragment"
    private var dialog: Dialog? = null
    lateinit var bundle: Bundle
    lateinit var restaurantViewModel: RestaurantViewModel

    var time = 0

    lateinit var shopId: String
    lateinit var shopImage: String
    lateinit var shopName: String
    lateinit var locationInfo: LocationInfo
    var lat = 0.0
    var lng = 0.0
    lateinit var loadingDialog: LoadingDialog
    lateinit var userInfo: UserInfo

    var branches = ArrayList<Branche>()

    companion object {

        val ID = "Id"
        val NAME = "name"
        val IMAGE = "image"
        var CURRENT_LOCATION = ""
        val Start = "start"
        val End = "end"
        var DATA = "data"
        val LOCATION_START = "lo-start"
        val LOCATION_END = "lo-end"
        val TIME = "time"
        val StreetStar = "s-start"
        val StreetEEnd = "s-end"
        val LAT = "lat"
        val LNG = "lng"

    }

    lateinit var VIEW: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        VIEW = inflater.inflate(R.layout.fragment_kentaky_restaurant, container, false)

        // This callback will only be called when MyFragment is at least Started.
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true/*  enabled by default*/) {
            override fun handleOnBackPressed() {
                // Handle the back button event
                requireActivity().finish()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)

        restaurantViewModel = ViewModelProviders.of(this).get(RestaurantViewModel::class.java)
        locationInfo = LocationInfo(activity!!.applicationContext)
        shopId = activity!!.intent.extras!!.getString(ID, "0")
        shopImage = activity!!.intent.extras!!.getString(IMAGE, "0")
        shopName = activity!!.intent.extras!!.getString(NAME, "0")
        lat = activity!!.intent.extras!!.getDouble(LAT, 0.0)
        lng = activity!!.intent.extras!!.getDouble(LNG, 0.0)
        loadingDialog = LoadingDialog(activity!!)
        userInfo = UserInfo(activity!!)

        Log.e(TAG, "onCreateView: lat $lat")
        Log.e(TAG, "onCreateView: lng $lng")

        data()


        Log.e("RestaurantFragment", "$shopId  ${UserInfo(activity!!).getUserToken()}")

        VIEW.back.setOnClickListener { activity!!.finish() }


        VIEW.share.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND);
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_TEXT, context!!.packageName + ".$shopId")//"arrayItem.zekr")
            val c = Intent.createChooser(intent, "choose")
            if (c.resolveActivity(context!!.packageManager) != null)
                context!!.startActivity(intent)
        }

        VIEW.tv_title.text = shopName

        Picasso.get().load(ApiCreater.STORES_URL + shopImage).into(VIEW.iv_rest)
        Picasso.get().load(ApiCreater.STORES_URL + shopImage).into(VIEW.vedio)

//        VIEW.back.setOnClickListener { it.findNavController().popBackStack() }

        bundle = Bundle()

        val foodsAdapter = FoodsAdapter(activity!!, restaurantViewModel)
        VIEW.rv_foodList.adapter = foodsAdapter

        VIEW.btn_orderKentaky.setOnClickListener {
            /* val orderFoodFrag = OrderFoodFragment()
             val manager = fragmentManager
             manager!!.beginTransaction().replace(R.id.main3_fragment, orderFoodFrag, orderFoodFrag.tag).commit()*/

            if (UserInfo(activity!!.applicationContext).isNotRegister()) {
                UserInfo(activity!!.applicationContext).logout()
                val dialog = Dialog(activity!!)
                dialog.setContentView(R.layout.dialog_should_register)
                dialog.setCancelable(false)
                dialog.tv_ok.setOnClickListener {
                    val intent = Intent(activity!!.applicationContext, AuthActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    activity!!.startActivity(intent)
                }
                dialog.show()
                return@setOnClickListener
            }

            if (foodsAdapter.myOrderList.size > 0 && VIEW.notification_time.text.isNotEmpty() && VIEW.tv3.text.isNotEmpty()) {

                bundle.putString(OrderFoodFragment.TIME, time.toString())
                bundle.putSerializable(OrderFoodFragment.ITEMS, foodsAdapter.myOrderList)
                bundle.putInt(OrderFoodFragment.ShopId, shopId.toInt())
                bundle.putString(OrderFoodFragment.ShopImage,shopImage)
                bundle.putString("name", shopName)
                it.findNavController().navigate(R.id.orderFoodFragment, bundle)
            } else {
                if (VIEW.notification_time.text.isEmpty()) {
                    Toast.makeText(activity!!.applicationContext, getString(R.string.send_loc), Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }
                if (VIEW.tv3.text.isEmpty()) {
                    Toast.makeText(activity!!.applicationContext, getString(R.string.received_loc), Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }
                if (foodsAdapter.myOrderList.size <= 0) {

//                    TODO("kdwddddddddddddd")
                    bundle.putInt("shop_id", shopId.toInt())
                    bundle.putString("type","store")

                    bundle.putString(SentRequestFragment.TIME, time.toString())
                    it.findNavController().navigate(R.id.sentRequestFragment, bundle)

                }
            }

        }

        VIEW.locEnd.setOnClickListener {
            CURRENT_LOCATION = End
            bundle.putString(ShareLocationFragment.KEY, ShareLocationFragment.StoreStart)
            it.findNavController().navigate(R.id.shareLocationFragment, bundle)

        }

        VIEW.locStart.isClickable = false
        VIEW.locStart.isEnabled = false

        VIEW.locStart.setOnClickListener {
            CURRENT_LOCATION = Start
            bundle.putSerializable("branches", branches)
            bundle.putString(ShareLocationFragment.KEY, ShareLocationFragment.StoreEnd)
            it.findNavController().navigate(R.id.shareLocationFragment, bundle)

        }
        val list = restaurantViewModel.getTimes()
        val arrayAdapter = ArrayAdapter(activity!!.applicationContext, R.layout.spinner_item, R.id.text, list)
        VIEW.spinner.adapter = arrayAdapter

        VIEW.spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                time = restaurantViewModel.getTimes()[0].length
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                time = p2 + 1
            }
        }

        Log.e("SHOP 00", shopId + "   ")

        restaurantViewModel.showShopById(UserInfo(activity!!.applicationContext).getUserToken(), shopId.toInt())

        restaurantViewModel.menuItemsMutableLiveData.observe(this, Observer { t ->

            branches = t.branches as ArrayList<Branche>

            Log.e("BRanch ", "   " + branches.size.toString())

            foodsAdapter.setData(t.menu as ArrayList<Menu>)

        })

        return VIEW
    }

    private fun data() {

        loadingDialog.show()
        restaurantViewModel.showShopById(userInfo.getUserToken(), shopId.toInt())
        restaurantViewModel.menuItemsMutableLiveData.observe(this, Observer {

            loadingDialog.dismiss()
            lng = it.data.longitude.toDouble()
            lat = it.data.latitude.toDouble()
            setLocation()

        })


    }

    private fun setLocation() {

        val geocode = Geocoder(context, Locale("ar"))//.getDefault())
        var addresses: List<Address>? = null
        addresses = geocode.getFromLocation(lat, lng, 1)

        if (addresses != null && addresses.isNotEmpty()) {

            val street = addresses[0].getAddressLine(0) //thoroughfare
            VIEW.notification_time.text = street
            locationInfo.setStreetEnd(street)

            locationInfo.setLocationEnd(lat.toString(), lng.toString())

        }


    }


    override fun onResume() {
        //        TODO("setLocation from Share Location Fragment")

        super.onResume()
        Log.e("location ====== *", "onResume")
        Log.e("location ====== ", locationInfo.getStreetStart())

        VIEW.tv3.text = locationInfo.getStreetStart()
        VIEW.notification_time.text = locationInfo.getStreetEnd()

        Log.e("location ====== *", "onResume  )")

            Log.e(this.javaClass.name, "CurrentScreen")


    }


}