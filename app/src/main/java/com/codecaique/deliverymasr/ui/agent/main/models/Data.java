package com.codecaique.deliverymasr.ui.agent.main.models;

public class Data
{
    String market;
    String time;
    String profile;
    String name;

    public Data(String market, String time, String profile, String name) {
        this.market = market;
        this.time = time;
        this.profile = profile;
        this.name = name;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
