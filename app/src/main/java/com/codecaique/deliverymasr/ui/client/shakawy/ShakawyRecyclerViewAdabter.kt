package com.codecaique.deliverymasr.ui.client.shakawy

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.client.SubActivity
import com.codecaique.deliverymasr.ui.client.shakawy.ShakawyRecyclerViewAdabter.ViewHolderShakawy
import com.codecaique.deliverymasr.ui.client.stores.adapters.StoresAdapter

class ShakawyRecyclerViewAdabter(var context: Context) : RecyclerView.Adapter<ViewHolderShakawy>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderShakawy {
        val view= LayoutInflater.from(context).inflate(R.layout.item_shakawy, parent, false)
        return ViewHolderShakawy(view)
    }

    override fun onBindViewHolder(holder: ViewHolderShakawy, position: Int) {
//        holder.tvUserName.setText("***سع");
//        holder.tvOrderName.setText("قهوة حجم كبير");
        holder.btn_showRequest.setOnClickListener {
            val intent = Intent(holder.itemView.context, SubActivity::class.java)
            StoresAdapter.fragName = R.layout.fragment_shakawy_list2
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return 1
    }

    inner class ViewHolderShakawy(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var btn_showRequest: Button
        var tvOrderName: TextView? = null

        init {
            btn_showRequest = itemView.findViewById(R.id.btn_showRequest)
            //        tvOrderName = itemView.findViewById(R.id.tv_OrderName);
        }
    }

}