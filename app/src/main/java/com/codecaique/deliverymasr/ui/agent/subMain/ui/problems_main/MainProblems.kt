package com.codecaique.deliverymasr.ui.agent.subMain.ui.problems_main

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.StoresMainActivity
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.pojo.response.Problem
import com.codecaique.deliverymasr.utils.Utils
import kotlinx.android.synthetic.main.fragment_problems.view.*
import kotlin.collections.ArrayList

class MainProblems : Fragment() {
    //    FragmentProblemsBinding binding;
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(
                R.layout.fragment_problems, container, false)
    }

    lateinit var problemsViewModel: ProblemsViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        problemsViewModel=ViewModelProviders.of(this).get(ProblemsViewModel::class.java)


        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true/*  enabled by default*/) {
            override fun handleOnBackPressed() {
                // Handle the back button event
                if (UserInfo(activity!!.applicationContext).getState()!="user"){

                    val intent = Intent(view.context, StoresMainActivity::class.java)
                    intent.flags= Intent.FLAG_ACTIVITY_NEW_TASK
                    intent.putExtra(StoresMainActivity.KEY,StoresMainActivity.SUB_SETTINGS)
                    startActivity(intent)
                    requireActivity().finish()                }
                else
                {

                    activity!!.finish()
                }
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)

        val adapter = MainProblemsAdapter(activity!!.applicationContext)
        val recyclerView: RecyclerView = view.findViewById(R.id.rec_problems)
        recyclerView.adapter = adapter


        view.back.setOnClickListener { activity!!.finish() }

        problemsViewModel.showProblems(UserInfo(activity!!.applicationContext).getUserToken())
        problemsViewModel.showProblemsMutableLiveData.observe(this, androidx.lifecycle.Observer {
            if (it.error == 3) {
                Utils.signout(requireActivity() , view!!)
                return@Observer;
            }
            Log.e("TAG-a",it.data.size.toString())
            adapter.setData(it.data as ArrayList<Problem>)

        })

    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}