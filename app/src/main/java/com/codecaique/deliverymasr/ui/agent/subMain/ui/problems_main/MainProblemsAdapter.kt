package com.codecaique.deliverymasr.ui.agent.subMain.ui.problems_main

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.response.Problem

class MainProblemsAdapter(var context: Context) : RecyclerView.Adapter<MainProblemsAdapter.ViewHolder>() {

    var list= ArrayList<Problem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        context = parent.context
        val mView = LayoutInflater.from(parent.context).inflate(R.layout.item_problem, parent, false)
        return ViewHolder(mView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        //        holder.notification_title.setText(list.get(position).getMarket());

        val arrayItem=list[position]

        if (arrayItem.state=="wait")
            holder.replay.text="طلبك في الانتظار"
        else
            holder.replay.text="تم الــرد"

        holder.id.text=arrayItem.id.toString()
        holder.num.text=arrayItem.id.toString()
        holder.shopName.text=arrayItem.shope_name
        holder.message.text=arrayItem.content


        holder.show.setOnClickListener {
            val bundle=Bundle()
            bundle.putString("id",arrayItem.id.toString())
            it.findNavController().navigate(R.id.problemDetailsFragment,bundle)
        }
    }

    override fun getItemCount(): Int {/*editProfile.EditProfileFragment*/
        return list.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var id:TextView=itemView.findViewById(R.id.id)
        var num: TextView = itemView.findViewById(R.id.num)
        var shopName: TextView = itemView.findViewById(R.id.name)
        var message: TextView = itemView.findViewById(R.id.message)
        var replay: TextView = itemView.findViewById(R.id.replay)
        var show:Button=itemView.findViewById(R.id.buttonViewOrder)
        var root: ConstraintLayout = itemView.findViewById(R.id.root)

    }

    fun setData(a:ArrayList<Problem>){
        this.list=a
        notifyDataSetChanged()
    }

}