package com.codecaique.deliverymasr.ui.client.joinAsCaptain

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.client.MainActivity
import kotlinx.android.synthetic.main.dialog_captain.*

class SuccDialog(context: Context) :Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_captain)

        tv_ok.setOnClickListener {
            dismiss()
//            context.!!.finish()
            context.startActivity(Intent(context,MainActivity::class.java))

        }

    }
}