package com.codecaique.deliverymasr.ui.agent.main.ui.homeStores

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.AuthActivity
import com.codecaique.deliverymasr.base.agent.SubMainActivity
import com.codecaique.deliverymasr.pojo.response.NearStore
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.dialog_should_register.*

class NearStoresAdapter(var context: Context) : RecyclerView.Adapter<NearStoresAdapter.ViewHolderMarkets>() {

    var dynamicList = ArrayList<NearStore>()
    var staticList = ArrayList<NearStore>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderMarkets {
        val view = LayoutInflater.from(context).inflate(R.layout.item_order, parent, false)
        return ViewHolderMarkets(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolderMarkets, position: Int) {

        val arrayItem = dynamicList[position]

        holder.title.text = arrayItem.name
        holder.body.text = arrayItem.address
        holder.distance.text = String.format("%.2f", arrayItem.distance.toDouble()) + "\nكم "

        if (arrayItem.image != null && arrayItem.image.isNotEmpty())
            Picasso.get().load(ApiCreater.STORES_URL + arrayItem.image).into(holder.image)

        if (arrayItem.discount != null && arrayItem.discount != "0" && arrayItem.discount != "null") {

            holder.discount.visibility = View.VISIBLE
            if (!arrayItem.discount.contains("%"))
                holder.discount.text = arrayItem.discount + " %"
            else
                holder.discount.text = arrayItem.discount

        }
        holder.itemView.setOnClickListener {


            if (UserInfo(context).isNotRegister()) {
                UserInfo(context).logout()
                val dialog = Dialog(context)
                dialog.setContentView(R.layout.dialog_should_register)
                dialog.setCancelable(false)
                dialog.tv_ok.setOnClickListener {
                    val intent = Intent(context, AuthActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    context.startActivity(intent)
                }
                dialog.show()
                return@setOnClickListener
            }


            val intent = Intent(context, SubMainActivity::class.java)

            //            fragName = R.layout.fragment_kentaky_restaurant

            /*
            intent.putExtra(SubActivity.KEY, SubActivity.Restaurant)
            intent.putExtra(RestaurantFragment.NAME,arrayItem.name)
            intent.putExtra(RestaurantFragment.ID,arrayItem.id.toString())
            intent.putExtra(RestaurantFragment.IMAGE,arrayItem.image)

*/

            intent.putExtra(SubMainActivity.KEY, SubMainActivity.ORDERDETAILS)
            intent.putExtra("shop_id", arrayItem.id)
            intent.putExtra("shop_name", arrayItem.name)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)

        }

    }

    /*Intent intent = new Intent(holder.itemView.getContext(), DelievryPlaceActivity2.class);
        view.getContext().startActivity(intent);*/

    override fun getItemCount(): Int {
        return dynamicList.size
    }

    inner class ViewHolderMarkets(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView = itemView.findViewById(R.id.market_name)
        var body: TextView = itemView.findViewById(R.id.order_time)
        var distance: TextView = itemView.findViewById(R.id.coupon_cancel)
        var discount: TextView = itemView.findViewById(R.id.tv_discount)
        var image: CircleImageView = itemView.findViewById(R.id.market_logo)
    }

    fun setData(a: ArrayList<NearStore>) {
        this.dynamicList = a
        this.staticList = a
        notifyDataSetChanged()
    }

    fun filter(catName: String) {

        dynamicList = ArrayList<NearStore>()

        if (catName.contains(context.getString(R.string.all))) {

            dynamicList = staticList
            notifyDataSetChanged()

            return
        }

        staticList.forEach {
            if (it.category_name.contains(catName))
                dynamicList.add(it)
        }

        notifyDataSetChanged()

    }
}