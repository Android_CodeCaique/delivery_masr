package com.codecaique.deliverymasr.ui.client.bankAccount;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codecaique.deliverymasr.R;


public class BankAccount extends Fragment {
   RecyclerView rv_BankAcount;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_bank_account, container, false);

        rv_BankAcount = view.findViewById(R.id.rv_BankAcount);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext(), RecyclerView.VERTICAL,false);
        BankAccountRecyclerViewAdabter bankAccountRecyclerViewAdabter = new BankAccountRecyclerViewAdabter(view.getContext());
        rv_BankAcount.setAdapter(bankAccountRecyclerViewAdabter);
        rv_BankAcount.setLayoutManager(linearLayoutManager);


        return view;

    }
}
