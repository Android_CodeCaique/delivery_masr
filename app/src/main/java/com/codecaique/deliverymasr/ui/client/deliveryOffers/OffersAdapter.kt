package com.codecaique.deliverymasr.ui.client.deliveryOffers

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.client.SubActivity
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.response.BillPriceResponse
import com.codecaique.deliverymasr.pojo.response.BillResponse
import com.codecaique.deliverymasr.pojo.response.Message
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.pojo.response.Offer
import com.codecaique.deliverymasr.ui.client.confirmOrders.ConfirmMyOrder
import com.codecaique.deliverymasr.ui.client.deliveryOffers.OffersAdapter.DeliveryOffersViewHolder
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import com.google.firebase.firestore.FirebaseFirestore
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import java.text.DecimalFormat

class OffersAdapter(var context: Context, var offersViewModel: OffersViewModel) : RecyclerView.Adapter<DeliveryOffersViewHolder>() {

    private val TAG = "OffersAdapter"
    var list = ArrayList<Offer>()

    val loadingDialog = LoadingDialog(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeliveryOffersViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_delivery_offers, parent,false)
        return DeliveryOffersViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: DeliveryOffersViewHolder, position: Int) {

        val arrayItem = list[position]

        Log.e(TAG, "onBindViewHolder: ${arrayItem.offer_id}")

        if (arrayItem.image != null && arrayItem.image.isNotEmpty())
            Picasso.get().load(ApiCreater.USER_URL + arrayItem.image).into(holder.userImage)

        holder.tvCost.text = arrayItem.price + "ج"
        holder.tvTime.text = arrayItem.delivery_time
        holder.userName.text = arrayItem.first_name
        //+ " " + arrayItem.last_name
        holder.tvRate.text = arrayItem.rate

        try {
            val total = arrayItem.price.toLong()

            holder.tvDeliverMasr.text = "${total * .2} ج "
            holder.tvDriver.text = "${total - (total * .2)} ج "


        } catch (e: Exception) {
        }

        val formater = DecimalFormat("00.0")

        try {

            holder.tvAgentDistance.text = formater.format(arrayItem.agent_Recivingdistance.toDouble()) + " كم"
            holder.tvClientDistance.text = formater.format(arrayItem.client_Recivingdistance.toDouble()) + " كم"
            holder.tvDistance.text = formater.format(arrayItem.away_distance.toDouble()) + " كم"
        } catch (e: Exception) {

        }


        try {
            holder.rating.rating = arrayItem.rate.toFloat()
        } catch (e: Exception) {
            holder.rating.rating = 0f
        }


        try {
            holder.credit.visibility = if (arrayItem.payment == null || arrayItem.payment == "1") View.GONE else View.VISIBLE
        } catch (e: Exception) {
        }

        holder.btnAccept.setOnClickListener { view ->


            loadingDialog.show()

            Log.e(TAG,"array item "+list[position].toString())
            Log.e(TAG, "${UserInfo(context).getUserToken()}  zz ${arrayItem.offer_id}")
            Log.e(TAG, "onBindViewHolder: arrayItem.offer_id "+arrayItem.offer_id)
            Log.e(TAG, "onBindViewHolder: arrayItem.id "+arrayItem.id)
            var offer_id = "";

            if(arrayItem.offer_id == null || arrayItem.offer_id == "null"){
                offer_id = arrayItem.id
            }else{
                offer_id = arrayItem.offer_id
            }
            Log.e(TAG, "onBindViewHolder: offer_id "+offer_id)
            offersViewModel.acceptOffer(UserInfo(context).getUserToken(), offer_id, context)

            offersViewModel.acceptMutableLiveData.observe(context as FragmentActivity, Observer { t ->

               sendMessageToCloud(arrayItem, t)


            })


        }


    }

    private fun sendMessageToCloud(arrayItem: Offer, billPriceResponse: BillResponse) {

        var offer_id = "";
        if(arrayItem.offer_id == null || arrayItem.offer_id == "null"){
            offer_id = arrayItem.id
        }else{
            offer_id = arrayItem.offer_id
        }

        val db = FirebaseFirestore.getInstance()
                .collection(ApiCreater.OrdersCollection)
                .document(offer_id)
                .collection(ApiCreater.ChatCollection)

        val message = Message()

        message.id = db.document().id
        message.created_at = System.currentTimeMillis().toString()
        message.updated_at = System.currentTimeMillis().toString()
        message.receiver_id = arrayItem.delivery_id
        message.sender_id = UserInfo(context).getId().toString()
        message.order_state = ApiCreater.OrderStates.processed
        message.message = "تم قبول العرض"

        Log.e(TAG, "sendMessageToCloud: Message -> $message")

        db.document(message.id).set(message).addOnSuccessListener {
            loadingDialog.dismiss()

            val intent = Intent(context, SubActivity::class.java)

            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK

            var offer_id = "";
            if(arrayItem.offer_id == null || arrayItem.offer_id == "null"){
                offer_id = arrayItem.id
            }else{
                offer_id = arrayItem.offer_id
            }
            //StoresAdapter.fragName = R.layout.fragment_confirm_my_order
            intent.putExtra(SubActivity.KEY, SubActivity.ChatOrder)
            intent.putExtra(ConfirmMyOrder.REQUEST_ID, offer_id)
//            intent.putExtra(ConfirmMyOrder.REQUEST_ID, 1arrayItem.request_id)
            intent.putExtra(ConfirmMyOrder.USER_ID,offer_id)


            Log.e("dada", "arrayItem.offer_id ${offer_id} arrayItem.delivery_id ${arrayItem.delivery_id}")

            context.startActivity(intent)
        }

    }


    override fun getItemCount(): Int {
        return list.size
    }

    inner class DeliveryOffersViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var btnAccept: Button = itemView.findViewById(R.id.btn_Accept)
        var userImage: CircleImageView = itemView.findViewById(R.id.profile_image)
        var userName: TextView = itemView.findViewById(R.id.tv_userName)
        var rating: RatingBar = itemView.findViewById(R.id.rate)
        var tvRate: TextView = itemView.findViewById(R.id.textDescription)
        var tvPushWay: TextView = itemView.findViewById(R.id.tvPushWay)
        var tvClientDistance: TextView = itemView.findViewById(R.id.tvClientDistance)
        var tvAgentDistance: TextView = itemView.findViewById(R.id.tvAgentDistance)
        var tvDistance: TextView = itemView.findViewById(R.id.tvDistance)
        var tvCost: TextView = itemView.findViewById(R.id.tvCost)
        var tvTime: TextView = itemView.findViewById(R.id.tvTime)
        var tvDriver: TextView = itemView.findViewById(R.id.tv_driver)
        var tvDeliverMasr: TextView = itemView.findViewById(R.id.tv_delivery_masr)
        var credit: ImageView = itemView.findViewById(R.id.credit)
    }

    fun setData(a: ArrayList<Offer>) {
        this.list = a
        notifyDataSetChanged()
    }

}