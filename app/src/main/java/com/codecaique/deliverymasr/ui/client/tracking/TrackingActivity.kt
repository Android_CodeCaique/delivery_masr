package com.codecaique.deliverymasr.ui.client.tracking

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.databinding.ActivityTrackingBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.firestore.FirebaseFirestore

class TrackingActivity : AppCompatActivity(), OnMapReadyCallback {

    var driverId = ""
    var driverName = ""
    private lateinit var mMap: GoogleMap
    lateinit var myLocation: LatLng
    lateinit var binding: ActivityTrackingBinding
    private var db = FirebaseFirestore.getInstance()
            .collection(ApiCreater.TrackingCollection)

    companion object {

        const val DRIVER_NAME = "driverName"
        const val DRIVER_ID = "driverId"

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_tracking)

        intent!!.extras!!.apply {
            driverId = getString(DRIVER_ID)!!
            driverName = getString(DRIVER_NAME)!!
        }

        val mapFragment = //(context!! as AppCompatActivity).supportFragmentManager
                supportFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment
        mapFragment.getMapAsync(this)

        binding.back.setOnClickListener {
            finish()
        }

    }


    override fun onMapReady(googleMap: GoogleMap?) {

        mMap = googleMap!!
        mMap.mapType = GoogleMap.MAP_TYPE_NORMAL

        db.document(driverId).addSnapshotListener { documentSnapshot, _ ->

            mMap.clear()
            myLocation = LatLng(
                    documentSnapshot!!["latitude"]!!.toString().toDouble(),
                    documentSnapshot["longitude"]!!.toString().toDouble()
            )

            mMap.addMarker(MarkerOptions().position(myLocation).title(driverName)).showInfoWindow()
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 15f))

        }



    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}