package com.codecaique.deliverymasr.ui.agent.subMain.ui.chat.dialogs

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.StoresMainActivity
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.client.chat.ChatViewModel
import kotlinx.android.synthetic.main.dialog_case_order_out.*

class CassOfOrderOutDialog(var context_: Context, var chatViewModel: ChatViewModel, var requestId: Int) : Dialog(context_), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_case_order_out)

        btn_close.setOnClickListener {
            dismiss()
        }
        tv_case_1.setOnClickListener(this)
        tv_case_2.setOnClickListener(this)
        tv_out.setOnClickListener (this)
    }

    override fun onClick(p0: View?) {

        if (p0!!.id == R.id.tv_case_1 || p0.id == R.id.tv_case_2  || p0.id == R.id.tv_out){

            val reason= (p0 as TextView).text .toString()

            chatViewModel.cancelOffer(UserInfo(context_).getUserToken(),requestId,reason)
            chatViewModel.cancelOfferMutableLiveData.observe(context_  as AppCompatActivity, Observer {

                if (it.error==0){

                    Log.e("test","efejlot")
                    val intent = Intent(context_, StoresMainActivity::class.java)
                    context_.startActivity(intent)

                    dismiss()
                }
                else{

                    Toast.makeText(context_,context_.getString(R.string.can_not_be_out), Toast.LENGTH_SHORT).show()
                    dismiss()

                }

            })

        }
    }


}
