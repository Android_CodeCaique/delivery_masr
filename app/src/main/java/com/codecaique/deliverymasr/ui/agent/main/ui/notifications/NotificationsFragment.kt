package com.codecaique.deliverymasr.ui.agent.main.ui.notifications

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.SharedHelper
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.pojo.response.Notification
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import com.codecaique.deliverymasr.ui.client.notification.NotificationsViewModel
import com.codecaique.deliverymasr.ui.client.notification.SwipeToDeleteCallback
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.fragment_notifications.view.*
import java.util.*

class NotificationsFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notifications, container, false)
    }

    lateinit var myNotificationsViewModel: NotificationsViewModel
    lateinit var notificationsAdapter: NotificationsAdapter
    lateinit var userInfo: UserInfo
    lateinit var loadingDialog: LoadingDialog

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {


        myNotificationsViewModel = ViewModelProviders.of(this).get(NotificationsViewModel::class.java)
        notificationsAdapter = NotificationsAdapter(activity!!.applicationContext)
        userInfo = UserInfo(activity!!.applicationContext)
        loadingDialog= LoadingDialog(activity!!)

        view.rv_notifications.adapter = notificationsAdapter

        view.back.setOnClickListener {
            activity!!.findViewById<BottomNavigationView>(R.id.bottom_nav).selectedItemId = R.id.offers_del
            it.findNavController().navigate(R.id.offersDeliveredFragment)
        }

        myNotificationsViewModel.getNotifications(userInfo.getUserToken(),"driver", SharedHelper().getKey(activity!!, "LANG")!!)
        myNotificationsViewModel.myNotificationsMutableLiveData.observe(this, androidx.lifecycle.Observer { t ->

            Log.e("data", t.data.size.toString())

            view.progressBar.visibility = View.GONE
            notificationsAdapter.setData(t.data as ArrayList<Notification>)

            if (t.data.isEmpty())
                Toast.makeText(activity!!.applicationContext, getString(R.string.no_notifi), Toast.LENGTH_SHORT).show()

        })

        view.refresh_layout.setOnRefreshListener {
            myNotificationsViewModel.getNotifications(userInfo.getUserToken(),"driver", SharedHelper().getKey(activity!!, "LANG")!!)
            myNotificationsViewModel.myNotificationsMutableLiveData.observe(this, androidx.lifecycle.Observer { t ->

                Log.e("data", t.data.size.toString())

                view.progressBar.visibility = View.GONE
                notificationsAdapter.setData(t.data as ArrayList<Notification>)

                if (t.data.isEmpty())
                    Toast.makeText(activity!!.applicationContext, getString(R.string.no_notifi), Toast.LENGTH_SHORT).show()

            })

            view.refresh_layout.isRefreshing = false

        }

        enableSwipeToDeleteAndUndo(view)

    }

    private fun enableSwipeToDeleteAndUndo(v: View) {
        val swipeToDeleteCallback: SwipeToDeleteCallback = object : SwipeToDeleteCallback(activity!!) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, i: Int) {

                loadingDialog.show()
                val position = viewHolder.adapterPosition

                myNotificationsViewModel.deleteNotification(activity!!.applicationContext,userInfo, notificationsAdapter,loadingDialog,position)
                myNotificationsViewModel.deleteNotificationsMutableLiveData.observe(this@NotificationsFragment, Observer {

                    if (it.error == 0) {

                        userInfo.setDiscountNotifiCount()
//                        notificationsAdapter.list.removeAt(position)
//                        v.rv_notifications.adapter!!.notifyDataSetChanged()

                        loadingDialog.dismiss()
                    }


                })


            }
        }
        val itemTouchpaper = ItemTouchHelper(swipeToDeleteCallback)
        itemTouchpaper.attachToRecyclerView(v.rv_notifications)
    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }

}