package com.codecaique.deliverymasr.ui.agent.subMain.ui.coupons

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.StoresMainActivity
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.pojo.response.Coupon
import com.codecaique.deliverymasr.ui.client.coupons.CouponsAdapter
import com.codecaique.deliverymasr.ui.client.coupons.CouponsViewModel
import kotlinx.android.synthetic.main.dialog_add_copoun.*
import kotlinx.android.synthetic.main.fragment_coupons.view.*

class CouponsFragment : Fragment() {

    private var dialog: Dialog? = null
    lateinit var couponsViewModel: CouponsViewModel
    lateinit var userInfo: UserInfo
    lateinit var couponsAdapter:CouponsAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_coupons, container, false)



        val intent = Intent(view.context, StoresMainActivity::class.java)
        intent.flags= Intent.FLAG_ACTIVITY_NEW_TASK

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true/*  enabled by default*/) {
            override fun handleOnBackPressed() {
                // Handle the back button event
                intent.putExtra(StoresMainActivity.KEY, StoresMainActivity.SETTINGS)
                startActivity(intent)
                requireActivity().finish()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)



        userInfo= UserInfo(activity!!.applicationContext)
        couponsViewModel=ViewModelProviders.of(this).get(CouponsViewModel::class.java)

        couponsAdapter=CouponsAdapter(activity!!.applicationContext)
        view.rv_coupons.adapter=couponsAdapter

        couponsViewModel.myCoupons(userInfo.getUserToken())
        couponsViewModel.myCouponsMutableLiveData.observe(this, Observer { t->

            view.progressBar2.visibility=View.GONE
            couponsAdapter.setData(t.data as ArrayList<Coupon>)

            if (t.data.size == 0)
                view.no_data.visibility = View.VISIBLE

        })




        view.iv_backCoupons.setOnClickListener {
            intent.putExtra(StoresMainActivity.KEY, StoresMainActivity.SETTINGS)
            startActivity(intent)
            requireActivity().finish()
        }

        view.btn_done.setOnClickListener {
            dialogAddCoupon()
        }
        return view
    }

    fun dialogAddCoupon() {
        dialog = Dialog(activity!!)
        dialog!!.setContentView(R.layout.dialog_add_copoun)
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog!!.btn_done.setOnClickListener {

            val msg=dialog!!.tv_camera.text.toString()

            if (msg.isNotEmpty()){
                dialog!!.progressBar.visibility=View.INVISIBLE
                dialog!!.btn_done.isEnabled=false

                couponsViewModel.addMyCoupon(userInfo.getUserToken(),msg)
                couponsViewModel.addCouponMutableLiveData.observe(this, Observer { t->

                    if (t.error==0)
                    {
                        Toast.makeText(activity!!.applicationContext,activity!!.resources.getString(R.string.coupon_added),Toast.LENGTH_SHORT).show()

                        view!!.progressBar2.visibility=View.VISIBLE
                        couponsViewModel.myCoupons(userInfo.getUserToken())
                        couponsViewModel.myCouponsMutableLiveData.observe(this, Observer { t->

                            view!!.progressBar2.visibility=View.GONE
                            couponsAdapter.setData(t.data as ArrayList<Coupon>)

                        })



                    }
                    else{
                        Toast.makeText(activity!!.applicationContext,t.message,Toast.LENGTH_SHORT).show()
                    }

                })

                dialog!!.dismiss()

            }
            else{
                dialog!!.tv_camera.error=activity!!.resources.getString(R.string.enter_coupon)
            }

        }

        dialog!!.show()
    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}