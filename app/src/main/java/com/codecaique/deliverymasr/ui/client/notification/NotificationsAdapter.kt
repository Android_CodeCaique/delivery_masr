package com.codecaique.deliverymasr.ui.client.notification

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.response.Notification
import com.codecaique.deliverymasr.ui.client.notification.NotificationsAdapter.ViewHolderNotification
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

class NotificationsAdapter(var context: Context) : RecyclerView.Adapter<ViewHolderNotification>() {


    var list = ArrayList<Notification>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderNotification {
        val view = LayoutInflater.from(context).inflate(R.layout.item_notifications, parent, false)
        return ViewHolderNotification(view)
    }

    override fun onBindViewHolder(holder: ViewHolderNotification, position: Int) {

        val arrayItem=list[position]

        holder.message.text=arrayItem.body
        holder.title.text=arrayItem.title

        if (arrayItem.user_image!=null && arrayItem.user_image.isNotEmpty())
            Picasso.get().load(ApiCreater.USER_URL+arrayItem.user_image).into(holder.image)

        /*
        holder.itemView.setOnClickListener {

            val intent=Intent(context,SubMainActivity::class.java)
            intent.putExtra(SubMainActivity.KEY,SubMainActivity.SPECIALORDER)
            intent.putExtra(ChatFragment.REQUEST_ID,arrayItem.offer_id)
            intent.putExtra(ChatFragment.USER_ID,arrayItem.user_id)
            intent.flags=Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)

        }
*/

        /*     Intent intent = new Intent(holder.itemView.getContext(), DelievryPlaceActivity2.class);
       view.getContext().startActivity(intent);*/

    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class ViewHolderNotification(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView=itemView.findViewById(R.id.market_name)
        var message: TextView=itemView.findViewById(R.id.order_time)
        var image:CircleImageView=itemView.findViewById(R.id.market_logo)
    }

    fun setData(a:ArrayList<Notification>){
        this.list=a
        notifyDataSetChanged()
    }


    fun notifyAdapter() {
//        list.removeAt(position)
        notifyDataSetChanged()
    }

}