package com.codecaique.deliverymasr.ui.agent.subMain.ui.orderDetails.framment.store_info_fragment

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.response.GeneralResponse
import com.codecaique.deliverymasr.pojo.response.ShowShopByIdResponse
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class Store_Info_ViewModel: ViewModel()
{
    val addShopData = MutableLiveData<GeneralResponse>()
    
    val shopMutableLiveData = MutableLiveData<ShowShopByIdResponse>()

    fun addShopAgent(token:String ,shop_id: String)
    {
        val observable =
                ApiCreater.instance.addShopAgent(token,shop_id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object  : Observer<GeneralResponse> {
            override fun onComplete() {

            }


            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: GeneralResponse) {
                addShopData.value = t
                if (t.error == 0)
                    Log.e("Response","OK 200")
            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)
    }


    fun showShopById(user_token: String, shop_id: Int) {

        val observable =
                ApiCreater.instance.showShopById(user_token, shop_id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())

        val observer = object : Observer<ShowShopByIdResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: ShowShopByIdResponse) {


                shopMutableLiveData.value = t
            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)
    }

}