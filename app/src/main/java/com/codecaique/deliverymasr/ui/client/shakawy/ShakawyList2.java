package com.codecaique.deliverymasr.ui.client.shakawy;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.codecaique.deliverymasr.R;


public class ShakawyList2 extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_shakawy_list2, container, false);
        ImageView iv_backShakawy2 = view.findViewById(R.id.iv_backShakawy2);
        iv_backShakawy2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent=new Intent(getContext(), MainActivity2.class);
//                fragName=R.layout.fragment_shakawy;
//                startActivity(intent);
                ShakawyFragment shakawy = new ShakawyFragment();
                FragmentManager manager = getFragmentManager();
                manager.beginTransaction().replace(R.id.main3_fragment, shakawy, shakawy.getTag()).commit();
            }
        });

        return view;
    }


}
