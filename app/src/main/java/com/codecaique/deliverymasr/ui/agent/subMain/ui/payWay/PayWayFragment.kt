package com.codecaique.deliverymasr.ui.agent.subMain.ui.payWay

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.codecaique.deliverymasr.R
import kotlinx.android.synthetic.main.fragment_pay_way.view.*

/**
 * A simple [Fragment] subclass.
 */
class PayWayFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pay_way, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.btn.setOnClickListener {



        }

        view.back.setOnClickListener {
            activity!!.finish()
        }




    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}
