package com.codecaique.deliverymasr.ui.agent.subMain.ui.chat.dialogs

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.Window
import android.view.WindowManager
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.ui.client.chat.ChatViewModel
import kotlinx.android.synthetic.main.dialog_order_out.*

class OrderOutDialog(var context_: Context, var chatViewModel: ChatViewModel, var  requestId: Int) : Dialog(context_) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_order_out)

        btn_close.setOnClickListener {

            dismiss()
        }
        tv_out.setOnClickListener {

            val dialog= CassOfOrderOutDialog(context_,chatViewModel,requestId)
            val window: Window = dialog.window!!
            val wlp: WindowManager.LayoutParams = window.attributes
            wlp.gravity = Gravity.BOTTOM
            wlp.flags = wlp.flags and WindowManager.LayoutParams.FLAG_DIM_BEHIND.inv()
            window.attributes = wlp
            dialog.show()
            dismiss()
        }
    }
}
