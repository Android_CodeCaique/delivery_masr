package com.codecaique.deliverymasr.ui.client.stores

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.client.SubActivity
import com.codecaique.deliverymasr.pojo.util.UserInfo
import kotlinx.android.synthetic.main.fragment_stores.view.*

class StoresFragment : Fragment() {

    var TAG = "StoresFragment"
    lateinit var storesViewModel: StoresViewModel
    lateinit var userInfo: UserInfo


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_stores, container, false)

        storesViewModel = ViewModelProviders.of(this).get(StoresViewModel::class.java)
        userInfo = UserInfo(activity!!.applicationContext)


        /*
        view.include_buy.setOnClickListener {

            val intent = Intent(context, SubActivity::class.java)
            intent.putExtra(SubActivity.KEY, SubActivity.Purchases)
            intent.putExtra(RestaurantFragment.NAME, getString(R.string.purchases))
            intent.putExtra(RestaurantFragment.ID, catList[0].id.toString())
            intent.putExtra(RestaurantFragment.IMAGE, catList[0].image)

            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context!!.startActivity(intent)


        }
*/


        view.tard.setOnClickListener {
            val intent = Intent(view.context, SubActivity::class.java)
            intent.putExtra(SubActivity.KEY, SubActivity.TardHayak)
            activity!!.startActivity(intent)
        }

        view.purchases.setOnClickListener {
            val intent = Intent(view.context, SubActivity::class.java)
            intent.putExtra(SubActivity.KEY, SubActivity.Purchases)
            activity!!.startActivity(intent)
        }

        return view
    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}