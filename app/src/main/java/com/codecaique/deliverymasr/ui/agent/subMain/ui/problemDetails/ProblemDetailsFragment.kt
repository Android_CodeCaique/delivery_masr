package com.codecaique.deliverymasr.ui.agent.subMain.ui.problemDetails

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.pojo.response.Answer
import com.codecaique.deliverymasr.pojo.response.Image
import com.codecaique.deliverymasr.ui.agent.subMain.ui.problems_main.ProblemsViewModel
import kotlinx.android.synthetic.main.fraggment_problem_details.view.*

class ProblemDetailsFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fraggment_problem_details, container, false)
    }

    lateinit var problemsViewModel: ProblemsViewModel

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        problemsViewModel = ViewModelProviders.of(this).get(ProblemsViewModel::class.java)
        val id = arguments!!.getString("id")!!

        view.back.setOnClickListener {
            it.findNavController().popBackStack()
        }

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true/*  enabled by default*/ ) {
            override fun handleOnBackPressed() {
                // Handle the back button event
                view.findNavController().popBackStack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)



        problemsViewModel.showComplianceById(UserInfo(activity!!.applicationContext).getUserToken(), id)

        problemsViewModel.makeProblemByIdMutableLiveData.observe(this, Observer { t ->


            val photosAdapter = PhotosAdapter(activity!!.applicationContext)
            view.rv_photo.adapter = photosAdapter

            photosAdapter.setData(t.images as ArrayList<Image>)



            if (t.data.answer!=null && t.data.answer!!.isNotEmpty())
                view.tv_replay.text = t.data.answer + "  "
            else
                view.tv_replay.text = " لم يتم الرد "

            view.tv_details.text=t.data.details
            view.tv_shop_name.text=t.data.shope_name
            view.tv_id.text=t.data.id.toString()

            val answersAdapter=AnswersAdapter(activity!!.applicationContext)
            view.rv_answers.adapter=answersAdapter

            answersAdapter.setData(t.answers as ArrayList<Answer>)

        })

    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}