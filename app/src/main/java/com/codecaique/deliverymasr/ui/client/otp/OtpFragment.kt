package com.codecaique.deliverymasr.ui.client.otp

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.client.MainActivity
import com.codecaique.deliverymasr.databinding.FragmentSignInConfirmBinding
import com.codecaique.deliverymasr.pojo.response.*
import com.codecaique.deliverymasr.pojo.util.UserInfo

/**
 * A simple [Fragment] subclass.
 */
class OtpFragment : Fragment() {

    lateinit var binding: FragmentSignInConfirmBinding
    lateinit var viewModel: OtpViewModel
    private lateinit var data: Any
    private var key = "key"

    companion object {

        const val DATA = "data"
        private const val TAG = "OtpFragment"
        private const val NEW = "new"
        private const val OLD = "old"

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_in_confirm, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(OtpViewModel::class.java)

        args()

        binding.tvNoMessage.setOnClickListener {
            args()
        }

        binding.buttonConfirm.setOnClickListener {

            if (binding.otpView.text.toString().isNotEmpty() && binding.otpView.text.toString().length == 6) {
                if (viewModel.otpCodeMutableLiveData.value== binding.otpView.text.toString()) {

                    when (key) {
                        OLD -> {
                            UserInfo(requireContext()).setUserData(data as Phone)
                            UserInfo(requireContext()).setState("user")
//                            activity!!.startActivity(Intent(activity!!.applicationContext, MainActivity::class.java))
                            val intent = Intent(context, MainActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                            requireContext().startActivity(intent)
                        }
                        NEW -> {

                            UserInfo(requireContext()).setUserData( data as UserData )

//                            Log.e(TAG, "onViewCreated: ${t.data.id}")
                            Toast.makeText(context, getString(R.string.wel_come), Toast.LENGTH_LONG).show()
//                            it.findNavController().navigate(R.id.action_fragment_Mobile_Verification2_to_fragment_Code_Verification)
                            val intent = Intent(context, MainActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                        }
                    }

                }
                else {
                    Toast.makeText(requireContext(), getString(R.string.otp_code_fialde), Toast.LENGTH_SHORT).show()
                }

            } else {
                Toast.makeText(requireContext(), getString(R.string.enter_otp), Toast.LENGTH_SHORT).show()
            }

        }


    }

    private fun args() {
        data = requireArguments().getSerializable(DATA)!!

        UserInfo(requireContext()).setUserData(
                data as UserData
        )
        when (data) {
            is UserData -> {

                Log.e(TAG, "args: 1")

                viewModel.otp(binding, requireActivity(), (data as UserData).phone.toString())
                key = NEW

            }
            is RegistrationResponse -> {
                Log.e(TAG, "args: 2")
            }
            is Phone -> {
                Log.e(TAG, "args: 3")
                viewModel.otp(binding, requireActivity(), (data as Phone).phone)
                key = OLD

            }
        }

    }

    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}
