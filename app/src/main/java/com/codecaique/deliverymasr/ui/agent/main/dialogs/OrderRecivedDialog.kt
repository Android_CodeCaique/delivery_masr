package com.codecaique.deliverymasr.ui.agent.main.dialogs

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import com.codecaique.deliverymasr.R

class OrderRecivedDialog(context: Context) : Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_order_recived)
    }
}
