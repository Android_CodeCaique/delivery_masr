package com.codecaique.deliverymasr.ui.client.shopsNotSupported.deliveryLocation

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.databinding.FragmentDeliveryLocationBinding
import com.codecaique.deliverymasr.pojo.response.Message
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import com.codecaique.deliverymasr.ui.client.shopsNotSupported.selectLocation.SelectLocationActivity
import com.codecaique.deliverymasr.ui.client.tardHayak.MakeOrderViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.codecaique.deliverymasr.pojo.util.MyUtil
import kotlinx.android.synthetic.main.dialog_order_wait.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.util.*

class DeliveryLocationFragment : Fragment(), OnMapReadyCallback {

    private var message: String? = null
    private var imageUri: String? = null
    private var coupon: String? = null
    private var paymentId: String? = null
    private var shopName: String? = null
    private var shopStreet: String? = null
    private var shopLat: String? = null
    private var shopLng: String? = null
    private var deliveryLocationLng = ""
    private var deliveryLocationLat = ""
    private var deliveryLocationStreet = ""
    private lateinit var binding: FragmentDeliveryLocationBinding
    lateinit var loadingDialog: LoadingDialog
    lateinit var makeOrderViewModel: MakeOrderViewModel
    lateinit var userInfo: UserInfo
    private var time = 1
    private val TAG = "DeliveryLocationFragment"
    private lateinit var mMap: GoogleMap

    companion object {

        const val MESSAGE = "param1"
        const val IMAGE_URI = "param2"
        const val COUPON = "param3"
        const val PAYMENT_ID = "param4"
        const val SHOP_NAME = "param5"
        const val SHOP_LATITUDE = "param6"
        const val SHOP_LONGITUDE = "param7"
        const val SHOP_STREET = "param8"
        const val LOCATION_REQUEST_CODE = 1233

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_delivery_location, container, false)

        return binding.root
    }

    @SuppressLint("LongLogTag")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        makeOrderViewModel = ViewModelProviders.of(this).get(MakeOrderViewModel::class.java)
        loadingDialog = LoadingDialog(activity!!)
        userInfo = UserInfo(activity!!)

        val mapFragment =
                childFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment
        mapFragment.getMapAsync(this)

        arguments!!.let {
            message = it.getString(MESSAGE)
            paymentId = it.getString(PAYMENT_ID)
            coupon = it.getString(COUPON)
            imageUri = it.getString(IMAGE_URI)
            shopName = it.getString(SHOP_NAME)
            shopLat = it.getString(SHOP_LATITUDE)
            shopLng = it.getString(SHOP_LONGITUDE)
            shopStreet = it.getString(SHOP_STREET)
        }

        binding.tvShopName.text = shopName

        binding.back.setOnClickListener {
            it.findNavController().popBackStack()
        }

        binding.conLastLocation.setOnClickListener {

            Log.e(TAG, "onViewCreated: conLastLocation.setOnClickListener")
            val intent = Intent(activity!!, SelectLocationActivity::class.java)
            startActivityForResult(intent, LOCATION_REQUEST_CODE)

        }

        val list = makeOrderViewModel.getTimes().toList()
        val arrayAdapter = ArrayAdapter(activity!!.applicationContext, R.layout.spinner_item, R.id.text, list)
        binding.spinner.adapter = arrayAdapter

        binding.spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                time = makeOrderViewModel.getTimes()[0].length
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                time = p2 + 1
            }
        }


        binding.buttonConfirm.setOnClickListener {


            if(userInfo.isNotRegister())
            {
                Toast.makeText(activity!!, getString(R.string.should_register), Toast.LENGTH_SHORT).show()
                return@setOnClickListener

            }

            if (deliveryLocationLat.isEmpty()) {

                Toast.makeText(activity!!, getString(R.string.chooseLocation), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            loadingDialog.show()

            Log.e(TAG, "onViewCreated: " +
                    " getUserToken ${userInfo.getUserToken()} \n" +
                    " shopName $shopName\n" +
                    " shopLat $shopLat \n" +
                    " shopLng $shopLng \n" +
                    " deliveryLocationStreet $deliveryLocationStreet \n" +
                    " deliveryLocationLng $deliveryLocationLng \n" +
                    " deliveryLocationLat $deliveryLocationLat \n" +
                    " time $time \n" +
                    " message $message \n" +
                    " paymentId $paymentId \n" +
                    " coupon $coupon \n" +
                    " imageUri $imageUri \n" +
                    " imageUri ${(imageUri == null || imageUri!!.contains("null")) } ")
            val currentTime = Calendar.getInstance().time

            Log.e(TAG, "onViewCreated: sentTard 2")
            makeOrderViewModel.sentTard(
                    RequestBody.create(MediaType.parse("multipart/form-data"), userInfo.getUserToken()),
                    3,
                    RequestBody.create(MediaType.parse("multipart/form-data"), "$shopName ${shopStreet!!}"),
                    shopLat!!.toDouble(),
                    shopLng!!.toDouble(),
                    RequestBody.create(MediaType.parse("multipart/form-data"), deliveryLocationStreet),
                    deliveryLocationLat.toDouble(),
                    deliveryLocationLng.toDouble(),
                    currentTime,
                    RequestBody.create(MediaType.parse("multipart/form-data"), message!!),
                    if (imageUri == null || imageUri!!.contains("null")) null else image(),
                    RequestBody.create(MediaType.parse("multipart/form-data"), coupon!!),
                    paymentId!!.toInt(),
                    RequestBody.create(MediaType.parse("multipart/form-data"), ""),
                    0.0,
                    loadingDialog,
                    activity!!
            )

            makeOrderViewModel.tardHayyakMutableLiveData.observe(this, Observer { t ->

                loadingDialog.dismiss()

                when (t.error) {
                    0 -> {

                        val message1 = Message()
                        message1.message = message!!

                        sendMessageToCloud(message1, it, t.data.id.toString())
                    }
                    4 -> {
                        waiteOrderDialog()
                    }
                    else -> {
                        Toast.makeText(activity!!.applicationContext, t.message, Toast.LENGTH_SHORT).show()
                    }
                }
                Log.e(TAG, "onViewCreated: t.error ${t.error}")

            })


        }

    }

    @SuppressLint("LongLogTag")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        Log.e(TAG, "onActivityResult: nn")

        if (resultCode == Activity.RESULT_OK && requestCode == LOCATION_REQUEST_CODE) {


            Log.e(TAG, "onActivityResult: aa")
            deliveryLocationLat = data!!.getStringExtra(SelectLocationActivity.LATITUDE)!!
            deliveryLocationLng = data.getStringExtra(SelectLocationActivity.LONGITUDE)!!
            deliveryLocationStreet = data.getStringExtra(SelectLocationActivity.STREET)!!
            binding.tv2.text = deliveryLocationStreet

        }

        Log.e(TAG, "onActivityResult: ss")

    }


    private fun waiteOrderDialog() {
        val dialog = Dialog(view!!.context)
        dialog.setContentView(R.layout.dialog_order_wait)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.show()

        dialog.tv_ok.setOnClickListener {
            activity!!.finish()
        }

    }

    private lateinit var db: CollectionReference
    private lateinit var mStorageRef: StorageReference

    private fun sendMessageToCloud(message: Message, it: View, requestId: String) {

        db = FirebaseFirestore.getInstance()
                .collection(ApiCreater.OrdersCollection)
                .document(requestId)
                .collection(ApiCreater.ChatCollection)

        mStorageRef = FirebaseStorage.getInstance()
                .getReference(ApiCreater.OrdersStorage).child(requestId)

        message.id = db.document().id
        message.created_at = System.currentTimeMillis().toString()
        message.updated_at = System.currentTimeMillis().toString()
        message.sender_id = UserInfo(activity!!).getId().toString()
        message.order_state = ApiCreater.OrderStates.receivedOrder


        val imageName = MyUtil.getRandomName() + ".jpg"

        if (message.image.isNotEmpty()) {

            mStorageRef.child(imageName).putFile(Uri.parse(imageUri)).continueWithTask {
                mStorageRef.child(imageName).downloadUrl
            }.addOnSuccessListener { u ->
                message.image = u.toString()
                sendTheMessage(message, it)
            }


        } else
            sendTheMessage(message, it)


    }

    private fun sendTheMessage(message: Message, it: View) {

        db.document(message.id).set(message).addOnSuccessListener { _ ->
            it.findNavController().navigate(R.id.orderSented)

        }
    }

    @SuppressLint("Recycle")
    private fun image(): MultipartBody.Part? {
        val uri = Uri.parse(imageUri)

        Log.e("URI 222", uri!!.path.toString() + "    ")

        val p: String
        val cursor = activity!!.contentResolver.query(uri, null, null, null, null)

        if (cursor == null) {
            p = uri.path.toString()
            Log.e("URI 222 0000", uri.path.toString() + "    ")
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            Log.e("URI 222 005500", uri.path.toString() + "    ")


            p = cursor.getString(idx)
        }

        val file = File(p)
        val requestFile = RequestBody.create(MediaType.parse("image/*"), file)
        return MultipartBody.Part.createFormData("image", file.name, requestFile)
    }

    override fun onMapReady(googleMap: GoogleMap?) {

        mMap = googleMap!!
        binding.progressBar.visibility = View.GONE
        mMap.mapType = GoogleMap.MAP_TYPE_NORMAL


        val shopLocation = LatLng(shopLat!!.toDouble(), shopLng!!.toDouble())

        val shopMarker = MarkerOptions()
                .position(shopLocation)
                .icon(bitmapDescriptorFromVector(R.drawable.ic_fork_and_knife_thin_outline))
                .title(shopName)

        mMap.addMarker(shopMarker).showInfoWindow()
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(shopLocation, 15f))

    }

    private fun bitmapDescriptorFromVector(@DrawableRes vectorDrawableResourceId: Int): BitmapDescriptor? {
        val vectorDrawable = ContextCompat.getDrawable(activity!!, vectorDrawableResourceId)
        vectorDrawable!!.setBounds(
                0,
                0,
                vectorDrawable.intrinsicWidth,
                vectorDrawable.intrinsicHeight
        )
        val bitmap = Bitmap.createBitmap(
                vectorDrawable.intrinsicWidth,
                vectorDrawable.intrinsicHeight,
                Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}