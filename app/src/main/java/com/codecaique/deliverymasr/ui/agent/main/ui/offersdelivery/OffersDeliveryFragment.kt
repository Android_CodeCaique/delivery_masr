package com.codecaique.deliverymasr.ui.agent.main.ui.offersdelivery

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.pojo.response.DeliveryOffers_Response
import com.codecaique.deliverymasr.ui.client.deliveryOffers.OffersViewModel
import kotlinx.android.synthetic.main.dialog_notification_off.btn_close
import kotlinx.android.synthetic.main.dialog_notification_off.progressBar
import kotlinx.android.synthetic.main.dialog_notification_on.*
import kotlinx.android.synthetic.main.fragment_offers_delivery.view.*
import java.util.*

class OffersDeliveryFragment : Fragment() {

    private val TAG = "OffersDeliveryFragment"
    lateinit var offersViewModel: OffersViewModel
    lateinit var userInfo: UserInfo

    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_offers_delivery, container, false)


        offersViewModel = ViewModelProviders.of(this).get(OffersViewModel::class.java)
        userInfo = UserInfo(activity!!.applicationContext)
        val offersAdapter = OffersAdapter(activity!!, offersViewModel)

        val linearLayoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)

        view.rec_orders.layoutManager = linearLayoutManager

        view.rec_orders.adapter = offersAdapter

        view.progressBar.visibility = View.VISIBLE

        offersViewModel.getOffersDel(userInfo.getUserToken())

        offersViewModel.offersDeliveryMutableLiveData.observe(this, Observer { t ->

            view.progressBar.visibility = View.GONE

            val data = ArrayList<DeliveryOffers_Response.DataBean>()
            var num = 0

            t.data.forEach { i ->
                Log.e(TAG, "onCreateView: ${i.id}")
                Log.e(TAG, "onCreateView: ${i.offered}")
                if (i.offered .equals("0")) {
                    data.add(i)
                    num++
                }
            }
            Log.e(TAG, "onCreateView: ${t.data.size}")

            if (num > 0)
                view.tv_not_found.visibility = View.GONE
            else
                view.tv_not_found.visibility = View.VISIBLE

            view.tv_3orders.text = "${getString(R.string.founded)} $num ${getString(R.string.orders_1)}"

            offersAdapter.setData(data)

        })


        if (userInfo.getNotifiState() == "on")
            view.stop.text = getString(R.string.stop_notifi)
        else {
            view.stop.text = getString(R.string.play_notifi)
            notifyDialog()
        }
        view.stop.setOnClickListener {

            if (userInfo.getNotifiState() == "on") {
                notifyDialogOn()
            } else {

                offersViewModel.turnNotifications(userInfo.getUserToken())
                offersViewModel.turnNotificationsMutableLiveData.observe(this, Observer {

                    userInfo.setNotifiState(it.data.notification.toLowerCase(Locale.ROOT))

                    if (it.data.notification.toLowerCase(Locale.ROOT) == "on") {
                        view.stop.text = getString(R.string.stop_notifi)
                    } else {
                        view.stop.text = getString(R.string.play_notifi)
                    }
                })
            }

        }

        view.refresh_layout.setOnRefreshListener {
            offersViewModel.getOffersDel(userInfo.getUserToken())

            offersViewModel.offersDeliveryMutableLiveData.observe(this, Observer { t ->

//            view.progressBar.visibility=View.GONE

                val data = ArrayList<DeliveryOffers_Response.DataBean>()
                var num = 0

                t.data.forEach { i ->
                    if (i.offered == "0") {
                        data.add(i)
                        num++
                    }
                }

                view.tv_3orders.text = getString(R.string.founded) + " $num " + getString(R.string.orders_1)



                offersAdapter.setData(data)

            })

            view.rec_orders.adapter = offersAdapter


            view.refresh_layout.isRefreshing = false;
        }


        return view
    }

    private fun notifyDialog() {

        val dialog = Dialog(activity!!)

        dialog.apply {
            setContentView(R.layout.dialog_notification_off)
            window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            show()
        }

        dialog.btn_close.setOnClickListener {

            dialog.btn_close.visibility = View.GONE
            dialog.progressBar.visibility = View.VISIBLE

            offersViewModel.turnNotifications(userInfo.getUserToken())
            offersViewModel.turnNotificationsMutableLiveData.observe(this, Observer {

                userInfo.setNotifiState(it.data.notification.toLowerCase(Locale.ROOT))

                if (it.data.notification.toLowerCase(Locale.ROOT) == "on") {
                    view!!.stop.text = getString(R.string.stop_notifi)
                } else {
                    view!!.stop.text = getString(R.string.play_notifi)
                }
                dialog.dismiss()
            })

        }

    }


    private fun notifyDialogOn() {

        val dialog = Dialog(activity!!)

        dialog.apply {
            setContentView(R.layout.dialog_notification_on)
            window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            show()
        }

        dialog.btn_close_.setOnClickListener {

            dialog.btn_close_.visibility = View.GONE
            dialog.progressBar_.visibility = View.VISIBLE

            offersViewModel.turnNotifications(userInfo.getUserToken())
            offersViewModel.turnNotificationsMutableLiveData.observe(this, Observer {

                userInfo.setNotifiState(it.data.notification.toLowerCase(Locale.ROOT))

                if (it.data.notification.toLowerCase(Locale.ROOT) == "on") {
                    view!!.stop.text = getString(R.string.stop_notifi)
                } else {
                    view!!.stop.text = getString(R.string.play_notifi)
                }
                dialog.dismiss()
            })

        }

        dialog.btn_cancel.setOnClickListener { dialog.dismiss() }

    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }

}