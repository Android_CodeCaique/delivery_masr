package com.codecaique.deliverymasr.ui.client.restaurant

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.client.MainActivity
import kotlinx.android.synthetic.main.fragment_order_sented.view.*

/**
 * A simple [Fragment] subclass.
 */
class OrderSentFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_order_sented, container, false)

        //        view!!. btn_done = view.findViewById(R.id.btn_done)

        view!!.btn_done.setOnClickListener(View.OnClickListener {

            val intent = Intent(activity!!.applicationContext, MainActivity::class.java)
//            StoresAdapter.fragName = R.layout.fragment_delivery_offers
            intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            intent.putExtra(MainActivity.KEY,MainActivity.DeliveryOffers)
            activity!!.startActivity(intent)

        })
        return view
    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}