package com.codecaique.deliverymasr.ui.agent.subMain.ui.comments;

public class Commentmodel {
    String name, problemtxt,time;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProblemtxt() {
        return problemtxt;
    }

    public void setProblemtxt(String problemtxt) {
        this.problemtxt = problemtxt;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Commentmodel(String name, String problemtxt, String time) {
        this.name = name;
        this.problemtxt = problemtxt;
        this.time = time;
    }
}
