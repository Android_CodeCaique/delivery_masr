package com.codecaique.deliverymasr.ui.agent.auth.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.client.AuthActivity
import kotlinx.android.synthetic.main.fragment_selected.view.*

/**
 * A simple [Fragment] subclass.
 */

class SelectedFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_selected, container, false)
    }

    private lateinit var animation: Animation
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.btn_mandowp.visibility=View.INVISIBLE
        view.btn_hayyak.visibility=View.INVISIBLE

        animation = AnimationUtils.loadAnimation(context, R.anim.anim_trans)

        view.imageView.startAnimation(animation)
        view.tv.startAnimation(animation)

        Handler().postDelayed({

            startAnim(view)

        }, 800)

        view.btn_hayyak.setOnClickListener {

            val intent = Intent(context, AuthActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            activity!!.startActivity(intent)

        }

         view.btn_mandowp.setOnClickListener {
            view.findNavController().navigate(R.id.splashFragment2)
        }

    }

    private fun startAnim(view: View) {
        view.btn_hayyak.visibility=View.VISIBLE
        view.btn_mandowp.visibility=View.VISIBLE

        animation = AnimationUtils.loadAnimation(context, R.anim.anim_scale)
        animation.duration = 800

        view.btn_hayyak.startAnimation(animation)
        view.btn_mandowp.startAnimation(animation)

    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}