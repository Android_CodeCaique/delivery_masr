package com.codecaique.deliverymasr.ui.client.myOrders

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codecaique.deliverymasr.pojo.response.MyOrdersResponse
import com.codecaique.deliverymasr.data.ApiCreater
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class MyOrdersViewModel:ViewModel() {

    val myOrdersMutableLiveData=MutableLiveData<MyOrdersResponse>()

    fun myOrders(token:String,type: String){

        val observable =
                ApiCreater.instance.getMyOrders(token,type)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<MyOrdersResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: MyOrdersResponse) {

                if(t != null)
                    myOrdersMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }
}