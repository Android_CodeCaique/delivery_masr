package com.codecaique.deliverymasr.ui.agent.main.ui.userSettings

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.BuildConfig
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.AuthActivity
import com.codecaique.deliverymasr.base.agent.SubMainActivity
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.util.MyUtil
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.client.allSitting.SettingsViewModel
import com.codecaique.deliverymasr.ui.client.coupons.CouponsViewModel
import com.codecaique.deliverymasr.ui.client.signUp.SignUpViewModel
import com.codecaique.deliverymasr.utils.Utils
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.dialog_add_copoun.*
import kotlinx.android.synthetic.main.dialog_should_register.*
import kotlinx.android.synthetic.main.fragment_user_settings.view.*
import kotlinx.android.synthetic.main.fragment_user_settings.view.civ_user
import kotlinx.android.synthetic.main.fragment_user_settings.view.con2
import kotlinx.android.synthetic.main.fragment_user_settings.view.share
//import kotlinx.android.synthetic.main.fragment_user_settings.view.telegram
import kotlinx.android.synthetic.main.fragment_user_settings.view.tv_credit
import kotlinx.android.synthetic.main.fragment_user_settings.view.tv_notes
import kotlinx.android.synthetic.main.fragment_user_settings.view.tv_order_no
import kotlinx.android.synthetic.main.fragment_user_settings.view.tv_userName

class UserSettingsFragment : Fragment() {

    companion object {

        val isConv = "isconv"
    }

    //   lateinit var binding: FragmentUserSettingsBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(
                R.layout.fragment_user_settings, container, false)
//        val view = binding.getRoot()
        return view
    }

    lateinit var settingsViewModel: SettingsViewModel
    lateinit var signUpViewModel: SignUpViewModel
    lateinit var couponsViewModel: CouponsViewModel
    private val TAG="UserSettingsFragment"


    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val userInfo = UserInfo(activity!!)

        signUpViewModel = ViewModelProviders.of(this).get(SignUpViewModel::class.java)


        settingsViewModel = ViewModelProviders.of(this).get(SettingsViewModel::class.java)
        couponsViewModel= ViewModelProvider(this).get(CouponsViewModel::class.java)

        settingsViewModel.getSettings(UserInfo(activity!!.applicationContext).getUserToken())
        settingsViewModel.settingsMutableLiveData.observe(this, Observer { t ->


            view.tv_credit.text = if (t.credit != null) t.credit else "0 ج "
            view.tv_order_no.text = "${t.requests}" + " طلبات "
            view.tv_notes.text = "${t.rate} "

        })


        view.share.setOnClickListener { share() }
//        view.addCoupon.setOnClickListener { dialogAddCoupon(view) }


        if (userInfo.getImage().isNotEmpty())
            Picasso.get().load(ApiCreater.USER_URL + userInfo.getImage()).into(view.civ_user)
        view.tv_userName.text = userInfo.getName()


        view.con2.setOnClickListener {
            if (UserInfo(activity!!.applicationContext).isNotRegister()) {
                val dialog = Dialog(activity!!)
                dialog.setContentView(R.layout.dialog_should_register)
                dialog.setCancelable(false)
                dialog.tv_ok.setOnClickListener {
                    val intent = Intent(activity!!.applicationContext, AuthActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    activity!!.startActivity(intent)
                }
                dialog.show()
                UserInfo(activity!!.applicationContext).logout()
                return@setOnClickListener
            }

            it.findNavController().navigate(R.id.totalMoneyFragment)
//            val fragment: Fragment = TotalDeliveryMoney()
//            loadFragment(fragment)
        }

        view.con4.setOnClickListener {
            val intent = Intent(context, SubMainActivity::class.java)
            intent.putExtra(SubMainActivity.KEY, SubMainActivity.COMMENTS)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context!!.startActivity(intent)
//            val fragment: Fragment = Notices_users()
//            loadFragment(fragment)
        }

        view.con9.setOnClickListener {
            if (UserInfo(activity!!.applicationContext).isNotRegister()) {
                val dialog = Dialog(activity!!)
                dialog.setContentView(R.layout.dialog_should_register)
                dialog.setCancelable(false)
                dialog.tv_ok.setOnClickListener {
                    UserInfo(activity!!.applicationContext).logout()
                    val intent = Intent(activity!!.applicationContext, AuthActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    activity!!.startActivity(intent)
                }
                dialog.show()
                return@setOnClickListener
            }
            val intent = Intent(context, SubMainActivity::class.java)
            intent.putExtra(SubMainActivity.KEY, SubMainActivity.COUPONS)
            context!!.startActivity(intent)

        }
        view.con10.setOnClickListener {
            if (UserInfo(activity!!.applicationContext).isNotRegister()) {
                val dialog = Dialog(activity!!)
                dialog.setContentView(R.layout.dialog_should_register)
                dialog.setCancelable(false)
                dialog.tv_ok.setOnClickListener {
                    UserInfo(activity!!.applicationContext).logout()
                    val intent = Intent(activity!!.applicationContext, AuthActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    activity!!.startActivity(intent)
                }
                dialog.show()
                return@setOnClickListener
            }

            it.findNavController().navigate(R.id.settingsFragment)
//            val fragment: Fragment = SettingsFragment()
//            loadFragment(fragment)
        }
        view.con11.setOnClickListener {
            /*val intent=Intent(activity!!, AuthActivity::class.java)
             intent.putExtra(isConv,true)
             activity!!.startActivity(intent)*/
            val intent = Intent(context, SubMainActivity::class.java)
            intent.putExtra(SubMainActivity.KEY, SubMainActivity.WAY)
            context!!.startActivity(intent)


        }


        view.civ_.setOnClickListener {

            val alertDialog = AlertDialog.Builder(activity!!)
            alertDialog.setMessage(getString(R.string.sure_exit))
            alertDialog.setIcon(R.drawable.app_logo)

            alertDialog.setPositiveButton(getString(R.string.yes)) { p0, p1 ->

                signUpViewModel.logout(userInfo.getUserToken())


                UserInfo(activity!!).logout()
                val intent = Intent(context, AuthActivity::class.java)


                context!!.startActivity(intent)


            }

            alertDialog.setNegativeButton(getString(R.string.no)) { p0, p1 ->
                p0!!.dismiss()
            }

            alertDialog.create().show()


        }

        if (userInfo.getNotifiState() == "on")
            view.civ_ntifi.setImageResource(R.drawable.ic_baseline_notifications_none_24)
        else
            view.civ_ntifi.setImageResource(R.drawable.ic_notifications_off_black_24dp)

        view.civ_ntifi.setOnClickListener {

            it.findNavController().navigate(R.id.turnNotificationFragment)
            /*
                settingsViewModel.turnNotifications(userInfo.getUserToken())
                settingsViewModel.turnNotificationsMutableLiveData.observe(this, Observer {


                    userInfo.setNotifiState(it.data.notification.toLowerCase(Locale.ROOT))

                    if (it.data.notification.toLowerCase(Locale.ROOT) == "on") {
                        Toast.makeText(activity!!.applicationContext, getString(R.string.notifi_is_on), Toast.LENGTH_LONG).show()
                        view.civ_ntifi.setImageResource(R.drawable.ic_bell)
                    } else {
                        Toast.makeText(activity!!.applicationContext, getString(R.string.notifi_is_off), Toast.LENGTH_LONG).show()
                        view.civ_ntifi.setImageResource(R.drawable.ic_notifications_off_black_24dp)
                    }


                })

    */

        }
/*
        view.telegram.setOnClickListener {
            Log.e(TAG, "onCreateView: telegram.setOnClickListener")
            MyUtil.telegram(activity!!, ApiCreater.TELEGRAM_PAGE_ID)
        }*/


    }

    fun loadFragment(fragment: Fragment?) {
        /*  FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();*/
    }


    fun dialogAddCoupon(view: View?) {
        val dialog = Dialog(activity!!)
        dialog.setContentView(R.layout.dialog_add_copoun)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.btn_done.setOnClickListener {

            val msg=dialog.tv_camera.text.toString()

            if (msg.isNotEmpty()){
                dialog.progressBar.visibility=View.VISIBLE
                dialog.btn_done.isEnabled=false

                couponsViewModel.addMyCoupon(UserInfo(activity!!).getUserToken(),msg)
                couponsViewModel.addCouponMutableLiveData.observe(this, Observer { t->

                    if (t.error==0)
                    {
                        Toast.makeText(activity!!.applicationContext,activity!!.resources.getString(R.string.coupon_added), Toast.LENGTH_SHORT).show()
//                        couponsAdapter.addCoupon(Coupon(msg,"",0))

                    }
                    else{
                        Toast.makeText(activity!!.applicationContext,t.message, Toast.LENGTH_SHORT).show()
                    }

                })

                dialog.dismiss()

            }
            else{
                dialog.tv_camera.error=activity!!.resources.getString(R.string.enter_coupon)
            }

        }

        dialog.show()   }

    fun share(){
        try {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name))
            var shareMessage = "\nLet me recommend you this application\n\n"
            shareMessage = """
                ${shareMessage}https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}
                
                
                """.trimIndent()
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
            startActivity(Intent.createChooser(shareIntent, "choose one"))
        } catch (e: Exception) {
            //e.toString();
        }

    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}