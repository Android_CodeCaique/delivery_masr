package com.codecaique.deliverymasr.ui.agent.subMain.ui.problemslist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.codecaique.deliverymasr.R;
import com.codecaique.deliverymasr.databinding.FragmentCommentsBinding;

import java.util.ArrayList;

public class Problems extends Fragment {
    FragmentCommentsBinding binding;

    ProblemAdapter adapterproblem;
    ArrayList<ProblemModel> problemModels;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_comments, container, false);
        final View view = binding.getRoot();

        problemModels=new ArrayList<>();
        problemModels.add(new ProblemModel("Ce32478923748","اسواق اليمن","اذا وصلت الرساله كلمني","234367"));
        problemModels.add(new ProblemModel("Ce32478923748","اسواق اليمن","اذا وصلت الرساله كلمني","234367"));

        adapterproblem=new ProblemAdapter(getContext(),problemModels);
        binding.recComments.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recComments.setAdapter(adapterproblem);

        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view_) {

                OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
                    @Override
                    public void handleOnBackPressed() {
                        // Handle the back button event

                        Navigation.findNavController(view_).navigate(R.id.orderChatFragment);


                    }
                };
                requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);

            }
        });

        return  view;
    }
}
