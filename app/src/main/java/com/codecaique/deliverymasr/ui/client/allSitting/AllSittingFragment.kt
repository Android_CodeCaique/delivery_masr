package com.codecaique.deliverymasr.ui.client.allSitting

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.codecaique.deliverymasr.BuildConfig
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.AuthActivity
import com.codecaique.deliverymasr.base.client.SubActivity
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.client.coupons.CouponsViewModel
import com.codecaique.deliverymasr.ui.client.editProfile.EditProfileViewModel
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import com.codecaique.deliverymasr.ui.client.notification.NotificationsViewModel
import com.codecaique.deliverymasr.ui.client.notificationsSettings.TurnNotificationFragment
import com.codecaique.deliverymasr.pojo.util.MyUtil
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.dialog_add_copoun.*
import kotlinx.android.synthetic.main.dialog_should_register.*
import kotlinx.android.synthetic.main.fragment_all_sitting.view.*

class AllSittingFragment : Fragment() {

    lateinit var settingsViewModel: SettingsViewModel
    lateinit var editProfileViewModel: EditProfileViewModel
    lateinit var notificationsViewModel: NotificationsViewModel
    lateinit var loadingDialog: LoadingDialog
    lateinit var couponsViewModel: CouponsViewModel
    private val TAG="AllSittingFragment"

    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_all_sitting, container, false)
        loadingDialog = LoadingDialog(activity!!)

        notificationsViewModel = ViewModelProviders.of(this).get(NotificationsViewModel::class.java)
        editProfileViewModel = ViewModelProviders.of(this).get(EditProfileViewModel::class.java)
        couponsViewModel = ViewModelProvider(this).get(CouponsViewModel::class.java)

        settingsViewModel = ViewModelProviders.of(this).get(SettingsViewModel::class.java)


        settingsViewModel.getSettings(UserInfo(activity!!.applicationContext).getUserToken())
        settingsViewModel.settingsMutableLiveData.observe(this, Observer { t ->

            if (t.credit != null) {
                view.tv_credit.text = t.credit
            } else
                view.tv_credit.text = "0"

            view.tv_order_no.text = "${t.requests}  طلبات "
            view.tv_notes.text = "${t.rate} "

        })


        view.iv_logout.setOnClickListener {

            val alertDialog = AlertDialog.Builder(activity!!)
            alertDialog.setMessage(getString(R.string.sure_exit))
            alertDialog.setIcon(R.drawable.app_logo)

            alertDialog.setPositiveButton(getString(R.string.yes)) { p0, p1 ->

                loadingDialog.show()

                editProfileViewModel.logout(UserInfo(activity!!).getUserToken())

                UserInfo(activity!!).logout()
                val intent = Intent(context, com.codecaique.deliverymasr.base.agent.AuthActivity::class.java)
                context!!.startActivity(intent)

            }

            alertDialog.setNegativeButton(getString(R.string.no)) { p0, p1 ->
                p0!!.dismiss()
            }

            alertDialog.create().show()


        }

        view.conuserNotes.setOnClickListener {
            val intent = Intent(view.context, SubActivity::class.java)
//            StoresAdapter.fragName = R.layout.fragment_user_notes
            intent.putExtra(SubActivity.KEY, SubActivity.UserNotes)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }

        view.conCoupon.setOnClickListener {
            if (UserInfo(activity!!.applicationContext).isNotRegister()) {
                UserInfo(activity!!.applicationContext).logout()
                val dialog = Dialog(activity!!)
                dialog.setContentView(R.layout.dialog_should_register)
                dialog.setCancelable(false)
                dialog.tv_ok.setOnClickListener {
                    val intent = Intent(activity!!.applicationContext, AuthActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    activity!!.startActivity(intent)
                }
                dialog.show()
                return@setOnClickListener
            }
            val intent = Intent(view.context, SubActivity::class.java)
            //            StoresAdapter.fragName = R.layout.fragment_coupons
            intent.putExtra(SubActivity.KEY, SubActivity.Coupons)
            startActivity(intent)
        }

        view.consitting.setOnClickListener {

            if (UserInfo(activity!!.applicationContext).isNotRegister()) {
                UserInfo(activity!!.applicationContext).logout()
                val dialog = Dialog(activity!!)
                dialog.setContentView(R.layout.dialog_should_register)
                dialog.setCancelable(false)
                dialog.tv_ok.setOnClickListener {
                    val intent = Intent(activity!!.applicationContext, AuthActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    activity!!.startActivity(intent)
                }
                dialog.show()
                return@setOnClickListener
            }


            val sittingsFrag = SittingsFragment()
            val manager = fragmentManager
            manager!!.beginTransaction().replace(R.id.main_fragment, sittingsFrag, sittingsFrag.tag)
                    .addToBackStack(null)
                    .commit()
        }

        view.conRegisterAccount.setOnClickListener {
            val intent = Intent(view.context, SubActivity::class.java)
//            StoresAdapter.fragName = R.layout.fragment_pay_way
            intent.putExtra(SubActivity.KEY, SubActivity.PayWay)
            startActivity(intent)
        }

        val userInfo = UserInfo(activity!!.applicationContext)
        view.notifi.setOnClickListener {

            val sittingsFrag = TurnNotificationFragment()
            val manager = fragmentManager
            manager!!.beginTransaction().replace(R.id.main_fragment, sittingsFrag, sittingsFrag.tag)
                    .addToBackStack(R.id.turnNotificationFragment.toString())
                    .commit()
//            view.findNavController().navigate(R.id.turnNotificationFragment)

            /*
            notificationsViewModel.turnNotifica-tions(userInfo.getUserToken())
            notificationsViewModel.turnNotificationsMutableLiveData.observe(t-  his, Observer {

                userInfo.setNotifiState(it.data.notification.toLowerCase(Locale.ROOT))


                if (it.data.notification.toLowerCase(Locale.ROOT)=="on")
                    view.turn_notifi.visibility = View.VISIBLE
                else
                    view.turn_notifi.visibility = View.INVISIBLE

            })
*/

        }

        if (userInfo.getNotifiState() == "on")
            view.turn_notifi.visibility = View.INVISIBLE
        else
            view.turn_notifi.visibility = View.VISIBLE

        if (userInfo.getImage().isNotEmpty())
            Picasso.get().load(ApiCreater.USER_URL + userInfo.getImage()).into(view.civ_user)
        view.tv_userName.text = userInfo.getName()

        view.share.setOnClickListener {
            share()
        }
        view.addCoupon.setOnClickListener {
            dialogAddCoupon(view)
        }
/*
        view.telegram.setOnClickListener {
            Log.e(TAG, "onCreateView: telegram.setOnClickListener")
            MyUtil.telegram(activity!!, ApiCreater.TELEGRAM_PAGE_ID)
        }*/

        return view
    }


    fun dialogAddCoupon(view: View?) {
        val dialog = Dialog(activity!!)
        dialog.setContentView(R.layout.dialog_add_copoun)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog!!.btn_done.setOnClickListener {

            val msg = dialog!!.tv_camera.text.toString()

            if (msg.isNotEmpty()) {
                dialog!!.progressBar.visibility = View.VISIBLE
                dialog!!.btn_done.isEnabled = false

                couponsViewModel.addMyCoupon(UserInfo(activity!!).getUserToken(), msg)
                couponsViewModel.addCouponMutableLiveData.observe(this, Observer { t ->

                    if (t.error == 0) {
                        Toast.makeText(activity!!.applicationContext, activity!!.resources.getString(R.string.coupon_added), Toast.LENGTH_SHORT).show()
//                        couponsAdapter.addCoupon(Coupon(msg,"",0))

                        dialog!!.progressBar.visibility = View.INVISIBLE

                    } else {
                        Toast.makeText(activity!!.applicationContext, t.message, Toast.LENGTH_SHORT).show()
                    }

                })

                dialog!!.dismiss()

            } else {
                dialog!!.tv_camera.error = activity!!.resources.getString(R.string.enter_coupon)
            }

        }

        dialog!!.show()
    }

    fun share() {
        try {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name))
            var shareMessage = "\nLet me recommend you this application\n\n"
            shareMessage = """
                ${shareMessage}https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}
                
                
                """.trimIndent()
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
            startActivity(Intent.createChooser(shareIntent, "choose one"))
        } catch (e: Exception) {
            //e.toString();
        }

    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}