package com.codecaique.deliverymasr.ui.agent.auth.ui.bankaccounts;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codecaique.deliverymasr.R;

import java.util.ArrayList;

public class Bank_accounts extends Fragment {
    Account_Adapter adapter;
    ArrayList<Accountmodel> accountmodels;
    RecyclerView recyclerView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_bank_accounts,container,false);


        recyclerView = view.findViewById(R.id.rec_acc_bank);
        accountmodels=new ArrayList<>();
        accountmodels.add(new Accountmodel(" Bank Masr ","ahmed","37284"));
        accountmodels.add(new Accountmodel(" Bank Naser ","ahmed","37284"));
        accountmodels.add(new Accountmodel(" Bank ssss ","ahmed","37284"));


        adapter=new Account_Adapter(getContext(),accountmodels);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        return  view;
    }
}
