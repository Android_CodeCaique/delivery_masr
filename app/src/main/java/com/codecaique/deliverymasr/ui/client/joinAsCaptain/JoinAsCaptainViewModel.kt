package com.codecaique.deliverymasr.ui.client.joinAsCaptain

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.StoresMainActivity
import com.codecaique.deliverymasr.pojo.response.*
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.agent.auth.ui.otp.OtpFragment
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_mobile_verification.view.*
import kotlinx.android.synthetic.main.fragment_sign_in.view.*
import kotlinx.android.synthetic.main.fragment_splash2.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.lang.Exception

class JoinAsCaptainViewModel : ViewModel() {


    var TAG = "JoinAsCaptainViewModel"

    val enterUserDataMutableLiveData = MutableLiveData<Driver_Response>()
    val banksMutableLiveData = MutableLiveData<ShowBanksResponse>()
    val notNowMutableLiveData = MutableLiveData<NotNowResponse>()
    val checkEmailMutableLiveData = MutableLiveData<CheckEmailResponse>()


    fun checkEmail( email: String)
    {
        val observable =
                ApiCreater.instance.checkEmail(email)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<CheckEmailResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: CheckEmailResponse) {

                checkEmailMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }

    fun notNow()
    {
        val observable =
                ApiCreater.instance.notNow(2)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<NotNowResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: NotNowResponse) {

                notNowMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)
    }



    fun enterUserData(
            user_id: Int?,
            first_name: String,
            last_name: String,
            nationality: RequestBody,
            card_number: RequestBody,
            gender_id: Int,
            image: MultipartBody.Part?,
            passwords: RequestBody,
            driving_license_image: MultipartBody.Part?,
            front_vehical_image: MultipartBody.Part?,
            car_license_image: MultipartBody.Part?,
            phone: RequestBody,
            email: RequestBody,
            firebase_token: RequestBody,
            longitude: Double,
            latitude: Double,
            bank_account: Int,
            bank: Int,
            apple_account: Int,
            account_number: RequestBody?,
            loadingDialog: LoadingDialog,
            context: Context
    ) {

        val name = first_name.split(" ")
        var lName = ""
        val fName = name[0]
        if (name.size > 1)
            for (i in name.indices) {
                if (i != 0 || i != null)
                    lName += name[i]

                try{
                    lName=name[1]+"_"+name[2]
                }catch (e:Exception){
                    if(name[1] != null)
                    lName = name[1];
                }
        }


        val observable:Observable<Driver_Response> =
                ApiCreater.instance.joinAsCaptain(user_id,
                                RequestBody.create(MediaType.parse("multipart/form-data"), fName),
                                RequestBody.create(MediaType.parse("multipart/form-data"), lName)
                                , nationality,card_number,gender_id, image, passwords, driving_license_image, front_vehical_image, car_license_image, phone, email, firebase_token, longitude, latitude, bank_account, bank, apple_account, account_number)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        if(observable == null ){
            Log.e(TAG, "enterUserData: observable ====== nulll",null )
            Log.e(TAG, "enterUserData: observable ====== nulll--->",null )
        }
        else{
            Log.e(TAG, "enterUserData: nooooooooot nulll",null )
        }
        val observer = object : Observer<Driver_Response> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: Driver_Response) {

                enterUserDataMutableLiveData.value = t

                Log.e("ss mandop viewmodel", t.message + t.error.toString())


                loadingDialog.dismiss()

            }

            override fun onError(e: Throwable) {
                Log.e("ss adsada", e.message)
                loadingDialog.dismiss()
                Toast.makeText(context,"There is an error in connection please try again"  ,LENGTH_SHORT).show()
            }
        }

        observable.subscribe(observer)

    }


    fun showBanks(){

        val observable =
                ApiCreater.instance.showBanks()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<ShowBanksResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: ShowBanksResponse) {

                banksMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)


    }


    fun phoneSignUpDriver(
            country: RequestBody,
            phone: RequestBody,
            firebase_token: RequestBody,
            progressBar: ProgressBar,
            context:Context,
            view: View
    ) {


        progressBar.visibility=View.VISIBLE
        view.btn_not_now.visibility=View.GONE

        val observable =
                ApiCreater.instance.phoneSignUp(country, phone,firebase_token)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<PhoneResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: PhoneResponse) {
                Log.e("ss ssss", t.message)

                /*progressBar.visibility = GONE
                signUpMutableLiveData.value = t*/

                if (t.error == 0) {
                    Log.e("tag", t.data.id.toString() + " " + t.data.phone + " " + t.data.state + " " + t.data.user_token)
                    UserInfo(context.applicationContext).setUserData(t.data.id, t.data.phone, "mandop", "")

//                        smsManager.sendTextMessage(phone, null, code, null, null);
                    val b = Bundle()
                    b.putString("phone", t.data.phone)
                    b.putString("code","+2")

                    view.findNavController().navigate(R.id.addMandopFragment, b)

                    Log.e("MOB USER", "NEW PHONE")


                } else {
                    if (t.data.state.contains("driver")) {
//                        view.tv_1_.text = getString(R.string.code_sent_1) + phone + getString(R.string.pls_enter_sent_code)

//                        val b = Bundle()
//                        b.putSerializable(OtpFragment.DATA,t.data)
//                        view.findNavController().navigate(R.id.otpFragment, b)
                        UserInfo(context).setUserData(t.data as Phone)
                        val intent = Intent(context, StoresMainActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                        context.startActivity(intent)

//                        isSigned = true
//                        phone = view.tv_phone.text.toString() + phone
//                        data = t.data


                    } else {
                        changeToDriverOrNo(context,view,t.data.phone)
                        Toast.makeText(context.applicationContext,context. getString(R.string.not_driver_account), Toast.LENGTH_LONG).show()

                    }

//                        view.constraint1.visibility = View.VISIBLE
//                        view.constraint2.visibility = View.INVISIBLE

//                        navController.navigate(R.id.action_fragment_Mobile_Verification2_to_fragment_SignUp)
//                    } else {
                    /* if (t.data.state.isNotEmpty()) {
                         phone = view.tv_phone.text.toString() + phone
                         data=t.data
                     }*/
                    /* else {
//                            Toast.makeText(activity!!.applicationContext, "هذا الحساب ليس لمندوب", Toast.LENGTH_LONG).show()
                    }*/

                    /*  phone = view.tv_phone.text.toString() + phone
                      data = t.data
  */
                }
                progressBar.visibility = View.GONE
//                    view.constraint1.visibility = View.VISIBLE
//                    view.constraint2.visibility = View.INVISIBLE
                UserInfo(context).setState("mandop")


            }

            override fun onError(e: Throwable) {
                Log.e("ss 00", e.message)
                progressBar.visibility = View.GONE
                view.btn_not_now.visibility=View.VISIBLE
            }
        }

        observable.subscribe(observer)

    }

    private fun changeToDriverOrNo(context: Context,view: View,phone: String) {
        val alertDialog = AlertDialog.Builder(context,R.style.AlertDialogTheme)
        alertDialog.setMessage(context.getString(R.string.not_driver_account))
        alertDialog.setIcon(R.drawable.app_logo)

        alertDialog.setPositiveButton(context.getString(R.string.yes)) { p0, p1 ->

            val b = Bundle()
            b.putString("phone", phone)
            b.putString("code", "+2")
            view.findNavController().navigate(R.id.addMandopFragment, b)


        }

        alertDialog.setNegativeButton(context.getString(R.string.no)) { p0, p1 ->
            p0!!.dismiss()
        }

        alertDialog.create().show()


    }


}