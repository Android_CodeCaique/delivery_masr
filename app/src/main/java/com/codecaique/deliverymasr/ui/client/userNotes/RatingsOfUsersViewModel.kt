package com.codecaique.deliverymasr.ui.client.userNotes

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codecaique.deliverymasr.pojo.response.*
import com.codecaique.deliverymasr.data.ApiCreater
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class RatingsOfUsersViewModel : ViewModel() {

    val notesOfUserMutableLiveData = MutableLiveData<UserRateResponse>()

    fun showRatingOfUser(token: String,user_id:String) {

        val observable =
                ApiCreater.instance.showRatingByUserId(token,user_id.toInt())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<UserRateResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: UserRateResponse) {

                notesOfUserMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }

}