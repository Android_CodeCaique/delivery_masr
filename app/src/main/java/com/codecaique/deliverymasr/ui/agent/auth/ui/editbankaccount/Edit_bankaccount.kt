package com.codecaique.deliverymasr.ui.agent.auth.ui.editbankaccount

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.StoresMainActivity
import kotlinx.android.synthetic.main.fragment_edit_bankaccount.view.*

class Edit_bankaccount : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_edit_bankaccount, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.btn_save.setOnClickListener {
            val intent = Intent(context, StoresMainActivity::class.java)
            context!!.startActivity(intent)

//            it.findNavController().navigate(R.id.bankAccountFragment)
        }

    }

    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}