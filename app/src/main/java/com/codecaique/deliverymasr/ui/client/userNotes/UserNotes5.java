package com.codecaique.deliverymasr.ui.client.userNotes;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.codecaique.deliverymasr.R;
import com.codecaique.deliverymasr.ui.client.signUp.ui.CodeVerification2;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserNotes5 extends Fragment {

    public static String STC_Pay;

    public UserNotes5() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_notes5, container, false);

        ImageView back = view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserNotes4 userNotes4 = new UserNotes4();
                FragmentManager manager = getFragmentManager();
                manager.beginTransaction().replace(R.id.main3_fragment, userNotes4, userNotes4.getTag()).commit();
            }
        });

        Button mobile_verification_btn = view.findViewById(R.id.mobile_verification_btn);
        mobile_verification_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CodeVerification2 codeVerification2 = new CodeVerification2();
                STC_Pay = "done";
                FragmentManager manager = getFragmentManager();
                manager.beginTransaction().replace(R.id.main3_fragment, codeVerification2, codeVerification2.getTag()).commit();
            }
        });

        return view;
    }
}
