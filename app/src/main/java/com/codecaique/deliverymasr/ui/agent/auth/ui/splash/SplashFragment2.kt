package com.codecaique.deliverymasr.ui.agent.auth.ui.splash

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.StoresMainActivity
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.client.joinAsCaptain.JoinAsCaptainViewModel
import com.codecaique.deliverymasr.utils.Utils
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.fragment_splash2.view.*
import kotlinx.android.synthetic.main.fragment_splash2.view.btn_not_now
import kotlinx.android.synthetic.main.fragment_splash2.view.btn_sign_in
import kotlinx.android.synthetic.main.fragment_splash2.view.facebook_sign_in
import kotlinx.android.synthetic.main.fragment_splash2.view.google_sign_in
import kotlinx.android.synthetic.main.fragment_splash2.view.imageView
import kotlinx.android.synthetic.main.fragment_splash2.view.tv
import kotlinx.android.synthetic.main.fragment_splash2.view.tv_costraint
import kotlinx.android.synthetic.main.fragment_splash2.view.tv_or
import kotlinx.android.synthetic.main.fragment_splash2.view.view
import okhttp3.MediaType
import okhttp3.RequestBody


/**
 * A simple [Fragment] subclass.
 */
class SplashFragment2 : Fragment(), TextWatcher {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_splash2, container, false)
    }

    private lateinit var animation: Animation
    lateinit var mGoogleSignInClient: GoogleSignInClient
    val RC_SIGN_IN = 12345
    lateinit var captainViewModel: JoinAsCaptainViewModel
    lateinit var callbackManager: CallbackManager

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()
        mGoogleSignInClient = GoogleSignIn.getClient(activity!!, gso)

        captainViewModel = ViewModelProviders.of(this).get(JoinAsCaptainViewModel::class.java)
        callbackManager = CallbackManager.Factory.create()


        view.google_sign_in.setOnClickListener {
            signIn()

        }


//        view.imageView.setImageResource(R.drawable.ic_mandob)
        view.btn_sign_in.visibility = View.INVISIBLE
//        view.btn_not_now.visibility = View.INVISIBLE
        view.tv_or.visibility = View.INVISIBLE
        view.view.visibility = View.INVISIBLE
        view.facebook_sign_in.visibility = View.INVISIBLE
        view.google_sign_in.visibility = View.INVISIBLE

        animation = AnimationUtils.loadAnimation(context, R.anim.anim_trans)

        view.imageView.startAnimation(animation)
        view.tv.startAnimation(animation)

        Handler().postDelayed({

            startAnim(view)

        }, 150)


//        view.btn_sign_in.setOnClickListener { view.findNavController().navigate(R.id.signInFragment) }
        view.btn_not_now.setOnClickListener {

            signInByPhone()

        }

        view.btn_sign_in.addTextChangedListener(this)

        view.facebook_sign_in.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                facebookSignIn(loginResult)
            }

            override fun onCancel() {
                Log.e("FBLOGIN_FAILD", "Cancel")
            }

            override fun onError(error: FacebookException) {
                Log.e("FBLOGIN_FAILD", "ERROR", error)
            }
        })

        view.tv_costraint.setOnClickListener {
            view.findNavController().navigate(R.id.constraintFragment)
        }

    }

    private fun signInByPhone() {

        captainViewModel.phoneSignUpDriver(
                RequestBody.create(MediaType.parse("multipart/form-data"), "1"),
                RequestBody.create(MediaType.parse("multipart/form-data"),
                        "${view!!.btn_sign_in.text}"),
                RequestBody.create(MediaType.parse("multipart/form-data"),
                        FirebaseInstanceId.getInstance().getToken()),
                view!!.progress_bar,activity!!,view!!
        )

    }

    private fun startAnim(view: View) {
        view.btn_sign_in.visibility = View.VISIBLE
//        view.btn_not_now.visibility = View.VISIBLE
        view.tv_or.visibility = View.VISIBLE
        view.view.visibility = View.VISIBLE
        view.facebook_sign_in.visibility = View.VISIBLE
        view.google_sign_in.visibility = View.VISIBLE
//        view.line.visibility=View.VISIBLE

        animation = AnimationUtils.loadAnimation(context, R.anim.anim_scale)
        animation.duration = 800

        view.btn_sign_in.startAnimation(animation)
        view.btn_not_now.startAnimation(animation)
        view.tv_or.startAnimation(animation)
        view.view.startAnimation(animation)
        view.facebook_sign_in.startAnimation(animation)
        view.google_sign_in.startAnimation(animation)

    }

    private fun signIn() {
        val signInIntent: Intent = mGoogleSignInClient.getSignInIntent()
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }


    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)


            checkEmail(account!!)

            Log.e("TAG", account.email!!)
            Log.e("TAG", account.photoUrl.toString())
            Log.e("TAG", account.givenName.toString())
            Log.e("TAG", account.displayName.toString())
            Log.e("TAG", account.familyName.toString())


        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("TAG", "signInResult:failed code=" + e.statusCode)
        }
    }

    private fun checkEmail(account: GoogleSignInAccount) {

        captainViewModel.checkEmail(account.email!!)
        captainViewModel.checkEmailMutableLiveData.observe(this, Observer { t ->

            if (t.error == 3) {
                Utils.signout(requireActivity() , view!!)
                return@Observer;
            }
            else if (t.error == 1) {

                val bundle = Bundle()
                bundle.putString("email", account.email)
                bundle.putString("name", account.displayName)
                bundle.putString("image", account.photoUrl.toString())
                view!!.findNavController().navigate(R.id.addMandopFragment, bundle)

            } else if (t.error == 0) {

                if (t.data.state.contains("driver")) {
                    UserInfo(activity!!.applicationContext).setUserData(t.data)
                    val intent =Intent(activity!!,StoresMainActivity::class.java)
                    intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                    activity!!.startActivity(intent)

                } else {
//                    Toast.makeText(activity!!.applicationContext, getString(R.string.not_driver_account), Toast.LENGTH_LONG).show()
                    changeToDriverOrNo(account)
                }

            }

        })

    }

    private fun changeToDriverOrNo(account: GoogleSignInAccount) {
        val alertDialog = AlertDialog.Builder(activity!!)
        alertDialog.setMessage(getString(R.string.not_driver_account))
        alertDialog.setIcon(R.drawable.app_logo)

        alertDialog.setPositiveButton(getString(R.string.yes)) { p0, p1 ->

            val bundle = Bundle()
            bundle.putString("email", account.email)
            bundle.putString("name", account.displayName)
            bundle.putString("image", account.photoUrl.toString())
            view!!.findNavController().navigate(R.id.addMandopFragment, bundle)


        }

        alertDialog.setNegativeButton(getString(R.string.no)) { p0, p1 ->
            p0!!.dismiss()
        }

        alertDialog.create().show()


    }

    private fun checkEmail(name: String, image: String, email: String) {

        captainViewModel.checkEmail(email)
        captainViewModel.checkEmailMutableLiveData.observe(this, Observer { t ->

            if (t.error == 1) {

                val bundle = Bundle()
                bundle.putString("email", email)
                bundle.putString("name", name)
                bundle.putString("image", image)

                view!!.findNavController().navigate(R.id.addMandopFragment, bundle)

            } else if (t.error == 0) {

                UserInfo(activity!!.applicationContext).setUserData(t.data)
                activity!!.startActivity(Intent(activity!!.applicationContext, StoresMainActivity::class.java))

            }

        })

    }

    private fun facebookSignIn(loginResult: LoginResult) {
        Log.d("FBLOGIN", loginResult.accessToken.token.toString())
        Log.d("FBLOGIN", loginResult.recentlyDeniedPermissions.toString())
        Log.d("FBLOGIN", loginResult.recentlyGrantedPermissions.toString())


        val request = GraphRequest.newMeRequest(loginResult.accessToken) { `object`, response ->
            try {
                //here is the data that you want
                Log.d("FBLOGIN_JSON_RES", `object`.toString())

                if (`object`.has("id")) {
                    Log.d("good", `object`.toString())

                    val name = `object`.getString("name")
                    val email = `object`.getString("email")
                    val id = `object`.getString("id")
                    val image = "http://graph.facebook.com/$id/picture?type=large"

                    checkEmail(name, image, email)

                } else {
                    Log.e("FBLOGIN_FAILD", `object`.toString())
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        val parameters = Bundle()
        parameters.putString("fields", "name,email,id,picture.type(large)")
        request.parameters = parameters
        request.executeAsync()

    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun afterTextChanged(p0: Editable?) {

        if (view!!.btn_sign_in.text.toString().length==11)
            view!!.btn_not_now.visibility=View.VISIBLE
        else
            view!!.btn_not_now.visibility=View.GONE

    }

    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}