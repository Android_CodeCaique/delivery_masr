package com.codecaique.deliverymasr.ui.agent.main.ui.myOrders

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.pojo.response.MyOrder
import com.codecaique.deliverymasr.ui.client.myOrders.MyOrdersViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.fragment_myorders.*
import kotlinx.android.synthetic.main.fragment_myorders.view.*


class Orders : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_myorders, container, false)
    }

    lateinit var myOrdersViewModel: MyOrdersViewModel
    lateinit var ordersAdapter: OrdersAdapter
     var data: ArrayList<MyOrder>?=null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        myOrdersViewModel = ViewModelProviders.of(this).get(MyOrdersViewModel::class.java)

        ordersAdapter = OrdersAdapter(activity!!)
        view.rec_myorders.adapter = ordersAdapter

        circularSticksLoader.visibility = View.VISIBLE
        myOrdersViewModel.myOrders(UserInfo(activity!!.applicationContext).getUserToken(), "driver")
        myOrdersViewModel.myOrdersMutableLiveData.observe(this, Observer { t ->

            view.progressBar.visibility = View.GONE
            circularSticksLoader.visibility = View.GONE
            data = t.data as ArrayList<MyOrder>
            fill('a')

            if (t.error == 3) {

            }


        })


        view.back.setOnClickListener {
            activity!!.findViewById<BottomNavigationView>(R.id.bottom_nav).selectedItemId = R.id.offers_del
            it.findNavController().navigate(R.id.offersDeliveredFragment)
        }

        view.tv_1.setOnClickListener {
            view.view1.visibility = View.VISIBLE
            view.view2.visibility = View.INVISIBLE
            fill('a')
        }

        view.tv_2.setOnClickListener {
            view.view2.visibility = View.VISIBLE
            view.view1.visibility = View.INVISIBLE
            fill('f')
        }


        view.refresh_layout.setOnRefreshListener {

            myOrdersViewModel.myOrders(UserInfo(activity!!.applicationContext).getUserToken(), "driver")
            myOrdersViewModel.myOrdersMutableLiveData.observe(this, Observer { t ->

                view.progressBar.visibility = View.GONE
                data = t.data as ArrayList<MyOrder>
                view.view1.visibility = View.VISIBLE
                view.view2.visibility = View.INVISIBLE
                fill('a')

            })

            view.rec_myorders.adapter = ordersAdapter
            view.refresh_layout.isRefreshing = false
        }


    }

    private fun fill(char: Char) {

        val array = ArrayList<MyOrder>()
        if (char == 'a') {
            data!!.forEach { i ->
                Log.e("TAG" +
                        "   000 ", i.state + "    ")
                if (i.state.trim() != "canceled" && i.state.trim() != "received_order" && i.rate_state == null) {
                    array.add(i)
                }
            }
        } else {
            data!!.forEach { i ->
                if (i.state.trim() == "canceled" || i.state.trim() == "received_order" || i.rate_state != null) {
                    array.add(i)
                }
            }
        }
        ordersAdapter.setData(array)
    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }

}