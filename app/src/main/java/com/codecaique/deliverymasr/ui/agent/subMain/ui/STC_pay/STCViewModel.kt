package com.codecaique.deliverymasr.ui.agent.subMain.ui.STC_pay

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codecaique.deliverymasr.pojo.response.GeneralResponse
import com.codecaique.deliverymasr.data.ApiCreater
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.RequestBody

class STCViewModel : ViewModel() {

    val editMutableLiveData = MutableLiveData<GeneralResponse>()

    fun editAccount(
            user_token: RequestBody,
            account_id: Int,
            name: RequestBody,
            address: RequestBody,
            account_number: RequestBody?,
            country: RequestBody?,
            phone: RequestBody?
    ) {
        val observable =
                ApiCreater.instance.editAccount(user_token, account_id, name, address, account_number, country, phone)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())

        val observer = object : Observer<GeneralResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: GeneralResponse) {

                editMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }

}