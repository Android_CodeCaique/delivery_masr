package com.codecaique.deliverymasr.ui.agent.subMain.ui.chat

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.response.Message
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.client.chat.PhotoActivity
import com.rygelouv.audiosensei.player.AudioSenseiPlayerView
import com.rygelouv.audiosensei.player.OnPlayerViewClickListener
import com.squareup.picasso.Picasso
import java.io.IOException


public class ChatAdapter(var context: Context) :
        BaseAdapter() {

    val TAG="ChatAdapter"
    var list = ArrayList<Message>()
    var userId = UserInfo(context).getId()
    var mediaMax = 0
    var mediaPos = 0
    var mp :MediaPlayer?=null



    @SuppressLint("InflateParams")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val arrayItem = list[position]

        var view= if (arrayItem.sender_id.toInt() == userId)
                    LayoutInflater.from(context)
                            .inflate(R.layout.item_chat, parent,false)
                else
                    LayoutInflater.from(context)
                            .inflate(R.layout.item_chat_back, parent,false)

        //  ui
        val msg = view.findViewById(R.id.tv_msg) as TextView
        val img = view.findViewById(R.id.img) as ImageView

        var player = view.findViewById(R.id.audio_player) as AudioSenseiPlayerView

        mp= MediaPlayer()
        Log.e(TAG, "getView: order_state ${arrayItem.order_state}")
        //  set data
        if (arrayItem.image.isNotEmpty() && arrayItem.image !="not null") {
            Log.e(TAG, arrayItem.image)
            Picasso.get().load(arrayItem.image).into(img)
            img.visibility = View.VISIBLE
            msg.visibility = View.GONE
            player.visibility=View.GONE
        } else if (arrayItem.message.isNotEmpty()) {
            Log.e(TAG, "getView: message isNotEmpty")
            img.visibility = View.GONE
            msg.text = arrayItem.message
            player.visibility=View.GONE

        } else if(arrayItem.voice.isNotEmpty()){

            img.visibility = View.GONE
            msg.visibility=View.GONE
            player.visibility=View.VISIBLE

            try {

                player.setAudioTarget(arrayItem.voice)
            } catch (e: IOException) {
            }

        }


        img.setOnClickListener {

            if (arrayItem.image.isNotEmpty()) {

                val intent = Intent(context, PhotoActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra(PhotoActivity.PHOTO_URL, arrayItem.image)
                context.startActivity(intent)

                return@setOnClickListener

            }
        }



        msg.setOnClickListener {
            try {

                val geoUri = arrayItem.message
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(geoUri))
                context.startActivity(intent)

            } catch (e: Exception) {
            }
        }


        return view
    }

    override fun getItem(p0: Int): Any {
        return list[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return list.size
    }

    fun setData(a: ArrayList<Message>) {
        this.list = ArrayList()
        this.list = a
        notifyDataSetChanged()
    }

    fun submitData(a: ArrayList<Message>) {
        this.list = ArrayList()
        this.list = a
        notifyDataSetChanged()
    }

    fun addMsg(a: Message) {
        this.list.add(a)
        notifyDataSetChanged()
    }

}
