package com.codecaique.deliverymasr.ui.client.restaurant.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.response.FoodType

class FoodTypesAdapter(var context: Context,var tvPrice :TextView,var tvQuantity :TextView) : RecyclerView.Adapter<FoodTypesAdapter.ViewHolderOffers>() {

    var list=ArrayList<FoodType>()

    var selectedItem:ImageView? = null
    var selectedTypeId=0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderOffers {
        val view = LayoutInflater.from(context).inflate(R.layout.item_types, parent, false)
        return ViewHolderOffers(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolderOffers, position: Int) {

        val arrayItem=list[position]

        holder.tvPrice.text =arrayItem.price+ "ريال"

        if (position==0 && selectedItem==null){
            selectedItem=holder.check
            select(holder.check)
            tvPrice.text=arrayItem.price
            selectedTypeId=arrayItem.id
        }

        holder.itemView.setOnClickListener {

            Log.e("aarrrr","SS")
            deSelect(selectedItem!!)
            selectedItem=holder.check
            select(holder.check)

//            if (b)

            tvPrice.text=arrayItem.price
            selectedTypeId=arrayItem.id
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

     class ViewHolderOffers(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvPrice=itemView.findViewById(R.id.price_ordinary) as TextView
         var radio=itemView.findViewById(R.id.radio) as TextView
         var check=itemView.findViewById(R.id.image) as ImageView
    }

    fun setData(a:ArrayList<FoodType>){
        this.list=a
        notifyDataSetChanged()
    }

    fun select(v:ImageView){
        v.setImageResource(R.drawable.ic_radio_button_checked_black_24dp)
    }

    fun deSelect(v:ImageView){
        v.setImageResource(R.drawable.ic_radio_button_unchecked_black_24dp)
    }

}