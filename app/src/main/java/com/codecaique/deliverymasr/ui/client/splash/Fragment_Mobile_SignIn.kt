package com.codecaique.deliverymasr.ui.client.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.LanguageHelper
import com.codecaique.deliverymasr.base.SharedHelper
import com.codecaique.deliverymasr.base.client.MainActivity
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.client.signUp.SignUpViewModel
import com.facebook.*
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.fragment_mobile_signin.view.*
import kotlinx.android.synthetic.main.fragment_mobile_signin.view.view
import okhttp3.MediaType
import okhttp3.RequestBody


/**
 * A simple [Fragment] subclass.
 */
class Fragment_Mobile_SignIn : Fragment(), TextWatcher {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_mobile_signin, container, false)
    }


    lateinit var languageHelper: LanguageHelper
    lateinit var sharedHelper: SharedHelper

    private lateinit var animation: Animation
    lateinit var mGoogleSignInClient: GoogleSignInClient
    val RC_SIGN_IN = 12345
    lateinit var callbackManager: CallbackManager
    lateinit var signUpViewModel: SignUpViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val navController = Navigation.findNavController(view)

        languageHelper = LanguageHelper()
        sharedHelper = SharedHelper()

        if(sharedHelper.getKey(context!!,"LANG").equals("en"))
        {
            languageHelper.ChangeLang(this.resources,"en")
        }else if(sharedHelper.getKey(context!!,"LANG").equals("ar"))
        {
            languageHelper.ChangeLang(this.resources,"ar")
        }

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.

        signUpViewModel = ViewModelProviders.of(this).get(SignUpViewModel::class.java)
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(activity!!, gso)
        callbackManager = CallbackManager.Factory.create()
        val account = GoogleSignIn.getLastSignedInAccount(activity!!)

        view.google_sign_in.setOnClickListener {

            signIn()
        }

        view.facebook_sign_in.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                facebookSignIn(loginResult)
            }

            override fun onCancel() {
                Log.e("FBLOGIN_FAILD", "Cancel")
            }

            override fun onError(error: FacebookException) {
                Log.e("FBLOGIN_FAILD", "ERROR", error)
            }
        })


        /*
    LoginManager.getInstance().retrieveLoginStatus(activity!!, object : LoginStatusCallback {
        override fun onFailure() {

        }

        override fun onError(exception: java.lang.Exception?) {
        }

        override fun onCompleted(accessToken: AccessToken?) {
            TODO("Not yet implemented")
        }
    })*/



        view.btn_sign_in.visibility = View.INVISIBLE
//        view.btn_not_now.visibility = View.INVISIBLE
        view.tv_or.visibility = View.INVISIBLE
        view.view.visibility = View.INVISIBLE
        view.facebook_sign_in.visibility = View.INVISIBLE
        view.google_sign_in.visibility = View.INVISIBLE

        animation = AnimationUtils.loadAnimation(context, R.anim.anim_trans)

        view.imageView.startAnimation(animation)
        view.tv.startAnimation(animation)

        Handler().postDelayed({

            startAnim(view)

        }, 100)


        view.btn_not_now.setOnClickListener {

            singIn()

        }

        view.btn_sign_in.addTextChangedListener(this)

        view.tv_costraint.setOnClickListener {
            view.findNavController().navigate(R.id.constraintFragment)
        }

    }

    private fun singIn() {
           var phone:String=""
        phone=view?.btn_sign_in?.text.toString();
        signUpViewModel.phoneSignUpClient(
                RequestBody.create(MediaType.parse("multipart/form-data"), "1"),
                RequestBody.create(MediaType.parse("multipart/form-data"), phone),
                RequestBody.create(MediaType.parse("multipart/form-data"),
                        FirebaseInstanceId.getInstance().getToken()),
                view!!.progress_bar,context!!,view!!
        )
    }

    private fun facebookSignIn(loginResult: LoginResult) {
        Log.d("FBLOGIN", loginResult.accessToken.token.toString())
        Log.d("FBLOGIN", loginResult.recentlyDeniedPermissions.toString())
        Log.d("FBLOGIN", loginResult.recentlyGrantedPermissions.toString())


        val request = GraphRequest.newMeRequest(loginResult.accessToken) { `object`, response ->
            try {
                //here is the data that you want
                Log.d("FBLOGIN_JSON_RES", `object`.toString())

                if (`object`.has("id")) {
                    Log.d("good", `object`.toString())

                    val name = `object`.getString("name")
                    val email = `object`.getString("email")
                    val id = `object`.getString("id")
                    val image= "http://graph.facebook.com/$id/picture?type=large"

                    checkEmail(name,image,email)

                } else {
                    Log.e("FBLOGIN_FAILD", `object`.toString())
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        val parameters = Bundle()
        parameters.putString("fields", "name,email,id,picture.type(large)")
        request.parameters = parameters
        request.executeAsync()

    }

    private fun signIn() {
        val signInIntent: Intent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    private fun startAnim(view: View) {
        view.btn_sign_in.visibility = View.VISIBLE
//        view.btn_not_now.visibility = View.VISIBLE
        view.tv_or.visibility = View.VISIBLE
        view.view.visibility = View.VISIBLE
        view.facebook_sign_in.visibility = View.VISIBLE
        view.google_sign_in.visibility = View.VISIBLE
//        view.line.visibility=View.VISIBLE

        try {

            animation = AnimationUtils.loadAnimation(context, R.anim.anim_scale)
            animation.duration = 800

        } catch (e: Exception) {
        }

        view.btn_sign_in.startAnimation(animation)
        view.btn_not_now.startAnimation(animation)
        view.tv_or.startAnimation(animation)
        view.view.startAnimation(animation)
        view.facebook_sign_in.startAnimation(animation)
        view.google_sign_in.startAnimation(animation)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Log.i("GoogleSignInTest","1")
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        Log.i("GoogleSignInTest","2")

        try {
            Log.i("GoogleSignInTest","3")

            val account = completedTask.getResult(ApiException::class.java)

            checkEmail(account!!)

            Log.e("TAG", account.email!!)
            Log.e("TAG", account.photoUrl.toString())
            Log.e("TAG", account.givenName.toString())
            Log.e("TAG", account.displayName.toString())
            Log.e("TAG", account.familyName.toString())
            Log.i("GoogleSignInTest","4")

        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("TAG", "signInResult:failed code=" + e.statusCode)
            //            updateUI(null)
        }
    }

    private fun checkEmail(account: GoogleSignInAccount) {
        Log.i("GoogleSignInTest","5")

        signUpViewModel.checkEmail(account.email!!)
        signUpViewModel.checkEmailMutableLiveData.observe(this, Observer { t ->

            Log.e("datata",t.error.toString())

            if (t.error == 1) {
                Log.i("GoogleSignInTest","6")

                val bundle = Bundle()
                bundle.putString("email", account.email)
                bundle.putString("name", account.displayName)
                bundle.putString("image", account.photoUrl.toString())
                this.view?.let { Navigation.findNavController(it).navigate(R.id.fragment_SignUp, bundle) }
                Log.i("GoogleSignInTest","7")

            } else if (t.error == 0) {

                UserInfo(activity!!.applicationContext).setUserData(t.data)
                activity!!.startActivity(Intent(activity!!.applicationContext, MainActivity::class.java))

            }

        })

    }

    private fun checkEmail(name: String, image: String, email: String) {

        signUpViewModel.checkEmail(email)
        signUpViewModel.checkEmailMutableLiveData.observe(this, Observer { t ->

            Log.e("datata",t.error.toString())

            if (t.error == 1) {

                val bundle = Bundle()
                bundle.putString("email", email)
                bundle.putString("name", name)
                bundle.putString("image", image)

                this.view?.let { Navigation.findNavController(it).navigate(R.id.fragment_SignUp, bundle) }

            } else if (t.error == 0) {

                UserInfo(activity!!.applicationContext).setUserData(t.data)
                activity!!.startActivity(Intent(activity!!.applicationContext, MainActivity::class.java))

            }

        })

    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun afterTextChanged(p0: Editable?) {
        if (p0.toString().length==11)
            view!!.btn_not_now.visibility=View.VISIBLE
        else
            view!!.btn_not_now.visibility=View.GONE

    }

    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}