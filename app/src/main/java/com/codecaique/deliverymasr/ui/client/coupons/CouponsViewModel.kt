package com.codecaique.deliverymasr.ui.client.coupons

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codecaique.deliverymasr.pojo.response.GeneralResponse
import com.codecaique.deliverymasr.pojo.response.MyCouponsResponse
import com.codecaique.deliverymasr.data.ApiCreater
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.RequestBody

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class CouponsViewModel : ViewModel() {

    val myCouponsMutableLiveData = MutableLiveData<MyCouponsResponse>()
    val addCouponMutableLiveData = MutableLiveData<GeneralResponse>()

    fun myCoupons(token: String) {

        val observable =
                ApiCreater.instance.showMyCoupons(token)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<MyCouponsResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: MyCouponsResponse) {

                myCouponsMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }

    fun addMyCoupon(token: String, coupon: String) {

        val observable =
                ApiCreater.instance.addMyCoupon(
                                RequestBody.create(MediaType.parse("multipart/form-data"), token),
                                RequestBody.create(MediaType.parse("multipart/form-data"), coupon)
                        )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<GeneralResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: GeneralResponse) {

                addCouponMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }

}