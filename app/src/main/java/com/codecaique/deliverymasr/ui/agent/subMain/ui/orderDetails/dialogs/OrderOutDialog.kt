package com.codecaique.deliverymasr.ui.agent.subMain.ui.orderDetails.dialogs

import android.app.Dialog
import android.content.Context
import android.content.ContextWrapper
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.client.chat.ChatViewModel
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import kotlinx.android.synthetic.main.dialog_order_out.*

class OrderOutDialog(var context_: Context, var chatViewModel: ChatViewModel, var requestId: Int,
                     var userId: Int, var driver_id: Int) : Dialog(context_) {

    lateinit var loadingDialog: LoadingDialog
    private val TAG = "OrderOutDialog"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_order_out)

        btn_close.setOnClickListener { dismiss() }

        loadingDialog = LoadingDialog(this.context)

        tv_out.setOnClickListener {

            val cancel_order = cancel_reason.text.toString()

            loadingDialog.show()



            chatViewModel.cancelOrder(UserInfo(context).getUserToken(), requestId, cancel_order, driver_id, loadingDialog)

            Log.e("ID", driver_id.toString())
            Log.e("Reason", "$cancel_order    ")

            chatViewModel.cancelOrderMutableLiveData.observe((context_ as ContextWrapper).baseContext as FragmentActivity, Observer {

                Log.e(TAG, "onCreate: error ${it.error}")

                if (it.error == 0) {
                    Toast.makeText(context, "تم  حذف الطلب بنجاح ", Toast.LENGTH_SHORT).show()
                    dismiss()
                    (context as AppCompatActivity).finish()
                    loadingDialog.dismiss()
                } else {
                    Log.e(TAG, "onCreate: message ${it.message}")
                    loadingDialog.dismiss()
                    Toast.makeText(context, context_.getString(R.string.can_not_be_out), Toast.LENGTH_SHORT).show()
                }

            })

        }


    }
}
