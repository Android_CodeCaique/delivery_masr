package com.codecaique.deliverymasr.ui.agent.auth.ui.addMandop

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.pojo.response.Bank
import com.codecaique.deliverymasr.ui.agent.auth.ui.otp.OtpFragment
import com.codecaique.deliverymasr.ui.client.joinAsCaptain.JoinAsCaptainViewModel
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import com.codecaique.deliverymasr.utils.Utils
import com.google.firebase.iid.FirebaseInstanceId
import com.squareup.picasso.Picasso
import io.reactivex.plugins.RxJavaPlugins
import kotlinx.android.synthetic.main.fragment_add_mandop.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class AddMandopFragment : Fragment() {

    lateinit var joinAsCaptainViewModel: JoinAsCaptainViewModel
    lateinit var arrayAdapter: ArrayAdapter<String>
    lateinit var bankList: ArrayList<Bank>
    lateinit var loadingDialog: LoadingDialog
    val TAG="AddMandopFragment"

    var bankAccount = 1
    var bankApple = 1
    var bankSelected = 0

    lateinit var VIEW: View

    var uriUserImage: Uri? = null
    var uriDrivingLicenseImage: Uri? = null
    var uriFrontVehicalImage: Uri? = null
    var uriCarLicenseImage: Uri? = null
    var isSocialMediaSignUP = false
    var isSocialMediaSignUPForID = false
    lateinit var code:String
    lateinit var phone:String

    @SuppressLint("ResourceAsColor", "SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        VIEW = inflater.inflate(R.layout.fragment_add_mandop, container, false)

        RxJavaPlugins.setErrorHandler { throwable: Throwable? -> }


        loadingDialog = LoadingDialog(context!!.applicationContext)

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true/*  enabled by default*/) {
            override fun handleOnBackPressed() {
                // Handle the back button event
                requireActivity().finish()
            }
        }


        if (arguments != null)
        {
            code = arguments!!.getString("code")!!
            phone = arguments!!.getString("phone")!!
            VIEW.et_phoneNumber.setText(code+phone)

        }



        requireActivity().onBackPressedDispatcher.addCallback(this, callback)



        VIEW.back.setOnClickListener { activity!!.finish() }

        try {
            Log.e("TAG add mandop", code + phone)
            if (arguments!!.getString("image") != null)
                Picasso.get().load(arguments!!.getString("image")).into(VIEW.img_user)
            VIEW.et_name.setText(arguments!!.getString("name"))
            /*VIEW.et_user_Edit_Email.setText(*/
            Log.e("TAG", arguments!!.getString("image"))
            isSocialMediaSignUP = true
            isSocialMediaSignUPForID = true
            VIEW.tv_phone.visibility = View.VISIBLE
            VIEW.et_phoneNumber.visibility = View.VISIBLE
        } catch (e: Exception) {
        }


        joinAsCaptainViewModel = ViewModelProviders.of(this).get(JoinAsCaptainViewModel::class.java)
        //هنعوز نعمل فريجمنت تاني لل code_verification

        arrayAdapter = ArrayAdapter(activity!!.applicationContext, R.layout.spinner_item, R.id.text)
        arrayAdapter.addAll(emptyList())

        bankList = ArrayList()
        bankList.add(Bank(0, "اختر نوع البنك "))

//        VIEW.sp_banks.adapter = arrayAdapter

        joinAsCaptainViewModel.showBanks()
        joinAsCaptainViewModel.banksMutableLiveData.observe(this, Observer { t ->

            if (t.error == 3) {
                Utils.signout(requireActivity() , view!!)
            }else{
            val arrayList = ArrayList<String>()
            arrayList.add("اختر نوع البنك ")


            t.data.forEach { i ->
                bankList.add(i)
                arrayList.add(i.name)
            }

            arrayAdapter.addAll(arrayList)
            arrayAdapter.notifyDataSetChanged()

        }}
        )

/*

        VIEW.sp_banks.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                bankSelected = bankList[p2].id
            }
        }

        VIEW.rg_banks.setOnCheckedChangeListener { radioGroup, i ->

            when (i) {
                R.id.rb_no -> {
                    VIEW.tv_account.visibility = View.GONE
                    VIEW.sp_banks.visibility = View.GONE
                    bankAccount = 0
                }
                R.id.rb_yes -> {
                    VIEW.tv_account.visibility = View.VISIBLE
                    VIEW.sp_banks.visibility = View.VISIBLE
                    bankAccount = 1
                }
            }

        }


        VIEW.rg_apple.setOnCheckedChangeListener { radioGroup, i ->

            when (i) {
                R.id.rb_apple_no -> {
                    VIEW.tv_apple.visibility = View.GONE
                    VIEW.et_apple.visibility = View.GONE
                    bankApple = 0
                }
                R.id.rb_apple_yes -> {
                    VIEW.tv_apple.visibility = View.VISIBLE
                    VIEW.et_apple.visibility = View.VISIBLE
                    bankApple = 1
                }
            }

        }
*/

        VIEW.btn_image.setOnClickListener { isStoragePermissionGranted(1) }
        VIEW.btn_lic.setOnClickListener { isStoragePermissionGranted(2) }
        VIEW.btn_front.setOnClickListener { isStoragePermissionGranted(3) }
        VIEW.btn_lic_car.setOnClickListener { isStoragePermissionGranted(4) }

        var gender = 2

        VIEW.tv_male.setOnClickListener {

            gender = 2
            VIEW.tv_male.background = resources.getDrawable(R.drawable.border_right_fill, null)
            VIEW.tv_female.background = resources.getDrawable(R.drawable.border_left, null)
            VIEW.tv_male.setTextColor(ContextCompat.getColor(activity!!,R.color.white))
            VIEW.tv_female.setTextColor(ContextCompat.getColor(activity!!,R.color.colorPrimaryDark))

        }

        VIEW.tv_female.setOnClickListener {

            gender = 1
            VIEW.tv_female.background = resources.getDrawable(R.drawable.border_left_fill, null)
            VIEW.tv_male.background = resources.getDrawable(R.drawable.border_right, null)
            VIEW.tv_male.setTextColor(  ContextCompat.getColor(activity!!,R.color.colorPrimaryDark))
            VIEW.tv_female.setTextColor(ContextCompat.getColor(activity!!,R.color.white))
        }


        VIEW.buttoContinue.setOnClickListener {

            val name = VIEW.et_name.text.toString()
            val cardNumber = VIEW.IdNumber.text.toString()
            val nationality = VIEW.phoneNumber.selectedItem.toString()
            val apple = ""//VIEW.et_apple.text.toString()

            val email = try {
                arguments!!.getString("email")!!
            } catch (e: Exception) {
                ""
            }

            val userID = UserInfo(activity!!.applicationContext).getId()

         /*   if (bankAccount == 1 && bankSelected == 0) {
                Toast.makeText(activity!!, getString(R.string.empty_bank), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (bankApple == 1 && apple.isEmpty()) {
                VIEW.et_apple.error = getString(R.string.empty_apple)
                return@setOnClickListener
            }
*/
            if (name.isNotEmpty()
                    && nationality.isNotEmpty()
                    && (uriUserImage != null || isSocialMediaSignUP)
                    && uriCarLicenseImage != null
                    && uriDrivingLicenseImage != null
                    && uriFrontVehicalImage != null
            ) {

                if (isSocialMediaSignUP) {
                    if (VIEW.et_phoneNumber.text.toString().isNotEmpty())
                        phone = "+2" + VIEW.et_phoneNumber.text.toString().trim()
                    else {
                        VIEW.et_phoneNumber.error = getString(R.string.empty_phone)
                        return@setOnClickListener
                    }

                }

                loadingDialog = LoadingDialog(activity!!)
                loadingDialog.show()

                val userInfo = UserInfo(activity!!.applicationContext)
                val testId= if (isSocialMediaSignUPForID) null else userID

                Log.e(TAG, "onCreateView: $testId")

                joinAsCaptainViewModel.enterUserData(
                       testId,
                        name, name,
                        RequestBody.create(MediaType.parse("multipart/form-data"), nationality),
                        RequestBody.create(MediaType.parse("multipart/form-data"), ""),
                        gender,
                        image("image"),
                        RequestBody.create(MediaType.parse("multipart/form-data"), ""),
                        image("driving_license_image"),
                        image("front_vehical_image"),
                        image("car_license_image"),
                        RequestBody.create(MediaType.parse("multipart/form-data"), code + phone),
                        RequestBody.create(MediaType.parse("multipart/form-data"), email),
                        RequestBody.create(MediaType.parse("multipart/form-data"), FirebaseInstanceId.getInstance().token),
                        userInfo.getLongitude().toDouble(), userInfo.getLatitude().toDouble(),
                        bankAccount,
                        bankSelected,
                        bankApple,
                        RequestBody.create(MediaType.parse("multipart/form-data"), apple),
                        loadingDialog,context!!
                )

                joinAsCaptainViewModel.enterUserDataMutableLiveData.observe(this, Observer { t ->

                    if (t.error == 3) {
                        Utils.signout(requireActivity() , view!!)
                    }
                    else if (t.error == 0) {
                        //
                        val bundle =Bundle()
                        bundle.putSerializable(OtpFragment.DATA,t.data)
                        VIEW.findNavController().navigate(R.id.otpFragment,bundle)
                    } else {
                        Log.e("TAG", "test "+t.error.toString())
                        Toast.makeText(activity!!, t.message, Toast.LENGTH_SHORT).show()
                        loadingDialog.dismiss()
                    }

                })


            } else {
                if (name.isEmpty()) {
                    VIEW.et_name.error = getString(R.string.empty_name)
                }
//                if (cardNumber.isEmpty()) {
//                    VIEW.IdNumber.error = getString(R.string.empty_nat)
//                }
//                if (nationality.isEmpty()) {
//                    VIEW.phoneNumber.error = getString(R.string.empty_nationality)
//                }
                if (uriUserImage == null && !isSocialMediaSignUP) {
                    Toast.makeText(activity!!, getString(R.string.empty_image), Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }
                if (uriDrivingLicenseImage == null) {
                    Toast.makeText(activity!!, getString(R.string.empty_licance), Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }
                if (uriCarLicenseImage == null) {
                    Toast.makeText(activity!!, getString(R.string.empty_car_lic), Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }
                if (uriFrontVehicalImage == null) {
                    Toast.makeText(activity!!, getString(R.string.empty_card_front), Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }
            }

            /*            val codeVerification2 = CodeVerification2()
            val manager = fragmentManager
            manager!!.beginTransaction().replace(R.id.main3_fragment, codeVerification2, codeVerification2.tag).commit()*/
        }

        return VIEW
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (Activity.RESULT_OK == resultCode) {

            when (requestCode) {
                1 -> {
                    VIEW.img_user.setImageURI(data!!.data!!)
                    uriUserImage = data.data!!
                    isSocialMediaSignUP = false
                }
                2 -> {
                    VIEW.img_lic.setImageURI(data!!.data!!)
                    uriDrivingLicenseImage = data.data!!
                }
                3 -> {
                    VIEW.img_front.setImageURI(data!!.data!!)
                    uriFrontVehicalImage = data.data!!
                }
                4 -> {
                    VIEW.img_lic_car.setImageURI(data!!.data!!)
                    uriCarLicenseImage = data.data!!
                }
            }
        }
    }

    private fun image(KEY: String): MultipartBody.Part? {

        var uri: Uri? = null

        when (KEY) {
            "image" -> uri = uriUserImage
            "driving_license_image" -> uri = uriDrivingLicenseImage
            "front_vehical_image" -> uri = uriFrontVehicalImage
            "car_license_image" -> uri = uriCarLicenseImage
        }

        if (!(KEY == "image" && isSocialMediaSignUP)) {
            val p: String
            val cursor = activity!!.contentResolver.query(uri!!, null, null, null, null)

            p = if (cursor == null) {
                uri.path.toString()
            } else {
                cursor.moveToFirst()
                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                cursor.getString(idx)
            }

            val file = File(p)
            val requestFile = RequestBody.create(MediaType.parse("image/*"), file)
            return MultipartBody.Part.createFormData(KEY, file.name, requestFile)
        } else {
            val bitmap: Bitmap = (view!!.img_user.drawable as BitmapDrawable).bitmap
            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            val imageInByte: ByteArray = baos.toByteArray()
            val requestFile: RequestBody =
                    RequestBody.create(MediaType.parse("image/*"), imageInByte)

            return MultipartBody.Part.createFormData("image", "${Calendar.getInstance().timeInMillis}.png", requestFile)
        }

    }

    @SuppressLint("WrongConstant")
    fun isStoragePermissionGranted(requestCode: Int): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                gotoImage(requestCode)

                true
            } else {

                ActivityCompat.requestPermissions(
                        activity!!,
                        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        requestCode
                )
                false
            }
        } else {

            gotoImage(requestCode)
            return true
        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            gotoImage(requestCode)
        } else {
            /*
        Toast.makeText(applicationContext, R.string.imagePprem_accept, Toast.LENGTH_SHORT)
                .show()
*/
        }
    }

    private fun gotoImage(requestCode: Int) {
        val intent = Intent(
                Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), requestCode)
    }


    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}