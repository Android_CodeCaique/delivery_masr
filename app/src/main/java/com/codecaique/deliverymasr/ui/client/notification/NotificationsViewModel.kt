package com.codecaique.deliverymasr.ui.client.notification

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.response.GeneralResponse
import com.codecaique.deliverymasr.pojo.response.NotificationsResponse
import com.codecaique.deliverymasr.pojo.response.TurnNotificationResponse
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import com.codecaique.deliverymasr.ui.client.notificationsSettings.TurnNotificationFragment
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class NotificationsViewModel : ViewModel() {

    val myNotificationsMutableLiveData = MutableLiveData<NotificationsResponse>()
    val turnNotificationsMutableLiveData = MutableLiveData<TurnNotificationResponse>()
    val deleteNotificationsMutableLiveData = MutableLiveData<GeneralResponse>()

    fun getNotifications(token: String,type: String,lang:String) {

        val observable =
                ApiCreater.instance.showNotifications(token,type,lang)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<NotificationsResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: NotificationsResponse) {

                myNotificationsMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
                TurnNotificationFragment().loadingDialog!!.dismiss()

            }
        }

        observable.subscribe(observer)

    }

    fun turnNotifications(token: String) {

        val observable =
                ApiCreater.instance.turnOnNotifications(token)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<TurnNotificationResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: TurnNotificationResponse) {

                turnNotificationsMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
                TurnNotificationFragment().loadingDialog!!.dismiss()
            }
        }

        observable.subscribe(observer)

    }

    fun deleteNotification(context: Context, user: UserInfo, notificationsAdapter: NotificationsAdapter, loadingDialog: LoadingDialog,position:Int) {

        loadingDialog.show()

        ApiCreater.instance.deleteNotification(user.getUserToken(), notificationsAdapter.list[position].id)
                .enqueue(object : Callback<GeneralResponse> {
                    override fun onFailure(call: Call<GeneralResponse>, t: Throwable) {
                        Toast.makeText(context, context.getString(R.string.cant), Toast.LENGTH_LONG).show()
                        loadingDialog.dismiss()
                        Log.e("ss", t.message)
                    }

                    override fun onResponse(call: Call<GeneralResponse>, response: Response<GeneralResponse>) {
//                        deleteNotificationsMutableLiveData.value = response.body()!!
                        Log.e("TAG",response.body()!!.message)
                        loadingDialog.dismiss()
                        user.setDiscountNotifiCount()
                        notificationsAdapter.list.removeAt(position)
                        notificationsAdapter.notifyAdapter()

                    }
                })
    }


    fun deleteNotification(context: Context, user: UserInfo, notificationsAdapter: com.codecaique.deliverymasr.ui.agent.main.ui.notifications.NotificationsAdapter, loadingDialog: LoadingDialog, position:Int) {

        loadingDialog.show()

        ApiCreater.instance.deleteNotification(user.getUserToken(), notificationsAdapter.list[position].id)
                .enqueue(object : Callback<GeneralResponse> {
                    override fun onFailure(call: Call<GeneralResponse>, t: Throwable) {
                        Toast.makeText(context, context.getString(R.string.cant), Toast.LENGTH_LONG).show()
                        loadingDialog.dismiss()
                        Log.e("ss", t.message)
                    }

                    override fun onResponse(call: Call<GeneralResponse>, response: Response<GeneralResponse>) {
//                        deleteNotificationsMutableLiveData.value = response.body()!!
                        Log.e("TAG",response.body()!!.message)
                        loadingDialog.dismiss()
                        user.setDiscountNotifiCount()
                        notificationsAdapter.list.removeAt(position)
                        notificationsAdapter.notifyAdapter()

                    }
                })
    }

}