package com.codecaique.deliverymasr.ui.client.stores.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.client.SubActivity
import com.codecaique.deliverymasr.pojo.response.NearStore
import com.codecaique.deliverymasr.pojo.response.Places_Response
import com.codecaique.deliverymasr.pojo.util.UserInfo
import de.hdodenhof.circleimageview.CircleImageView

class Map_NearStoresAdapter(var context: Context,var view:View): RecyclerView.Adapter<Map_NearStoresAdapter.ViewHolderMarkets>()
{
    var dynamicList = ArrayList<Places_Response.ResultsBean>()
    var staticList = ArrayList<NearStore>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderMarkets {
        val view = LayoutInflater.from(context).inflate(R.layout.item_order, parent, false)
        return ViewHolderMarkets(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolderMarkets, position: Int) {

        val arrayItem = dynamicList[position]

        // 18.329384, 42.759365

        val lat1:Double = UserInfo(context).getLatitude().toDouble()
        val lng1:Double = UserInfo(context).getLongitude().toDouble()
        val lat2:Double = arrayItem.geometry.location.lat
        val lng2:Double = arrayItem.geometry.location.lng

        holder.title.text = arrayItem.name
        holder.distance.text = String.format("%.2f", getDistanceFromLatLonInKm(lat1,lng1,lat2,lng2)) + "\nكم "
        holder.body.visibility = View.GONE

        holder.image.setImageResource(R.drawable.restaurant)

//        if (arrayItem.discount != "0" && arrayItem.discount.isNotEmpty() && arrayItem.discount != "null") {
//
//            Log.e("Ssss" , "NOt null")
//
//            holder.discount.visibility = View.VISIBLE
//            if (!arrayItem.discount.contains("%"))
//                holder.discount.text = arrayItem.discount + " %"
//            else
//                holder.discount.text = arrayItem.discount
//
//        }

        holder.itemView.setOnClickListener {


            val intent = Intent(context, SubActivity::class.java)
            //StoresAdapter.fragName = R.layout.fragment_tard__hayak_
            intent.putExtra(SubActivity.KEY,SubActivity.TardHayak)
            intent.addFlags(FLAG_ACTIVITY_NEW_TASK)

            intent.putExtra("name",arrayItem.name)
            intent.putExtra("type","store")
            intent.putExtra("lat",dynamicList.get(position).geometry.location.lat)
            intent.putExtra("lng",dynamicList.get(position).geometry.location.lng)
            intent.putExtra("location",arrayItem.vicinity)
            context.applicationContext.startActivity(intent)

        }

    }

    /*Intent intent = new Intent(holder.itemView.getContext(), DelievryPlaceActivity2.class);
        view.getContext().startActivity(intent);*/

    override fun getItemCount(): Int {
        return dynamicList.size
    }

    inner class ViewHolderMarkets(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView = itemView.findViewById(R.id.market_name)
        var body: TextView = itemView.findViewById(R.id.order_time)
        var distance: TextView = itemView.findViewById(R.id.coupon_cancel)
        var discount: TextView = itemView.findViewById(R.id.tv_discount)
        var image: CircleImageView = itemView.findViewById(R.id.market_logo)
    }

    fun setData(a: ArrayList<Places_Response.ResultsBean>) {
        this.dynamicList = a
//        this.staticList = a
//
//        staticList.forEach { t ->
//            Log.e("AAAa", t.category_name)
//        }

        notifyDataSetChanged()
    }

//    fun filter(catName: String) {
//
//        dynamicList = ArrayList<NearStore>()
//
//        Log.e("NAME 20008",catName + "  :  " + context.getString(R.string.all))
//
//
//        if (catName.equals(context.getString(R.string.all))) {
//
//            dynamicList = staticList
//            notifyDataSetChanged()
//
//            Log.e("SIZE of dynamic" , dynamicList.size.toString() + "    ")
//            Log.e("SIZE of static" , staticList.size.toString() + "    ")
//
//            return
//        }
//
//        staticList.forEach {
//            if (it.category_name.contains(catName))
//                dynamicList.add(it)
//        }
//
//        notifyDataSetChanged()
//
//
//    }


    fun getDistanceFromLatLonInKm(lat1:Double,lon1:Double,lat2:Double,lon2:Double): Double {
        val r = 6371
        val dLat = deg2rad(lat2-lat1)  // deg2rad below
        val dLon = deg2rad(lon2-lon1);
        val a =
                Math.sin(dLat/2) * Math.sin(dLat/2) +
                        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                        Math.sin(dLon/2) * Math.sin(dLon/2)
        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        val d = r * c; // Distance in km
        return d
    }

    fun deg2rad(deg:Double): Double {
        return deg* (Math.PI / 180)
    }

}