package com.codecaique.deliverymasr.ui.client.otp

import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.databinding.FragmentSignInConfirmBinding
import com.google.firebase.FirebaseException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import java.util.concurrent.TimeUnit

class OtpViewModel:ViewModel() {

    companion object
    {
        private const val TAG="OtpViewModel"
    }

    val otpCodeMutableLiveData= MutableLiveData<String>()

    fun otp(binding: FragmentSignInConfirmBinding, activity: FragmentActivity,phone:String){

        val  phoneNum= "+2$phone"
        binding.tvPhone.text=phone

        binding.progressBar.visibility=View.VISIBLE
        binding.buttonConfirm.visibility=View.VISIBLE

        val options = PhoneAuthOptions.newBuilder(Firebase.auth)
                .setPhoneNumber(phoneNum)
                .setTimeout(30L, TimeUnit.SECONDS)
                .setActivity(activity)
                .setCallbacks(object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

                    override fun onCodeSent(
                            verificationId: String,
                            forceResendingToken: PhoneAuthProvider.ForceResendingToken
                    ) {

                        Log.e(TAG, "onCodeSent: verificationId --> $verificationId")

                    }

                    override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {
                        // Sign in with the credential
                        // ...
                        binding.progressBar.visibility = View.GONE
                        binding.buttonConfirm.visibility=View.VISIBLE
                        Log.e(TAG, "onVerificationCompleted: phoneAuthCredential --> ${phoneAuthCredential.smsCode}")
                        Toast.makeText(activity, activity.getString(R.string.otp_sent), Toast.LENGTH_SHORT).show()
                        otpCodeMutableLiveData.value = phoneAuthCredential.smsCode


                    }

                    override fun onVerificationFailed(e: FirebaseException) {
                        // ...
                        Log.e(TAG, "onVerificationFailed:  --> ", e)
                        binding.progressBar.visibility = View.GONE
                        binding.buttonConfirm.visibility=View.VISIBLE
                        Toast.makeText(activity, activity.getString(R.string.may_error), Toast.LENGTH_SHORT).show()
                    }
                })
                .build()
        PhoneAuthProvider.verifyPhoneNumber(options)

    }


}