package com.codecaique.deliverymasr.ui.client.signUp.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.telephony.SmsManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.client.MainActivity
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.pojo.response.Phone
import kotlinx.android.synthetic.main.fragment_code_verifcation.view.*
import java.util.*

class Fragment_Code_Verification : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_code_verifcation, container, false)
    }

    var phone = ""
    var code = ""

    companion object {

        val PHONE = "phone"
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        phone = arguments!!.getString(PHONE, "")
        val isSigned = arguments!!.getBoolean("isSigned", false)

        val r = Random()
        val smsManager = SmsManager.getDefault()


        code = String.format("%04d", Integer.valueOf(r.nextInt(1001)) as Any?)
        smsManager.sendTextMessage(phone, null, code, null, null);

        Log.e("CODE", code)

        view.header_txt.text = getString(R.string.code_sent_1) + phone + getString(R.string.pls_enter_sent_code)

        view.tv_no_message.setOnClickListener {

            code = String.format(Locale("en"), "%04d", Integer.valueOf(r.nextInt(1001)) as Any?)
            smsManager.sendTextMessage(phone, null, code, null, null);


        }

        val navController = Navigation.findNavController(view)

        view.mobile_verification_btn.setOnClickListener {


            val confirm = view.et_code.text.toString()

            if (confirm.isNotEmpty() && confirm.length == 4) {

                if (confirm == code) {

                    if (isSigned) {

                        val data = arguments!!.getSerializable("data") as Phone
                        UserInfo(activity!!.applicationContext).setUserData(data)
                        UserInfo(activity!!.applicationContext).setState("user")
                        //activity!!.startActivity(Intent(activity!!.applicationContext, MainActivity::class.java))
                        val intent =Intent(activity!!, MainActivity::class.java)
                        intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                        activity!!.startActivity(intent)


                    } else {

                        navController.navigate(R.id.action_fragment_Code_Verification_to_fragment_SignUp)

                    }

                } else {
                    view.et_code.error = "من فضلك اكتب الكود صحيحا"
                }

            } else {
                view.et_code.error = "من فضلك اكتب الكود صحيحا"

            }


        }

    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}