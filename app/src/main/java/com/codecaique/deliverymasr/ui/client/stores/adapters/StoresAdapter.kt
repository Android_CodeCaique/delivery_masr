package com.codecaique.deliverymasr.ui.client.stores.adapters

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.client.SubActivity
import com.codecaique.deliverymasr.ui.client.restaurant.RestaurantFragment
import com.codecaique.deliverymasr.ui.client.stores.adapters.StoresAdapter.ViewHolderStoreTop
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.response.Category
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

class StoresAdapter(var context: Context) : RecyclerView.Adapter<ViewHolderStoreTop>() {

    var list = ArrayList<Category>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderStoreTop {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.stores_item_horizontal, parent, false)
        return ViewHolderStoreTop(view)
    }

    override fun onBindViewHolder(holder: ViewHolderStoreTop, position: Int) {

        val arrayItem = list[position]


        holder.title.text = arrayItem.name
        holder.subTitle.text = context.getString(R.string.orderAnythingFromResturant)

        if (arrayItem.id == -1) {

            Picasso.get().load(R.drawable.from_any_where).into(holder.image)


        } else {
            /*if (arrayItem.name.contains("مطاعم") || arrayItem.name.contains("restaurant")) {
                holder.image.setImageResource(R.drawable.restaurants)
            } else if (arrayItem.name.contains("سوبر ماركت") || arrayItem.name.contains("supermarket")) {
                holder.image.setImageResource(R.drawable.supermaret)
            } else if (arrayItem.image.isNotEmpty())*/
            Picasso.get().load(ApiCreater.CATEGORIES_URL + arrayItem.image).into(holder.image)
        }

        holder.itemView.setOnClickListener {

            try {
                val intent = Intent(context, SubActivity::class.java)

                if (arrayItem.id == -1) {

                    intent.putExtra(SubActivity.KEY, SubActivity.TardHayak)
                    context.startActivity(intent)
                    return@setOnClickListener
                }

//            fragName = R.layout.fragment_kentaky_restaurant

                intent.putExtra(SubActivity.KEY, SubActivity.SubCat)
                intent.putExtra(RestaurantFragment.NAME, arrayItem.name)
                intent.putExtra(RestaurantFragment.ID, arrayItem.id.toString())
                intent.putExtra(RestaurantFragment.IMAGE, arrayItem.image)

                Log.e("RestaurantFragment$$$", "${arrayItem.id} ${arrayItem.name}  ${arrayItem.image}")
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                context.startActivity(intent)
            } catch (e: Exception) {

                FirebaseCrashlytics.getInstance().recordException(e)

            }
        }

    }
    ////                Intent intent =new Intent(view.getContext(),TardHayakActivity.class);
    /*val intent = Intent(view.context, MainActivity3::class.java)
            fragName = R.layout.fragment_kentaky_restaurant
            context.startActivity(intent)*/

    override fun getItemCount(): Int {
        return list.size
    }

    inner class ViewHolderStoreTop(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var image: CircleImageView = itemView.findViewById(R.id.imageView)
        var subTitle: TextView = itemView.findViewById(R.id.tv_sub_title)
        var title: TextView = itemView.findViewById(R.id.tv_title)
        var root: LinearLayout = itemView.findViewById(R.id.root)
    }

    companion object {
        @JvmField
        var fragName = 0
    }

    fun setData(a: ArrayList<Category>) {
        this.list = a
        notifyDataSetChanged()
    }

}