package com.codecaique.deliverymasr.ui.client.shopsNotSupported.details

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.databinding.FragmentSentRequistBinding
import com.codecaique.deliverymasr.ui.client.shopsNotSupported.deliveryLocation.DeliveryLocationFragment
import kotlinx.android.synthetic.main.dialog_add_copoun.*
import kotlinx.android.synthetic.main.dialog_attach_photo.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class ShopOrderDetailsFragment : Fragment() {

    private val TAG = "ShopOrderDetailsFragment"
    private var shopName: String? = null
    private lateinit var binding: FragmentSentRequistBinding
    var currentPhotoPath: String? = null
    var uri: Uri? = null
    var coupon = ""
    var payment = 1

    companion object {

        const val SHOP_NAME = "param1"
        const val SHOP_LATITUDE = "param11"
        const val SHOP_LONGITUDE = "param2"
        const val SHOP_STREET = "param3"

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_sent_requist, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        arguments!!.let {
            shopName = it.getString(SHOP_NAME)
        }

        binding.tvSentRequest.text = getString(R.string.next)
//        binding.tvShopName.text = shopName

        binding.tvSentRequest.setOnClickListener {

            if (binding.tvCamera.text.isEmpty()) {
                binding.tvCamera.error = getString(R.string.sould_be_not_empty)
                return@setOnClickListener
            }

            val bundle = Bundle()

            bundle.putString(DeliveryLocationFragment.MESSAGE, binding.tvCamera.text.toString())
            bundle.putString(DeliveryLocationFragment.PAYMENT_ID, payment.toString())
            bundle.putString(DeliveryLocationFragment.COUPON, coupon)
            bundle.putString(DeliveryLocationFragment.SHOP_NAME, shopName)
            bundle.putString(DeliveryLocationFragment.IMAGE_URI, uri.toString())
            bundle.putString(DeliveryLocationFragment.SHOP_STREET, arguments!!.getString(SHOP_STREET)!!)
            bundle.putString(DeliveryLocationFragment.SHOP_LONGITUDE, arguments!!.getString(SHOP_LONGITUDE)!!)
            bundle.putString(DeliveryLocationFragment.SHOP_LATITUDE, arguments!!.getString(SHOP_LATITUDE)!!)

            it.findNavController().navigate(R.id.deliveryLocationFragment, bundle)

        }

        binding.ivOpenCamera.setOnClickListener { dialogGetPhoto() }

        binding.tvAddCoupon.setOnClickListener { dialogAddCoupon() }

        binding.rgPayment.setOnCheckedChangeListener { _, i ->

            when (i) {
                R.id.rb_cash -> payment = 1
                R.id.rb_credit -> payment = 2
            }

        }

    }


    private fun dialogGetPhoto() {
        val dialog = Dialog(activity!!)
        dialog.setContentView(R.layout.dialog_attach_photo)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))


        dialog.tv_camera_.setOnClickListener {

            isStoragePermissionGranted(222)
            dialog.cancel()
        }

        dialog.tv_media.setOnClickListener {

            isStoragePermissionGranted(111)
            dialog.cancel()
        }
        dialog.show()
    }

    @SuppressLint("WrongConstant")
    fun isStoragePermissionGranted(requestCode: Int): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return if (requestCode == 111) {
                when (PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.checkSelfPermission(context!!, Manifest.permission.READ_EXTERNAL_STORAGE) -> {
                        gotoImage(requestCode)

                        true
                    }
                    else -> {

                        ActivityCompat.requestPermissions(
                                activity!!,
                                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA),
                                requestCode
                        )
                        false
                    }
                }
            } else {
                if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    gotoImage(requestCode)

                    true
                } else {

                    ActivityCompat.requestPermissions(
                            activity!!,
                            arrayOf(Manifest.permission.CAMERA),
                            requestCode
                    )
                    false
                }
            }
        } else {

            gotoImage(requestCode)
            return true
        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            gotoImage(requestCode)
        }
        if (grantResults.isNotEmpty() && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            gotoImage(requestCode)
        }
    }

    private fun gotoImage(requestCode: Int) {

        val intent: Intent

        if (requestCode == 111) {

            intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), requestCode)

        } else {

            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (takePictureIntent.resolveActivity(activity!!.packageManager) != null) {
                // Create the File where the photo should go
                var photoFile: File? = null
                try {
                    photoFile = createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    val photoURI = FileProvider.getUriForFile(context!!,
                            "com.codecaique.hiakk.fileprovider",
                            photoFile)
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, requestCode)
                }
            }

        }
    }

    @SuppressLint("SimpleDateFormat")
    @Throws(IOException::class)
    private fun createImageFile(): File? {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = activity!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",  /* suffix */
                storageDir /* directory */
        )

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.absolutePath
        return image
    }

    @SuppressLint("LongLogTag")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (Activity.RESULT_OK == resultCode) {

            Log.e("a", requestCode.toString())
            when (requestCode) {
                222 -> {
                    val bitmapFromMedia = BitmapFactory.decodeFile(currentPhotoPath)
//                    binding.image.setImageBitmap(bitmapFromMedia)
                    uri = Uri.parse(currentPhotoPath)
                }
                111 -> {
                    Log.e(TAG, requestCode.toString())
//                    binding.image.setImageURI(data!!.data!!)
                    uri = data!!.data!!
                }

            }
        }
    }

    private fun dialogAddCoupon() {

        val dialog = Dialog(activity!!)
        dialog.setContentView(R.layout.dialog_add_copoun)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.btn_done.setOnClickListener {

            if (dialog.tv_camera.text.toString().isNotEmpty()) {
                coupon = dialog.tv_camera.text.toString()
                dialog.dismiss()
            } else {
                dialog.tv_camera.error = getString(R.string.enter_your_copon)
            }

        }

        dialog.show()
    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }

}