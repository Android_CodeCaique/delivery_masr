package com.codecaique.deliverymasr.ui.client.myOrders

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.pojo.response.MyOrder
import com.codecaique.deliverymasr.ui.client.chat.ChatViewModel
import com.codecaique.deliverymasr.ui.client.stores.StoresFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.fragment_myorders.*
import kotlinx.android.synthetic.main.fragment_myorders.view.*

class MyOrdersFragment : Fragment(R.layout.fragment_myorders) {

    lateinit var myOrdersViewModel: MyOrdersViewModel
    lateinit var ordersAdapter: OrdersAdapter
    lateinit var data: ArrayList<MyOrder>

    lateinit var chatViewModel: ChatViewModel

//captaion
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        myOrdersViewModel = ViewModelProviders.of(this).get(MyOrdersViewModel::class.java)

        chatViewModel = ViewModelProviders.of(this).get(ChatViewModel::class.java)

        data = ArrayList()

        ordersAdapter = OrdersAdapter(activity!!, UserInfo(context!!).getUserToken(), UserInfo(context!!).getId()
                , chatViewModel)
        view.rec_myorders.adapter = ordersAdapter

        circularSticksLoader.visibility = View.VISIBLE
        myOrdersViewModel.myOrders(UserInfo(activity!!.applicationContext).getUserToken(), "user")
        myOrdersViewModel.myOrdersMutableLiveData.observe(this, Observer { t ->

            circularSticksLoader.visibility = View.GONE
            view.progressBar.visibility = View.GONE
            data = t.data as ArrayList<MyOrder>
            fill('a')


        })



        view.back.setOnClickListener {
            activity!!.supportFragmentManager.beginTransaction().replace(R.id.main_fragment, StoresFragment()).commit()
//            it.findNavController().navigate(R.id.notificationsFragment)
            activity!!.findViewById<BottomNavigationView>(R.id.main_nav_view).selectedItemId = R.id.navigation_Stores
        }

        view.tv_1.setOnClickListener {
            view.view1.visibility = View.VISIBLE
            view.view2.visibility = View.INVISIBLE
            fill('a')
        }

        view.tv_2.setOnClickListener {
            view.view2.visibility = View.VISIBLE
            view.view1.visibility = View.INVISIBLE
            fill('f')
        }


        view.refresh_layout.setOnRefreshListener {

            circularSticksLoader.visibility = View.VISIBLE
            myOrdersViewModel.myOrders(UserInfo(activity!!.applicationContext).getUserToken(), "user")
            myOrdersViewModel.myOrdersMutableLiveData.observe(this, Observer { t ->

                circularSticksLoader.visibility = View.GONE
                view.progressBar.visibility = View.GONE
                data = t.data as ArrayList<MyOrder>
                view.view1.visibility = View.VISIBLE
                view.view2.visibility = View.INVISIBLE
                fill('a')

            })

            view.rec_myorders.adapter = ordersAdapter
            view.refresh_layout.isRefreshing = false
        }

    }

    private fun fill(char: Char) {

        val array = ArrayList<MyOrder>()
        if (char.equals('a')) {
            data.forEach { i ->
                Log.e("TAG", i.state)
                if (i.state.trim() != "canceled" && i.state.trim() !=  "received_order" && i.rate_state == null) {
                    array.add(i)
                }
            }
        } else {
            data.forEach { i ->
                if (i.state.trim() == "canceled" || i.state.trim() ==  "received_order"|| i.rate_state != null) {
                    array.add(i)
                }
            }
        }
        ordersAdapter.setData(array)
    }

    override fun onResume() {
        super.onResume()

        myOrdersViewModel.myOrders(UserInfo(activity!!.applicationContext).getUserToken(), "user")
        myOrdersViewModel.myOrdersMutableLiveData.observe(this, Observer { t ->

            view!!.progressBar.visibility = View.GONE
            data = t.data as ArrayList<MyOrder>
            fill('a')


        })

            Log.e(this.javaClass.name, "CurrentScreen")



    }

}