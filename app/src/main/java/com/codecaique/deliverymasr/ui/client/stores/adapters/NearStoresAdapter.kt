package com.codecaique.deliverymasr.ui.client.stores.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.client.SubActivity
import com.codecaique.deliverymasr.pojo.response.NearStore
import com.codecaique.deliverymasr.ui.client.restaurant.RestaurantFragment
import com.codecaique.deliverymasr.ui.client.stores.adapters.NearStoresAdapter.ViewHolderMarkets
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.response.Places_Response
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.client.shopsNotSupported.shopLocation.ShopLocationFragment
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*
import kotlin.collections.ArrayList

class NearStoresAdapter(var context: Context) : RecyclerView.Adapter<ViewHolderMarkets>() {

    private val TAG = "NearStoresAdapter"
    var dynamicList = ArrayList<NearStore>()
    var filterList = ArrayList<NearStore>()
    var staticList = ArrayList<NearStore>()
    var googlePlaces = ArrayList<Places_Response.ResultsBean>()

    //    var googleRestaurantsList = ArrayList<Places_Response.ResultsBean>()
    var googleCafesList = ArrayList<Places_Response.ResultsBean>()
    var googleMarketsList = ArrayList<Places_Response.ResultsBean>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderMarkets {
        val view = LayoutInflater.from(context).inflate(R.layout.item_order, parent, false)
        return ViewHolderMarkets(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolderMarkets, position: Int) {

        val arrayItem = dynamicList[position]

        holder.title.text = arrayItem.name
        holder.body.text = arrayItem.address
        holder.distance.text = String.format("%.2f", arrayItem.distance.toDouble()) + "\nكم "

//        if (arrayItem.image != null && arrayItem.image.isNotEmpty())
//            Picasso.get().load(ApiCreater.STORES_URL + arrayItem.image).into(holder.image)

        if (arrayItem.id == "")
            holder.image.setImageResource(R.drawable.restaurant)
        else {
            if (arrayItem.image != null && arrayItem.image.isNotEmpty())
                Picasso.get().load(ApiCreater.STORES_URL + arrayItem.image).into(holder.image)
            else {
                holder.image.setImageResource(R.drawable.group_5)
            }
        }
        if (arrayItem.discount != "0" && arrayItem.discount.isNotEmpty() && arrayItem.discount != "null") {

            Log.e("Ssss", "NOt null")

            holder.discount.visibility = View.VISIBLE
            if (!arrayItem.discount.contains("%"))
                holder.discount.text = arrayItem.discount + " %"
            else
                holder.discount.text = arrayItem.discount

        }

        holder.itemView.setOnClickListener {

            if (arrayItem.id == "") {
                val intent = Intent(context, SubActivity::class.java)
                //StoresAdapter.fragName = R.layout.fragment_tard__hayak_
                intent.putExtra(SubActivity.KEY, SubActivity.ShopsNotSupported)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)

                intent.putExtra(ShopLocationFragment.SHOP_NAME, arrayItem.name)
                intent.putExtra("type", "store")
                intent.putExtra(ShopLocationFragment.SHOP_LATITUDE, dynamicList[position].lat.toString())
                intent.putExtra(ShopLocationFragment.SHOP_LONGITUDE, dynamicList[position].lng.toString())
                intent.putExtra("address", dynamicList[position].address)
                context.startActivity(intent)

            } else {

                val intent = Intent(context, SubActivity::class.java)
//            fragName = R.layout.fragment_kentaky_restaurant

                intent.putExtra(SubActivity.KEY, SubActivity.Restaurant)
                intent.putExtra(RestaurantFragment.NAME, arrayItem.name)
                intent.putExtra(RestaurantFragment.ID, arrayItem.id)
                intent.putExtra(RestaurantFragment.IMAGE, arrayItem.image)
                intent.putExtra(RestaurantFragment.LAT, arrayItem.lat)
                intent.putExtra(RestaurantFragment.LNG, arrayItem.lng)

                Log.e(TAG, "onBindViewHolder: lat ${arrayItem.lat}")
                Log.e(TAG, "onBindViewHolder: lng ${arrayItem.lng}")
                Log.e("RestaurantFragment$$$", "${arrayItem.id}  ${arrayItem.image}")

                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                context.startActivity(intent)
            }

        }

    }

    /*Intent intent = new Intent(holder.itemView.getContext(), DelievryPlaceActivity2.class);
        view.getContext().startActivity(intent);*/

    override fun getItemCount(): Int {
        Log.e("SIZE in ItemCount", dynamicList.size.toString() + "     ")

        return dynamicList.size
    }

    inner class ViewHolderMarkets(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView = itemView.findViewById(R.id.market_name)
        var body: TextView = itemView.findViewById(R.id.order_time)
        var distance: TextView = itemView.findViewById(R.id.coupon_cancel)
        var discount: TextView = itemView.findViewById(R.id.tv_discount)
        var image: CircleImageView = itemView.findViewById(R.id.market_logo)
    }

    fun setGoogleList(b: ArrayList<Places_Response.ResultsBean>) {
        Log.e("SIZE of setGoogleList", b.size.toString() + "     ")


        this.googlePlaces = b

        val all = (merge(dynamicList, googlePlaces))
//        dynamicList.clear()
        dynamicList=all
        Log.e(TAG, "setData: dynamicList s ${dynamicList.size}")
        notifyDataSetChanged()
    }

    fun setGoogleCafes(b: ArrayList<Places_Response.ResultsBean>) {
        this.googleCafesList = b
        Log.e("Cafes SIZE", googleCafesList.size.toString() + " ")

    }

    fun setGoogleMarket(b: ArrayList<Places_Response.ResultsBean>) {
        this.googleMarketsList = b
        Log.e("MARKET SIZE", googleMarketsList.size.toString() + " ")
    }


    fun setData(a: ArrayList<NearStore>) {
        this.dynamicList = a
        this.staticList = a

        Log.e("SIZE ofSetData", dynamicList.size.toString() + "     ")


        staticList.forEach { t ->
            Log.e("AAAa", t.category_name)
        }

        dynamicList = (merge(dynamicList, googlePlaces))


        Log.e(TAG, "setData: dynamicList s ${dynamicList.size}")

        notifyDataSetChanged()
    }


    fun filter(catName: String) {

        var places_google: ArrayList<Places_Response.ResultsBean> = ArrayList<Places_Response.ResultsBean>()


        val category: String

        dynamicList = ArrayList<NearStore>()

        Log.e("NAME 20008", catName + "  :  " + context.getString(R.string.all))


        if (catName.equals(context.getString(R.string.all))) {

            dynamicList = staticList
            notifyDataSetChanged()


            Log.e("SIZE of dynamic", dynamicList.size.toString() + "    ")
            Log.e("SIZE of static", staticList.size.toString() + "    ")

            return
        }

        staticList.forEach {
            if (it.category_name.contains(catName))
                dynamicList.add(it)
        }



        when (catName) {
            "مطاعم" -> {
                dynamicList = merge(dynamicList, googlePlaces)
                Log.e("REStaurant filter ", dynamicList.size.toString() + "  ")
            }
            "سوبر ماركت" -> {
                dynamicList = merge(dynamicList, googleMarketsList)
                Log.e("Market filter ", dynamicList.size.toString() + "  ")
            }
            "كافيه و مشروبات" -> {
                dynamicList = merge(dynamicList, googleCafesList)
                Log.e("Cafe filter ", dynamicList.size.toString() + "  ")
            }
        }


        notifyDataSetChanged()
    }

    fun search(name: String) {

        dynamicList = ArrayList<NearStore>()

        Log.e("NAME 20008", name + "  :  " + context.getString(R.string.all))


        if (name.equals(context.getString(R.string.all))) {

            dynamicList = staticList
            notifyDataSetChanged()


            Log.e("SIZE of dynamic", dynamicList.size.toString() + "    ")
            Log.e("SIZE of static", staticList.size.toString() + "    ")

            return
        }

        staticList.forEach {
            if (it.name.toLowerCase(Locale.ROOT).contains(name.toLowerCase(Locale.ROOT)))
                dynamicList.add(it)
        }



        when (name) {
            "مطاعم" -> {
                dynamicList = merge(dynamicList, googlePlaces)
                Log.e("REStaurant filter ", dynamicList.size.toString() + "  ")
            }
            "سوبر ماركت" -> {
                dynamicList = merge(dynamicList, googleMarketsList)
                Log.e("Market filter ", dynamicList.size.toString() + "  ")
            }
            "كافيه و مشروبات" -> {
                dynamicList = merge(dynamicList, googleCafesList)
                Log.e("Cafe filter ", dynamicList.size.toString() + "  ")
            }
        }


        notifyDataSetChanged()
    }

//    fun setGoogleRestaurants(b: ArrayList<Places_Response.ResultsBean>)
//    {
//        this.googleRestaurantsList = b
//        Log.e("Restaurants SIZE" , googleRestaurantsList.size.toString() + " ")
//    }


    private fun merge(staticList: ArrayList<NearStore>, placesGoogle: ArrayList<Places_Response.ResultsBean>): ArrayList<NearStore> {

        Log.e("Size of google", placesGoogle.size.toString() + "  ")
        Log.e("Size of List", staticList.size.toString() + "  ")

        val lat1: Double = UserInfo(context).getLatitude().toDouble()
        val lng1: Double = UserInfo(context).getLongitude().toDouble()


        val list2 = placesGoogle.sortedWith(compareBy {
            getDistanceFromLatLonInKm(lat1, lng1, it.geometry.location.lat, it.geometry.location.lng)
        })

        for (list in list2) {
            Log.e("LISt params", list.name + "  : " +
                    getDistanceFromLatLonInKm(lat1, lng1, list.geometry.location.lat, list.geometry.location.lng))
        }

        list2.forEach {

            val lat2: Double = it.geometry.location.lat
            val lng2: Double = it.geometry.location.lng

            staticList.add(NearStore(it.vicinity, it.types.toString(), getDistanceFromLatLonInKm(lat1, lng1, lat2, lng2).toString(), "", "", "", it.name, it.geometry.location.lat, it.geometry.location.lng))
        }

        Log.e("SIZE aaaa", staticList.size.toString() + "      ")


        staticList.sortedWith(compareBy {
            it.distance.toDouble()
        })


        Log.e(TAG, "merge() returned: ${staticList.size}")
        return staticList
    }


    fun getDistanceFromLatLonInKm(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
        val r = 6371
        val dLat = deg2rad(lat2 - lat1)  // deg2rad below
        val dLon = deg2rad(lon2 - lon1);
        val a =
                Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                        Math.sin(dLon / 2) * Math.sin(dLon / 2)
        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        val d = r * c; // Distance in km
        return d
    }

    fun deg2rad(deg: Double): Double {
        return deg * (Math.PI / 180)
    }


}