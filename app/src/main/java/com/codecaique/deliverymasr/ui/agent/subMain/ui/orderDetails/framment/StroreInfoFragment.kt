package com.codecaique.deliverymasr.ui.agent.subMain.ui.orderDetails.framment.store_info_fragment

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_strore_info.view.*

/**
 * A simple [Fragment] subclass.
 */
class StroreInfoFragment : Fragment(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    lateinit var viewModel: Store_Info_ViewModel

    lateinit var VIEW: View
    lateinit var user: UserInfo
    lateinit var shop_id: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        VIEW = inflater.inflate(R.layout.fragment_strore_info, container, false)
        return VIEW
    }

    @SuppressLint("ResourceAsColor")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mapFragment = //(context!! as AppCompatActivity).supportFragmentManager
                childFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment
        mapFragment.getMapAsync(this)

        user = UserInfo(activity!!)
        shop_id = activity!!.intent.extras!!.getString("shop_id", "0")
        Log.e("shop_id", shop_id)

        val token: String = user.getUserToken()
        Log.e("token", token)

        viewModel = ViewModelProviders.of(this).get(Store_Info_ViewModel::class.java)

        var b = true
        view.con1.setOnClickListener {
            if (b) {
                view.tv_count.setTextColor(Color.parseColor("#ffffff"))
                view.tv_name.setTextColor(Color.parseColor("#ffffff"))
                view.tv_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_circle_black_24dp, 0)
                view.con1.setBackgroundResource(R.drawable.bg_cornor_primary_dark)
                viewModel.addShopAgent(token, shop_id)
                viewModel.addShopData.observe(this, Observer {
                    if (it.error == 0)
                        Toast.makeText(context, "You have been added successfully to this store", Toast.LENGTH_SHORT).show()

                })
            } else {
                view.tv_count.setTextColor(R.color.colorPrimaryDark)
                view.tv_name.setTextColor(R.color.colorPrimaryDark)
                view.tv_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.right_1, 0)
                view.con1.setBackgroundResource(R.drawable.bordered_circle_white)

            }
            b = !b


        }

    }

    @SuppressLint("SetTextI18n", "ResourceAsColor")
    override fun onMapReady(googleMap: GoogleMap?) {

        mMap = googleMap!!
        mMap.mapType = GoogleMap.MAP_TYPE_NORMAL

        Log.e("data_", "${user.getUserToken()}  ${shop_id.toInt()}")
        viewModel.showShopById(user.getUserToken(), shop_id.toInt())

        viewModel.shopMutableLiveData.observe(this, Observer {


            Log.e("data_", "${it.data.agent_num}  ${it.data.description}  ${it.data.latitude} ${it.data.longitude}")

            val latLng = LatLng(it.data.latitude.toDouble(), it.data.longitude.toDouble())

            mMap.addMarker(MarkerOptions().position(latLng).title(it.data.name))

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))


            VIEW.tv_count.text = it.data.agent_num.toString() + " مندوب متواجدين"
            VIEW.tv_description.text=it.data.description

            if (it.i_agent == 1) {
                VIEW.tv_count.setTextColor(Color.parseColor("#ffffff"))
                VIEW.tv_name.setTextColor(Color.parseColor("#ffffff"))
                VIEW.tv_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_circle_black_24dp, 0)
                VIEW.con1.setBackgroundResource(R.drawable.bg_cornor_primary_dark)

            } else {
                VIEW.tv_count.setTextColor(R.color.colorPrimaryDark)
                VIEW.tv_name.setTextColor(R.color.colorPrimaryDark)
                VIEW.tv_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.right_1, 0)
                VIEW.con1.setBackgroundResource(R.drawable.bordered_circle_white)

            }

        })

    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}
