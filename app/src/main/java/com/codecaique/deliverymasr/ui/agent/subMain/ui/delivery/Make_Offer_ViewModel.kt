package com.codecaique.deliverymasr.ui.agent.subMain.ui.delivery

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codecaique.deliverymasr.pojo.response.GeneralResponse
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.response.ShowOrderResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.RequestBody
import io.reactivex.Observer
import io.reactivex.disposables.Disposable


class Make_Offer_ViewModel : ViewModel()
{
    val makeOfferData = MutableLiveData<GeneralResponse>()

    fun makeOffer(token: String,request_id: String,price : Int,lang : String)
    {
        val observable =
                ApiCreater.instance.makeOffer(
                        RequestBody.create(MediaType.parse("multipart/form-data"), token)
                        ,RequestBody.create(MediaType.parse("multipart/form-data"), request_id)
                        ,price
                        ,RequestBody.create(MediaType.parse("multipart/form-data"), lang))
                   //    .subscribeOn(Schedulers.io())
                  //      .observeOn(AndroidSchedulers.mainThread())

        val observer = object : Observer<GeneralResponse>
        {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {

            }

            override fun onNext(t: GeneralResponse) {
                makeOfferData.value = t
            }

            override fun onError(e: Throwable) {
                DeliveryFragment().loadingDialog.dismiss()
                Toast.makeText(DeliveryFragment().context, "Connection Error \n" + e.message, Toast.LENGTH_SHORT).show()

            }
        }
//        observable.subscribe(observer)
    }




    val showOrderMutableLiveData = MutableLiveData<ShowOrderResponse>()

    fun showOrderById(token: String, request_id: Int) {

        val observable =
                ApiCreater.instance.orderById(token, request_id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        Log.e("ORder by"," done ")

        Log.e("TOKEn sasa",token +" done ")

        Log.e("ID sasa",request_id.toString() +" done ")

        val observer = object : Observer<ShowOrderResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: ShowOrderResponse) {
                Log.e("SIZE sss",t.message +" ssss  ")

                Log.e("ORder by in"," done ")


                showOrderMutableLiveData.value = t



            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)

                Log.e("ORder by"," done 222")

            }
        }

        observable.subscribe(observer)

    }



}