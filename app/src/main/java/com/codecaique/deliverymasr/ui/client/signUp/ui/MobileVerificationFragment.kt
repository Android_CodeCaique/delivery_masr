package com.codecaique.deliverymasr.ui.client.signUp.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.telephony.SmsManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.codecaique.deliverymasr.BuildConfig
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.client.MainActivity
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import com.codecaique.deliverymasr.ui.client.signUp.SignUpViewModel
import com.google.firebase.iid.FirebaseInstanceId
import io.reactivex.plugins.RxJavaPlugins
import kotlinx.android.synthetic.main.fragment_mobile_verification.view.*
import okhttp3.MediaType
import okhttp3.RequestBody
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 */
class MobileVerificationFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_mobile_verification,
                container, false)
    }

    lateinit var signUpViewModel: SignUpViewModel
    lateinit var loadingDialog: LoadingDialog
    private val TAG="MobileVerificationFragment"

    @SuppressLint("LongLogTag")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        RxJavaPlugins.setErrorHandler { throwable: Throwable? -> }


        signUpViewModel = ViewModelProviders.of(this).get(SignUpViewModel::class.java)
        loadingDialog = LoadingDialog(activity!!)

        if (BuildConfig.DEBUG) {
            view.et_phone.hint = "01234567897"
        }

        val spinnerAdapter = ArrayAdapter(
                activity!!.applicationContext,
                R.layout.spinner_item, R.id.text,
                ArrayList<String>(signUpViewModel.getCountries().keys)
        )


        view.country_spinner.adapter = spinnerAdapter

        view.country_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                view.phone_code.text = signUpViewModel.getCountries()["مصر"]
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                view.phone_code.text = signUpViewModel.getCountries()[view.country_spinner.selectedItem.toString()]
            }
        }

        var isSigned = false

        view.mobile_verification_btn.setOnClickListener {

            try {
                val phone = view.et_phone.text.toString()
                val country =/*"1"*/ view.country_spinner.selectedItem.toString()

                if (phone.isNotEmpty()) {

                    Log.e("PHONE sa","sasdasr"/*view.phone_code.text.toString() + phone*/)

                    view.mobile_verification_btn.isEnabled = false
                    view.progress_bar.visibility = View.VISIBLE
                    loadingDialog.show()

                    signUpViewModel.phoneSignUpClient(
                            RequestBody.create(MediaType.parse("multipart/form-data"), country),
                            RequestBody.create(MediaType.parse("multipart/form-data"),
                                    "${view.phone_code.text}$phone"),
                            RequestBody.create(MediaType.parse("multipart/form-data"),
                            FirebaseInstanceId.getInstance().token),
                            view.progress_bar,context!!,view
                    )

                    signUpViewModel.signUpMutableLiveData.observe(this, Observer { t ->
                        loadingDialog.dismiss()

                        val smsManager = SmsManager.getDefault()
//                    smsManager.sendTextMessage(phone, null, "12343", null, null);

                        if (t.error == 0) {
                            /*  val b = Bundle()
                          b.putString(Fragment_Code_Verification.PHONE, view.phone_code.text.toString() + phone)
                          b.putBoolean("isSigned",isSigned)
                          UserInfo(activity!!.applicationContext).setUserData(t.data.id, t.data.phone, "user","")
                          it.findNavController().navigate(R.id.action_fragment_Mobile_Verification2_to_fragment_Code_Verification, b)

                          //     navController.navigate(R.id.action_fragment_Mobile_Verification2_to_fragment_SignUp)

                      } else {
                          isSigned = true
  //                        if (t.data.state=="user") {

                              val b = Bundle()
                              b.putString(Fragment_Code_Verification.PHONE, view.phone_code.text.toString() + phone)
                              b.putBoolean("isSigned",isSigned)
                              b.putSerializable("data",t.data)
                              it.findNavController().navigate(R.id.action_fragment_Mobile_Verification2_to_fragment_Code_Verification, b)*/

/*
                        } else {
                            Toast.makeText(activity!!.applicationContext, "رقم التليفون موجود بالفعل", Toast.LENGTH_LONG).show()
*/
                            Log.e("MOB USER", "NEW PHONE")

                            UserInfo(activity!!.applicationContext).setUserData(t.data.id, t.data.phone, "user", "")
                            Log.e(TAG, "onViewCreated: data.id ${t.data.id}")
                            val bundle = Bundle()
                            bundle.putString("code", "+2")
                            bundle.putString("phone", phone)

                            Log.e("CODE verify", view.phone_code.text.toString())

                            it.findNavController().navigate(R.id.fragment_SignUp, bundle)

                        } else {
                            view.mobile_verification_btn.isEnabled = true
                            view.progress_bar.visibility = View.GONE
                            UserInfo(activity!!.applicationContext).setUserData(t.data)
                            UserInfo(activity!!.applicationContext).setState("user")
//                            activity!!.startActivity(Intent(activity!!.applicationContext, MainActivity::class.java))
                            val intent = Intent(activity!!, MainActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                            activity!!.startActivity(intent)


//                        }

                        }

                    })

                } else {

                    view.et_phone.error = getString(R.string.please_enter_phone)

                }


            }catch (e:Exception)
            {
                Toast.makeText(requireContext(),e.message,Toast.LENGTH_SHORT).show()
            }
        }


        /*mobile_verification_btn.setOnClickListener {
            navController.navigate(R.id.action_fragment_Mobile_Verification2_to_fragment_SignUp)
        }*/

    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}