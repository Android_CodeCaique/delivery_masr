package com.codecaique.deliverymasr.ui.client.deliveryOffers

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codecaique.deliverymasr.pojo.response.*
import com.codecaique.deliverymasr.data.ApiCreater
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import com.codecaique.deliverymasr.ui.client.deliveryOffers.OffersAdapter as OffersAdapter

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class OffersViewModel : ViewModel() {
    private val TAG = "OffersViewModel"
    val offersMutableLiveData = MutableLiveData<ShowOffersResponse>()
    val offersDeliveryMutableLiveData = MutableLiveData<DeliveryOffers_Response>()
    val acceptMutableLiveData=MutableLiveData<BillResponse>()
    val turnNotificationsMutableLiveData = MutableLiveData<TurnNotificationResponse>()

    fun turnNotifications(token: String) {

        val observable =
                ApiCreater.instance.turnOnNotifications(token)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<TurnNotificationResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: TurnNotificationResponse) {

                turnNotificationsMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }

    fun getOffers(token: String) {

        val observable =
                ApiCreater.instance.showOffers(token)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<ShowOffersResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: ShowOffersResponse) {
                Log.e(TAG, "onNext: offersMutableLiveData --- >"+t)
                offersMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }

    fun getOffersDel(token: String) {

        val observable =
                ApiCreater.instance.showOffersDel(token)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<DeliveryOffers_Response> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: DeliveryOffers_Response) {

                offersDeliveryMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }

    fun acceptOffer(token: String,offer_id:String,context: Context) {

        Log.e("data", "${token} a ${offer_id}")

        val observable =
                ApiCreater.instance.userAcceptOffer(token,offer_id.toInt())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<BillResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: BillResponse) {

                acceptMutableLiveData.value = t
                OffersAdapter(context,this@OffersViewModel).loadingDialog.dismiss()

            }

            override fun onError(e: Throwable) {
                Log.e("ss da a", e.message)
                OffersAdapter(context,this@OffersViewModel).loadingDialog.dismiss()
                Toast.makeText(context," " + e.message, Toast.LENGTH_SHORT).show()
            }
        }

        observable.subscribe(observer)

    }

}