package com.codecaique.deliverymasr.ui.client.shopsNotSupported.selectLocation

import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.databinding.FragmentShareLocationBinding
import com.codecaique.deliverymasr.pojo.util.LocationInfo
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import java.io.IOException
import java.util.*

class SelectLocationActivity : AppCompatActivity(), OnMapReadyCallback {

    lateinit var binding: FragmentShareLocationBinding
    private lateinit var mMap: GoogleMap
    lateinit var myLocation: LatLng
    lateinit var locationInfo: LocationInfo
    val TAG = "SelectLocationActivity"

    companion object{

        const val LATITUDE = "param1"
        const val LONGITUDE = "param2"
        const val STREET = "param3"

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(
                this, R.layout.fragment_share_location)

        locationInfo = LocationInfo(applicationContext)
        myLocation = locationInfo.getMyLocation()

        val mapFragment = //(context!! as AppCompatActivity).supportFragmentManager
                supportFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment
        mapFragment.getMapAsync(this)

        binding.btn.setOnClickListener {

            Log.e(TAG, "onCreate: btn.setOnClickListener s " )

            val lat = myLocation.latitude.toString()
            val lng = myLocation.longitude.toString()
            val street = if (binding.etPlace.text.toString().isNotEmpty())
                binding.etPlace.text.toString() + " " + binding.tvPlace.text.toString()
            else
                binding.tvPlace.text.toString()

            Log.e(TAG, "onCreate: btn.setOnClickListener m " )

            val returnIntent = Intent()
            returnIntent.putExtra(LATITUDE, lat)
            returnIntent.putExtra(LONGITUDE, lng)
            returnIntent.putExtra(STREET, street)
            setResult(RESULT_OK, returnIntent)
            finish()
            Log.e(TAG, "onCreate: btn.setOnClickListener e " )


        }


        binding.typeSatlite.setOnClickListener {

            mMap.mapType = GoogleMap.MAP_TYPE_SATELLITE

            binding.typeMap.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorAccent))
            binding.typeSatlite.setTextColor(ContextCompat.getColor(applicationContext, R.color.white))

            binding.typeSatlite.background = ContextCompat.getDrawable(applicationContext, R.drawable.border_left_fill)
            binding.typeMap.background = ContextCompat.getDrawable(applicationContext, R.drawable.border_right)


        }

        binding.typeMap.setOnClickListener {

            mMap.mapType = GoogleMap.MAP_TYPE_NORMAL

            binding.typeMap.setTextColor(ContextCompat.getColor(applicationContext, R.color.white))
            binding.typeSatlite.setTextColor(ContextCompat.getColor(applicationContext, R.color.colorAccent))

            binding.typeSatlite.background = ContextCompat.getDrawable(applicationContext, R.drawable.border_left)
            binding.typeMap.background = ContextCompat.getDrawable(applicationContext, R.drawable.border_right_fill)


        }


    }


    override fun onMapReady(googleMap: GoogleMap?) {

        mMap = googleMap!!
        binding.progressBar.visibility = View.GONE
        mMap.mapType = GoogleMap.MAP_TYPE_NORMAL

        val geocode = Geocoder(applicationContext, Locale("ar"))//.getDefault())
        var addresses: List<Address>?
        myLocation = locationInfo.getMyLocation()

        addresses = geocode.getFromLocation(myLocation.latitude, myLocation.longitude, 1)

        mMap.addMarker(MarkerOptions().position(myLocation))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 15f))

        if (addresses != null && addresses.isNotEmpty()) {

            binding.tvPlace.text = addresses[0].getAddressLine(0)//thoroughfare

        }

        mMap.setOnMapClickListener { latLng ->

            myLocation = latLng
            Log.e("location ====== ", "${latLng.latitude} uu")

            mMap.clear()

            mMap.addMarker(MarkerOptions().position(myLocation))//.title("Marker in Sydney"))
            mMap.moveCamera(CameraUpdateFactory.newLatLng(myLocation))

            try {

                addresses = geocode.getFromLocation(latLng.latitude, latLng.longitude, 1)

                if (addresses != null && addresses!!.isNotEmpty()) {
                    binding.tvPlace.text = addresses!![0].getAddressLine(0)//thoroughfare
                }

            } catch (ignored: IOException) {
                //do something
            }
        }


    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}