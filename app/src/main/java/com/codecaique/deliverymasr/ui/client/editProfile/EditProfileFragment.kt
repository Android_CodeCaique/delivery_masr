package com.codecaique.deliverymasr.ui.client.editProfile

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.client.MainActivity
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import com.codecaique.deliverymasr.data.ApiCreater
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_edit_ptofile.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class EditProfileFragment : Fragment() {

    lateinit var loadingDialog: LoadingDialog
    lateinit var editProfileViewModel: EditProfileViewModel
    var uri: Uri? = null
    lateinit var v: View
    lateinit var userInfo: UserInfo


    @SuppressLint("NewApi", "ResourceAsColor")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_edit_ptofile, container, false)

        editProfileViewModel = ViewModelProviders.of(this).get(EditProfileViewModel::class.java)
        userInfo = UserInfo(activity!!.applicationContext)

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true/*  enabled by default*/) {
            override fun handleOnBackPressed() {
                // Handle the back button event
                requireActivity().finish()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)


        loadingDialog = LoadingDialog(activity!!)

        loadData(v)

        /*v.iv_backEditProfile.setOnClickListener {
            activity!!.finish()
        }*/

        var gender = userInfo.getGender()

        if (gender == 1) {

            v.tv_female.background = resources.getDrawable(R.drawable.border_left_fill, null)
            v.tv_male.background = resources.getDrawable(R.drawable.border_right, null)
            v.tv_male.setTextColor(ContextCompat.getColor(activity!!, R.color.color_5))
            v.tv_female.setTextColor(ContextCompat.getColor(activity!!, R.color.white))

        } else {
            v.tv_male.background = resources.getDrawable(R.drawable.border_right_fill, null)
            v.tv_female.background = resources.getDrawable(R.drawable.border_left, null)
            v.tv_female.setTextColor(ContextCompat.getColor(activity!!, R.color.color_5))
            v.tv_male.setTextColor(ContextCompat.getColor(activity!!, R.color.white))
        }


        v.tv_male.setOnClickListener {

            gender = 1
            v.tv_male.setTextColor(ContextCompat.getColor(activity!!, R.color.white))
            v.tv_female.setTextColor(ContextCompat.getColor(activity!!, R.color.color_5))

            v.tv_male.background = resources.getDrawable(R.drawable.border_right_fill, null)
            v.tv_female.background = resources.getDrawable(R.drawable.border_left, null)

        }

        v.tv_female.setOnClickListener {

            gender = 2

            v.tv_female.setTextColor(ContextCompat.getColor(activity!!, R.color.white))
            v.tv_male.setTextColor(ContextCompat.getColor(activity!!, R.color.color_5))

            v.tv_female.background = resources.getDrawable(R.drawable.border_left_fill, null)
            v.tv_male.background = resources.getDrawable(R.drawable.border_right, null)

        }

        v.user_Edit_Photo.setOnClickListener {
            isStoragePermissionGranted()
        }

        v.btn_SaveEditProfile.setOnClickListener {

            val name = v.et_user_Edit_Name.text.toString()
            val email = v.et_user_Edit_Email.text.toString()
            val whatsNo = v.et_user_Edit_whats.text.toString()

            if (name.isNotEmpty() && email.isNotEmpty() && whatsNo.isNotEmpty()) {
                loadingDialog.show()

                editProfileViewModel.enterUserData(
                        RequestBody.create(MediaType.parse("multipart/form-data"), userInfo.getUserToken()),
                        name,
                        RequestBody.create(MediaType.parse("multipart/form-data"), name),
                        RequestBody.create(MediaType.parse("multipart/form-data"), ""),
                        if (uri != null) getImage() else null,
                        RequestBody.create(MediaType.parse("multipart/form-data"), whatsNo),
                        RequestBody.create(MediaType.parse("multipart/form-data"), email),
                        gender
                )

                editProfileViewModel.enterUserDataMutableLiveData.observe(this, Observer { t ->

                    when (t.error) {
                        0 -> {
                            userInfo.setUserData(
                                    t.data
                            )
                            userInfo.setGender(gender)
                            Toast.makeText(context, getString(R.string.edit_done), Toast.LENGTH_LONG).show()
//                            it.findNavController().navigate(R.id.action_fragment_Mobile_Verification2_to_fragment_Code_Verification)
                            val intent = Intent(context, MainActivity::class.java)
                            startActivity(intent)
                        }
                        5 -> {
                            v.et_user_Edit_Email.error = getString(R.string.email_exist)
                        }
                        else -> {
                            Log.e("aaaaaaa", t.message)
                            Toast.makeText(context, getString(R.string.may_error), Toast.LENGTH_LONG).show()
                        }
                    }

                    loadingDialog.dismiss()
                    v.btn_SaveEditProfile.isEnabled = true

                })

            } else {
                if (name.isEmpty())
                    v.et_user_Edit_Name.error = getString(R.string.sould_be_not_empty)
                if (email.isEmpty())
                    v.et_user_Edit_Email.error = getString(R.string.sould_be_not_empty)
                if (whatsNo.isEmpty())
                    v.et_user_Edit_whats.error = getString(R.string.sould_be_not_empty)
            }

        }
        return v
    }

    private fun loadData(view: View?) {

        if (userInfo.getImage().isNotEmpty())
            Picasso.get().load(ApiCreater.USER_URL + userInfo.getImage()).into(view!!.user_Edit_Photo)

        view!!.et_user_Edit_Name.setText(userInfo.getName())
        view.et_user_Edit_Email.setText(userInfo.getEmail())
        view.et_user_Edit_whats.setText(userInfo.getPhone())


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 111 && Activity.RESULT_OK == resultCode) {
            data?.let {
                v.user_Edit_Photo.setImageURI(data.data!!)
                uri = data.data!!
            }
        }
    }

    fun getImage(): MultipartBody.Part? {
        val p: String
        val cursor = activity!!.contentResolver.query(uri!!, null, null, null, null)

        p = if (cursor == null) {
            uri!!.path.toString()
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            cursor.getString(idx)
        }

        val file = File(p)
        val requestFile = RequestBody.create(MediaType.parse("image/*"), file)
        return MultipartBody.Part.createFormData("image", file.name, requestFile)
    }

    @SuppressLint("WrongConstant")
    fun isStoragePermissionGranted(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                gotoImage()

                true
            } else {

                ActivityCompat.requestPermissions(
                        activity!!,
                        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        111
                )
                false
            }
        } else {

            gotoImage()
            return true
        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            gotoImage()
        } else {
            /*
        Toast.makeText(applicationContext, R.string.imagePprem_accept, Toast.LENGTH_SHORT)
                .show()
*/
        }
    }

    private fun gotoImage() {
        val intent = Intent(
                Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 111)
    }

    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}