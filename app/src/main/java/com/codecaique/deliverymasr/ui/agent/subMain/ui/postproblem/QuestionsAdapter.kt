package com.codecaique.deliverymasr.ui.agent.subMain.ui.postproblem

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.request.AnswerRequest
import com.codecaique.deliverymasr.pojo.response.Question
import org.json.JSONArray
import org.json.JSONObject

class QuestionsAdapter(var context: Context) : RecyclerView.Adapter<QuestionsAdapter.ViewHolderCoupon>() {

    var list = ArrayList<Question>()
    var answer = ArrayList<AnswerRequest>()
    private val TAG = "QuestionsAdapter"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderCoupon {
        val view = LayoutInflater.from(context).inflate(R.layout.item_proplem_quetions, parent, false)
        return ViewHolderCoupon(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolderCoupon, position: Int) {

        val arrayItem = list[position]

        holder.text.text = arrayItem.name

        Log.e(TAG, "onBindViewHolder: ${arrayItem.name}")

        holder.radioGroup.setOnCheckedChangeListener { p0, p1 ->

            Log.e("dtatata", answer[position].answer)

            when (p1) {
                R.id.rb_yes -> answer[position].answer = "نعم"
                else -> answer[position].answer = " لا "
            }
            Log.e(TAG, answer[position].answer)

        }


    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class ViewHolderCoupon(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var text: TextView = itemView.findViewById(R.id.text)
        var radioGroup: RadioGroup = itemView.findViewById(R.id.rg)
    }

    fun setData(a: ArrayList<Question>) {
        this.list = a

        Log.e(TAG, "setData: Question ${a.size}")

        a.forEach {

            answer.add(AnswerRequest(" نعم ", it.id))
        }

        notifyDataSetChanged()
    }

    fun getAnswers(): JSONArray {
        val jsonArray = JSONArray()
        answer.forEach {
            val jsonObject = JSONObject()
            jsonObject.put("answer", it.answer)
            jsonObject.put("id", it.id)
            jsonArray.put(jsonObject)
            Log.e("dtatata", it.answer)
        }

        return jsonArray
    }

}