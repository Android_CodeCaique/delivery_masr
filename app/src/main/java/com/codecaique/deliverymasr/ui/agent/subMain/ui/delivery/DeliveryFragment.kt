package com.codecaique.deliverymasr.ui.agent.subMain.ui.delivery


import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.RadioGroup
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.SharedHelper
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.response.GeneralResponse
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.ui.agent.main.dialogs.OrderSentDialog
import com.codecaique.deliverymasr.ui.agent.subMain.ui.delivery.direction.drawroute
import com.codecaique.deliverymasr.ui.client.loading.LoadingDialog
import com.facebook.FacebookSdk.getApplicationContext
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_delivery.*
import kotlinx.android.synthetic.main.fragment_delivery.view.*
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DecimalFormat
import java.util.*
import kotlin.math.roundToInt

/**
 * A simple [Fragment] subclass.
 */
@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class DeliveryFragment : Fragment(), OnMapReadyCallback {

    val TAG = "DeliveryFragment"

    lateinit var viewModel: Make_Offer_ViewModel
    lateinit var userInfo: UserInfo
    lateinit var VIEW: View
    lateinit var loadingDialog: LoadingDialog
    var payment: String = ""
   var priceoffer: Int = 0

    private var mapFragment: SupportMapFragment? = null

    val PATTERN_DASH_LENGTH_PX = 20
    val PATTERN_GAP_LENGTH_PX = 20
    val DOT: PatternItem = Dot()
    val DASH: PatternItem = Dash(PATTERN_DASH_LENGTH_PX.toFloat())
    val GAP: PatternItem = Gap(PATTERN_GAP_LENGTH_PX.toFloat())
    val PATTERN_POLYGON_ALPHA: List<PatternItem> = Arrays.asList(GAP, DASH)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        VIEW = inflater.inflate(R.layout.fragment_delivery, container, false)

        activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

        return VIEW
    }

    private lateinit var mMap: GoogleMap
    var requestId = ""

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

        viewModel = ViewModelProviders.of(this).get(Make_Offer_ViewModel::class.java)
        userInfo = UserInfo(activity!!.applicationContext)

         mapFragment = //(context!! as AppCompatActivity).supportFragmentManager
                childFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment
        mapFragment!!.getMapAsync(this)

        requestId = try {
            arguments!!.getString("requestId", "0")

        } catch (e: Exception) {
            activity!!.intent.extras!!.getString("requestId", "0")

        }

        Log.e("request_id", requestId)

        val token: String = UserInfo(activity!!).getUserToken()
        Log.e("token", token)

        VIEW.price_et.addTextChangedListener(object : android.text.TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s?.let {
                    if (s.length > 0) {
                        val precentage: Float = s.toString().toFloat()
                        VIEW.tv_cost.text = " هتستلم "+((precentage + (precentage * 20/100)).roundToInt()).toString() + " جنيه من العميل "
                        priceoffer=((precentage + (precentage * 20/100)).roundToInt())
                    }
                    if (s.length == 0)
                        VIEW.tv_cost.text = " "
                }
            }
        })


        Log.e("PAYMENT", payment)


        view.buttoConfirm.setOnClickListener {

            val price: String = view.price_et.text.toString()


            if (price.isNotEmpty()) {
                loadingDialog = LoadingDialog(activity!!)
                loadingDialog.show()

                ApiCreater.instance.makeOffer(
                        RequestBody.create(MediaType.parse("multipart/form-data"), token)
                        , RequestBody.create(MediaType.parse("multipart/form-data"), requestId)
                        , (priceoffer)
                        , RequestBody.create(MediaType.parse("multipart/form-data"), SharedHelper().getKey(activity!!, "LANG"))
                )
                        .enqueue(object : Callback<GeneralResponse> {
                            override fun onFailure(call: Call<GeneralResponse>, t: Throwable) {
                                Toast.makeText(activity!!, getString(R.string.internt_error), LENGTH_SHORT).show()
                            }

                            override fun onResponse(call: Call<GeneralResponse>, response: Response<GeneralResponse>) {

                                val it1 = response.body()!!
                                when (it1.error) {
                                    0 -> {

                                        loadingDialog.dismiss()

                                        val dialog = OrderSentDialog(activity!!)
                                        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                                        dialog.show()
                                    }
                                    5 -> {
                                        loadingDialog.dismiss()

                                        Log.e(TAG, "onViewCreated: ${it1.message}")
                                        Log.e("ERROR 5", it1.message + "   ")
                                        view.price_et.error = it1.message + "  "
                                    }
                                    else -> {
                                        loadingDialog.dismiss()
                                        Toast.makeText(context, "Connection Error \n" + it1.message, LENGTH_SHORT).show()
                                    }
                                }
                            }
                        })

                /*    viewModel.makeOffer(token, requestId, price)
                    viewModel.makeOfferData.observe(viewLifecycleOwner, Observer {

                    })
    */

            } else {
                if (price.isEmpty()) {
                    view.price_et.error = getString(R.string.sould_be_not_empty)
                    return@setOnClickListener
                }
//                if (price.toInt() < 15) {
//                    view.price_et.error = getString(R.string.should_geater_15)
//                    return@setOnClickListener
//                }
            }
        }

        view.iv_loc.setOnClickListener {
            activity!!.finish()
        }

    }

    @SuppressLint("SetTextI18n", "MissingPermission")
    override fun onMapReady(googleMap: GoogleMap?) {

         mMap = googleMap!!
        //    mMap.mapType = GoogleMap.MAP_TYPE_NORMAL
        mMap.uiSettings.isZoomGesturesEnabled=true
      //  mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID)




        img_map.setOnClickListener {

            checked()

        }


        viewModel.showOrderById(userInfo.getUserToken(), requestId.toInt())

        Log.e("TOKEN sss", userInfo.getUserToken() + "    ")

        viewModel.showOrderMutableLiveData.observe(this, Observer {

            Log.e("SIZE sss", it.data.size.toString() + "   ")
            Log.e(TAG, "onMapReady: ${it.data}")

            try {

                Log.e(TAG, "onMapReady: t11")
            if (it.data[0].shope_image != null && it.data[0].shope_image.isNotEmpty())
                Picasso.get().load(ApiCreater.STORES_URL + it.data[0].shope_image).into(VIEW.img)

                Log.e(TAG, "onMapReady: t22")
            }catch (e:Exception){
                Log.e(TAG, "onMapReady: t33")
            }



            try {

                VIEW.tv_name.text = it.data[0].name
//            VIEW.tv_time.text = it.data[0].delivery_time
                VIEW.textMarketAddress.text = it.data[0].delivery_address + " "
                VIEW.textAddress.text = it.data[0].receiving_address + " "
//            view.textAddress.text=it.data[0].
                VIEW.tv_time.text = "${it.data[0].delivery_time}ساعات "

                payment = it.data[0].payment_id
                Log.e(TAG, "onMapReady: try" )

                Log.e("distance", it.data[0].client_shope_distance)
            } catch (e: Exception) {
                Log.e(TAG, "onMapReady: catch" )
            }


            try {

            if (it.data[0].client_shope_distance.toDouble() > 1) {
                Log.e("IF distance", "true")
                if (payment == "1") {
                  //  VIEW.price_et.hint = "ادخل 18 ج فاكثر"
                    Log.e("PAYMENT 222", payment + "    ")
                }
                if (payment == "2") {
                    Log.e("PAYMENT 222", payment + "    ")
                //    VIEW.price_et.hint = "ادخل 22 ج فاكثر"
                }
            } else
               // VIEW.price_et.hint = "ادخل 15 ج فقط"
                Log.e("PAYMENT 222", payment + "    ")
            }catch (e:Exception){}


            val latLng = LatLng(it.data[0].client_latitude.toDouble(), it.data[0].client_longitude.toDouble())

            Log.e("data", it.data[0].client_latitude + " nkjrg  " + it.data[0].client_longitude.toDouble())

           mMap.addMarker(MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.group_3581)))

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10f))

            Log.e(TAG, "onMapReady: test  444" )

            val latLng2 = LatLng(it.data[0].receiving_latitude.toDouble(), it.data[0].receiving_longitude.toDouble())

            Log.e(TAG, "onMapReady: "+it.data[0].receiving_latitude + " nkjrg  " + it.data[0].receiving_longitude.toDouble())

           mMap.addMarker(MarkerOptions().position(latLng2).icon(BitmapDescriptorFactory.fromResource(R.drawable.group_3583)))

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng2, 10f))



//            //Define list to get all latlng for the route
//            val path: List<LatLng> = ArrayList()
//            //Execute Directions API request
//            val context: GeoApiContext = GeoApiContext.Builder()
//                    .apiKey("AIzaSyCBYpxNhbkA1UBK6c87zAdzFn7S1-6yea8")
//                    .build()
//            val req: DirectionsApiRequest = DirectionsApi.getDirections(context, latLng.toString(), latLng2.toString())
//            try {
//                val res: DirectionsResult = req.await()
//
//                //Loop through legs and steps to get encoded polylines of each step
//                if (res.routes != null && res.routes.size > 0) {
//                    val route: DirectionsRoute = res.routes.get(0)
//                    if (route.legs != null) {
//                        for (i in 0 until route.legs.size) {
//                            val leg: DirectionsLeg = route.legs.get(i)
//                            if (leg.steps != null) {
//                                for (j in 0 until leg.steps.size) {
//                                    val step: DirectionsStep = leg.steps.get(j)
//                                    if (step.steps != null && step.steps.size > 0) {
//                                        for (k in 0 until step.steps.size) {
//                                            val step1: DirectionsStep = step.steps.get(k)
//                                            val points1: EncodedPolyline = step1.polyline
//                                            if (points1 != null) {
//                                                //Decode polyline and add points to list of route coordinates
//                                                val coords1: List<com.google.maps.model.LatLng> = points1.decodePath()
//                                                for (coord1 in coords1) {
//                                                    path.toMutableList().add(LatLng(coord1.lat, coord1.lng))
//                                                }
//                                            }
//                                        }
//                                    } else {
//                                        val points: EncodedPolyline = step.polyline
//                                        if (points != null) {
//                                            //Decode polyline and add points to list of route coordinates
//                                            val coords: List<com.google.maps.model.LatLng> = points.decodePath()
//                                            for (coord in coords) {
//                                                path.toMutableList().add(LatLng(coord.lat, coord.lng))
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            } catch (ex: java.lang.Exception) {
//                Log.e(TAG, ex.localizedMessage)
//            }

            //Draw the polyline

//            //Draw the polyline
//            if (path.size > 0) {
//                val opts = PolylineOptions().addAll(path).color(Color.BLUE).width(5f)
//                mMap.addPolyline(opts)
//            }

            val routePath = drawroute(getApplicationContext(), mMap)
            routePath.setRoutePath(latLng, latLng2)
//

//            Log.e(TAG, "onMapReady: test  555" )
//            mMap.addPolyline(PolylineOptions()
//                    .add(latLng,latLng2)
//                    .width(20f)
//                    .color(ContextCompat.getColor(activity!!, R.color.red))
//                    .geodesic(true))

            //  AIzaSyAP1inY8SIeJJaKA7SzqR5pK0_Uhmj8Q4o
//            val marker: Marker = mMap.addMarker(MarkerOptions()
//                    .position(latLng)
//                    .title("Start")
//                    .snippet(null))






            val formatter = DecimalFormat("00.0")

            try {
                VIEW.tvAgentDistance.text = formatter.format(it.data[0].agent_shope_distance.toDouble()) + " كم "
                VIEW.tvClientDistance.text = formatter.format(it.data[0].client_shope_distance.toDouble()) + " كم "
            } catch (e: Exception) {
                Log.e("EXCEPTION Deliver", e.message)
            }


        })


    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }


private fun checked(){
    VIEW.rg_views.visibility=View.VISIBLE

    val checkedChangeListener: RadioGroup.OnCheckedChangeListener = object : RadioGroup.OnCheckedChangeListener {
        override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {

            // Getting Reference to map_view of the layout activity_main

            // Currently checked is rb_map
            if (checkedId == R.id.rb_map) {
                mMap.mapType = GoogleMap.MAP_TYPE_NORMAL
                VIEW.rg_views.visibility=View.GONE
            }

            // Currently checked is rb_satellite
            if (checkedId == R.id.rb_satellite) {
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID)
                VIEW.rg_views.visibility=View.GONE

            }
        }


    }
    // Setting Checked ChangeListener
    VIEW.rg_views.setOnCheckedChangeListener(checkedChangeListener)


}


}