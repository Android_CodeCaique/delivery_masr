package com.codecaique.deliverymasr.ui.client.editProfile

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codecaique.deliverymasr.pojo.response.EditProfileResponse
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.response.GeneralResponse
import com.codecaique.deliverymasr.ui.client.allSitting.AllSittingFragment
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody

class EditProfileViewModel : ViewModel() {

    val enterUserDataMutableLiveData = MutableLiveData<EditProfileResponse>()
    val logoutMutableLiveData = MutableLiveData<GeneralResponse>()


    fun enterUserData(
            user_token:RequestBody,
            first_name: String,
            last_name: RequestBody,
            passwords: RequestBody,
            image: MultipartBody.Part?,
            phone: RequestBody?,
            email: RequestBody?,
            gender_id: Int
    ) {

      var  name = first_name.split(" ")
        var lName = ""
        val fName = name[0]
        if (name.size > 1) for (i in name.indices) {
            if (i != 0)
                lName += name[i]
        }

        val observable =
                ApiCreater.instance.editUserProfile(user_token,
                        RequestBody.create(MediaType.parse("multipart/form-data"), fName),
                                RequestBody.create(MediaType.parse("multipart/form-data"), lName)
                                , passwords, image, phone, email, gender_id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<EditProfileResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: EditProfileResponse) {

                enterUserDataMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
                EditProfileFragment().loadingDialog.dismiss()
            }
        }

        observable.subscribe(observer)

    }

    fun logout(
            user_token:String
    ) {

        val observable =
                ApiCreater.instance.logout(
                        RequestBody.create(MediaType.parse("multipart/form-data"), user_token))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<GeneralResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: GeneralResponse) {

                logoutMutableLiveData.value = t
                Log.e("ss", t.message)


            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
                AllSittingFragment().loadingDialog.dismiss()
            }
        }

        observable.subscribe(observer)

    }


}