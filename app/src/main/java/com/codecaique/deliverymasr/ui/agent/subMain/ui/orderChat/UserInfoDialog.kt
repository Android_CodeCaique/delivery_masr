package com.codecaique.deliverymasr.ui.agent.subMain.ui.orderChat

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.base.agent.SubMainActivity
import kotlinx.android.synthetic.main.dialog_user_info.*

class UserInfoDialog(context: Context) : Dialog(context) {

    companion object{

        val isComments="isComments"

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_user_info)

        comments.setOnClickListener {

            val intent= Intent(context, SubMainActivity::class.java)
            intent.putExtra(SubMainActivity.KEY, SubMainActivity.COMMENTS)
            context.startActivity(intent)
            dismiss()
        }
    }
}
