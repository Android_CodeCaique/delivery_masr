package com.codecaique.deliverymasr.ui.agent.subMain.ui.problemslist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.codecaique.deliverymasr.R;
import java.util.List;

public class ProblemAdapter extends RecyclerView.Adapter<ProblemAdapter.ViewHolder> {


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView id, name, mess, num;

        public ViewHolder(View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.id);
            name = itemView.findViewById(R.id.name);
            mess = itemView.findViewById(R.id.message);
            num = itemView.findViewById(R.id.num);
        }
    }


    private Context context;
    private List<ProblemModel> list;

    public ProblemAdapter(Context c, List<ProblemModel> p) {
        this.context = c;
        this.list = p;

    }

    @Override
    public ProblemAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v;
        v = LayoutInflater.from(context).inflate(R.layout.item_problem, viewGroup, false);
        return new ProblemAdapter.ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ProblemAdapter.ViewHolder viewHolder, int i) {

        ProblemModel model = list.get(i);
        viewHolder.id.setText(model.getT1());
        viewHolder.name.setText(model.getT2());
        viewHolder.mess.setText(model.getT3());
        viewHolder.num.setText(model.getT4());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
