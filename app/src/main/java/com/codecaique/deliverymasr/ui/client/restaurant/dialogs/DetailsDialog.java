package com.codecaique.deliverymasr.ui.client.restaurant.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;

import com.codecaique.deliverymasr.R;

public class DetailsDialog extends Dialog {

    Context context;

    public DetailsDialog(@NonNull Context context) {
        super(context);
        this.context=context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_food_type);

        Button done=findViewById(R.id.btn_orderKentaky);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*OrderFoodFrag orderFoodFrag = new OrderFoodFrag();
                FragmentManager manager =((AppCompatActivity) context).getFragmentManager();
                manager.beginTransaction().replace(R.id.main3_fragment, new OrderFoodFrag()).commit();*/
                dismiss();
            }
        });

    }
}
