package com.codecaique.deliverymasr.ui.client.deliveryOffers

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.pojo.util.UserInfo
import com.codecaique.deliverymasr.pojo.response.Offer
import com.codecaique.deliverymasr.ui.client.stores.StoresFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.fragment_delivery_offers.view.*
import kotlinx.android.synthetic.main.fragment_delivery_offers.view.progressBar
import kotlinx.android.synthetic.main.fragment_delivery_offers.view.refresh_layout
import kotlinx.android.synthetic.main.fragment_delivery_offers.view.rv_DeliveryOffers
import kotlinx.android.synthetic.main.fragment_delivery_offers.view.tv_not_found

class DeliveryOffersFragment : Fragment() {

    lateinit var offersViewModel: OffersViewModel
    lateinit var offersAdapter: OffersAdapter

    private val TAG = "DeliveryOffersFragment"
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_delivery_offers, container, false)

        offersViewModel=ViewModelProviders.of(this).get(OffersViewModel::class.java)
        offersAdapter= OffersAdapter(activity!!,offersViewModel)

        view.rv_DeliveryOffers.adapter=offersAdapter
        view.tv_not_found.visibility = View.GONE

        offersViewModel.getOffers(UserInfo(activity!!.applicationContext).getUserToken())
        offersViewModel.offersMutableLiveData.observe(this, Observer { t ->

            if (t.data.size > 0)
                view.tv_not_found.visibility = View.GONE
            else
                view.tv_not_found.visibility = View.VISIBLE

            view.progressBar.visibility = View.GONE

            Log.e(TAG, "onCreateView: offersAdapter setdata 1")
            offersAdapter.setData(t.data as ArrayList<Offer>)

        })


        view.back.setOnClickListener {
            activity!!.supportFragmentManager.beginTransaction().replace(R.id.main_fragment, StoresFragment()).commit()
//            it.findNavController().navigate(R.id.notificationsFragment)
            activity!!.findViewById<BottomNavigationView>(R.id.main_nav_view).selectedItemId = R.id.navigation_Stores
        }

        view.refresh_layout.setOnRefreshListener {
            offersViewModel.getOffers(UserInfo(activity!!.applicationContext).getUserToken())
            offersViewModel.offersMutableLiveData.observe(this, Observer { t ->

                view.progressBar.visibility = View.GONE
                Log.e(TAG, "onCreateView: offersAdapter setdata 22222")
                offersAdapter.setData(t.data as ArrayList<Offer>)

            })

            view.refresh_layout.isRefreshing = false

        }

        return view
    }

    override fun onResume() {
        super.onResume()
        offersViewModel.getOffers(UserInfo(activity!!.applicationContext).getUserToken())
        offersViewModel.offersMutableLiveData.observe(this, Observer { t ->

            view!!.progressBar.visibility = View.GONE
            Log.e(TAG, "onResume: offersAdapter setdata 3333333")
            Log.e(TAG, "ArrayList<Offer>: "+(t.data as ArrayList<Offer>))
            offersAdapter.setData(t.data as ArrayList<Offer>)

        })

            Log.e(this.javaClass.name, "CurrentScreen")



    }


}