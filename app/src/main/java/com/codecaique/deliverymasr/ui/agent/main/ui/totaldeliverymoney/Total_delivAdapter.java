package com.codecaique.deliverymasr.ui.agent.main.ui.totaldeliverymoney;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import com.codecaique.deliverymasr.R;
import com.codecaique.deliverymasr.data.ApiCreater;
import com.codecaique.deliverymasr.pojo.response.Total_Delivery_Response;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class Total_delivAdapter  extends RecyclerView.Adapter<Total_delivAdapter.ViewHolder>
{


    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView Name_offer,Des_offer,d1,d2,Date,sum;
        CircleImageView thumb;

        public ViewHolder(View itemView) {
            super(itemView);
            Name_offer=itemView.findViewById(R.id.offername);
            Des_offer=itemView.findViewById(R.id.offer_details);
            d1=itemView.findViewById(R.id.dis1);
            d2=itemView.findViewById(R.id.dis2);
            Date=itemView.findViewById(R.id.date_offer);
            sum=itemView.findViewById(R.id.sum_money_offer);
            thumb = itemView.findViewById(R.id.civ_user);
        }
    }


    private Context context;
//    private List<Total_sumModel> list;

    List<Total_Delivery_Response.DataBean> list = new ArrayList<>();

    public Total_delivAdapter(Context c) {
        this.context = c;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v;
        v = LayoutInflater.from(context).inflate(R.layout.item_sum_offer_money, viewGroup, false);
        return new ViewHolder(v);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

//        String url = "http://hyakapi.codecaique.com/uploads/users/";

        if (!list.get(i).getImage().isEmpty() && list.get(i).getImage() != null)
        {
            Picasso.get().load(ApiCreater.Companion.getSTORES_URL() + list.get(i).getImage()).into(viewHolder.thumb);
        }

        viewHolder.Name_offer.setText(list.get(i).getName());
        viewHolder.Des_offer.setText(list.get(i).getDescription());

//        Log.e("DIST",list.get(i).getClient_Recivingdistance());

        try {
            DecimalFormat df = new DecimalFormat("####0.00");
//            String agent_dest = df.format(Double.parseDouble(list.get(i).getAgent_Recivingdistance()));
//            Log.e("FORMAT",df.format(Double.parseDouble(list.get(i).getAgent_Recivingdistance())));
//            String client = df.format(Double.parseDouble(list.get(i).getClient_Recivingdistance()));



            viewHolder.d1.setText(df.format(Double.parseDouble(list.get(i).getAgent_Recivingdistance())) + " كم ");
            viewHolder.d2.setText(df.format(Double.parseDouble(list.get(i).getClient_Recivingdistance()))+ " كم ");
        }

        catch (Exception e)
        {
            Log.e("Error",e.getMessage());
        }

        viewHolder.Date.setText(list.get(i).getCreated_at());
        viewHolder.sum.setText(list.get(i).getPrice() +"ر.س");

    }

    public void setList(List<Total_Delivery_Response.DataBean> list)
    {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}


