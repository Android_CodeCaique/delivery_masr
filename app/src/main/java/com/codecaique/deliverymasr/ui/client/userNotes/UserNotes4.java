package com.codecaique.deliverymasr.ui.client.userNotes;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.codecaique.deliverymasr.R;
import com.codecaique.deliverymasr.ui.client.bankAccount.BankAccount;


public class UserNotes4 extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_notes4, container, false);

        ImageView iv_back = view.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent=new Intent(getContext(), MainActivity2.class);
//                fragName=R.layout.fragment_bank_account;
//                startActivity(intent);
                BankAccount bankAccount = new BankAccount();
                FragmentManager manager = getFragmentManager();
                manager.beginTransaction().replace(R.id.main3_fragment, bankAccount, bankAccount.getTag()).commit();
            }
        });

        Button btn_yes = view.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserNotes5 userNotes5 = new UserNotes5();
                FragmentManager manager = getFragmentManager();
                manager.beginTransaction().replace(R.id.main3_fragment, userNotes5, userNotes5.getTag()).commit();
            }
        });

        return view;
    }
}
