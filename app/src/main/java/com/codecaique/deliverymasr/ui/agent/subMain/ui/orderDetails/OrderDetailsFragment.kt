package com.codecaique.deliverymasr.ui.agent.subMain.ui.orderDetails

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import com.codecaique.deliverymasr.R
import com.codecaique.deliverymasr.ui.agent.subMain.ui.orderDetails.framment.store_info_fragment.StroreInfoFragment
import com.codecaique.deliverymasr.ui.agent.subMain.ui.orderDetails.framment.wait.OrdersWaitsFragment
import kotlinx.android.synthetic.main.fragment_order_details.view.*

/**
 * A simple [Fragment] subclass.
 */
class OrderDetailsFragment : Fragment() {

    @SuppressLint("ResourceAsColor")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view=inflater.inflate(R.layout.fragment_order_details, container, false)

        val  shop_name=activity!!.intent.extras!!.getString("shop_name")

        view.shop_name.text=shop_name

        loadFragment(StroreInfoFragment())

        view.tv_1.setTextColor(R.color.colorPrimaryDark)
        view.view1.visibility=View.VISIBLE
        view.view2.visibility=View.INVISIBLE
        view.tv_2.setTextColor(R.color.black_)


        view.tv_1.setOnClickListener {

            view.tv_1.setTextColor(R.color.colorPrimaryDark)
            view.view1.visibility=View.VISIBLE
            view.view2.visibility=View.INVISIBLE
            view.tv_2.setTextColor(R.color.black)

            loadFragment(StroreInfoFragment())
        }

        view.tv_2.setOnClickListener {

            view.tv_2.setTextColor(R.color.colorPrimaryDark)
            view.tv_1.setTextColor(R.color.black)
            view.view2.visibility=View.VISIBLE
            view.view1.visibility=View.INVISIBLE

            loadFragment(OrdersWaitsFragment())

        }


        return view
    }

    @SuppressLint("ResourceAsColor")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            activity!!.finish()
        }

        }

    fun loadFragment(fragment: Fragment) {
        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_stores_details, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
    override fun onResume() {
        super.onResume()
        Log.e(this.javaClass.name, "CurrentScreen")

    }
}