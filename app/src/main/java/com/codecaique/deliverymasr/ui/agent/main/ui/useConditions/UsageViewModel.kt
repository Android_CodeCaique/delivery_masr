package com.codecaique.deliverymasr.ui.agent.main.ui.useConditions

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codecaique.deliverymasr.data.ApiCreater
import com.codecaique.deliverymasr.pojo.response.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class UsageViewModel : ViewModel() {

    val aboutUsMutableLiveData = MutableLiveData<AboutUsResponse>()
    val showPolicyMutableLiveData = MutableLiveData<ShowPolicyResponse>()
    val showUseConditionsMutableLiveData = MutableLiveData<ConditionsResponse>()

    fun aboutUs(token: String) {

        val observable =
                ApiCreater.instance.aboutUs(token)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<AboutUsResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: AboutUsResponse) {

                aboutUsMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }

    fun showPolicy(token: String) {

        val observable =
                ApiCreater.instance.showPolicy(token)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<ShowPolicyResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: ShowPolicyResponse) {

                showPolicyMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }

    fun showUseConditions(token: String) {

        val observable =
                ApiCreater.instance.showUseConditions(token)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

        val observer = object : Observer<ConditionsResponse> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: ConditionsResponse) {

                showUseConditionsMutableLiveData.value = t

            }

            override fun onError(e: Throwable) {
                Log.e("ss", e.message)
            }
        }

        observable.subscribe(observer)

    }

}